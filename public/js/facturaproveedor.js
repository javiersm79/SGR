/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 105);
/******/ })
/************************************************************************/
/******/ ({

/***/ 105:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(106);


/***/ }),

/***/ 106:
/***/ (function(module, exports) {


$(document).ready(function () {

  $('#fecha').val(hoy());
  //Inicializa los datapicker
  $('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    endDate: hoy(),
    language: "es"
  });
}); // Fin del Document ready


$("#btnIngresar").click(function () {
  var formData = new FormData();

  var nproceso = $("#nproceso").val();
  var valor = $("#valor").val();
  var fecha = $("#fecha").val();
  var nfactura = $("#nfactura").val();
  var rut = $("#rut").val();
  var idrut = $("#idrut").val();
  var razonsocial = $("#rsocial").val();
  var concepto = $("#concepto").val();

  formData.append('nproceso', nproceso);
  formData.append('valor', valor);
  formData.append('fecha', fecha);
  formData.append('nfactura', nfactura);
  formData.append('rut', rut);
  formData.append('idrut', idrut);
  formData.append('razonsocial', razonsocial);
  formData.append('concepto', concepto);

  axios.post("/FacturaTrasladoResource/", formData).then(function (response) {
    mensaje = "";
    Object.keys(response.data).forEach(function (key) {

      //console.log(key, response.data[key]);
      mensaje = mensaje + response.data[key] + "<br>";
    });
    if (response.data) {
      swal({
        title: "Factura",
        html: true,
        text: mensaje,
        type: "warning"
      }, function () {
        //window.location = "listaractas";
      });
    } else {
      swal({
        title: "Factura",
        text: "Factura Ingresada",
        type: "success"
      }, function () {
        //window.location = "listaractas";
      });
    }
  });
}); //Fin click Ingresar factura de traslado

window.buscarIngreso = function () {
  var nproceso = $("#nproceso").val();
  if (nproceso != "") {
    axios.get("/showFullDataIngresoNproceso/" + nproceso).then(function (response) {
      console.log('RESULTADO:' + response.data);
      if (response.data != 0) {
        //console.log(response.data)
        /*$("#nombres").val(response.data.nombre);
        $("#apellidos").val(response.data.apellido);
        $("#telefono").val(response.data.fono1);
        $("#email").val(response.data.email);*/
        $("#valor").val(response.data.ingreso.vtraslado);
        //$("#vref").val(response.data.ingreso.vfacturacia);
        $("#nfactura").val(response.data.ingreso.nfacturaproveedor);
        $("#idrut").val(response.data.ingreso.transportista);
        $("#concepto").val("Traslado del Vehiculo " + response.data.vehiculo.patente + " desde ");
        $("#patente").html(response.data.vehiculo.patente);
        $("#anio").html(response.data.vehiculo.anio);
        $("#marca").html(response.data.marcavehiculo.nombre);
        $("#modelo").html(response.data.modelovehiculo.nombre);
        $("#ingresoFactura").removeClass("hidden");
        //console.log(response.data)
        if (response.data.transportista) {
          $("#rsocial").val(response.data.transportista.nombreempresa);
          $("#rut").val(response.data.transportista.rutempresa);
        } else {
          swal({
            title: "Aviso",
            html: true,
            text: "Existencia sin <strong style='color:red'>Transportista</strong> asignado",
            type: "warning"
          }, function () {
            $("#rut").focus();
          });
        }
      } else {
        swal({
          title: "Aviso",
          html: true,
          text: "Nº de Proceso <strong style='color:red'>No registrado</strong>",
          type: "warning"
        }, function () {
          $("#ingresoFactura").addClass("hidden");
        });
      }
    }); //FIN AXIOS GET

  } else {
    swal({
      title: "Validar datos",
      html: true,
      text: "Ingrese el <strong style='color:red'> Numero de Proceso</strong> a buscar",
      type: "warning"
    }, function () {
      $("#rut").focus();
    });
  }
};
window.buscarproveedor = function () {

  var rut = $("#rut").val();

  if (rut != "") {
    axios.get("/buscarDatosTransportistas/" + rut).then(function (response) {
      if (response.data != 0) {
        /*$("#nombres").val(response.data.nombre);
        $("#apellidos").val(response.data.apellido);
        $("#telefono").val(response.data.fono1);
        $("#email").val(response.data.email);*/
        $("#rsocial").val(response.data.nombreempresa);
        $("#idrut").val(response.data.id);
      } else {
        //$("#buscarResult").html('RUT No Registrado');
        swal({
          title: "Notificación",
          html: true,
          text: "El <strong style='color:red'>RUT</strong> ingresado no existe",
          type: "warning"
        }, function () {
          $("#rut").focus();
        });
      }
    }); //FIN AXIOS GET

  } else {
    swal({
      title: "Validar datos",
      html: true,
      text: "Ingrese el <strong style='color:red'> RUT</strong> a buscar",
      type: "warning"
    }, function () {
      $("#rut").focus();
    });
  }
}; //Fin click buscar proveedor


window.buscarDatosLote = function (lote, acta) {
  $("form").trigger("reset");
  formIngresarData.lote = lote;
  formIngresarData.acta = acta;
  /*
  let formData = new FormData();
  
    formData.append('lote', lote)
    formData.append('acta', acta)*/
  axios.get("/buscarDatosLote/" + acta + "/" + lote).then(function (response) {
    var datosIngreso = response.data[0];
    if (datosIngreso.cliente) {
      $("#nombres").val(datosIngreso.cliente.nombre);
      $("#apellidos").val(datosIngreso.cliente.apellido);
      $("#rut").val(datosIngreso.cliente.rut);
      $("#telefono").val(datosIngreso.cliente.fono1);
      $("#email").val(datosIngreso.cliente.email);
    }

    $("#monto").val(datosIngreso.monto);
    $("#garantia").val(datosIngreso.garantia);
    $("#tasacionfiscal").val(datosIngreso.tasacionfiscal);
    formIngresarData.idcliente = datosIngreso.idcliente;
    if (datosIngreso.estadoingreso == "Ingresado") {
      $("#estado" + lote).removeClass();
      $("#estado" + lote).addClass("text-success");
    } else {
      $("#estado" + lote).addClass("text-danger");
    }
    //console.log(response.data)
  });
};

/***/ })

/******/ });