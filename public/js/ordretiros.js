/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 77);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(78);


/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_OrdenesRetiro_vue__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_OrdenesRetiro_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_OrdenesRetiro_vue__);
//Vue.component('listausuarios', require('./components/Usuarios.vue'));

var appOrdRetComp = new Vue({
  components: {
    'listaordenesretiro': __WEBPACK_IMPORTED_MODULE_0__components_OrdenesRetiro_vue___default.a

  },
  el: '#listado_ordenesRetiro',
  data: {
    ordRetiroData: '',
    galeriaFotos: ''
  },
  methods: {

    getOrdRetData: function getOrdRetData(idorden) {

      var vm = this;
      $('#collapseTaller').collapse('show');
      $('#collapseVehiculos').collapse('show');
      $('#collapseImg').collapse('hide');
      $('#tbodyArchivos').html('');

      $('#accion').val('ACTUALIZAR ORDEN');
      $('#idordenretiro').val(idorden);
      $('#btnEnviar').html('ACTUALIZAR ORDEN');
      $('#btnEnviarTraslado').html('ACTUALIZAR ORDEN');
      $(".collapseTab").removeClass('disabledTab');

      $(".tipoOR").html('');
      //$('#patente').val(vm.ordRetiroData.vehiculo.patente);


      //Obteniendo datos propios de la orden
      axios.get('showFullDataOR/' + idorden).then(function (response) {
        vm.ordRetiroData = response.data;

        //Seccion de la orden
        $('#titulomodal').html('ACTUALIZAR ORDEN - #' + vm.ordRetiroData.ordenretiro.nproceso);
        $("#nsiniestro").val(vm.ordRetiroData.ordenretiro.nsiniestro);
        //$("#estado").val(vm.ordRetiroData.ordenretiro.estado)
        $("#estado").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.ordenretiro.estado }
        });
        $("#compania").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.ordenretiro.compania }
        });

        //$('#emailcontac').html('')


        //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
        var urlContactosEmpresa = "usuariosPorCompania/" + vm.ordRetiroData.ordenretiro.compania;
        axios.get(urlContactosEmpresa).then(function (response) {
          var contactos = response.data;
          //console.log(contactos)
          $('#emailcontac').html('');
          $("#liquidador").html('');
          $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

          contactos.forEach(function (contacto, key, contactos) {
            // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
            //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
            $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
          });

          $("#emailcontac").select2("trigger", "select", {
            data: { id: vm.ordRetiroData.ordenretiro.contacto }
          });
        });

        var urlLiquidadoresEmpresa = "liquidadoresPorCompania/" + vm.ordRetiroData.ordenretiro.compania;
        axios.get(urlLiquidadoresEmpresa).then(function (response) {
          var liquidadores = response.data;
          $("#liquidador").html('');

          $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

          liquidadores.forEach(function (liquidador, key, contactos) {
            if (vm.ordRetiroData.ordenretiro.liquidador == liquidador.id) {
              $("#liquidador").append($("<option></option>").attr("value", liquidador.id).attr("selected", true).text(liquidador.liquidador));
            } else {
              $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
            }
          });
        });

        $("#vtraslado").val(formatearNumero(vm.ordRetiroData.ordenretiro.vtraslado));
        $("#nfacturaproveedor").val(vm.ordRetiroData.ordenretiro.nfacturaproveedor);
        $("#vfacturacia").val(formatearNumero(vm.ordRetiroData.ordenretiro.vfacturacia));
        $("#nfactura").val(vm.ordRetiroData.ordenretiro.nfactura);
        $("#tasacion").val(formatearNumero(vm.ordRetiroData.ordenretiro.tasacion));
        $("#vminremate").val(formatearNumero(vm.ordRetiroData.ordenretiro.vminremate));
        $("#vindem").val(formatearNumero(vm.ordRetiroData.ordenretiro.vindem));

        //Seccion del vehiculo
        $("#patente").val(vm.ordRetiroData.vehiculo.patente);

        $("#marca").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.vehiculo.marcaid }
        });
        /*setTimeout(function(){
          $("#modelo").select2("trigger", "select", {
            data: { id: vm.ordRetiroData.vehiculo.modeloid }
          });
        }, 1000);*/

        $('#modelo').html('');
        var urlModelo = 'showFullDataByMarca/' + vm.ordRetiroData.vehiculo.marcaid;
        axios.get(urlModelo).then(function (response) {
          var modelos = response.data;
          $("#modelo").select2("trigger", "select", {
            data: { id: vm.ordRetiroData.vehiculo.modeloid }
          });
        });

        $("#anio").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.vehiculo.anio }
        });
        $("#tipo").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.vehiculo.tipoid }
        });
        $("#color").val(vm.ordRetiroData.vehiculo.color);

        $("#combustible").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.vehiculo.combustible }
        });
        $("#nmotor").val(vm.ordRetiroData.vehiculo.nummotor);
        $("#nchasis").val(vm.ordRetiroData.vehiculo.numchasis);
        $("#vim").val(vm.ordRetiroData.vehiculo.VIM);
        $("#nserie").val(vm.ordRetiroData.vehiculo.serial);
        $("#nombreprop").val(vm.ordRetiroData.vehiculo.nombrepropietario);
        $("#rutprop").val(vm.ordRetiroData.vehiculo.rutpropietario);

        //Seccion del taller
        $("#idtaller").val(vm.ordRetiroData.taller.id);
        $("#nombretaller").val(vm.ordRetiroData.taller.nombre);
        $("#ruttaller").val(vm.ordRetiroData.taller.rut);
        $("#direcciontaller").val(vm.ordRetiroData.taller.direccion);
        $("#direcciontaller").val(vm.ordRetiroData.taller.direccion);
        $("#emailtaller").val(vm.ordRetiroData.taller.email);
        $("#nombcontactotaller").val(vm.ordRetiroData.taller.contacto);
        $("#telefonotaller").val(vm.ordRetiroData.taller.telefono);

        //Seccion de traslado
        if (vm.ordRetiroData.transportista) {
          $("#idtransportista").val(vm.ordRetiroData.transportista.id);
          $("#emailtransp").val(vm.ordRetiroData.transportista.email);
          $("#telefonotransp").val(vm.ordRetiroData.transportista.telefono);
          $("#ruttransp").val(vm.ordRetiroData.transportista.rutempresa);
          $("#nombtransp").val(vm.ordRetiroData.transportista.nombreempresa);
          $("#nombchofertransp").val(vm.ordRetiroData.transportista.nombrechofer);
          $("#gruaexternatransp").val(vm.ordRetiroData.transportista.gruaexterna);
          $("#patentecamiontransp").val(vm.ordRetiroData.transportista.patentecamion);
          $("#rutchofertransp").val(vm.ordRetiroData.transportista.rutchofer);
        }

        axios.get('scanfolderGaleriaVehiculo/' + vm.ordRetiroData.vehiculo.patente).then(function (response) {
          //vm.galeriaFotos = response.data  
          $("#galeriaVehiculo").html(response.data);
          $('#galeriaVehiculo').trigger('destroy.owl.carousel');
          $('#galeriaVehiculo').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        });

        //Seccion de archivos

        axios.get('scanfolderArchivosOR/' + vm.ordRetiroData.ordenretiro.nproceso).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyArchivos').html('');
          $("#tbodyArchivos").append(response.data);
        });

        //Seccion de Observaciones

        axios.get('showObservaciones/' + vm.ordRetiroData.ordenretiro.id).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyObervaciones').html('');
          $("#tbodyObervaciones").append(response.data);
        });

        //Seccion Historico
        axios.get('showHistorico/' + vm.ordRetiroData.ordenretiro.nproceso).then(function (response) {

          $('#operacioneshist').html('');
          $("#operacioneshist").append(response.data);
        });
      }); // Fin de recuperar datos de la orden

    } //Fin de getOrdRetData


    //Fin de METHODS del componente
  } });

var appOrModal = new Vue({

  el: '#capaModal',
  data: {
    ordenesList: '',
    marcasList: '',
    modelosList: '',
    tiposList: '',
    talleresList: '',
    companiaList: '',
    vehiculo: ''

  },
  created: function created() {
    this.getMarcas();
    this.getTipos();
    this.getCompania();
    this.getTalleres();
  },
  methods: {
    crearOR: function crearOR() {
      $('#titulomodal').html('CREAR ORDEN');
      $('#accion').val('CREAR ORDEN');
      $('#btnEnviar').html('CREAR ORDEN');
      $("#_method").val("POST");
      $('.tipoOR').html("OR Nueva");
      $(".collapseTab").addClass('disabledTab');
      $("#galeriaVehiculo").html("");

      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('.nav-tabs a[href="#vehiculo"]').tab('show');
      $('.collapse').collapse('hide');
      setTimeout(function () {
        $('#collapseVehiculos').collapse('show');
      }, 500);
      $("#compania").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#marca").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#modelo").html("");
      $("#tipo").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#anio").select2("trigger", "select", {
        data: { id: new Date().getFullYear() + 1 }
      });
      $('#idordenretiro').val('');
    },
    crearPDF: function crearPDF() {
      var idorden = $('#idordenretiro').val();
      window.location.replace("pdfor/" + idorden + "/descargar");
    },

    enviarDatosBasicos: function enviarDatosBasicos() {
      var _this = this;

      var validardatos = 0;
      var validarpatentesiniestro = 0;
      $(".requerido").each(function (index) {
        if ($(this).val() == "" || $(this).val() == "0") {
          swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
          //Asigna 1 si hay un campo requerido vacio
          validardatos = 1;
        }
      });

      if (validardatos == 0) {
        var formData = new FormData();
        formData.append('patente', $("#patente").val());
        formData.append('nsiniestro', $("#nsiniestro").val());
        axios.post("/validarPatenteSiniestroOR/", formData).then(function (response) {
          validarpatentesiniestro = response.data;

          if (validarpatentesiniestro == 1 && $('#accion').val() == 'CREAR ORDEN') {
            swal('VALIDAR DATOS', 'PATENTE Y Nº SINIESTRO REGISTRADO', 'warning');
          } else {
            if ($('#accion').val() == 'CREAR ORDEN') {
              _this.$refs.formOrdRet._method.value = "POST";
              //alert("enviando el contacto nuevo")
              //console.log("enviando datos basicos")
              var validardatos = 0;

              $("#formOrdRet").submit();
            } else {
              _this.$refs.formOrdRet._method.value = "PATCH";
              _this.$refs.formOrdRet.action = "/ordenretiroResource/" + $('#idordenretiro').val();
              $("#formOrdRet").submit();
            }
          }
        });
      } //fin if validardatos

    },
    enviarObservacion: function enviarObservacion() {
      var formData = new FormData();
      var nombrearchivo;
      var idordenretiro = $("#idordenretiro").val();
      var observ = $("#obsertxt").val();
      var privada = 0;
      if ($("#observprivada").is(":checked")) {
        //alert('Seleccionado');
        privada = 1;
      }

      formData.append('idordenretiro', idordenretiro);
      formData.append('obsertxt', observ);
      formData.append('privada', privada);

      axios.post("/storeObservacion/" + idordenretiro, formData).then(function (response) {
        swal('Observación', 'Observación ingresada', 'success');

        axios.get('showObservaciones/' + idordenretiro).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyObervaciones').html('');
          $("#tbodyObervaciones").append(response.data);
        });
      });
    },

    enviarTraslado: function enviarTraslado() {
      var formData = new FormData();
      var idordenretiro = $("#idordenretiro").val();
      var idtransportista = $("#idtransportista").val();

      formData.append('idordenretiro', idordenretiro);
      formData.append('idtransportista', idtransportista);
      axios.post("/updateTransportista/", formData).then(function (response) {
        swal('Transportista', 'Transportista Actualizado', 'success');
      });
    },
    enviarEmailTraslado: function enviarEmailTraslado() {
      var formData = new FormData();
      var idordenretiro = $("#idordenretiro").val();
      var idtransportista = $("#idtransportista").val();

      if (idtransportista != '') {
        //this.enviarTraslado();
        formData.append('idordenretiro', idordenretiro);
        formData.append('idtransportista', idtransportista);
        swal('Enviando', 'Enviando Email...', 'warning');

        axios.post("/updateTransportista/", formData).then(function (response) {
          axios.post("/notificarTransportista/", formData).then(function (response) {
            swal('Transportista', 'Transportista notificado', 'success');
            axios.post("/orasignada/", formData);
          });
        });
      } else {
        swal('Validar', 'Debe seleccionar un transportista', 'warning');
      }
    },
    getMarcas: function getMarcas() {
      var _this2 = this;

      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(function (response) {
        _this2.marcasList = response.data;
      });
    },
    getTipos: function getTipos() {
      var _this3 = this;

      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(function (response) {
        _this3.tiposList = response.data;
        //console.log(response.data)
      });
    },
    getCompania: function getCompania() {
      var _this4 = this;

      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this4.companiaList = response.data;
      });
    },
    getTalleres: function getTalleres() {
      var _this5 = this;

      var urlTaller = 'talleresResource';
      axios.get(urlTaller).then(function (response) {
        _this5.talleresList = response.data;
      });
    },
    getContacto: function getContacto(idcontacto) {
      var urlContacto = 'contactosResource/' + idcontacto;
      axios.get(urlContacto).then(function (response) {
        $('#emailcontac').val(response.data.email);
        $('#tlfcontacto').val(response.data.fono1);
        $('#nombcontacto').val(response.data.nombre + ' ' + response.data.apellido);
      });
    },

    buscarVehiculo: function buscarVehiculo() {
      var _this6 = this;

      //const vm = this
      patente = $('#patente').val();
      if (!patente) {
        swal('INGRESE UNA PATENTE', 'DATO REQUERIDO', 'warning');
      } else {
        //var urlVehiculo = 'buscarpatente/'+patente;
        var urlVehiculo = 'buscarPatenteTasacion/' + patente;
        axios.get(urlVehiculo).then(function (response) {
          _this6.vehiculo = response.data;
          //alert (response.data.lenght)
          if (_this6.vehiculo.length == 0) {
            swal('PATENTE NO REGISTRADA', '', 'error');
          } else {
            swal('PATENTE REGISTRADA', 'REGISTRADA EN TASACIONES', 'success');
            /*$("#idvehiculo").val(this.vehiculo[0].id)*/

            //$("#idvehiculo").val(this.vehiculo.id)

            $("#marca").select2("trigger", "select", {
              data: { id: _this6.vehiculo.marcaid }
            });
            $('#modelo').html('');

            //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
            var urlModelo = 'showFullDataByMarca/' + _this6.vehiculo.marcaid;
            axios.get(urlModelo).then(function (response) {
              var modelos = response.data;
              //console.log(modelos)

              modelos.forEach(function (modelo, key, modelos) {
                // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
                //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
                $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
              });

              $("#modelo").select2("trigger", "select", {
                data: { id: _this6.vehiculo.modeloid }
              });
            });

            $("#anio").select2("trigger", "select", {
              data: { id: _this6.vehiculo.anio }
            });
            $("#tipo").select2("trigger", "select", {
              data: { id: _this6.vehiculo.tipoid }
            });
            $("#tasacion").val(_this6.vehiculo.valor);
          }

          //console.log(this.vehiculo[0].patente)

        });
      }
      this.vehiculo = '';
    }

  }
});

/*$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});*/

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateOrdenRetiro").on("hidden.bs.modal", function () {
  //document.getElementById("formOrdRet").reset();

  $("form").trigger("reset");
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$('#importarCertificadoModal').on('shown.bs.modal', function (e) {
  $('#datosCertificados').focus();
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');
  if (haserror == 1) {
    $('#newUpdateOrdenRetiro').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }

  $('[data-fancybox="images"]').fancybox({});

  //Detecta si se ha creado una nueva OR y se seguira 
  if ($("#idordenretiro").val() != '') {

    $('#newUpdateOrdenRetiro').modal('show');
    setTimeout(function () {
      appOrdRetComp.getOrdRetData($("#idordenretiro").val());
    }, 1500);
  }

  $('#combustible').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
  $("#marca").select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
  $("#modelo").select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
  $('#tipo').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
  $('#compania').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });

  $('select:not(.normal)').each(function () {
    $(this).select2({
      dropdownParent: $(this).parent()
    });
  });
  var anio = new Date().getFullYear() + 1;
  var i = 0;
  for (i = anio; i > 1969; i--) {
    // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
    $("#anio").append($("<option></option>").attr("value", i).text(i));
  };

  $('#anio').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
  $('#emailcontac').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });

  /* $('#direcciontaller').select2({
      dropdownParent: $("#newUpdateOrdenRetiro"),
      tags: true,
      createTag: function (tag) {
              return {id: 'nuevotaller', text: tag.term, tag: true};
          }
    });*/

  var options = {
    url: "talleresResource",

    getValue: "direccion",

    list: {
      onClickEvent: function onClickEvent() {
        var nombretaller = $("#direcciontaller").getSelectedItemData().nombre;
        var ruttaller = $("#direcciontaller").getSelectedItemData().rut;
        var telefonotaller = $("#direcciontaller").getSelectedItemData().telefono;
        var emailtaller = $("#direcciontaller").getSelectedItemData().email;
        var contactotaller = $("#direcciontaller").getSelectedItemData().contacto;
        var idtaller = $("#direcciontaller").getSelectedItemData().id;

        $("#nombretaller").val(nombretaller).trigger("change");
        $("#ruttaller").val(ruttaller).trigger("change");
        $("#telefonotaller").val(telefonotaller).trigger("change");
        $("#emailtaller").val(emailtaller).trigger("change");
        $("#nombcontactotaller").val(contactotaller).trigger("change");
        $("#idtaller").val(idtaller).trigger("change");
      },
      match: {
        enabled: true
      }
    }
  };

  $("#direcciontaller").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Autocompletar del Transportista
  var options = {
    url: "transportistaResource",

    getValue: "nombrechofer",

    list: {
      onClickEvent: function onClickEvent() {

        var idtransportista = $("#nombchofertransp").getSelectedItemData().id;
        var ruttransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var telefonotransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var emailtransp = $("#nombchofertransp").getSelectedItemData().email;
        var nombreempresa = $("#nombchofertransp").getSelectedItemData().nombreempresa;
        var gruaexterna = $("#nombchofertransp").getSelectedItemData().gruaexterna;
        var patentecamiontransp = $("#nombchofertransp").getSelectedItemData().patentecamion;
        var rutchofertransp = $("#nombchofertransp").getSelectedItemData().rutchofer;
        $("#idtransportista").val(idtransportista).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#telefonotransp").val(telefonotransp).trigger("change");
        $("#ruttransp").val(ruttransp).trigger("change");
        $("#nombtransp").val(nombreempresa).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#gruaexternatransp").val(gruaexterna).trigger("change");
        $("#patentecamiontransp").val(patentecamiontransp).trigger("change");
        $("#rutchofertransp").val(rutchofertransp).trigger("change");
        $("#btnEnviarTraslado").prop("disabled", false);
        $("#btnEnviarEmailTraslado").prop("disabled", false);
      },
      match: {
        enabled: true
      }
    }
  };

  $("#nombchofertransp").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Evento que ocurre cuando se selecciona una marca para llenar los modelos
  $('#marca').on('select2:select', function (e) {
    $('#modelo').html('');
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlModelo = 'showFullDataByMarca/' + data.id;
    axios.get(urlModelo).then(function (response) {
      var modelos = response.data;
      //console.log(modelos)
      $("#modelo").append($("<option></option>").attr("value", "0").text("Seleccione modelo"));

      modelos.forEach(function (modelo, key, modelos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
      });
    });
  });

  //Evento que ocurre cuando se selecciona un email de contacto para llenar los modelos
  $('#emailcontac').on('select2:select', function (e) {
    //$('#modelo').html('')
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    $('#idcontacto').val(data.id);
  });

  //Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
  $('#compania').on('select2:select', function (e) {

    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "usuariosPorCompania/" + data.id;
    axios.get(urlContactosEmpresa).then(function (response) {
      var contactos = response.data;
      $('#emailcontac').html('');
      $("#liquidador").html('');
      $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

      contactos.forEach(function (contacto, key, contactos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
      });
    });

    var urlLiquidadoresEmpresa = "liquidadoresPorCompania/" + data.id;
    axios.get(urlLiquidadoresEmpresa).then(function (response) {
      var liquidadores = response.data;

      $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

      liquidadores.forEach(function (liquidador, key, contactos) {
        $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
      });
    });
  }); // Fin del evento change de compania


  $("#direcciontaller").focusin(function () {
    $("#nombretaller").val('');
    $("#ruttaller").val('');
    $("#telefonotaller").val('');
    $("#emailtaller").val('');
    $("#nombcontactotaller").val('');
    $("#idtaller").val('');
  });

  $("#nombchofertransp").focusin(function () {
    $("#idtransportista").val('');
    $("#ruttransp").val('');
    $("#nombtransp").val('');
    $("#patentecamiontransp").val('');
    $("#telefonotransp").val('');
    $("#emailtransp").val('');
    $("#rutchofertransp").val('');
    $("#btnEnviarTraslado").prop("disabled", true);
    $("#btnEnviarEmailTraslado").prop("disabled", true);
  });
}); // Fin del Document ready


window.eliminarArchivo = function (nproceso, nombrearchivo, idarchivoor) {
  axios.post('eliminararchivo/' + nproceso + '/' + nombrearchivo + '/' + idarchivoor).then(function (response) {
    swal('Archivo Eliminado', 'Se eliminó el archivo: '.nombrearchivo, 'success');
  });
  //alert('#doc'+idarchivo)
  //document.getElementById("tablaarchivos").deleteRow(0);
  //$(this).closest('tr').remove();
};

$("#tablaarchivos").on('click', '.btnDelete', function () {
  $(this).closest('tr').remove();
});

//Funcion que comprueba el estado de la OR
window.eliminarFoto = function (patente, idfoto) {
  var galeriaFotos = void 0;

  axios.post('eliminarfoto/' + appOrdRetComp.ordRetiroData.vehiculo.patente + '/' + idfoto).then(function (response) {});

  galeriaFotos = axios.get('scanfolderGaleriaVehiculo/' + appOrdRetComp.ordRetiroData.vehiculo.patente).then(function (response) {
    return response.data;
  });

  //
  galeriaFotos.then(function (result) {
    swal('Imegen Eliminada', 'Se eliminó la imagen: '.idfoto, 'success');

    $("#galeriaVehiculo").html(result);
    $('#galeriaVehiculo').trigger('destroy.owl.carousel');
    $('#galeriaVehiculo').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    }); //Fin de funcion de inicializacion de OWN CORUSEL

  });

  $.fancybox.close(true);
};

$("#btnObservation").click(function () {}); //Fin click guardar observacion

$("#cargarArchivo").click(function () {

  var formData = new FormData();
  var nombrearchivo;
  var idordenretiro = $("#idordenretiro").val();
  var nproceso = appOrdRetComp.ordRetiroData.ordenretiro.nproceso;
  var observ = $("#obserfile").val();

  formData.append('archivo', document.getElementById('archivoOR').files[0]);
  formData.append('idordenretiro', idordenretiro);
  formData.append('nproceso', nproceso);
  formData.append('observacion', observ);

  var archivocargado = axios.post('subirarchivo', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {

    //return response.data;
    axios.get('scanfolderArchivosOR/' + nproceso).then(function (response) {
      swal('Cargar Archivo', 'Archivo cargado', 'success');
      $('#tbodyArchivos').html('');
      $("#tbodyArchivos").append(response.data);
    });
  });
}); //Fin click carga de arechivo

$("#btnImportarCert").click(function () {
  //alert("btnImportarCert");
  var inicio = 0;
  var fin = 0;

  var aLineas = document.getElementById("datosCertificados").value.split('\n');
  var datosCertificados = document.getElementById("datosCertificados").value;
  //alert("El textarea tiene " + aLineas.length + " líneas");

  //Importador del Dato Patente
  inicio = datosCertificados.indexOf("Inscripción : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var patente = datosCertificados.substring(inicio + 14, fin - 2);
  patente = patente.replace(".", "-");
  $("#patente").val(patente);

  //Importador del Dato Marca
  inicio = datosCertificados.indexOf("Marca : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var marcanombre = datosCertificados.substring(inicio + 8, fin);
  var resultMarca = appOrModal.marcasList.filter(function (marca) {
    return marca.nombre === marcanombre;
  });
  $("#marca").select2("trigger", "select", {
    data: { id: resultMarca[0].id }
  });

  //Importador del Dato Modelo
  inicio = datosCertificados.indexOf("Modelo : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var modelonombre = datosCertificados.substring(inicio + 9, fin);
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function () {
    var urlbuscarModeloId = 'buscarModeloPorNombre/' + modelonombre;
    axios.get(urlbuscarModeloId).then(function (response) {
      $("#modelo").select2("trigger", "select", {
        data: { id: response.data[0].id }
      });
    });
  }, 1500);

  //Importador del Dato Año
  inicio = datosCertificados.indexOf("Año : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var anio = datosCertificados.substring(inicio + 6, fin);
  $("#anio").val(anio);

  //Importador del Dato Color
  inicio = datosCertificados.indexOf("Color : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var color = datosCertificados.substring(inicio + 8, fin);
  $("#color").val(color);

  //Importador del Dato Combustible
  inicio = datosCertificados.indexOf("Combustible : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var combustible = datosCertificados.substring(inicio + 14, fin);
  $("#combustible").select2("trigger", "select", {
    data: { id: combustible }
  });

  //Importador del Dato TIPO
  inicio = datosCertificados.indexOf("Tipo Vehículo : ");
  fin = datosCertificados.indexOf(' Año : ');
  var nombretipo = datosCertificados.substring(inicio + 16, fin);

  var resultTipo = appOrModal.tiposList.filter(function (tipo) {
    return tipo.nombre === nombretipo;
  });
  //console.log(resultTipo)
  $("#tipo").select2("trigger", "select", {
    data: { id: resultTipo[0].id }
  });

  //Importador del Dato Nro. Motor 
  inicio = datosCertificados.indexOf("Nro. Motor : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nmotor = datosCertificados.substring(inicio + 13, fin);
  $("#nmotor").val(nmotor);

  //Importador del Dato Nro. Chasis
  inicio = datosCertificados.indexOf("Nro. Chasis : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nchasis = datosCertificados.substring(inicio + 14, fin);
  $("#nchasis").val(nchasis);

  //Importador del Dato Nro. Vin
  inicio = datosCertificados.indexOf("Nro. Vin : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);
  if (inicio < 0) {
    $("#vim").val('');
  } else {
    $("#vim").val(nvim);
    $("#nserie").val(nvim);
  }

  //Importador del Dato Nombre del propietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#nombreprop").val('');
  } else {
    inicio = datosCertificados.indexOf("Nombre : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var nombreprorp = datosCertificados.substring(inicio + 9, fin);
    $("#nombreprop").val(nombreprorp);
  }

  //Importador del Dato rut del prropietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#rutprop").val('');
  } else {
    inicio = datosCertificados.indexOf("R.U.N. : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var rutprop = datosCertificados.substring(inicio + 9, fin);
    $("#rutprop").val(rutprop);
  }

  inicio = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);

  /*
  $("#anio").val(aLineas[4].substr(-4))
  //Obtiene el ID del marca para seleccionarlo dinamicamente
  const resultMarca = appOrModal.marcasList.filter(marca => marca.nombre ===aLineas[6]);
  $("#marca").select2("trigger", "select", {
         data: { id: resultMarca[0].id }
         });
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function(){
  var urlbuscarModeloId = 'buscarModeloPorNombre/'+aLineas[8];
      axios.get(urlbuscarModeloId).then(response => {
          $("#modelo").select2("trigger", "select", {
           data: { id: response.data[0].id }
           });
           
      });
    
  }, 1500);
  //Obtiene el ID del tipo para seleccionarlo dinamicamente
  var n = aLineas[4].indexOf(" ");
  var nombretipo = aLineas[4].substring(0, n);
  const resultTipo = appOrModal.tiposList.filter(tipo => tipo.nombre ===nombretipo);
  $("#tipo").select2("trigger", "select", {
         data: { id: resultTipo[0].id }
         });
  $("#nmotor").val(aLineas[10])
  $("#nchasis").val(aLineas[12])
  
  $("#vin").val(aLineas[14])
  $("#color").val(aLineas[16])
  $("#combustible").select2("trigger", "select", {
         data: { id: aLineas[18] }
         });*/
  //$("#color").val(aLineas[18])


  //$("#marca").select2("val",'TOYOTA' );
  //$('#marca').val(3).trigger('change');


  /*$("#marca").select2("trigger", "select", {
         data: { id: 1 }
         });*/
  //$("#marca option:contains("+aLineas[6]+")").attr('selected', true);

  $('#importarCertificadoModal').modal('hide');
}); //Fin click importar datos del certificado


$("#btnsubirimg").click(function () {
  var formData = new FormData();

  for (var key in document.getElementById('imagenesVehiculo').files) {
    formData.append('fotosvehiculo[]', document.getElementById('imagenesVehiculo').files[key]);
  }
  formData.append('patente', appOrdRetComp.ordRetiroData.vehiculo.patente);
  formData.append('nproceso', appOrdRetComp.ordRetiroData.ordenretiro.nproceso);

  axios.post('subirfotosor', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {
    axios.get('scanfolderGaleriaVehiculo/' + appOrdRetComp.ordRetiroData.vehiculo.patente).then(function (response) {

      swal('Cargar Imagen', 'Imagen(es) Cargada(s)', 'success');
      $("#galeriaVehiculo").html(response.data);
      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('#galeriaVehiculo').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      }); //Fin de funcion de inicializacion de OWN CORUSEL

    }); // Fin de scanfolderGaleriaVehiculo

  }); // Fin de subirfotos


  //alert($("#galeriaVehiculo").html())
  $.fancybox.close(true);
}); //Fin click carga de archivos

window.filtrarEstado = function (estado) {
  $("#tablaOrdenesRetiro").dataTable().api().search(estado).draw();
};

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(80),
  /* template */
  __webpack_require__(81),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\OrdenesRetiro.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] OrdenesRetiro.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1c600bb4", Component.options)
  } else {
    hotAPI.reload("data-v-1c600bb4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            ordenesretirolist: [],
            idordenretiro: ''
        };
    },

    created: function created() {
        this.getOrdenesRetiro();
    },

    methods: {
        getOrdenesRetiro: function getOrdenesRetiro() {
            var _this = this;

            var urlOR = 'ordenretiroFullDataAll';
            axios.get(urlOR).then(function (response) {
                _this.ordenesretirolist = response.data;
                setTimeout(function () {
                    $('#tablaOrdenesRetiro').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },

        viewOR: function viewOR(idorden) {
            //document.getElementById("action").value = "editaUsuario";
            //document.getElementById("iduser").value = idUser;
            //appUser.ruta = 'new message'
            //this.$set(this.formdata, 'age', 27)
            this.$emit('view-orden', idorden);
        }, // fin de la funcion viewUser

        /* crearExist: function(idorden) {
           //this.$emit('view-orden', idorden);
           axios.get("/actasResource").then(response => {
                swal({
                    title: "Orden Retiro",
                    text: "Ingreso Generado",
                    type: "success"
                }, function() {
                    //location.reload();
                });
                //$("#lista_actas").html(response.data)
             });
          }, // fin de la funcion crearExist */
        crearExist: function crearExist(idor, nproceso, patente) {
            swal({
                title: 'Generar existencia a partir de:<br> ' + 'Nº de Proceso: ' + nproceso + '</b><br>' + 'Patente: <b>' + patente + '</b>',
                html: 'Generar existencia a partir de:<br> ',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#52b74d",
                confirmButtonText: "Aceptar",
                cancelButtonColor: "#DD6B55",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    axios.get("/generarexistencia/" + idor).then(function (response) {
                        swal({
                            title: "Ingreso Generado",
                            text: 'Vehiculo Patente: <b>' + patente + '</b> <a href="/existencias">(Ver existencias)</a>',
                            html: true,
                            type: "success" });
                        //$("#lista_actas").html(response.data)
                    });
                } else {
                    swal("Cancelado", "Existencia no generada", "error");
                }
            });
            $('.sweet-alert button.cancel').css('background-color', 'red');
            //$('.sweet-alert button.cancel').addClass("pull-right")
        } // fin de la funcion crearExist */            

    }

});

/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.ordenesretirolist.length === 0) ? _c('li', [_vm._v("Sin Ordenes de Retiro registrados")]) : _vm._e(), _vm._v(" "), (_vm.ordenesretirolist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    staticStyle: {
      "font-size": "13px"
    },
    attrs: {
      "id": "tablaOrdenesRetiro"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.ordenesretirolist), function(ordenretiro) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(ordenretiro.nproceso))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.patente))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.nombrecompania))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.nsiniestro))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.nombremarca))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.nombremodelo))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.anio))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ordenretiro.diasenretiro))]), _vm._v(" "), _c('td', [_c('label', {
      class: [ordenretiro.estado.replace(" ", "")]
    }, [_vm._v(_vm._s(ordenretiro.estado))])]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary btn-sm",
      attrs: {
        "id": "btnVerOR",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateOrdenRetiro"
      },
      on: {
        "click": function($event) {
          _vm.viewOR(ordenretiro.id)
        }
      }
    }, [_vm._v("Editar")]), _vm._v(" "), _c('button', {
      staticClass: "btn btn-danger btn-sm",
      attrs: {
        "id": "btnCrearExist",
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.crearExist(ordenretiro.id, ordenretiro.nproceso, ordenretiro.patente)
        }
      }
    }, [_vm._v("Crear Exist")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("N° Proceso")]), _vm._v(" "), _c('th', [_vm._v("Patente")]), _vm._v(" "), _c('th', [_vm._v("Compañia")]), _vm._v(" "), _c('th', [_vm._v("N° Siniestro")]), _vm._v(" "), _c('th', [_vm._v("Marca")]), _vm._v(" "), _c('th', [_vm._v("Modelo")]), _vm._v(" "), _c('th', [_vm._v("Año")]), _vm._v(" "), _c('th', [_vm._v("Dias Retiro")]), _vm._v(" "), _c('th', [_vm._v("Estado")]), _vm._v(" "), _c('th', [_vm._v("Operaciones")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1c600bb4", module.exports)
  }
}

/***/ })

/******/ });