/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 109);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 109:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(110);


/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Existencias_vue__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Existencias_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Existencias_vue__);
//Vue.component('listausuarios', require('./components/Usuarios.vue'));

var appExistencias = new Vue({
  components: {
    'listadoexistencias': __WEBPACK_IMPORTED_MODULE_0__components_Existencias_vue___default.a

  },
  el: '#listado_existencias',
  data: {
    ingresoData: '',
    galeriaFotos: ''
  },
  methods: {

    getIngreso: function getIngreso(idingreso) {

      var vm = this;
      $('#collapseTaller').collapse('show');
      $('#collapseVehiculos').collapse('show');
      $('#collapseImg').collapse('hide');
      $('#tbodyArchivos').html('');

      $('#accion').val('ACTUALIZAR INGRESO');
      $('#idingreso').val(idingreso);
      $('#btnEnviar').html('ACTUALIZAR INGRESO');
      $('#btnEnviarTraslado').html('ACTUALIZAR INGRESO');
      $(".collapseTab").removeClass('disabledTab');

      $(".tipoOR").html('');
      //$('#patente').val(vm.ingresoData.vehiculo.patente);


      //alert("getIngreso")
      //Obteniendo datos propios de la orden
      axios.get('showFullDataIngreso/' + idingreso).then(function (response) {
        vm.ingresoData = response.data;

        //Seccion de la orden
        $('#titulomodal').html('ACTUALIZAR ORDEN - #' + vm.ingresoData.ingreso.nproceso);
        $("#nsiniestro").val(vm.ingresoData.ingreso.nsiniestro);
        $("#estado").select2("trigger", "select", {
          data: { id: vm.ingresoData.ingreso.estado }
        });
        //$("#estado").val(vm.ingresoData.ingreso.estado)
        $("#compania").select2("trigger", "select", {
          data: { id: vm.ingresoData.ingreso.compania }
        });

        //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
        var urlContactosEmpresa = "usuariosPorCompania/" + vm.ingresoData.ingreso.compania;
        axios.get(urlContactosEmpresa).then(function (response) {
          var contactos = response.data;
          $('#emailcontac').html('');
          $("#liquidador").html('');
          $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

          contactos.forEach(function (contacto, key, contactos) {
            // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
            //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
            $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
          });

          $("#emailcontac").select2("trigger", "select", {
            data: { id: vm.ingresoData.ingreso.contacto }
          });
        });

        var urlLiquidadoresEmpresa = "liquidadoresPorCompania/" + vm.ingresoData.ingreso.compania;
        axios.get(urlLiquidadoresEmpresa).then(function (response) {
          var liquidadores = response.data;
          $("#liquidador").html('');

          $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

          liquidadores.forEach(function (liquidador, key, contactos) {
            if (vm.ingresoData.ingreso.liquidador == liquidador.id) {
              $("#liquidador").append($("<option></option>").attr("value", liquidador.id).attr("selected", true).text(liquidador.liquidador));
            } else {
              $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
            }
          });
        });

        $("#vtraslado").val(formatearNumero(vm.ingresoData.ingreso.vtraslado));
        $("#nfacturaproveedor").val(vm.ingresoData.ingreso.nfacturaproveedor);
        $("#vfacturacia").val(formatearNumero(vm.ingresoData.ingreso.vfacturacia));
        $("#nfactura").val(vm.ingresoData.ingreso.nfactura);
        $("#tasacion").val(formatearNumero(vm.ingresoData.ingreso.tasacion));
        $("#vminremate").val(formatearNumero(vm.ingresoData.ingreso.vminremate));
        $("#vindem").val(formatearNumero(vm.ingresoData.ingreso.vindem));

        //Seccion del vehiculo
        $("#patente").val(vm.ingresoData.vehiculo.patente);

        $("#marca").select2("trigger", "select", {
          data: { id: vm.ingresoData.vehiculo.marcaid }
        });
        /*setTimeout(function(){
          $("#modelo").select2("trigger", "select", {
            data: { id: vm.ingresoData.vehiculo.modeloid }
          });
        }, 1000);*/

        $('#modelo').html('');
        var urlModelo = 'showFullDataByMarca/' + vm.ingresoData.vehiculo.marcaid;
        axios.get(urlModelo).then(function (response) {
          var modelos = response.data;
          $("#modelo").select2("trigger", "select", {
            data: { id: vm.ingresoData.vehiculo.modeloid }
          });
        });

        $("#anio").select2("trigger", "select", {
          data: { id: vm.ingresoData.vehiculo.anio }
        });
        $("#tipo").select2("trigger", "select", {
          data: { id: vm.ingresoData.vehiculo.tipoid }
        });
        $("#color").val(vm.ingresoData.vehiculo.color);

        $("#combustible").select2("trigger", "select", {
          data: { id: vm.ingresoData.vehiculo.combustible }
        });
        $("#nmotor").val(vm.ingresoData.vehiculo.nummotor);
        $("#nchasis").val(vm.ingresoData.vehiculo.numchasis);
        $("#vim").val(vm.ingresoData.vehiculo.VIM);
        $("#nserie").val(vm.ingresoData.vehiculo.serial);
        $("#nombreprop").val(vm.ingresoData.vehiculo.nombrepropietario);
        $("#rutprop").val(vm.ingresoData.vehiculo.rutpropietario);

        //Seccion del taller
        $("#idtaller").val(vm.ingresoData.taller.id);
        $("#nombretaller").val(vm.ingresoData.taller.nombre);
        $("#ruttaller").val(vm.ingresoData.taller.rut);
        $("#direcciontaller").val(vm.ingresoData.taller.direccion);
        $("#direcciontaller").val(vm.ingresoData.taller.direccion);
        $("#emailtaller").val(vm.ingresoData.taller.email);
        $("#nombcontactotaller").val(vm.ingresoData.taller.contacto);
        $("#telefonotaller").val(vm.ingresoData.taller.telefono);

        //Seccion de traslado
        if (vm.ingresoData.transportista) {
          $("#idtransportista").val(vm.ingresoData.transportista.id);
          $("#emailtransp").val(vm.ingresoData.transportista.email);
          $("#telefonotransp").val(vm.ingresoData.transportista.telefono);
          $("#ruttransp").val(vm.ingresoData.transportista.rutempresa);
          $("#nombtransp").val(vm.ingresoData.transportista.nombreempresa);
          $("#nombchofertransp").val(vm.ingresoData.transportista.nombrechofer);
          $("#gruaexternatransp").val(vm.ingresoData.transportista.gruaexterna);
          $("#patentecamiontransp").val(vm.ingresoData.transportista.patentecamion);
          $("#rutchofertransp").val(vm.ingresoData.transportista.rutchofer);
        }

        //Seccion de Bodega

        if (vm.ingresoData.ingreso.permiso == 'on') {
          $("#permiso").prop("checked", true);
        } else {
          $("#permiso").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.seguro == 'on') {
          $("#seguro").prop("checked", true);
        } else {
          $("#seguro").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.padron == 'on') {
          $("#padron").prop("checked", true);
        } else {
          $("#padron").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.revtec == 'on') {
          $("#revtec").prop("checked", true);
        } else {
          $("#revtec").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.gases == 'on') {
          $("#gases").prop("checked", true);
        } else {
          $("#gases").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.condicionado == 'on') {
          $("#condicionado").prop("checked", true);
        } else {
          $("#condicionado").prop("checked", false);
        }

        if (vm.ingresoData.ingreso.desarme == 'on') {
          $("#desarme").prop("checked", true);
        } else {
          $("#desarme").prop("checked", false);
        }

        $("#llaves").val(vm.ingresoData.ingreso.llaves);
        $("#placas").val(vm.ingresoData.ingreso.placas);
        $("#tag").val(vm.ingresoData.ingreso.tag);
        $("#computador").val(vm.ingresoData.ingreso.computador);
        $("#motorarranca").val(vm.ingresoData.ingreso.motorarranca);
        $("#airbag").val(vm.ingresoData.ingreso.airbag);
        /*var urlBodegas = 'bodegasResource';
        axios.get(urlBodegas).then(response => {
            this.bodegalist = response.data
        });*/
        $("#bodegas").val(vm.ingresoData.ingreso.bodegas);

        axios.get('scanfolderGaleriaVehiculo/' + vm.ingresoData.vehiculo.patente).then(function (response) {
          //vm.galeriaFotos = response.data  
          $("#galeriaVehiculo").html(response.data);
          $('#galeriaVehiculo').trigger('destroy.owl.carousel');
          $('#galeriaVehiculo').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        });

        //Seccion de archivos

        axios.get('scanfolderArchivosIngreso/' + vm.ingresoData.ingreso.nproceso).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyArchivos').html('');
          $("#tbodyArchivos").append(response.data);
        });

        //Seccion de Observaciones

        axios.get('showObservacionesExist/' + vm.ingresoData.ingreso.id).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyObervaciones').html('');
          $("#tbodyObervaciones").append(response.data);
        });

        //Seccion Historico
        axios.get('showHistoricoExist/' + vm.ingresoData.ingreso.nproceso).then(function (response) {

          $('#operacioneshist').html('');
          $("#operacioneshist").append(response.data);
        });
      }); // Fin de recuperar datos de la orden

    } //Fin de getIngreso


    //Fin de METHODS del componente
  } });

var appOrModal = new Vue({

  el: '#capaModal',
  data: {
    ordenesList: '',
    marcasList: '',
    modelosList: '',
    tiposList: '',
    talleresList: '',
    companiaList: '',
    bodegalist: '',
    vehiculo: ''

  },
  created: function created() {
    this.getMarcas();
    this.getTipos();
    this.getCompania();
    this.getTalleres();
    this.getBodegas();
  },
  methods: {
    crearIngreso: function crearIngreso() {
      $('#titulomodal').html('CREAR INGRESO');
      $('#accion').val('CREAR INGRESO');
      $('#btnEnviar').html('CREAR INGRESO');
      $("#_method").val("POST");
      $('.tipoOR').html("Ingreso Nuevo");
      $(".collapseTab").addClass('disabledTab');
      $("#galeriaVehiculo").html("");

      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('.nav-tabs a[href="#vehiculo"]').tab('show');
      $('.collapse').collapse('hide');
      setTimeout(function () {
        $('#collapseVehiculos').collapse('show');
      }, 500);
      $("#compania").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#marca").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#modelo").html("");
      $("#tipo").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#anio").select2("trigger", "select", {
        data: { id: new Date().getFullYear() + 1 }
      });
      $('#idingreso').val('');
    },
    crearPDF: function crearPDF() {
      var idingreso = $('#idingreso').val();
      window.location.replace("pdfor/" + idingreso + "/descargar");
    },

    enviarDatosBasicos: function enviarDatosBasicos() {
      var _this = this;

      var validardatos = 0;
      var validarpatentesiniestro = 0;
      $(".requerido").each(function (index) {
        if ($(this).val() == "" || $(this).val() == "0") {
          swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
          //Asigna 1 si hay un campo requerido vacio
          validardatos = 1;
        }
      });

      if (validardatos == 0) {
        var formData = new FormData();
        formData.append('patente', $("#patente").val());
        formData.append('nsiniestro', $("#nsiniestro").val());
        axios.post("/validarPatenteSiniestroExistencia/", formData).then(function (response) {
          validarpatentesiniestro = response.data;

          if (validarpatentesiniestro == 1 && $('#accion').val() == 'CREAR INGRESO') {
            swal('VALIDAR DATOS', 'PATENTE Y Nº SINIESTRO REGISTRADO', 'warning');
          } else {
            if ($('#accion').val() == 'CREAR INGRESO') {
              _this.$refs.formExist._method.value = "POST";
              //alert("enviando el contacto nuevo")
              //console.log("enviando datos basicos")
              var validardatos = 0;

              $("#formExist").submit();
            } else {
              _this.$refs.formExist._method.value = "PATCH";
              _this.$refs.formExist.action = "/existenciaResource/" + $('#idingreso').val();
              $("#formExist").submit();
            }
          }
        });
      } //fin if validardatos
    },
    enviarObservacion: function enviarObservacion() {
      var formData = new FormData();
      var nombrearchivo;
      var idingreso = $("#idingreso").val();
      var observ = $("#obsertxt").val();
      var privada = 0;
      if ($("#observprivada").is(":checked")) {
        alert('Seleccionado');
        privada = 1;
      }

      formData.append('idingreso', idingreso);
      formData.append('obsertxt', observ);
      formData.append('privada', privada);

      axios.post("/storeObservacionExist/" + idingreso, formData).then(function (response) {
        swal('Observación', 'Observación ingresada', 'success');

        axios.get('showObservacionesExist/' + idingreso).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyObervaciones').html('');
          $("#tbodyObervaciones").append(response.data);
        });
      });
    },

    enviarTraslado: function enviarTraslado() {
      var formData = new FormData();
      var idingreso = $("#idingreso").val();
      var idtransportista = $("#idtransportista").val();

      formData.append('idingreso', idingreso);
      formData.append('idtransportista', idtransportista);

      axios.post("/updateTransportistaExist/", formData).then(function (response) {
        swal('Transportista', 'Transportista Actualizado', 'success');
      });
    },
    enviarEmailTraslado: function enviarEmailTraslado() {
      var formData = new FormData();
      var idingreso = $("#idingreso").val();
      var idtransportista = $("#idtransportista").val();

      if (idtransportista != '') {
        //this.enviarTraslado();
        formData.append('idingreso', idingreso);
        formData.append('idtransportista', idtransportista);
        swal('Enviando', 'Enviando Email...', 'warning');

        axios.post("/updateTransportista/", formData).then(function (response) {
          axios.post("/notificarTransportista/", formData).then(function (response) {
            swal('Transportista', 'Transportista notificado', 'success');
            axios.post("/orasignada/", formData);
          });
        });
      } else {
        swal('Validar', 'Debe seleccionar un transportista', 'warning');
      }
    },
    getMarcas: function getMarcas() {
      var _this2 = this;

      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(function (response) {
        _this2.marcasList = response.data;
      });
    },
    getTipos: function getTipos() {
      var _this3 = this;

      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(function (response) {
        _this3.tiposList = response.data;
        //console.log(response.data)
      });
    },
    getCompania: function getCompania() {
      var _this4 = this;

      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this4.companiaList = response.data;
      });
    },
    getTalleres: function getTalleres() {
      var _this5 = this;

      var urlTaller = 'talleresResource';
      axios.get(urlTaller).then(function (response) {
        _this5.talleresList = response.data;
      });
    },
    getBodegas: function getBodegas() {
      var _this6 = this;

      var urlBodegas = 'bodegasResource';
      axios.get(urlBodegas).then(function (response) {
        _this6.bodegalist = response.data;
      });
    },
    getContacto: function getContacto(idcontacto) {
      var urlContacto = 'contactosResource/' + idcontacto;
      axios.get(urlContacto).then(function (response) {
        $('#emailcontac').val(response.data.email);
        $('#tlfcontacto').val(response.data.fono1);
        $('#nombcontacto').val(response.data.nombre + ' ' + response.data.apellido);
      });
    },

    buscarVehiculo: function buscarVehiculo() {
      var _this7 = this;

      //const vm = this
      patente = $('#patente').val();
      if (!patente) {
        swal('INGRESE UNA PATENTE', 'DATO REQUERIDO', 'warning');
      } else {
        //var urlVehiculo = 'buscarpatente/'+patente;
        var urlVehiculo = 'buscarPatenteTasacion/' + patente;
        axios.get(urlVehiculo).then(function (response) {
          _this7.vehiculo = response.data;
          //alert (response.data.lenght)
          if (_this7.vehiculo.length == 0) {
            swal('PATENTE NO REGISTRADA', '', 'error');
          } else {
            swal('PATENTE REGISTRADA', 'REGISTRADA EN TASACIONES', 'success');
            /*$("#idvehiculo").val(this.vehiculo[0].id)*/

            //$("#idvehiculo").val(this.vehiculo.id)

            $("#marca").select2("trigger", "select", {
              data: { id: _this7.vehiculo.marcaid }
            });
            $('#modelo').html('');

            //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
            var urlModelo = 'showFullDataByMarca/' + _this7.vehiculo.marcaid;
            axios.get(urlModelo).then(function (response) {
              var modelos = response.data;
              //console.log(modelos)
              $("#modelo").append($("<option></option>").attr("value", "0").text("Seleccione modelo"));

              modelos.forEach(function (modelo, key, modelos) {
                // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
                //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
                $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
              });

              $("#modelo").select2("trigger", "select", {
                data: { id: _this7.vehiculo.modeloid }
              });
            });

            $("#anio").select2("trigger", "select", {
              data: { id: _this7.vehiculo.anio }
            });
            $("#tipo").select2("trigger", "select", {
              data: { id: _this7.vehiculo.tipoid }
            });
          }

          //console.log(this.vehiculo[0].patente)

        });
      }
      this.vehiculo = '';
    }

  }
});

/*$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});*/

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateExistencias").on("hidden.bs.modal", function () {
  //document.getElementById("formExist").reset();

  $("form").trigger("reset");
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$('#importarCertificadoModal').on('shown.bs.modal', function (e) {
  $('#datosCertificados').focus();
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');
  if (haserror == 1) {
    $('#newUpdateExistencias').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }

  $('[data-fancybox="images"]').fancybox({});

  //Detecta si se ha creado una nueva OR y se seguira 
  if ($("#idingreso").val() != '') {

    $('#newUpdateExistencias').modal('show');
    setTimeout(function () {
      appExistencias.getIngreso($("#idingreso").val());
    }, 1500);
  }

  $('#combustible').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $("#marca").select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $("#modelo").select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#tipo').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#compania').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#estado').select2();

  $('select:not(.normal)').each(function () {
    $(this).select2({
      dropdownParent: $(this).parent()
    });
  });

  var anio = new Date().getFullYear() + 1;
  var i = 0;
  for (i = anio; i > 1969; i--) {
    // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
    $("#anio").append($("<option></option>").attr("value", i).text(i));
  };

  $('#anio').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#emailcontac').select2({
    dropdownParent: $("#newUpdateExistencias")
  });

  var options = {
    url: "talleresResource",

    getValue: "direccion",

    list: {
      onClickEvent: function onClickEvent() {
        var nombretaller = $("#direcciontaller").getSelectedItemData().nombre;
        var ruttaller = $("#direcciontaller").getSelectedItemData().rut;
        var telefonotaller = $("#direcciontaller").getSelectedItemData().telefono;
        var emailtaller = $("#direcciontaller").getSelectedItemData().email;
        var contactotaller = $("#direcciontaller").getSelectedItemData().contacto;
        var idtaller = $("#direcciontaller").getSelectedItemData().id;

        $("#nombretaller").val(nombretaller).trigger("change");
        $("#ruttaller").val(ruttaller).trigger("change");
        $("#telefonotaller").val(telefonotaller).trigger("change");
        $("#emailtaller").val(emailtaller).trigger("change");
        $("#nombcontactotaller").val(contactotaller).trigger("change");
        $("#idtaller").val(idtaller).trigger("change");
      },
      match: {
        enabled: true
      }
    }
  };

  $("#direcciontaller").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Autocompletar del Transportista
  var options = {
    url: "transportistaResource",

    getValue: "nombrechofer",

    list: {
      onClickEvent: function onClickEvent() {

        var idtransportista = $("#nombchofertransp").getSelectedItemData().id;
        var ruttransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var telefonotransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var emailtransp = $("#nombchofertransp").getSelectedItemData().email;
        var nombreempresa = $("#nombchofertransp").getSelectedItemData().nombreempresa;
        var gruaexterna = $("#nombchofertransp").getSelectedItemData().gruaexterna;
        var patentecamiontransp = $("#nombchofertransp").getSelectedItemData().patentecamion;
        var rutchofertransp = $("#nombchofertransp").getSelectedItemData().rutchofer;
        $("#idtransportista").val(idtransportista).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#telefonotransp").val(telefonotransp).trigger("change");
        $("#ruttransp").val(ruttransp).trigger("change");
        $("#nombtransp").val(nombreempresa).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#gruaexternatransp").val(gruaexterna).trigger("change");
        $("#patentecamiontransp").val(patentecamiontransp).trigger("change");
        $("#rutchofertransp").val(rutchofertransp).trigger("change");
        $("#btnEnviarTraslado").prop("disabled", false);
        $("#btnEnviarEmailTraslado").prop("disabled", false);
      },
      match: {
        enabled: true
      }
    }
  };

  $("#nombchofertransp").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Evento que ocurre cuando se selecciona una marca para llenar los modelos
  $('#marca').on('select2:select', function (e) {
    $('#modelo').html('');
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlModelo = 'showFullDataByMarca/' + data.id;
    axios.get(urlModelo).then(function (response) {
      var modelos = response.data;
      //console.log(modelos)
      $("#modelo").append($("<option></option>").attr("value", "0").text("Seleccione modelo"));

      modelos.forEach(function (modelo, key, modelos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
      });
    });
  });

  //Evento que ocurre cuando se selecciona un email de contacto para llenar los modelos
  $('#emailcontac').on('select2:select', function (e) {
    //$('#modelo').html('')
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    $('#idcontacto').val(data.id);
  });

  $('#filtrarEstado').on('select2:select', function (e) {
    var data = e.params.data;
    $("#tablaExistencias").dataTable().api().search(data.text).draw();
  }); // Fin del evento change de estado


  //Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
  $('#compania').on('select2:select', function (e) {
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "usuariosPorCompania/" + data.id;
    axios.get(urlContactosEmpresa).then(function (response) {
      var contactos = response.data;
      $('#emailcontac').html('');
      $("#liquidador").html('');
      $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

      contactos.forEach(function (contacto, key, contactos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
      });
    });

    var urlLiquidadoresEmpresa = "liquidadoresPorCompania/" + data.id;
    axios.get(urlLiquidadoresEmpresa).then(function (response) {
      var liquidadores = response.data;

      $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

      liquidadores.forEach(function (liquidador, key, contactos) {
        $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
      });
    });
  }); // Fin del evento change de compania


  $("#direcciontaller").focusin(function () {
    $("#nombretaller").val('');
    $("#ruttaller").val('');
    $("#telefonotaller").val('');
    $("#emailtaller").val('');
    $("#nombcontactotaller").val('');
    $("#idtaller").val('');
  });

  $("#nombchofertransp").focusin(function () {
    $("#idtransportista").val('');
    $("#ruttransp").val('');
    $("#nombtransp").val('');
    $("#patentecamiontransp").val('');
    $("#telefonotransp").val('');
    $("#emailtransp").val('');
    $("#rutchofertransp").val('');
    $("#btnEnviarTraslado").prop("disabled", true);
    $("#btnEnviarEmailTraslado").prop("disabled", true);
  });
}); // Fin del Document ready


window.eliminarArchivo = function (nproceso, nombrearchivo, idingreso) {
  axios.post('eliminararchivoExist/' + nproceso + '/' + nombrearchivo + '/' + idingreso).then(function (response) {
    swal('Archivo Eliminado', 'Se eliminó el archivo: '.nombrearchivo, 'success');
  });
  //alert('#doc'+idarchivo)
  //document.getElementById("tablaarchivos").deleteRow(0);
  //$(this).closest('tr').remove();
};

$("#tablaarchivos").on('click', '.btnDelete', function () {
  $(this).closest('tr').remove();
});

//Funcion que comprueba el estado de la OR
window.eliminarFoto = function (patente, idfoto) {
  var galeriaFotos = void 0;

  axios.post('eliminarfoto/' + appExistencias.ingresoData.vehiculo.patente + '/' + idfoto).then(function (response) {});

  galeriaFotos = axios.get('scanfolderGaleriaVehiculo/' + appExistencias.ingresoData.vehiculo.patente).then(function (response) {
    return response.data;
  });

  //
  galeriaFotos.then(function (result) {
    swal('Imegen Eliminada', 'Se eliminó la imagen: '.idfoto, 'success');

    $("#galeriaVehiculo").html(result);
    $('#galeriaVehiculo').trigger('destroy.owl.carousel');
    $('#galeriaVehiculo').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    }); //Fin de funcion de inicializacion de OWN CORUSEL

  });

  $.fancybox.close(true);
};

$("#limpiarfiltro").click(function () {
  $("#tablaExistencias").dataTable().api().search("").draw();
}); //Fin click guardar observacion

$("#cargarArchivo").click(function () {

  var formData = new FormData();
  var nombrearchivo;
  var idingreso = $("#idingreso").val();
  var nproceso = appExistencias.ingresoData.ingreso.nproceso;
  var observ = $("#obserfile").val();

  formData.append('archivo', document.getElementById('archivoOR').files[0]);
  formData.append('idingreso', idingreso);
  formData.append('nproceso', nproceso);
  formData.append('observacion', observ);

  var archivocargado = axios.post('subirarchivoExist', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {

    //return response.data;
    axios.get('scanfolderArchivosIngreso/' + nproceso).then(function (response) {
      swal('Cargar Archivo', 'Archivo cargado', 'success');
      $('#tbodyArchivos').html('');
      $("#tbodyArchivos").append(response.data);
    });
  });
}); //Fin click carga de arechivo

$("#btnActBdg").click(function () {
  //let formData = new FormData();
  //let formData = document.getElementById("frmBodega");
  var myForm = document.getElementById('frmBodega');

  var formData = new FormData(myForm);
  //var nombrearchivo;
  var idingreso = $("#idingreso").val();
  var nproceso = appExistencias.ingresoData.ingreso.nproceso;

  formData.append('idingreso', idingreso);
  formData.append('nproceso', nproceso);
  axios.post('updateBodega', formData).then(function (response) {
    swal('Actualizar Inventario', 'Datos de inventario actualizados', 'success');
    $("#resultado").html(response.data);
  });
}); //Fin click carga de actualizar datos de bodega

$("#btnImportarCert").click(function () {
  //alert("btnImportarCert");
  var inicio = 0;
  var fin = 0;

  var aLineas = document.getElementById("datosCertificados").value.split('\n');
  var datosCertificados = document.getElementById("datosCertificados").value;
  //alert("El textarea tiene " + aLineas.length + " líneas");

  //Importador del Dato Patente
  inicio = datosCertificados.indexOf("Inscripción : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var patente = datosCertificados.substring(inicio + 14, fin - 2);
  patente = patente.replace(".", "-");
  $("#patente").val(patente);

  //Importador del Dato Marca
  inicio = datosCertificados.indexOf("Marca : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var marcanombre = datosCertificados.substring(inicio + 8, fin);
  var resultMarca = appOrModal.marcasList.filter(function (marca) {
    return marca.nombre === marcanombre;
  });
  $("#marca").select2("trigger", "select", {
    data: { id: resultMarca[0].id }
  });

  //Importador del Dato Modelo
  inicio = datosCertificados.indexOf("Modelo : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var modelonombre = datosCertificados.substring(inicio + 9, fin);
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function () {
    var urlbuscarModeloId = 'buscarModeloPorNombre/' + modelonombre;
    axios.get(urlbuscarModeloId).then(function (response) {
      $("#modelo").select2("trigger", "select", {
        data: { id: response.data[0].id }
      });
    });
  }, 1500);

  //Importador del Dato Año
  inicio = datosCertificados.indexOf("Año : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var anio = datosCertificados.substring(inicio + 6, fin);
  $("#anio").val(anio);

  //Importador del Dato Color
  inicio = datosCertificados.indexOf("Color : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var color = datosCertificados.substring(inicio + 8, fin);
  $("#color").val(color);

  //Importador del Dato Combustible
  inicio = datosCertificados.indexOf("Combustible : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var combustible = datosCertificados.substring(inicio + 14, fin);
  $("#combustible").select2("trigger", "select", {
    data: { id: combustible }
  });

  //Importador del Dato TIPO
  inicio = datosCertificados.indexOf("Tipo Vehículo : ");
  fin = datosCertificados.indexOf(' Año : ');
  var nombretipo = datosCertificados.substring(inicio + 16, fin);

  var resultTipo = appOrModal.tiposList.filter(function (tipo) {
    return tipo.nombre === nombretipo;
  });
  //console.log(resultTipo)
  $("#tipo").select2("trigger", "select", {
    data: { id: resultTipo[0].id }
  });

  //Importador del Dato Nro. Motor 
  inicio = datosCertificados.indexOf("Nro. Motor : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nmotor = datosCertificados.substring(inicio + 13, fin);
  $("#nmotor").val(nmotor);

  //Importador del Dato Nro. Chasis
  inicio = datosCertificados.indexOf("Nro. Chasis : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nchasis = datosCertificados.substring(inicio + 14, fin);
  $("#nchasis").val(nchasis);

  //Importador del Dato Nro. Vin
  inicio = datosCertificados.indexOf("Nro. Vin : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);
  if (inicio < 0) {
    $("#vim").val('');
  } else {
    $("#vim").val(nvim);
    $("#nserie").val(nvim);
  }

  //Importador del Dato Nombre del propietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#nombreprop").val('');
  } else {
    inicio = datosCertificados.indexOf("Nombre : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var nombreprorp = datosCertificados.substring(inicio + 9, fin);
    $("#nombreprop").val(nombreprorp);
  }

  //Importador del Dato rut del prropietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#rutprop").val('');
  } else {
    inicio = datosCertificados.indexOf("R.U.N. : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var rutprop = datosCertificados.substring(inicio + 9, fin);
    $("#rutprop").val(rutprop);
  }

  inicio = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);

  $('#importarCertificadoModal').modal('hide');
}); //Fin click importar datos del certificado


$("#btnsubirimg").click(function () {
  var formData = new FormData();

  for (var key in document.getElementById('imagenesVehiculo').files) {
    formData.append('fotosvehiculo[]', document.getElementById('imagenesVehiculo').files[key]);
  }
  formData.append('patente', appExistencias.ingresoData.vehiculo.patente);
  formData.append('nproceso', appExistencias.ingresoData.ingreso.nproceso);

  axios.post('subirfotosexist', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {
    axios.get('scanfolderGaleriaVehiculo/' + appExistencias.ingresoData.vehiculo.patente).then(function (response) {

      swal('Cargar Imagen', 'Imagen(es) Cargada(s)', 'success');
      $("#galeriaVehiculo").html(response.data);
      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('#galeriaVehiculo').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      }); //Fin de funcion de inicializacion de OWN CORUSEL

    }); // Fin de scanfolderGaleriaVehiculo

  }); // Fin de subirfotos


  //alert($("#galeriaVehiculo").html())
  $.fancybox.close(true);
}); //Fin click carga de archivos

/***/ }),

/***/ 111:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(112),
  /* template */
  __webpack_require__(113),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Existencias.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Existencias.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ef7e99a2", Component.options)
  } else {
    hotAPI.reload("data-v-ef7e99a2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            ingresolist: [],
            idingreso: '2'
        };
    },

    created: function created() {
        this.getIngresos();
    },

    methods: {
        getIngresos: function getIngresos() {
            var _this = this;

            var urlOR = 'existenciasFullDataAll';
            axios.get(urlOR).then(function (response) {
                _this.ingresolist = response.data;
                setTimeout(function () {
                    $('#tablaExistencias').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },

        viewIngreso: function viewIngreso(idingreso) {
            this.$emit('view-ingreso', idingreso);
        }, // fin de la funcion viewIngreso
        /*viewFicha: function(idingreso) {
           this.$emit('view-ficha', idingreso);
          }, // fin de la funcion viewFicha*/
        formatoEstado: function formatoEstado(estado) {
            //return $.trim(estado)
            return estado.replace(/ /g, "");
            //var valor='A1 B2 C3';   console.log($.trim(valor));

        } // fin de la funcion viewFicha

    }

});

/***/ }),

/***/ 113:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.ingresolist.length === 0) ? _c('li', [_vm._v("Sin Ordenes de Retiro registrados")]) : _vm._e(), _vm._v(" "), (_vm.ingresolist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    staticStyle: {
      "font-size": "13px"
    },
    attrs: {
      "id": "tablaExistencias"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.ingresolist), function(ingreso) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(ingreso.nproceso))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.patente))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombrecompania))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nsiniestro))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombremarca))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombremodelo))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.anio))]), _vm._v(" "), _c('td', [_c('label', {
      class: [_vm.formatoEstado(ingreso.estado)]
    }, [_vm._v(_vm._s(ingreso.estado))])]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary btn-sm",
      attrs: {
        "id": "btnVerIngreso",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateExistencias"
      },
      on: {
        "click": function($event) {
          _vm.viewIngreso(ingreso.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("N° Proceso")]), _vm._v(" "), _c('th', [_vm._v("Patente")]), _vm._v(" "), _c('th', [_vm._v("Compañia")]), _vm._v(" "), _c('th', [_vm._v("N° Siniestro")]), _vm._v(" "), _c('th', [_vm._v("Marca")]), _vm._v(" "), _c('th', [_vm._v("Modelo")]), _vm._v(" "), _c('th', [_vm._v("Año")]), _vm._v(" "), _c('th', [_vm._v("Estado")]), _vm._v(" "), _c('th', [_vm._v("Operaciones")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-ef7e99a2", module.exports)
  }
}

/***/ })

/******/ });