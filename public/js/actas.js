/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 97);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            ingresolist: [],
            idingreso: ''
        };
    },

    created: function created() {
        this.getIngresos();
    },

    methods: {
        getIngresos: function getIngresos() {
            var _this = this;

            var urlOR = 'existenciasFullDataAllActa';
            axios.get(urlOR).then(function (response) {
                _this.ingresolist = response.data;
                if (_this.ingresolist.length === 0) {
                    $('#btnCrearActa').remove();
                }

                setTimeout(function () {
                    $('#tablaExistencias').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },
        viewIngreso: function viewIngreso(idingreso) {
            this.$emit('view-ingreso', idingreso);
        }, // fin de la funcion viewIngreso
        viewFicha: function viewFicha(idingreso) {
            this.$emit('view-ficha', idingreso);
        } // fin de la funcion viewFicha

    }

});

/***/ }),

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.ingresolist.length === 0) ? _c('li', [_vm._v("Sin vehículos en el acta")]) : _vm._e(), _vm._v(" "), (_vm.ingresolist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    staticStyle: {
      "font-size": "13px"
    },
    attrs: {
      "id": "tablaExistencias"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.ingresolist), function(ingreso) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(ingreso.nproceso))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.patente))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombrecompania))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nsiniestro))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombremarca))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.nombremodelo))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ingreso.anio))])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("N° Proceso")]), _vm._v(" "), _c('th', [_vm._v("Patente")]), _vm._v(" "), _c('th', [_vm._v("Compañia")]), _vm._v(" "), _c('th', [_vm._v("N° Siniestro")]), _vm._v(" "), _c('th', [_vm._v("Marca")]), _vm._v(" "), _c('th', [_vm._v("Modelo")]), _vm._v(" "), _c('th', [_vm._v("Año")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-54c157e2", module.exports)
  }
}

/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(103),
  /* template */
  __webpack_require__(104),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Actas.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Actas.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d627bbb", Component.options)
  } else {
    hotAPI.reload("data-v-7d627bbb", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            actasList: [],
            idacta: '',
            fechaformat: ''
        };
    },

    created: function created() {
        this.getactas();
    },
    methods: {
        getactas: function getactas() {
            var _this = this;

            var urlOR = 'actasResource';
            axios.get(urlOR).then(function (response) {
                _this.actasList = response.data;
                setTimeout(function () {
                    $('#tablaActas').dataTable({
                        "order": [[0, "desc"]],
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },

        viewActa: function viewActa(idacta) {
            this.$emit('view-acta', idacta);
        }, // fin de la funcion viewacta

        nuevaFecha: function nuevaFecha(fecha) {

            //var nuevafecha = window.formatearFecha(fecha)
            var nuevafecha = fecha.split("-");
            return nuevafecha[2] + "/" + nuevafecha[1] + "/" + nuevafecha[0];
        } // fin de la funcion viewacta


    }

});

/***/ }),

/***/ 104:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.actasList.length === 0) ? _c('li', [_vm._v("Sin actas registradas")]) : _vm._e(), _vm._v(" "), (_vm.actasList.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    staticStyle: {
      "font-size": "13px"
    },
    attrs: {
      "id": "tablaActas"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.actasList), function(acta) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(acta.numeroacta))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.nuevaFecha(acta.fecha)))]), _vm._v(" "), _c('td', {
      class: ['acta' + acta.estado]
    }, [_vm._v(_vm._s(acta.estado))]), _vm._v(" "), _c('td', [_c('a', {
      staticClass: "btn btn-primary btn-sm",
      attrs: {
        "href": 'acta/' + acta.numeroacta,
        "id": "verActa"
      }
    }, [_vm._v("Ver")]), _vm._v(" "), (acta.estado === "Cerrada") ? _c('a', {
      staticClass: "btn btn-warning btn-sm",
      attrs: {
        "href": 'acta/resumen/' + acta.numeroacta,
        "id": "verActa"
      }
    }, [_vm._v("Resumen")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-success btn-sm",
      attrs: {
        "href": 'acta/planillacobranza/' + acta.numeroacta,
        "id": "planillacobranza"
      }
    }, [_vm._v("Planilla Cobranza")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-success btn-sm",
      attrs: {
        "href": 'acta/procesar/' + acta.numeroacta,
        "id": "procesaracta"
      }
    }, [_vm._v("Procesar")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-danger btn-sm",
      attrs: {
        "href": 'acta/lotes/' + acta.numeroacta,
        "id": "asignarlotes"
      }
    }, [_vm._v("Asignar Lote")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-default btn-sm",
      attrs: {
        "href": 'acta/actamartillero/' + acta.numeroacta,
        "id": "actamartillero"
      }
    }, [_vm._v("Acta Martillero")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-default btn-sm",
      attrs: {
        "href": 'acta/imprimirlotes/' + acta.numeroacta,
        "id": "imprimirlotes"
      }
    }, [_vm._v("Carteles lotes")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-default btn-sm",
      attrs: {
        "href": 'acta/revisionacta/' + acta.numeroacta,
        "id": "revisionacta"
      }
    }, [_vm._v("Revisón Acta")]) : _vm._e(), _vm._v(" "), (acta.estado === "Activa") ? _c('a', {
      staticClass: "btn btn-default btn-sm",
      attrs: {
        "href": 'acta/boletinremate/' + acta.numeroacta,
        "id": "boletinremate"
      }
    }, [_vm._v("Boletines remate")]) : _vm._e()])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Numero")]), _vm._v(" "), _c('th', [_vm._v("Fecha")]), _vm._v(" "), _c('th', [_vm._v("Estado")]), _vm._v(" "), _c('th', [_vm._v("Operaciones")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7d627bbb", module.exports)
  }
}

/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(98);


/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_ExistenciasDisponible_vue__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_ExistenciasDisponible_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_ExistenciasDisponible_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Actas_vue__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Actas_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_Actas_vue__);
//Vue.component('listausuarios', require('./components/Usuarios.vue'));


var appExistencias = new Vue({
  components: {
    'listadoexistencias': __WEBPACK_IMPORTED_MODULE_0__components_ExistenciasDisponible_vue___default.a

  },
  el: '#listado_existencias',
  data: {},
  created: function created() {},

  methods: {

    verFicha: function verFicha(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/" + idingreso);
    },
    getLastacta: function getLastacta(idingreso) {} //Fin de getIngreso


    //Fin de METHODS del componente
  } });

var appActas = new Vue({
  components: {
    'listadoactas': __WEBPACK_IMPORTED_MODULE_1__components_Actas_vue___default.a

  },
  el: '#lista_actas',
  data: {},
  created: function created() {},

  methods: {

    verFicha: function verFicha(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/" + idingreso);
    },
    getDatoRemate: function getDatoRemate(idingreso) {

      alert("Carga el acta");
    } //Fin de getIngreso


    //Fin de METHODS del componente
  } });

var formIngresarData = new Vue({

  el: '#capaModal',
  data: {
    idcliente: 0,
    lote: '',
    acta: '',
    desarme: ''
  },
  created: function created() {},

  methods: {

    verFicha: function verFicha(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/" + idingreso);
    },
    ingresarDatos: function ingresarDatos(idingreso) {

      var validardatos = 0;
      $(".requerido").each(function (index) {
        if ($(this).val() == "" || $(this).val() == "0") {
          swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
          //return

          //Asigna 1 si hay un campo requerido vacio
          validardatos = 1;
        }
      });

      if (validardatos === 0) {
        if ($('#accion').val() == 'CREAR ORDEN') {
          //this.$refs.formExist._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log("enviando datos basicos")
          var validardatos = 0;
          alert("guardando datos");

          //$("#formExist").submit()
        } else {
            //this.$refs.formExist._method.value="PATCH";
            //this.$refs.formExist.action="/existenciaResource/"+$('#idingreso').val();
            //$("#formExist").submit();
          }
      }
    }, //Fin de getIngreso

    buscarCliente: function buscarCliente() {
      var vm = this;
      var rut = $("#rut").val();

      if (rut != "") {
        axios.get("/buscarDatosCliente/" + rut).then(function (response) {
          if (response.data != 0) {
            $("#nombres").val(response.data.nombre);
            $("#apellidos").val(response.data.apellido);
            $("#telefono").val(response.data.fono1);
            $("#email").val(response.data.email);
            vm.idcliente = response.data.id;
          } else {
            $("#buscarResult").html('RUT No Registrado');
            vm.idcliente = 0;
          }
        }); //FIN AXIOS GET

      } else {
        swal({
          title: "Validar datos",
          html: true,
          text: "Ingrese el <strong style='color:red'> RUT</strong> a buscar",
          type: "warning"
        }, function () {
          $("#rut").focus();
        });
      }
    } //Fin de buscarPersona


    //Fin de METHODS del componente
  } });

$(document).ready(function () {

  var numeroacta = axios.get('/ultimaacta').then(function (response) {
    $('#nacta').val(response.data);
  });

  $('#tablaVehiculos').dataTable({

    "language": {
      "url": "htps://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    dom: 'Bfrtip', buttons: ['pdf', 'excel']
  });

  $('#tablaProcesar').dataTable({

    "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    paging: false
  });

  $('#fecha').val(hoy());
}); // Fin del Document ready


$("#btnAsignarLotes").click(function () {

  var formData = new FormData();
  var numeroacta = $("#numeroacta").val();
  //formData.append('test', nacta);
  var str = "";
  var elem = document.getElementById('frmLotes').elements;
  for (var i = 0; i < elem.length; i++) {
    if (elem[i].type == "text") {
      formData.append(elem[i].name, elem[i].value);
      /*str += "<b>Type:</b>" + elem[i].type + "&nbsp&nbsp";
      str += "<b>Name:</b>" + elem[i].name + "&nbsp;&nbsp;";
      str += "<b>Value:</b><i>" + elem[i].value + "</i>&nbsp;&nbsp;";
      str += "<BR>";*/
    }
  }
  //document.getElementById('lista_actas').innerHTML = str;
  axios.post("/acta/lotes/guardar/" + numeroacta, formData).then(function (response) {
    swal({
      title: "Lotes",
      text: "Lotes Asignados",
      type: "success"
    }, function () {
      location.reload();
    });
    //$("#lista_actas").html(response.data)
  });
}); //Fin click Crear Acta

$("#btnCrearActa").click(function () {
  var formData = new FormData();
  var nacta = $("#nacta").val();

  formData.append('nacta', nacta);

  axios.post("/actasResource/", formData).then(function (response) {
    swal({
      title: "Acta",
      text: "Acta creada",
      type: "success"
    }, function () {
      window.location = "listaractas";
    });
    //$("#capaModal").html(response.data)
  });
}); //Fin click Crear Acta

$("#btnCerrarActa").click(function () {
  swal({
    title: "Esta seguro de cerrar el acta?",
    text: "Este proceso no se puede reversar!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Carrar Acta",
    cancelButtonText: "Cancelar",
    closeOnConfirm: false
  }, function () {
    var formData = new FormData();
    var numeroacta = $("#numeroacta").val();
    formData.append('numeroacta', numeroacta);
    axios.post("/cerrarActa", formData).then(function (response) {
      swal({
        title: "Acta",
        text: "Acta cerrada exitosamente.",
        type: "success"
      }, function () {
        window.location = "/listaractas";
      });
    });
  });
}); //Fin click Crear Acta

window.ingresarDatosLote = function () {
  var formData = new FormData();
  var monto = $("#monto").val();

  formData.append('_token', $("input[name*='_token']").val());
  formData.append('lote', formIngresarData.lote);
  formData.append('acta', formIngresarData.acta);
  formData.append('idcliente', formIngresarData.idcliente);
  formData.append('nombre', $("#nombres").val());
  formData.append('apellido', $("#apellidos").val());
  formData.append('rut', $("#rut").val());
  formData.append('email', $("#email").val());
  formData.append('telefono', $("#telefono").val());
  formData.append('monto', monto);
  formData.append('tasacionfiscal', $("#tasacionfiscal").val());
  formData.append('garantia', $("#garantia").val());
  formData.append('desarme', formIngresarData.desarme);
  /*
      if (formIngresarData.desarme)
      {
        alert("verdadero")
      }
      else
      {
        alert("falso")
      }*/

  var validardatos = 0;
  $(".requerido").each(function (index) {
    if ($(this).val() == "" || $(this).val() == "0") {
      swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
      //return

      //Asigna 1 si hay un campo requerido vacio
      validardatos = 1;
    }
  });

  if (validardatos === 0) {
    axios.post("/ingresarDatosLote", formData).then(function (response) {
      swal({
        title: "Datos ingresados",
        text: "Datos de remate ingresados exitosamente",
        type: "success"
      }, function () {
        $("#monto" + formIngresarData.lote).html('$' + parseFloat($("#monto").val()).toLocaleString('es-CL'));
        $("#rut" + formIngresarData.lote).html($("#rut").val());
        $("#estado" + formIngresarData.lote).html("<strong>Ingresado</strong");
        $("#estado" + formIngresarData.lote).removeClass();
        $("#estado" + formIngresarData.lote).addClass("text-success");
      });
      //$("#capaModal").html(response.data)

    });
  }
}; //Fin click Crear Acta

window.buscarDatosLote = function (lote, acta) {
  $("form").trigger("reset");
  formIngresarData.lote = lote;
  formIngresarData.acta = acta;
  /*
  let formData = new FormData();
  
    formData.append('lote', lote)
    formData.append('acta', acta)*/
  axios.get("/buscarDatosLote/" + acta + "/" + lote).then(function (response) {
    var datosIngreso = response.data[0];
    if (datosIngreso.cliente) {
      $("#nombres").val(datosIngreso.cliente.nombre);
      $("#apellidos").val(datosIngreso.cliente.apellido);
      $("#rut").val(datosIngreso.cliente.rut);
      $("#telefono").val(datosIngreso.cliente.fono1);
      $("#email").val(datosIngreso.cliente.email);
    }

    $("#monto").val(datosIngreso.monto);
    $("#garantia").val(datosIngreso.garantia);
    $("#tasacionfiscal").val(datosIngreso.tasacionfiscal);
    formIngresarData.idcliente = datosIngreso.idcliente;
    if (datosIngreso.estadoingreso == "Ingresado") {
      $("#estado" + lote).removeClass();
      $("#estado" + lote).addClass("text-success");
    } else {
      $("#estado" + lote).addClass("text-danger");
    }
    //console.log(response.data)
  });
};

/***/ }),

/***/ 99:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(100),
  /* template */
  __webpack_require__(101),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\ExistenciasDisponible.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ExistenciasDisponible.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-54c157e2", Component.options)
  } else {
    hotAPI.reload("data-v-54c157e2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

/******/ });