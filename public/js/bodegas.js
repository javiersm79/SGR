/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 52);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(53);


/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Bodegas_vue__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Bodegas_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Bodegas_vue__);


var appBodegaComp = new Vue({
  components: {
    'listabodegas': __WEBPACK_IMPORTED_MODULE_0__components_Bodegas_vue___default.a

  },
  el: '#listado_bodegas',
  data: {
    bodegaData: ''
  },
  methods: {
    actualizarBodega: function actualizarBodega(idbodega) {
      $('#titulomodal').html('ACTUALIZAR BODEGA');
      $('#accion').val('ACTUALIZAR BODEGA');
      $('#btnEnviar').html('ACTUALIZAR BODEGA');
    },
    getBodegaData: function getBodegaData(idbodega) {
      var vm = this;
      $('#titulomodal').html('ACTUALIZAR BODEGA');
      $('#accion').val('ACTUALIZAR BODEGA');
      $('#idBodega').val(idbodega);
      $('#btnEnviar').html('ACTUALIZAR BODEGA');
      var urlBodega = 'bodegasResource/' + idbodega;
      axios.get(urlBodega).then(function (response) {
        vm.bodegaData = response.data;
      });

      setTimeout(function () {
        $('#idBodega').val(idbodega);
        $('#nombre').val(vm.bodegaData.nombre);
        $('#habilitado').val(vm.bodegaData.habilitado);
      }, 500);
    }
  }
});

var appBodegaModal = new Vue({

  el: '#capaModal',
  data: {
    bodegaList: ''
  },
  created: function created() {
    //this.getCompania();
  },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearBodega: function crearBodega() {
      $('#titulomodal').html('CREAR BODEGA');
      $('#accion').val('CREAR BODEGA');
      $('#btnEnviar').html('CREAR BODEGA');
      $("#_method").val("POST");
    },
    enviarFormulario: function enviarFormulario() {
      if ($('#accion').val() == 'CREAR BODEGA') {
        this.$refs.formBodega._method.value = "POST";
        //alert("enviando el contacto nuevo")
        //console.log(this.$refs.formContacto._method)
        $("#formBodega").submit();
      } else {
        this.$refs.formBodega._method.value = "PATCH";
        this.$refs.formBodega.action = "/bodegasResource/" + $('#idBodega').val();
        $("#formBodega").submit();
      }
    }

  }
});

//Inicializa los datapicker
$('.datepicker').datepicker({
  format: "dd/mm/yyyy",
  startView: 2,
  endDate: hoy(),
  language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateBodega").on("hidden.bs.modal", function () {
  document.getElementById("formBodega").reset();
  $('.text-danger').html("");
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');

  if (haserror == 1) {
    $('#newUpdateBodega').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }
});

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(55),
  /* template */
  __webpack_require__(56),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Bodegas.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Bodegas.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-238f427c", Component.options)
  } else {
    hotAPI.reload("data-v-238f427c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    // props: ['valor'],

    data: function data() {
        return {
            bodegalist: []
            //unvalor: '',

            /*idusuario:'',
            usuario: {
                id: '',
                email: '',
                unvalor:''
            }*/
        };
    },

    created: function created() {

        //this.formtearFechas();
        this.getBodegas();
    },
    mounted: function mounted() {
        /*const vm = this;
        //this.formtearFechas();
        //this.getBodegas();
        //this.listaComputada();
        //console.log(this.results[1].name) ;
        console.log("Ahora Bodega list mounted");
        //console.log(vm.bodegalist);
        console.log(vm.$el.textContent)*/
        //this.getBodegas();
    },
    methods: {
        getBodegas: function getBodegas() {
            var _this = this;

            var urlBodegas = 'bodegasResource';
            axios.get(urlBodegas).then(function (response) {
                _this.bodegalist = response.data;
                setTimeout(function () {
                    $('#tablaBodegas').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                    //console.log("CARGODT");
                }, 1000);
            });
        },

        viewBodega: function viewBodega(idBodega) {

            this.$emit('view-bodega', idBodega);
        } // fin de la funcion viewUser*/

    }
});

/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.bodegalist.length === 0) ? _c('li', [_vm._v("Sin Bodegas registradas")]) : _vm._e(), _vm._v(" "), (_vm.bodegalist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaBodegas"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.bodegalist), function(bodega) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(bodega.nombre))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(bodega.habilitado))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(bodega.created_at))]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary btn-xs",
      attrs: {
        "id": "btnNuevaBodega",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateBodega"
      },
      on: {
        "click": function($event) {
          _vm.viewBodega(bodega.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Nombre")]), _vm._v(" "), _c('th', [_vm._v("Habilitada")]), _vm._v(" "), _c('th', [_vm._v("Creada")]), _vm._v(" "), _c('th', [_vm._v("Accion")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-238f427c", module.exports)
  }
}

/***/ })

/******/ });