/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 87);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(88);


/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Liquidadores_vue__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Liquidadores_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Liquidadores_vue__);


var appLiquidadorComp = new Vue({
  components: {
    'listaliquidadores': __WEBPACK_IMPORTED_MODULE_0__components_Liquidadores_vue___default.a

  },
  el: '#listado_liquidadores',
  data: {
    liquidadorData: ''
  },
  methods: {
    actualizarLiquidador: function actualizarLiquidador(idliquidador) {
      $('#titulomodal').html('ACTUALIZAR LIQUIDADOR');
      $('#accion').val('ACTUALIZAR LIQUIDADOR');
      $('#btnEnviar').html('ACTUALIZAR LIQUIDADOR');
    },
    getLiquidadorData: function getLiquidadorData(idliquidador) {
      var vm = this;
      $('#titulomodal').html('ACTUALIZAR LIQUIDADOR');
      $('#accion').val('ACTUALIZAR LIQUIDADOR');
      $('#idMarca').val(idliquidador);
      $('#btnEnviar').html('ACTUALIZAR LIQUIDADOR');
      var urlLiquidador = 'liquidadoresResource/' + idliquidador;
      axios.get(urlLiquidador).then(function (response) {
        vm.liquidadorData = response.data;
        $('#idliquidador').val(vm.liquidadorData.id);
        $('#liquidador').val(vm.liquidadorData.liquidador);
        $('#compania').val(vm.liquidadorData.compania);
      });
      //console.log(vm.datamarcas)
    },
    eliminarLiquidador: function eliminarLiquidador(idliquidador) {
      //var idliquidador = $('#idliquidador').val();

      var urlLiquidador = 'liquidadoresResource/' + idliquidador;
      axios.delete(urlLiquidador).then(function (response) {
        swal('Eliminar', 'Liquidador eliminado', 'success');
        $('#btneliminar').closest('tr').remove();
      });
    }
  }
});

var appLiquidadorModal = new Vue({

  el: '#capaModal',
  data: {
    marcaData: '',
    companiaList: ''
  },
  created: function created() {
    this.getCompania();
  },
  methods: {
    getCompania: function getCompania() {
      var _this = this;

      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this.companiaList = response.data;
      });
    },
    crearLiquidador: function crearLiquidador() {
      $('#titulomodal').html('CREAR LIQUIDADOR');
      $('#accion').val('CREAR LIQUIDADOR');
      $('#btnEnviar').html('CREAR LIQUIDADOR');
      $("#_method").val("POST");
    },

    enviarFormulario: function enviarFormulario() {
      if ($('#accion').val() == 'CREAR LIQUIDADOR') {
        this.$refs.formLiquidador._method.value = "POST";
        $("#formLiquidador").submit();
      } else {
        this.$refs.formLiquidador._method.value = "PATCH";
        this.$refs.formLiquidador.action = "/liquidadoresResource/" + $('#idliquidador').val();
        $("#formLiquidador").submit();
      }
    }

  }
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateLiquidador").on("hidden.bs.modal", function () {
  document.getElementById("formLiquidador").reset();
  $('.text-danger').html("");
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');

  if (haserror == 1) {
    $('#newUpdateLiquidador').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }
});

/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(90),
  /* template */
  __webpack_require__(91),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Liquidadores.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Liquidadores.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-74478b89", Component.options)
  } else {
    hotAPI.reload("data-v-74478b89", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    //props: ['valor'],
    data: function data() {
        return {
            liquidadoresList: [],
            idliquidador: ''

        };
    },

    created: function created() {
        this.getLiquidadores();
    },

    methods: {
        getLiquidadores: function getLiquidadores() {
            var _this = this;

            var urlLiquidadores = 'liquidadoresResource';
            axios.get(urlLiquidadores).then(function (response) {
                _this.liquidadoresList = response.data;
                setTimeout(function () {
                    $('#tablaLiquidadores').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },

        viewLiquidador: function viewLiquidador(idliquidador) {
            this.$emit('view-liquidador', idliquidador);
        }, // fin de la funcion viewLiquidador

        eliminarLiquidador: function eliminarLiquidador(idliquidador) {
            this.$emit('eliminar-liquidador', idliquidador);
        } // fin de la funcion viewLiquidador

    }
});

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.liquidadoresList.length === 0) ? _c('li', [_vm._v("Sin liquidadores registrados")]) : _vm._e(), _vm._v(" "), (_vm.liquidadoresList.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaLiquidadores"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.liquidadoresList), function(liquidador) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(liquidador.liquidador))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(liquidador.empresa))]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary",
      attrs: {
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateLiquidador"
      },
      on: {
        "click": function($event) {
          _vm.viewLiquidador(liquidador.id)
        }
      }
    }, [_vm._v("Editar")]), _vm._v(" "), _c('button', {
      staticClass: "btn btn-danger",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.eliminarLiquidador(liquidador.id)
        }
      }
    }, [_vm._v("Eliminar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Nombre")]), _vm._v(" "), _c('th', [_vm._v("Empresa")]), _vm._v(" "), _c('th', [_vm._v("Accion")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-74478b89", module.exports)
  }
}

/***/ })

/******/ });