/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 37);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(38);


/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Usuarios_vue__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Usuarios_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Usuarios_vue__);


var appUserComp = new Vue({
  components: {
    'listausuarios': __WEBPACK_IMPORTED_MODULE_0__components_Usuarios_vue___default.a

  },
  el: '#listado_user',
  data: {
    userData: ''
  },
  methods: {
    actualizarUsuario: function actualizarUsuario(iduser) {
      $('#titulomodal').html('ACTUALIZAR USUARIO');
      $('#accion').val('ACTUALIZAR USUARIO');
      $('#btnEnviarUser').html('ACTUALIZAR USUARIO');
    },
    getUserData: function getUserData(iduser) {
      var vm = this;
      $('#titulomodal').html('ACTUALIZAR USUARIO');
      $('#accion').val('ACTUALIZAR USUARIO');
      $('#idusuario').val(iduser);
      $('#btnEnviarUser').html('ACTUALIZAR USUARIO');
      var urlUsuario = 'userFullData/' + iduser;
      axios.get(urlUsuario).then(function (response) {
        vm.userData = response.data[0];
      });
      setTimeout(function () {
        $('#idusuario').val(iduser);
        $('#nombre').val(vm.userData.nombre);
        $('#fechanac').val(vm.userData.fechanac);
        $('#fono1').val(vm.userData.fono1);
        $('#fono2').val(vm.userData.fono2);
        $('#apellidos').val(vm.userData.apellido);
        $('#email').val(vm.userData.email);
        $('#emailalt').val(vm.userData.emailalt);
        $("#sexo").val(vm.userData.sexo);
        $("#compania").val(vm.userData.compania);
        $("#perfil").val(vm.userData.tipo);
        $("#estado").val(vm.userData.estado);
      }, 500);
    }
  }
});

var appUserModal = new Vue({

  el: '#capaModal',
  data: {
    companiaList: ''
  },
  created: function created() {
    this.getCompania();
  },
  methods: {
    getCompania: function getCompania() {
      var _this = this;

      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this.companiaList = response.data;
      });
    },
    crearUsuario: function crearUsuario() {
      $('#titulomodal').html('CREAR USUARIO');
      $('#accion').val('CREAR USUARIO');
      $('#btnEnviarUser').html('CREAR USUARIO');
      $("#_method").val("POST");
    },
    enviarFormulario: function enviarFormulario() {
      if ($('#accion').val() == 'CREAR USUARIO') {
        this.$refs.formUsuario._method.value = "POST";
        $("#formUsuario").submit();
      } else {
        this.$refs.formUsuario._method.value = "PATCH";
        this.$refs.formUsuario.action = "/adminusuarios/" + $('#idusuario').val();
        $("#formUsuario").submit();
      }
    }

  }
});

//Inicializa los datapicker
$('.datepicker').datepicker({
  format: "dd/mm/yyyy",
  startView: 2,
  endDate: hoy(),
  language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
if (haserror == 1) {
  $('#newUpdateUser').modal('show');
  $('#btnEnviarUser').html($('#accion').val());
}

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateUser").on("hidden.bs.modal", function () {
  document.getElementById("formUsuario").reset();
  $('.text-danger').html("");
});

;

/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(40),
  /* template */
  __webpack_require__(41),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Usuarios.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Usuarios.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7dc95044", Component.options)
  } else {
    hotAPI.reload("data-v-7dc95044", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['valor'],
    data: function data() {
        return {
            usuarioslist: [],
            idusuario: ''
            /*usuario: {
                id: '',
                email: '',
                unvalor:''
            }*/
        };
    },

    created: function created() {
        this.getUser();
    },

    methods: {
        getUser: function getUser() {
            var _this = this;

            var urlUsuarios = 'userFullDataAll';
            axios.get(urlUsuarios).then(function (response) {
                _this.usuarioslist = response.data;
                setTimeout(function () {
                    $('#tablaUsuarios').dataTable({
                        "language": {
                            "url": "js/datables.Spanish.json"
                        }
                    });
                    //console.log("CARGODT");
                }, 1000);
            });
        },

        viewUser: function viewUser(idUser, emailUser) {
            //document.getElementById("action").value = "editaUsuario";
            //document.getElementById("iduser").value = idUser;
            //appUser.ruta = 'new message'
            //this.$set(this.formdata, 'age', 27)
            this.$emit('view-user', idUser);
        } // fin de la funcion viewUser

    }
});

/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.usuarioslist.length === 0) ? _c('li', [_vm._v("Sin usuarios registrados")]) : _vm._e(), _vm._v(" "), (_vm.usuarioslist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaUsuarios"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.usuarioslist), function(usuario) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(usuario.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(usuario.email))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(usuario.tipo))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(usuario.companiadesc))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(usuario.estado))]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary btn-sm",
      attrs: {
        "id": "btnNuevoUsuario",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateUser"
      },
      on: {
        "click": function($event) {
          _vm.viewUser(usuario.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Nombre")]), _vm._v(" "), _c('th', [_vm._v("Email")]), _vm._v(" "), _c('th', [_vm._v("Tipo")]), _vm._v(" "), _c('th', [_vm._v("Empresa")]), _vm._v(" "), _c('th', [_vm._v("Estado")]), _vm._v(" "), _c('th', [_vm._v("Accion")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7dc95044", module.exports)
  }
}

/***/ })

/******/ });