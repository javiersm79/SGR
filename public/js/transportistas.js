/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 57);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 57:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(58);


/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Transportistas_vue__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Transportistas_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Transportistas_vue__);
//Vue.component('listausuarios', require('./components/Usuarios.vue'));

var appTranspComp = new Vue({
  components: {
    'listatransportistas': __WEBPACK_IMPORTED_MODULE_0__components_Transportistas_vue___default.a

  },
  el: '#listado_transportista',
  data: {
    transportistaData: ''
  },
  methods: {
    /*actualizarModelo: function (idmodelo) {
       $('#titulomodal').html('ACTUALIZAR MODELO'); 
       $('#accion').val('ACTUALIZAR MODELO'); 
       $('#btnEnviar').html( 'ACTUALIZAR MODELO'); 
    },*/
    getTransportistaData: function getTransportistaData(idtransportista) {
      var vm = this;
      //console.log(idmodelo)
      $('#titulomodal').html('ACTUALIZAR TRANSPORTISTA');
      $('#accion').val('ACTUALIZAR TRANSPORTISTA');
      $('#idModelo').val(idtransportista);
      $('#btnEnviar').html('ACTUALIZAR TRANSPORTISTA');
      var urlTransportista = 'transportistaResource/' + idtransportista;
      axios.get(urlTransportista).then(function (response) {
        vm.transportistaData = response.data;
        console.log(response.data);
        $('#idTransportista').val(idtransportista);
        $('#rutempresa').val(vm.transportistaData.rutempresa);
        $('#nombreempresa').val(vm.transportistaData.nombreempresa);
        $('#patentecamion').val(vm.transportistaData.patentecamion);
        $('#telefono').val(vm.transportistaData.telefono);
        $('#email').val(vm.transportistaData.email);
        $('#rutchofer').val(vm.transportistaData.rutchofer);
        $('#nombrechofer').val(vm.transportistaData.nombrechofer);
        $('#gruaexterna').val(vm.transportistaData.gruaexterna);
        $('#habilitado').val(vm.transportistaData.habilitado);
      });

      /*        setTimeout(function(){
                $('#idTransportista').val(idtransportista);
                $('#rutempresa').val(vm.transportistaData.rutempresa);
                $('#nombreempresa').val(vm.transportistaData.nombreempresa);
                $('#patentecamion').val(vm.transportistaData.patentecamion);
                $('#telefono').val(vm.transportistaData.telefono);
                $('#email').val(vm.transportistaData.email);
                $('#rutchofer').val(vm.transportistaData.rutchofer);
                $('#nombrechofer').val(vm.transportistaData.nombrechofer);
                $('#gruaexterna').val(vm.transportistaData.gruaexterna);
                $('#habilitado').val(vm.transportistaData.habilitado);
               
      
      
                }, 500);*/
    }
  }
});
var appTranspModal = new Vue({

  el: '#capaModal',
  data: {
    transportistaList: ''

  },
  created: function created() {
    // this.getMarcas();
    //this.getTipos();
  },
  methods: {
    crearTransportista: function crearTransportista() {
      $('#titulomodal').html('CREAR TRANSPORTISTA');
      $('#accion').val('CREAR TRANSPORTISTA');
      $('#btnEnviar').html('CREAR TRANSPORTISTA');
      $("#_method").val("POST");
    },

    enviarFormulario: function enviarFormulario() {

      if ($('#accion').val() == 'CREAR TRANSPORTISTA') {
        this.$refs.formTransportista._method.value = "POST";
        //alert("enviando el contacto nuevo")
        //console.log(this.$refs.formContacto._method)
        $("#formTransportista").submit();
      } else {
        this.$refs.formTransportista._method.value = "PATCH";
        this.$refs.formTransportista.action = "/transportistaResource/" + $('#idTransportista').val();
        $("#formTransportista").submit();
      }
    }

  }
});

$('.datepicker').datepicker({
  format: "dd/mm/yyyy",
  startView: 2,
  endDate: hoy(),
  language: "es"
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateTransportista").on("hidden.bs.modal", function () {
  document.getElementById("formTransportista").reset();
  //$('.text-danger').html("");
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');

  if (haserror == 1) {
    $('#newUpdateTransportista').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }
});

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(60),
  /* template */
  __webpack_require__(61),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Transportistas.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Transportistas.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-46fecdcc", Component.options)
  } else {
    hotAPI.reload("data-v-46fecdcc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            tranportistaslist: [],
            idtranportista: ''
            /*usuario: {
                id: '',
                email: '',
                unvalor:''
            }*/
        };
    },

    created: function created() {
        this.getTranportista();
    },

    methods: {
        getTranportista: function getTranportista() {
            var _this = this;

            var urlTranportista = 'transportistaResource';
            axios.get(urlTranportista).then(function (response) {
                _this.tranportistaslist = response.data;
                setTimeout(function () {
                    $('#tablaTranportistas').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                    //console.log("CARGODT");
                }, 1000);
            });
        },

        viewTranportista: function viewTranportista(idtranportista) {
            //document.getElementById("action").value = "editaUsuario";
            //document.getElementById("iduser").value = idUser;
            //appUser.ruta = 'new message'
            //this.$set(this.formdata, 'age', 27)
            this.$emit('view-transportista', idtranportista);
        } // fin de la funcion viewUser

    }
});

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.tranportistaslist.length === 0) ? _c('li', [_vm._v("Sin tranportistas registrados")]) : _vm._e(), _vm._v(" "), (_vm.tranportistaslist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaTranportistas"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.tranportistaslist), function(tranportista) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(tranportista.rutempresa))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.nombreempresa))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.patentecamion))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.telefono))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.rutchofer))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.nombrechofer))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tranportista.gruaexterna))]), _vm._v(" "), (tranportista.habilitado == 'SI') ? _c('td', [_c('label', {
      staticClass: "label label-success"
    }, [_vm._v(_vm._s(tranportista.habilitado))])]) : _c('td', [_c('label', {
      staticClass: "label label-danger"
    }, [_vm._v(_vm._s(tranportista.habilitado))])]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary",
      attrs: {
        "id": "btnNuevoTranportista",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateTransportista"
      },
      on: {
        "click": function($event) {
          _vm.viewTranportista(tranportista.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Rut")]), _vm._v(" "), _c('th', [_vm._v("Nombre")]), _vm._v(" "), _c('th', [_vm._v("Patente")]), _vm._v(" "), _c('th', [_vm._v("Telefono")]), _vm._v(" "), _c('th', [_vm._v("Rut Chofer")]), _vm._v(" "), _c('th', [_vm._v("Nombre Chofer")]), _vm._v(" "), _c('th', [_vm._v("Grua Ext.")]), _vm._v(" "), _c('th', [_vm._v("Habilitado")]), _vm._v(" "), _c('th', [_vm._v("Acciones")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-46fecdcc", module.exports)
  }
}

/***/ })

/******/ });