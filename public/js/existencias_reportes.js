/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 107);
/******/ })
/************************************************************************/
/******/ ({

/***/ 107:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(108);


/***/ }),

/***/ 108:
/***/ (function(module, exports) {

function getIngreso(idingreso) {

  var vm = this;
  $('#collapseTaller').collapse('show');
  $('#collapseVehiculos').collapse('show');
  $('#collapseImg').collapse('hide');
  $('#tbodyArchivos').html('');

  console.log(idingreso);
  $('#accion').val('ACTUALIZAR INGRESO');
  //$('#idingreso').val(idingreso);
  $('#btnEnviar').html('ACTUALIZAR INGRESO');
  $('#btnEnviarTraslado').html('ACTUALIZAR INGRESO');
  $(".collapseTab").removeClass('disabledTab');

  $(".tipoOR").html('');
  //$('#patente').val(vm.ingresoData.vehiculo.patente);


  //alert("getIngreso")
  //Obteniendo datos propios de la orden
  axios.get('/showFullDataIngreso/' + idingreso).then(function (response) {
    vm.ingresoData = response.data;

    //Seccion de la orden
    $('#titulomodal').html('ACTUALIZAR ORDEN - #' + vm.ingresoData.ingreso.nproceso);
    $("#nsiniestro").val(vm.ingresoData.ingreso.nsiniestro);
    $("#estado").select2("trigger", "select", {
      data: { id: vm.ingresoData.ingreso.estado }
    });
    //$("#estado").val(vm.ingresoData.ingreso.estado)
    $("#compania").select2("trigger", "select", {
      data: { id: vm.ingresoData.ingreso.compania }
    });

    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "/usuariosPorCompania/" + vm.ingresoData.ingreso.compania;
    axios.get(urlContactosEmpresa).then(function (response) {
      var contactos = response.data;
      $('#emailcontac').html('');
      $("#liquidador").html('');
      $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

      contactos.forEach(function (contacto, key, contactos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
      });

      $("#emailcontac").select2("trigger", "select", {
        data: { id: vm.ingresoData.ingreso.contacto }
      });
    });

    var urlLiquidadoresEmpresa = "/liquidadoresPorCompania/" + vm.ingresoData.ingreso.compania;
    axios.get(urlLiquidadoresEmpresa).then(function (response) {
      var liquidadores = response.data;
      $("#liquidador").html('');

      $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

      liquidadores.forEach(function (liquidador, key, contactos) {
        if (vm.ingresoData.ingreso.liquidador == liquidador.id) {
          $("#liquidador").append($("<option></option>").attr("value", liquidador.id).attr("selected", true).text(liquidador.liquidador));
        } else {
          $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
        }
      });
    });

    $("#vtraslado").val(formatearNumero(vm.ingresoData.ingreso.vtraslado));
    $("#nfacturaproveedor").val(vm.ingresoData.ingreso.nfacturaproveedor);
    $("#vfacturacia").val(formatearNumero(vm.ingresoData.ingreso.vfacturacia));
    $("#nfactura").val(vm.ingresoData.ingreso.nfactura);
    $("#tasacion").val(formatearNumero(vm.ingresoData.ingreso.tasacion));
    $("#vminremate").val(formatearNumero(vm.ingresoData.ingreso.vminremate));
    $("#vindem").val(formatearNumero(vm.ingresoData.ingreso.vindem));

    //Seccion del vehiculo
    $("#patente").val(vm.ingresoData.vehiculo.patente);

    $("#marca").select2("trigger", "select", {
      data: { id: vm.ingresoData.vehiculo.marcaid }
    });
    /*setTimeout(function(){
      $("#modelo").select2("trigger", "select", {
        data: { id: vm.ingresoData.vehiculo.modeloid }
      });
    }, 1000);*/

    $('#modelo').html('');
    var urlModelo = '/showFullDataByMarca/' + vm.ingresoData.vehiculo.marcaid;
    axios.get(urlModelo).then(function (response) {
      var modelos = response.data;
      $("#modelo").select2("trigger", "select", {
        data: { id: vm.ingresoData.vehiculo.modeloid }
      });
    });

    $("#anio").select2("trigger", "select", {
      data: { id: vm.ingresoData.vehiculo.anio }
    });
    $("#tipo").select2("trigger", "select", {
      data: { id: vm.ingresoData.vehiculo.tipoid }
    });
    $("#color").val(vm.ingresoData.vehiculo.color);

    $("#combustible").select2("trigger", "select", {
      data: { id: vm.ingresoData.vehiculo.combustible }
    });
    $("#nmotor").val(vm.ingresoData.vehiculo.nummotor);
    $("#nchasis").val(vm.ingresoData.vehiculo.numchasis);
    $("#vim").val(vm.ingresoData.vehiculo.VIM);
    $("#nserie").val(vm.ingresoData.vehiculo.serial);
    $("#nombreprop").val(vm.ingresoData.vehiculo.nombrepropietario);
    $("#rutprop").val(vm.ingresoData.vehiculo.rutpropietario);

    //Seccion del taller
    $("#idtaller").val(vm.ingresoData.taller.id);
    $("#nombretaller").val(vm.ingresoData.taller.nombre);
    $("#ruttaller").val(vm.ingresoData.taller.rut);
    $("#direcciontaller").val(vm.ingresoData.taller.direccion);
    $("#direcciontaller").val(vm.ingresoData.taller.direccion);
    $("#emailtaller").val(vm.ingresoData.taller.email);
    $("#nombcontactotaller").val(vm.ingresoData.taller.contacto);
    $("#telefonotaller").val(vm.ingresoData.taller.telefono);

    //Seccion de traslado
    if (vm.ingresoData.transportista) {
      $("#idtransportista").val(vm.ingresoData.transportista.id);
      $("#emailtransp").val(vm.ingresoData.transportista.email);
      $("#telefonotransp").val(vm.ingresoData.transportista.telefono);
      $("#ruttransp").val(vm.ingresoData.transportista.rutempresa);
      $("#nombtransp").val(vm.ingresoData.transportista.nombreempresa);
      $("#nombchofertransp").val(vm.ingresoData.transportista.nombrechofer);
      $("#gruaexternatransp").val(vm.ingresoData.transportista.gruaexterna);
      $("#patentecamiontransp").val(vm.ingresoData.transportista.patentecamion);
      $("#rutchofertransp").val(vm.ingresoData.transportista.rutchofer);
    }

    //Seccion de Bodega

    if (vm.ingresoData.ingreso.permiso == 'on') {
      $("#permiso").prop("checked", true);
    } else {
      $("#permiso").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.seguro == 'on') {
      $("#seguro").prop("checked", true);
    } else {
      $("#seguro").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.padron == 'on') {
      $("#padron").prop("checked", true);
    } else {
      $("#padron").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.revtec == 'on') {
      $("#revtec").prop("checked", true);
    } else {
      $("#revtec").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.gases == 'on') {
      $("#gases").prop("checked", true);
    } else {
      $("#gases").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.condicionado == 'on') {
      $("#condicionado").prop("checked", true);
    } else {
      $("#condicionado").prop("checked", false);
    }

    if (vm.ingresoData.ingreso.desarme == 'on') {
      $("#desarme").prop("checked", true);
    } else {
      $("#desarme").prop("checked", false);
    }

    $("#llaves").val(vm.ingresoData.ingreso.llaves);
    $("#placas").val(vm.ingresoData.ingreso.placas);
    $("#tag").val(vm.ingresoData.ingreso.tag);
    $("#computador").val(vm.ingresoData.ingreso.computador);
    $("#motorarranca").val(vm.ingresoData.ingreso.motorarranca);
    $("#airbag").val(vm.ingresoData.ingreso.airbag);
    /*var urlBodegas = 'bodegasResource';
    axios.get(urlBodegas).then(response => {
        this.bodegalist = response.data
    });*/
    $("#bodegas").val(vm.ingresoData.ingreso.bodegas);

    axios.get('/scanfolderGaleriaVehiculo/' + vm.ingresoData.vehiculo.patente).then(function (response) {
      console.log(response.data);
      $("#galeriaVehiculo").html(response.data);
      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('#galeriaVehiculo').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      });
    });

    //Seccion de archivos

    axios.get('/scanfolderArchivosIngreso/' + vm.ingresoData.ingreso.nproceso).then(function (response) {
      //vm.galeriaFotos = response.data  
      $('#tbodyArchivos').html('');
      $("#tbodyArchivos").append(response.data);
    });

    //Seccion de Observaciones

    axios.get('/showObservacionesExist/' + vm.ingresoData.ingreso.id).then(function (response) {
      //vm.galeriaFotos = response.data  
      $('#tbodyObervaciones').html('');
      $("#tbodyObervaciones").append(response.data);
    });

    //Seccion Historico
    axios.get('/showHistoricoExist/' + vm.ingresoData.ingreso.nproceso).then(function (response) {

      $('#operacioneshist').html('');
      $("#operacioneshist").append(response.data);
    });
  }); // Fin de recuperar datos de la orden

} //Fin de getIngreso


var appOrModal = new Vue({

  el: '#capaModal',
  data: {
    ordenesList: '',
    marcasList: '',
    modelosList: '',
    tiposList: '',
    talleresList: '',
    companiaList: '',
    bodegalist: '',
    vehiculo: ''

  },
  created: function created() {
    this.getMarcas();
    this.getTipos();
    this.getCompania();
    this.getTalleres();
    this.getBodegas();
  },
  methods: {
    crearIngreso: function crearIngreso() {
      $('#titulomodal').html('CREAR INGRESO');
      $('#accion').val('CREAR INGRESO');
      $('#btnEnviar').html('CREAR INGRESO');
      $("#_method").val("POST");
      $('.tipoOR').html("Ingreso Nuevo");
      $(".collapseTab").addClass('disabledTab');
      $("#galeriaVehiculo").html("");

      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('.nav-tabs a[href="#vehiculo"]').tab('show');
      $('.collapse').collapse('hide');
      setTimeout(function () {
        $('#collapseVehiculos').collapse('show');
      }, 500);
      $("#compania").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#marca").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#modelo").html("");
      $("#tipo").select2("trigger", "select", {
        data: { id: '0' }
      });
      $("#anio").select2("trigger", "select", {
        data: { id: new Date().getFullYear() + 1 }
      });
      $('#idingreso').val('');
    },
    crearPDF: function crearPDF() {
      var idingreso = $('#idingreso').val();
      window.location.replace("pdfor/" + idingreso + "/descargar");
    },

    enviarDatosBasicos: function enviarDatosBasicos() {
      var _this = this;

      var validardatos = 0;
      var validarpatentesiniestro = 0;
      $(".requerido").each(function (index) {
        if ($(this).val() == "" || $(this).val() == "0") {
          swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
          //Asigna 1 si hay un campo requerido vacio
          validardatos = 1;
        }
      });

      if (validardatos == 0) {
        var formData = new FormData();
        formData.append('patente', $("#patente").val());
        formData.append('nsiniestro', $("#nsiniestro").val());
        axios.post("/validarPatenteSiniestroExistencia/", formData).then(function (response) {
          validarpatentesiniestro = response.data;

          if (validarpatentesiniestro == 1 && $('#accion').val() == 'CREAR INGRESO') {
            swal('VALIDAR DATOS', 'PATENTE Y Nº SINIESTRO REGISTRADO', 'warning');
          } else {
            if ($('#accion').val() == 'CREAR INGRESO') {
              _this.$refs.formExist._method.value = "POST";
              //alert("enviando el contacto nuevo")
              //console.log("enviando datos basicos")
              var validardatos = 0;

              $("#formExist").submit();
            } else {
              _this.$refs.formExist._method.value = "PATCH";
              _this.$refs.formExist.action = "/existenciaResource/" + $('#idingreso').val();
              $("#formExist").submit();
            }
          }
        });
      } //fin if validardatos
    },
    enviarObservacion: function enviarObservacion() {
      var formData = new FormData();
      var nombrearchivo;
      var idingreso = $("#idingreso").val();
      var observ = $("#obsertxt").val();
      var privada = 0;
      if ($("#observprivada").is(":checked")) {
        alert('Seleccionado');
        privada = 1;
      }

      formData.append('idingreso', idingreso);
      formData.append('obsertxt', observ);
      formData.append('privada', privada);

      axios.post("/storeObservacionExist/" + idingreso, formData).then(function (response) {
        swal('Observación', 'Observación ingresada', 'success');

        axios.get('/showObservacionesExist/' + idingreso).then(function (response) {
          //vm.galeriaFotos = response.data  
          $('#tbodyObervaciones').html('');
          $("#tbodyObervaciones").append(response.data);
        });
      });
    },

    enviarTraslado: function enviarTraslado() {
      var formData = new FormData();
      var idingreso = $("#idingreso").val();
      var idtransportista = $("#idtransportista").val();

      formData.append('idingreso', idingreso);
      formData.append('idtransportista', idtransportista);

      axios.post("/updateTransportistaExist/", formData).then(function (response) {
        swal('Transportista', 'Transportista Actualizado', 'success');
      });
    },
    enviarEmailTraslado: function enviarEmailTraslado() {
      var formData = new FormData();
      var idingreso = $("#idingreso").val();
      var idtransportista = $("#idtransportista").val();

      if (idtransportista != '') {
        //this.enviarTraslado();
        formData.append('idingreso', idingreso);
        formData.append('idtransportista', idtransportista);
        swal('Enviando', 'Enviando Email...', 'warning');

        axios.post("/updateTransportista/", formData).then(function (response) {
          axios.post("/notificarTransportista/", formData).then(function (response) {
            swal('Transportista', 'Transportista notificado', 'success');
            axios.post("/orasignada/", formData);
          });
        });
      } else {
        swal('Validar', 'Debe seleccionar un transportista', 'warning');
      }
    },
    getMarcas: function getMarcas() {
      var _this2 = this;

      var urlMarcas = '/marcasResource';
      axios.get(urlMarcas).then(function (response) {
        _this2.marcasList = response.data;
      });
    },
    getTipos: function getTipos() {
      var _this3 = this;

      var urlTipos = '/getTipos';
      axios.get(urlTipos).then(function (response) {
        _this3.tiposList = response.data;
        //console.log(response.data)
      });
    },
    getCompania: function getCompania() {
      var _this4 = this;

      var urlCompania = '/companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this4.companiaList = response.data;
      });
    },
    getTalleres: function getTalleres() {
      var _this5 = this;

      var urlTaller = '/talleresResource';
      axios.get(urlTaller).then(function (response) {
        _this5.talleresList = response.data;
      });
    },
    getBodegas: function getBodegas() {
      var _this6 = this;

      var urlBodegas = '/bodegasResource';
      axios.get(urlBodegas).then(function (response) {
        _this6.bodegalist = response.data;
      });
    },
    getContacto: function getContacto(idcontacto) {
      var urlContacto = '/contactosResource/' + idcontacto;
      axios.get(urlContacto).then(function (response) {
        $('#emailcontac').val(response.data.email);
        $('#tlfcontacto').val(response.data.fono1);
        $('#nombcontacto').val(response.data.nombre + ' ' + response.data.apellido);
      });
    },

    buscarVehiculo: function buscarVehiculo() {
      var _this7 = this;

      //const vm = this
      patente = $('#patente').val();
      if (!patente) {
        swal('INGRESE UNA PATENTE', 'DATO REQUERIDO', 'warning');
      } else {
        //var urlVehiculo = 'buscarpatente/'+patente;
        var urlVehiculo = '/buscarPatenteTasacion/' + patente;
        axios.get(urlVehiculo).then(function (response) {
          _this7.vehiculo = response.data;
          //alert (response.data.lenght)
          if (_this7.vehiculo.length == 0) {
            swal('PATENTE NO REGISTRADA', '', 'error');
          } else {
            swal('PATENTE REGISTRADA', 'REGISTRADA EN TASACIONES', 'success');
            /*$("#idvehiculo").val(this.vehiculo[0].id)*/

            //$("#idvehiculo").val(this.vehiculo.id)

            $("#marca").select2("trigger", "select", {
              data: { id: _this7.vehiculo.marcaid }
            });
            $('#modelo').html('');

            //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
            var urlModelo = '/showFullDataByMarca/' + _this7.vehiculo.marcaid;
            axios.get(urlModelo).then(function (response) {
              var modelos = response.data;
              //console.log(modelos)
              $("#modelo").append($("<option></option>").attr("value", "0").text("Seleccione modelo"));

              modelos.forEach(function (modelo, key, modelos) {
                // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
                //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
                $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
              });

              $("#modelo").select2("trigger", "select", {
                data: { id: _this7.vehiculo.modeloid }
              });
            });

            $("#anio").select2("trigger", "select", {
              data: { id: _this7.vehiculo.anio }
            });
            $("#tipo").select2("trigger", "select", {
              data: { id: _this7.vehiculo.tipoid }
            });
          }

          //console.log(this.vehiculo[0].patente)

        });
      }
      this.vehiculo = '';
    }

  }
});

/*$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});*/

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateExistencias").on("hidden.bs.modal", function () {
  //document.getElementById("formExist").reset();

  $("form").trigger("reset");
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$('#importarCertificadoModal').on('shown.bs.modal', function (e) {
  $('#datosCertificados').focus();
});

$(document).ready(function () {
  getIngreso($('#idingreso').val());
  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');
  if (haserror == 1) {

    $('#btnEnviar').html($('#accion').val());
  }

  $('[data-fancybox="images"]').fancybox({});

  //Detecta si se ha creado una nueva OR y se seguira 
  /*if ($("#idingreso").val() != '')
  {
  
   
    setTimeout(function(){
      appExistencias.getIngreso($("#idingreso").val())
    }, 1500);
  }*/

  $('#combustible').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $("#marca").select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $("#modelo").select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#tipo').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#compania').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#estado').select2();

  $('select:not(.normal)').each(function () {
    $(this).select2({
      dropdownParent: $(this).parent()
    });
  });

  var anio = new Date().getFullYear() + 1;
  var i = 0;
  for (i = anio; i > 1969; i--) {
    // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
    $("#anio").append($("<option></option>").attr("value", i).text(i));
  };

  $('#anio').select2({
    dropdownParent: $("#newUpdateExistencias")
  });
  $('#emailcontac').select2({
    dropdownParent: $("#newUpdateExistencias")
  });

  var options = {
    url: "talleresResource",

    getValue: "direccion",

    list: {
      onClickEvent: function onClickEvent() {
        var nombretaller = $("#direcciontaller").getSelectedItemData().nombre;
        var ruttaller = $("#direcciontaller").getSelectedItemData().rut;
        var telefonotaller = $("#direcciontaller").getSelectedItemData().telefono;
        var emailtaller = $("#direcciontaller").getSelectedItemData().email;
        var contactotaller = $("#direcciontaller").getSelectedItemData().contacto;
        var idtaller = $("#direcciontaller").getSelectedItemData().id;

        $("#nombretaller").val(nombretaller).trigger("change");
        $("#ruttaller").val(ruttaller).trigger("change");
        $("#telefonotaller").val(telefonotaller).trigger("change");
        $("#emailtaller").val(emailtaller).trigger("change");
        $("#nombcontactotaller").val(contactotaller).trigger("change");
        $("#idtaller").val(idtaller).trigger("change");
      },
      match: {
        enabled: true
      }
    }
  };

  $("#direcciontaller").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Autocompletar del Transportista
  var options = {
    url: "transportistaResource",

    getValue: "nombrechofer",

    list: {
      onClickEvent: function onClickEvent() {

        var idtransportista = $("#nombchofertransp").getSelectedItemData().id;
        var ruttransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var telefonotransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
        var emailtransp = $("#nombchofertransp").getSelectedItemData().email;
        var nombreempresa = $("#nombchofertransp").getSelectedItemData().nombreempresa;
        var gruaexterna = $("#nombchofertransp").getSelectedItemData().gruaexterna;
        var patentecamiontransp = $("#nombchofertransp").getSelectedItemData().patentecamion;
        var rutchofertransp = $("#nombchofertransp").getSelectedItemData().rutchofer;
        $("#idtransportista").val(idtransportista).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#telefonotransp").val(telefonotransp).trigger("change");
        $("#ruttransp").val(ruttransp).trigger("change");
        $("#nombtransp").val(nombreempresa).trigger("change");
        $("#emailtransp").val(emailtransp).trigger("change");
        $("#gruaexternatransp").val(gruaexterna).trigger("change");
        $("#patentecamiontransp").val(patentecamiontransp).trigger("change");
        $("#rutchofertransp").val(rutchofertransp).trigger("change");
        $("#btnEnviarTraslado").prop("disabled", false);
        $("#btnEnviarEmailTraslado").prop("disabled", false);
      },
      match: {
        enabled: true
      }
    }
  };

  $("#nombchofertransp").easyAutocomplete(options);
  $('div.easy-autocomplete').removeAttr('style');

  //Evento que ocurre cuando se selecciona una marca para llenar los modelos
  $('#marca').on('select2:select', function (e) {
    $('#modelo').html('');
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlModelo = '/showFullDataByMarca/' + data.id;
    axios.get(urlModelo).then(function (response) {
      var modelos = response.data;
      //console.log(modelos)
      $("#modelo").append($("<option></option>").attr("value", "0").text("Seleccione modelo"));

      modelos.forEach(function (modelo, key, modelos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
      });
    });
  });

  //Evento que ocurre cuando se selecciona un email de contacto para llenar los modelos
  $('#emailcontac').on('select2:select', function (e) {
    //$('#modelo').html('')
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    $('#idcontacto').val(data.id);
  });

  $('#filtrarEstado').on('select2:select', function (e) {
    var data = e.params.data;
    $("#tablaExistencias").dataTable().api().search(data.text).draw();
  }); // Fin del evento change de estado


  //Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
  $('#compania').on('select2:select', function (e) {
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "/usuariosPorCompania/" + data.id;
    axios.get(urlContactosEmpresa).then(function (response) {
      var contactos = response.data;
      $('#emailcontac').html('');
      $("#liquidador").html('');
      $("#emailcontac").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

      contactos.forEach(function (contacto, key, contactos) {
        // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
        $("#emailcontac").append($("<option></option>").attr("value", contacto.id).text(contacto.email));
      });
    });

    var urlLiquidadoresEmpresa = "/liquidadoresPorCompania/" + data.id;
    axios.get(urlLiquidadoresEmpresa).then(function (response) {
      var liquidadores = response.data;

      $("#liquidador").append($("<option></option>").attr("value", "0").text("Seleccione liquidador"));

      liquidadores.forEach(function (liquidador, key, contactos) {
        $("#liquidador").append($("<option></option>").attr("value", liquidador.id).text(liquidador.liquidador));
      });
    });
  }); // Fin del evento change de compania


  $("#direcciontaller").focusin(function () {
    $("#nombretaller").val('');
    $("#ruttaller").val('');
    $("#telefonotaller").val('');
    $("#emailtaller").val('');
    $("#nombcontactotaller").val('');
    $("#idtaller").val('');
  });

  $("#nombchofertransp").focusin(function () {
    $("#idtransportista").val('');
    $("#ruttransp").val('');
    $("#nombtransp").val('');
    $("#patentecamiontransp").val('');
    $("#telefonotransp").val('');
    $("#emailtransp").val('');
    $("#rutchofertransp").val('');
    $("#btnEnviarTraslado").prop("disabled", true);
    $("#btnEnviarEmailTraslado").prop("disabled", true);
  });
}); // Fin del Document ready


window.eliminarArchivo = function (nproceso, nombrearchivo, idingreso) {
  axios.post('/eliminararchivoExist/' + nproceso + '/' + nombrearchivo + '/' + idingreso).then(function (response) {
    swal('Archivo Eliminado', 'Se eliminó el archivo: '.nombrearchivo, 'success');
  });
  //alert('#doc'+idarchivo)
  //document.getElementById("tablaarchivos").deleteRow(0);
  //$(this).closest('tr').remove();
};

$("#tablaarchivos").on('click', '.btnDelete', function () {
  $(this).closest('tr').remove();
});

//Funcion que comprueba el estado de la OR
window.eliminarFoto = function (patente, idfoto) {
  var galeriaFotos = void 0;

  axios.post('/eliminarfoto/' + $('#patente').val() + '/' + idfoto).then(function (response) {});

  galeriaFotos = axios.get('/scanfolderGaleriaVehiculo/' + $('#patente').val()).then(function (response) {
    return response.data;
  });

  //
  galeriaFotos.then(function (result) {
    swal('Imegen Eliminada', 'Se eliminó la imagen: '.idfoto, 'success');

    $("#galeriaVehiculo").html(result);
    $('#galeriaVehiculo').trigger('destroy.owl.carousel');
    $('#galeriaVehiculo').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    }); //Fin de funcion de inicializacion de OWN CORUSEL

  });

  $.fancybox.close(true);
};

$("#limpiarfiltro").click(function () {
  $("#tablaExistencias").dataTable().api().search("").draw();
}); //Fin click guardar observacion

$("#cargarArchivo").click(function () {

  var formData = new FormData();
  var nombrearchivo;
  var idingreso = $("#idingreso").val();
  var nproceso = $('#nproceso').val();
  var observ = $("#obserfile").val();

  formData.append('archivo', document.getElementById('archivoOR').files[0]);
  formData.append('idingreso', idingreso);
  formData.append('nproceso', nproceso);
  formData.append('observacion', observ);

  var archivocargado = axios.post('/subirarchivoExist', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {

    //return response.data;
    axios.get('/scanfolderArchivosIngreso/' + nproceso).then(function (response) {
      swal('Cargar Archivo', 'Archivo cargado', 'success');
      $('#tbodyArchivos').html('');
      $("#tbodyArchivos").append(response.data);
    });
  });
}); //Fin click carga de arechivo

$("#btnActBdg").click(function () {
  //let formData = new FormData();
  //let formData = document.getElementById("frmBodega");
  var myForm = document.getElementById('frmBodega');

  var formData = new FormData(myForm);
  //var nombrearchivo;
  var idingreso = $("#idingreso").val();
  var nproceso = $('#nproceso').val();

  formData.append('idingreso', idingreso);
  formData.append('nproceso', nproceso);
  axios.post('/updateBodega', formData).then(function (response) {
    swal('Actualizar Inventario', 'Datos de inventario actualizados', 'success');
    $("#resultado").html(response.data);
  });
}); //Fin click carga de actualizar datos de bodega

$("#btnImportarCert").click(function () {
  //alert("btnImportarCert");
  var inicio = 0;
  var fin = 0;

  var aLineas = document.getElementById("datosCertificados").value.split('\n');
  var datosCertificados = document.getElementById("datosCertificados").value;
  //alert("El textarea tiene " + aLineas.length + " líneas");

  //Importador del Dato Patente
  inicio = datosCertificados.indexOf("Inscripción : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var patente = datosCertificados.substring(inicio + 14, fin - 2);
  patente = patente.replace(".", "-");
  $("#patente").val(patente);

  //Importador del Dato Marca
  inicio = datosCertificados.indexOf("Marca : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var marcanombre = datosCertificados.substring(inicio + 8, fin);
  var resultMarca = appOrModal.marcasList.filter(function (marca) {
    return marca.nombre === marcanombre;
  });
  $("#marca").select2("trigger", "select", {
    data: { id: resultMarca[0].id }
  });

  //Importador del Dato Modelo
  inicio = datosCertificados.indexOf("Modelo : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var modelonombre = datosCertificados.substring(inicio + 9, fin);
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function () {
    var urlbuscarModeloId = '/buscarModeloPorNombre/' + modelonombre;
    axios.get(urlbuscarModeloId).then(function (response) {
      $("#modelo").select2("trigger", "select", {
        data: { id: response.data[0].id }
      });
    });
  }, 1500);

  //Importador del Dato Año
  inicio = datosCertificados.indexOf("Año : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var anio = datosCertificados.substring(inicio + 6, fin);
  $("#anio").val(anio);

  //Importador del Dato Color
  inicio = datosCertificados.indexOf("Color : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var color = datosCertificados.substring(inicio + 8, fin);
  $("#color").val(color);

  //Importador del Dato Combustible
  inicio = datosCertificados.indexOf("Combustible : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var combustible = datosCertificados.substring(inicio + 14, fin);
  $("#combustible").select2("trigger", "select", {
    data: { id: combustible }
  });

  //Importador del Dato TIPO
  inicio = datosCertificados.indexOf("Tipo Vehículo : ");
  fin = datosCertificados.indexOf(' Año : ');
  var nombretipo = datosCertificados.substring(inicio + 16, fin);

  var resultTipo = appOrModal.tiposList.filter(function (tipo) {
    return tipo.nombre === nombretipo;
  });
  //console.log(resultTipo)
  $("#tipo").select2("trigger", "select", {
    data: { id: resultTipo[0].id }
  });

  //Importador del Dato Nro. Motor 
  inicio = datosCertificados.indexOf("Nro. Motor : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nmotor = datosCertificados.substring(inicio + 13, fin);
  $("#nmotor").val(nmotor);

  //Importador del Dato Nro. Chasis
  inicio = datosCertificados.indexOf("Nro. Chasis : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nchasis = datosCertificados.substring(inicio + 14, fin);
  $("#nchasis").val(nchasis);

  //Importador del Dato Nro. Vin
  inicio = datosCertificados.indexOf("Nro. Vin : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);
  if (inicio < 0) {
    $("#vim").val('');
  } else {
    $("#vim").val(nvim);
    $("#nserie").val(nvim);
  }

  //Importador del Dato Nombre del propietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#nombreprop").val('');
  } else {
    inicio = datosCertificados.indexOf("Nombre : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var nombreprorp = datosCertificados.substring(inicio + 9, fin);
    $("#nombreprop").val(nombreprorp);
  }

  //Importador del Dato rut del prropietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  if (inicioseccion < 0) {
    $("#rutprop").val('');
  } else {
    inicio = datosCertificados.indexOf("R.U.N. : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var rutprop = datosCertificados.substring(inicio + 9, fin);
    $("#rutprop").val(rutprop);
  }

  inicio = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio + 11, fin);

  $('#importarCertificadoModal').modal('hide');
}); //Fin click importar datos del certificado


$("#btnsubirimg").click(function () {
  var formData = new FormData();

  for (var key in document.getElementById('imagenesVehiculo').files) {
    formData.append('fotosvehiculo[]', document.getElementById('imagenesVehiculo').files[key]);
  }
  formData.append('patente', $('#patente').val());
  formData.append('nproceso', $('#nproceso').val());

  axios.post('/subirfotosexist', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(function (response) {
    axios.get('/scanfolderGaleriaVehiculo/' + $('#patente').val()).then(function (response) {

      swal('Cargar Imagen', 'Imagen(es) Cargada(s)', 'success');
      $("#galeriaVehiculo").html(response.data);
      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('#galeriaVehiculo').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      }); //Fin de funcion de inicializacion de OWN CORUSEL

    }); // Fin de scanfolderGaleriaVehiculo

  }); // Fin de subirfotos


  //alert($("#galeriaVehiculo").html())
  $.fancybox.close(true);
}); //Fin click carga de archivos

/***/ })

/******/ });