/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 67);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(68);


/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Tasaciones_vue__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Tasaciones_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Tasaciones_vue__);

//Vue.component('listausuarios', require('./components/Usuarios.vue'));

var appTasacComp = new Vue({
  components: {
    'listatasaciones': __WEBPACK_IMPORTED_MODULE_0__components_Tasaciones_vue___default.a

  },
  el: '#listado_tasaciones',
  data: {
    tasacionesData: ''
  },
  methods: {
    getTasacionData: function getTasacionData(idtasacion) {
      var vm = this;
      $('#titulomodal').html('ACTUALIZAR TASACIÓN');
      $('#accion').val('ACTUALIZAR TASACIÓN');
      $('#btnEnviar').html('ACTUALIZAR TASACIÓN');
      $("#_method").val("POST");
      axios.get('tasacionesResource/' + idtasacion).then(function (response) {
        vm.tasacionesData = response.data;
        $("#compania").select2("trigger", "select", {
          data: { id: vm.tasacionesData.tasacion.compania }
        });
        $('#idtasacion').val(vm.tasacionesData.tasacion.id);
        $("#email").val(vm.tasacionesData.tasacion.email);
        $("#nombre").val(vm.tasacionesData.tasacion.nombre);
        $("#telefono").val(vm.tasacionesData.tasacion.telefono);
        $("#patente").val(vm.tasacionesData.tasacion.patente);
        $("#nsiniestro").val(vm.tasacionesData.tasacion.siniestro);
        $("#anio").val(vm.tasacionesData.tasacion.anio);
        $("#fecha").val(vm.tasacionesData.tasacion.fecha);
        $("#marca").select2("trigger", "select", {
          data: { id: vm.tasacionesData.tasacion.marcaid }
        });
        $("#valor").val(vm.tasacionesData.tasacion.valor);
      });

      //Seccion de la orden 
    }
  }
});

var appTasacionModal = new Vue({

  el: '#capaModal',
  data: {
    marcasList: '',
    tiposList: '',
    companiaList: ''
  },
  created: function created() {
    this.getMarcas();
    //this.getTipos();
    this.getCompania();
  },
  methods: {
    crearTasacion: function crearTasacion(event) {
      $('#titulomodal').html('CREAR TASACIÓN');
      $('#accion').val('CREAR TASACION');
      $('#btnEnviar').html('CREAR TASACIÓN');
      $("#_method").val("POST");
    },
    enviarDatos: function enviarDatos() {

      if ($('#accion').val() == 'CREAR TASACION') {
        this.$refs.formTasacion._method.value = "POST";
        //alert("enviando el contacto nuevo")
        //console.log("enviando datos basicos")
        var validardatos = 0;

        $(".requerido").each(function (index) {

          if ($(this).val() == "") {
            swal('FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS', 'VALIDAR CAMPOS REQUERIDOS', 'warning');
            //return

            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1;
          }
        });

        if (validardatos === 0) {
          $("#formTasacion").submit();
        }
      } else {
        //alert(this.$refs.formTasacion._method)
        this.$refs.formTasacion._method.value = "PATCH";
        this.$refs.formTasacion.action = "/tasacionesResource/" + $('#idtasacion').val();
        $("#formTasacion").submit();
      }
    },
    getMarcas: function getMarcas() {
      var _this = this;

      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(function (response) {
        _this.marcasList = response.data;
      });
    },
    getTipos: function getTipos() {
      var _this2 = this;

      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(function (response) {
        _this2.tiposList = response.data;
        //console.log(response.data)
      });
    },
    getCompania: function getCompania() {
      var _this3 = this;

      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(function (response) {
        _this3.companiaList = response.data;
      });
    }
  }
});

$('.datepicker').datepicker({
  format: "dd/mm/yyyy",
  startView: 2,
  endDate: hoy(),
  language: "es"
});
$(document).ready(function () {
  //$('#fcrear').val = hoy();
  $("#marca").select2({
    dropdownParent: $("#newUpdateTasacion"),
    tags: true,
    createTag: function createTag(params) {

      $('.select2-search__field').bind('keyup', function (e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13) {
          // ENTER ( TAB doesn't work with code==9, i'll looking for that later
          var formData = new FormData();
          formData.append('nombre', params.term);
          formData.append('habilitado', "SI");
          axios.post("/crearMarcaDinamico/", formData).then(function (response) {
            var newOption = new Option(params.term, response.data, true, true);
            $('#marca').append(newOption).trigger('change');
          });
        }
      });
    },
    maximumInputLength: 20, // only allow terms up to 20 characters long
    closeOnSelect: true
  });
  $("#modelo").select2({
    dropdownParent: $("#newUpdateTasacion"),
    tags: true,
    createTag: function createTag(params) {

      $('.select2-search__field').bind('keyup', function (e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13) {
          // ENTER ( TAB doesn't work with code==9, i'll looking for that later
          var formData = new FormData();
          formData.append('nombre', params.term);
          formData.append('marca', $("#marca").val());
          axios.post("/crearModeloDinamico/", formData).then(function (response) {
            var newOption = new Option(params.term, response.data, true, true);
            $('#modelo').append(newOption).trigger('change');
          });
        }
      });
    }
  });
  $('#tipo').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
  $('#compania').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
  $('#email').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
  var anio = new Date().getFullYear() + 1;
  var i = 0;
  for (i = anio; i > 1969; i--) {
    // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
    $("#anio").append($("<option></option>").attr("value", i).text(i));
  };

  $('#anio').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
});

//Evento que ocurre cuando se selecciona una marca para llenar los modelos
$('#marca').on('select2:select', function (e) {
  $('#modelo').html('');

  var data = e.params.data;
  //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
  //addopt(data.text.toUpperCase(), data.id)
  var urlModelo = 'showFullDataByMarca/' + data.id;
  axios.get(urlModelo).then(function (response) {
    var modelos = response.data;
    //console.log(modelos)
    $("#modelo").append($("<option></option>").attr("value", "").text("Seleccione modelo"));

    modelos.forEach(function (modelo, key, modelos) {
      // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
      //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
      $("#modelo").append($("<option></option>").attr("value", modelo.id).text(modelo.nombre));
    });

    /*         if(appTasacComp.tasacionesData.tasacion.modeloid)
            {
              $("#modelo").select2("trigger", "select", {
                  data: { id: appTasacComp.tasacionesData.tasacion.modeloid }
              });
            } */
    if (appTasacComp.tasacionesData.hasOwnProperty("tasacion")) {
      $("#modelo").select2("trigger", "select", {
        data: { id: appTasacComp.tasacionesData.tasacion.modeloid }
      });
    }
  });
}); // Fin del evento change de Marca

//Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
$('#compania').on('select2:select', function (e) {
  $('#email').html('');

  var data = e.params.data;
  //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
  var urlContactosEmpresa = "usuariosPorCompania/" + data.id;
  axios.get(urlContactosEmpresa).then(function (response) {
    var contactos = response.data;
    //console.log(modelos)
    $("#email").append($("<option></option>").attr("value", "0").text("Seleccione contacto"));

    contactos.forEach(function (contacto, key, contactos) {
      // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
      //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
      $("#email").append($("<option></option>").attr("nombrecontacto", contacto.nombre).attr("tlfncontacto", contacto.fono1).attr("value", contacto.email).text(contacto.email));
    });
  });
}); // Fin del evento change de compania

//Evento que ocurre cuando se selecciona un email de los contactos
$('#email').on('select2:select', function (e) {

  var data = e.params.data;
  $("#nombre").val(data.element.attributes.nombrecontacto.value);
  $("#telefono").val(data.element.attributes.tlfncontacto.value);
}); // Fin del evento change de compania

/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(70),
  /* template */
  __webpack_require__(71),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Tasaciones.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Tasaciones.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35f0d187", Component.options)
  } else {
    hotAPI.reload("data-v-35f0d187", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            tasacioneslist: [],
            idtasacion: ''
            /*usuario: {
                id: '',
                email: '',
                unvalor:''
            }*/
        };
    },

    created: function created() {
        this.getTasacion();
    },

    methods: {
        getTasacion: function getTasacion() {
            var _this = this;

            var urlTasacion = 'tasacionesResource';
            axios.get(urlTasacion).then(function (response) {
                _this.tasacioneslist = response.data;
                setTimeout(function () {
                    $('#tablaTasaciones').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                    //console.log("CARGODT");
                }, 1000);
            });
        },

        viewTasacion: function viewTasacion(idtasacion) {
            this.$emit('view-tasacion', idtasacion);
        } // fin de la funcion viewUser

    }
});

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.tasacioneslist.length === 0) ? _c('li', [_vm._v("Sin tasaciones registrados")]) : _vm._e(), _vm._v(" "), (_vm.tasacioneslist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaTasaciones"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.tasacioneslist), function(tasacion) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(tasacion.patente))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.nombrecompania))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.siniestro))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.nombremarca))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.nombremodelo))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.anio))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.valor))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(tasacion.fechafor))]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary",
      attrs: {
        "id": "btnNuevoTranportista",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateTasacion"
      },
      on: {
        "click": function($event) {
          _vm.viewTasacion(tasacion.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Patente")]), _vm._v(" "), _c('th', [_vm._v("Compañia")]), _vm._v(" "), _c('th', [_vm._v("Nº Siniestro")]), _vm._v(" "), _c('th', [_vm._v("Marca")]), _vm._v(" "), _c('th', [_vm._v("Modelo")]), _vm._v(" "), _c('th', [_vm._v("Año")]), _vm._v(" "), _c('th', [_vm._v("Valor")]), _vm._v(" "), _c('th', [_vm._v("Fecha")]), _vm._v(" "), _c('th', [_vm._v("Acciones")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-35f0d187", module.exports)
  }
}

/***/ })

/******/ });