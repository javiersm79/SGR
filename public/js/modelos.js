/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 62);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(63);


/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Modelos_vue__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Modelos_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Modelos_vue__);

//Vue.component('listausuarios', require('./components/Usuarios.vue'));

var appModeloComp = new Vue({
  components: {
    'listamodelos': __WEBPACK_IMPORTED_MODULE_0__components_Modelos_vue___default.a

  },
  el: '#listado_modelos',
  data: {
    modeloData: ''

  },
  methods: {
    /*actualizarModelo: function (idmodelo) {
       $('#titulomodal').html('ACTUALIZAR MODELO'); 
       $('#accion').val('ACTUALIZAR MODELO'); 
       $('#btnEnviar').html( 'ACTUALIZAR MODELO'); 
    },*/
    getModeloData: function getModeloData(idmodelo) {
      var vm = this;
      //console.log(idmodelo)
      $('#titulomodal').html('ACTUALIZAR MODELO');
      $('#accion').val('ACTUALIZAR MODELO');
      $('#idModelo').val(idmodelo);
      $('#btnEnviar').html('ACTUALIZAR MODELO');
      var urlModelo = 'modeloFullData/' + idmodelo;
      axios.get(urlModelo).then(function (response) {
        vm.modeloData = response.data[0];
        //console.log(response.data)
      });

      setTimeout(function () {
        $('#idModelo').val(idmodelo);
        $('#nombre').val(vm.modeloData.nombre);
        $('#marca').val(vm.modeloData.marca).trigger('change');
        //alert(vm.modeloData.marca);
        $('#tipo').val(vm.modeloData.tipo).trigger('change');
        //alert($('#tipo').val());
        //$('#habilitado').val(vm.marcaData.habilitado);

      }, 500);
    }
  }
});

var appModeloModal = new Vue({

  el: '#capaModal',
  data: {
    marcasList: '',
    tiposList: ''

  },
  created: function created() {
    this.getMarcas();
    this.getTipos();
  },
  methods: {
    getMarcas: function getMarcas() {
      var _this = this;

      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(function (response) {
        _this.marcasList = response.data;
      });
    },
    getTipos: function getTipos() {
      var _this2 = this;

      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(function (response) {
        _this2.tiposList = response.data;
        console.log(response.data);
      });
    },
    crearModelo: function crearModelo() {
      $('#titulomodal').html('CREAR MODELO');
      $('#accion').val('CREAR MODELO');
      $('#btnEnviar').html('CREAR MODELO');
      $("#_method").val("POST");
    },

    enviarFormulario: function enviarFormulario() {

      if ($('#accion').val() == 'CREAR MODELO') {
        this.$refs.formModelo._method.value = "POST";
        //alert("enviando el contacto nuevo")
        //console.log(this.$refs.formContacto._method)
        $("#formModelo").submit();
      } else {
        this.$refs.formModelo._method.value = "PATCH";
        this.$refs.formModelo.action = "/modelosResource/" + $('#idModelo').val();
        $("#formModelo").submit();
      }
    }

  }
});

//Inicializa Plugin para selects
$('#marca').select2();
$('#tipo').select2();
//$('#fcrear').val = hoy();


//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateModelo").on("hidden.bs.modal", function () {
  document.getElementById("formModelo").reset();
  $('.text-danger').html("");
});

$(document).ready(function () {

  //Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
  var haserror;
  haserror = $('meta[name=haserror]').attr('content');

  if (haserror == 1) {
    $('#newUpdateModelo').modal('show');
    $('#btnEnviar').html($('#accion').val());
  }
});

/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(65),
  /* template */
  __webpack_require__(66),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "D:\\xampp\\htdocs\\sive\\resources\\assets\\js\\components\\Modelos.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modelos.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-746f0e04", Component.options)
  } else {
    hotAPI.reload("data-v-746f0e04", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            modeloslist: [],
            idmodelo: ''
            /*usuario: {
                id: '',
                email: '',
                unvalor:''
            }*/
        };
    },

    created: function created() {
        this.getModelo();
    },

    methods: {
        getModelo: function getModelo() {
            var _this = this;

            var urlModelos = 'modeloFullDataAll';
            axios.get(urlModelos).then(function (response) {
                _this.modeloslist = response.data;
                setTimeout(function () {
                    $('#tablaModelos').dataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                }, 1000);
            });
        },

        viewModelo: function viewModelo(idmodelo) {
            //document.getElementById("action").value = "editaUsuario";
            //document.getElementById("iduser").value = idUser;
            //appUser.ruta = 'new message'
            //this.$set(this.formdata, 'age', 27)
            this.$emit('view-modelo', idmodelo);
        } // fin de la funcion viewUser

    }
});

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [(_vm.modeloslist.length === 0) ? _c('li', [_vm._v("Sin Modelos registrados")]) : _vm._e(), _vm._v(" "), (_vm.modeloslist.length > 0) ? _c('table', {
    staticClass: "table table-striped table-hover",
    attrs: {
      "id": "tablaModelos"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.modeloslist), function(modelo) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(modelo.marcadesc))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(modelo.nombre))]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-primary",
      attrs: {
        "id": "btnEditar",
        "type": "button",
        "data-toggle": "modal",
        "data-target": "#newUpdateModelo"
      },
      on: {
        "click": function($event) {
          _vm.viewModelo(modelo.id)
        }
      }
    }, [_vm._v("Editar")])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Marca")]), _vm._v(" "), _c('th', [_vm._v("Nombre")]), _vm._v(" "), _c('th', [_vm._v("Acción")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-746f0e04", module.exports)
  }
}

/***/ })

/******/ });