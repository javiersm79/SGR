<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    //return view('auth.login');
   if (Auth::check() ) {
    	return redirect()->to(Auth::user()->tipo);
	}
	else
	{
		return view('auth.login');
	}
});


Route::get('/home', 'HomeController@index')->name('home');


/*Route::get('administrador', function () {
    return view('administrador');
});*/

Route::get('socio', function () {
    return view('socio');
    //return view('socio')->with('msg', 'msgtester');
});

/*Route::get('administrador', ['middleware' => 'admin', function () {
    return view('administrador');
}]);*/
Route::get('administrador', ['middleware' => 'admin', function () {
    return view('administrador');
      }]);
    Route::get('/usuarios', function () {
        return view('administrador.usuarios');
    });
/*Route::group(['middleware' => 'admin'], function () {
    Route::get('/administrador', function ()    {
        return view('administrador');
    });

    Route::get('/usuarios', function () {
        return view('administrador.usuarios');
    });
});
*/
//Route::get('/usuarios', 'vueAxiosController@index')->name('usuarios');
Route::resource('adminusuarios', 'vueAxiosController', ['except' => 'show']);
/*
Route::prefix('api')->group(function() {
    Route::resource('index', 'vueAxiosController');
});*/

