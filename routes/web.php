<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    //return view('auth.login');
   if (Auth::check() ) {
        return redirect()->to(Auth::user()->tipo);
    }
    else
    {
        return view('auth.login');
    }
});



/*Route::resource('ordenretiroEmpresasRecursos', 'Empresas\ordenretiroEmpresasController');*/
Route::get('/archivotxt', 'HomeController@archivoTxt')->name('archivotxt');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{dato}', 'HomeController@homedata')->name('homedata');
Route::get('validaror/{nproceso}', 'ordenretiroController@validarRetiroOR');
//Route::get('validaror/{nproceso}', 'ordenretiroController@validarRetiroOR');



/*Route::get('email', function () {
    //$usuario = new App\User
    Mail::to('jjlion79@gmail.com')->send(new Testemail(Auth::user()));
    //return new Testemail(Auth::user());
    return "Mail enviado";
});*/


Route::get('orretirada', function () {
    return view('orretirada');
    //return view('socio')->with('msg', 'msgtester');
});
Route::get('Usuario', function () {
    return view('usuario');
    //return view('socio')->with('msg', 'msgtester');
});

Route::get('Supervisor', function () {
    return view('supervisor');
    //return view('socio')->with('msg', 'msgtester');
});

/*Route::get('administrador', ['middleware' => 'admin', function () {
    return view('administrador');
}]);*/

Route::group(['middleware' => 'admin'], function () {

     Route::get('/Administrador', function () {
        return view('administrador.dashboard');
    });

    Route::get('/usuarios', function ()    {
        return view('administrador.usuarios.main');
    });

    Route::get('/contactos', function ()    {
        return view('administrador.contactos.main');
    });

    Route::get('/clientes', function () {
        return view('administrador.clientes.main');
    });
   
    Route::get('/talleres', function () {
        return view('administrador.talleres.main');
    });   
   
    Route::get('/liquidadores', function () {
        return view('administrador.liquidadores.main');
    });    

    Route::get('/bodegas', function () {
        return view('administrador.bodegas.main');
    });
    Route::get('/transportistas', function () {
        return view('administrador.transportistas.main');
    });
    Route::get('/marcas', function () {
        return view('administrador.marcas.main');
    });
    Route::get('/modelos', function () {
        return view('administrador.modelos.main');
    });
    Route::get('/tasaciones', function () {
        return view('administrador.tasaciones.main');
    });
    Route::get('/ordretiros', function () {
        return view('administrador.ordretiros.main');
    });
    Route::get('/ordretirosguide', function () {
        return view('administrador.ordretiros');
    });
    Route::get('/existencias', function () {
        return view('administrador.existencias.main');
    });
    Route::get('/extest', function () {
        return view('administrador.existencias.formtest');
    });
    Route::get('/companias', function () {
        return view('administrador.companias.main');
    });
    Route::get('actanueva', function () {
        return view('administrador.actas.main');
    });
    Route::get('/listaractas', function () {
        return view('administrador.actas.veractas');
    });
    Route::get('/crearfacturatraslado', function () {
        return view('administrador.facturacion.proveedor.creartraslados');
    });
    Route::get('/facturastraslado', function () {
        return view('administrador.facturacion.proveedor.listatraslados');
    });
});

//Route::get('/usuarios', 'vueAxiosController@index')->name('usuarios');
Route::post('eliminarfoto/{patente}/{idfoto}', 'ordenretiroController@eliminarfoto');
Route::post('eliminararchivo/{nproceso}/{nombrearchivo}/{idarchivo}', 'ordenretiroController@eliminararchivo');
Route::post('eliminararchivoExist/{nproceso}/{nombrearchivo}/{idarchivo}', 'existenciasController@eliminararchivoExist');
Route::get('scanfolderGaleriaVehiculo/{patente}', 'ordenretiroController@scanfolderGaleriaVehiculo');
Route::get('scanfolderArchivosOR/{nproceso}', 'ordenretiroController@scanfolderArchivosOR');
Route::get('scanfolderArchivosIngreso/{nproceso}', 'existenciasController@scanfolderArchivosIngreso');
Route::post('subirfotosor', 'ordenretiroController@subirfotos');
Route::post('subirfotosexist', 'existenciasController@subirfotos');
Route::post('subirarchivo', 'ordenretiroController@subirarchivo');
Route::post('subirarchivoExist', 'existenciasController@subirarchivoExist');
Route::get('userFullData/{id}', 'usuariosController@showFullData');
Route::get('userFullDataAll', 'usuariosController@showFullDataAll');
Route::get('modeloFullData/{id}', 'modelosController@showFullData');
Route::get('modeloFullDataAll', 'modelosController@showFullDataAll');
Route::get('buscarModeloPorNombre/{nombremodelo}', 'modelosController@buscarModeloPorNombre');
Route::get('showFullDataByMarca/{idmarca}', 'modelosController@showFullDataByMarca');
Route::get('contactosFullDataAll', 'ContactosController@showFullDataAll');
Route::get('buscarContactoEmail/{email}', 'ContactosController@buscarContactoEmail');
Route::get('getTipos', 'modelosController@getTipos');
Route::resource('adminusuarios', 'usuariosController');
Route::resource('contactosResource', 'ContactosController');
Route::resource('bodegasResource', 'bodegaController');
Route::resource('transportistaResource', 'transportistaController');
Route::resource('marcasResource', 'marcasController');
Route::resource('modelosResource', 'modelosController');
Route::resource('tasacionesResource', 'tasacionesController');
Route::resource('companiaResource', 'CompaniaController');
Route::resource('talleresResource', 'talleresController');
Route::resource('ordenretiroResource', 'ordenretiroController');
Route::resource('existenciaResource', 'existenciasController');
Route::resource('actasResource', 'actasController');
Route::get('ordenretiroFullDataAll', 'ordenretiroController@showFullDataAll');
Route::get('generarexistencia/{idor}', 'ordenretiroController@generarexistencia');
Route::get('existenciasFullDataAll', 'existenciasController@showFullDataAll');
Route::get('existenciasFullDataAllActa', 'actasController@showFullDataAllActa');
Route::get('buscarpatente/{patente}', 'vehiculoController@buscarpatente');
Route::post('updateTrasladoOR/{id}', 'ordenretiroController@updateTrasladoOR');
Route::get('showFullDataOR/{idor}', 'ordenretiroController@showFullDataOR');
Route::get('showFullDataIngreso/{idingreso}', 'existenciasController@showFullDataIngreso');
Route::get('showFullDataIngresoNproceso/{nproceso}', 'existenciasController@showFullDataIngresoNproceso');
Route::get('buscarPatenteTasacion/{patente}', 'ordenretiroController@buscarPatenteTasacion');
Route::get('usuariosPorCompania/{compania}', 'ContactosController@usuariosPorCompania');
Route::get('liquidadoresPorCompania/{compania}', 'liquidadoresController@liquidadoresPorCompania');
Route::post('storeObservacion/{idordenretiro}', 'ordenretiroController@storeObservacion');
Route::post('storeObservacionExist/{idingreso}', 'existenciasController@storeObservacionExist');
Route::get('showObservaciones/{idordenretiro}', 'ordenretiroController@showObservaciones');
Route::get('showObservacionesExist/{idingreso}', 'existenciasController@showObservacionesExist');
Route::post('updateTransportista', 'ordenretiroController@updateTransportista');
Route::post('updateTransportistaExist', 'existenciasController@updateTransportistaExist');
Route::post('notificarTransportista', 'ordenretiroController@notificarTransportista');
Route::resource('liquidadoresResource', 'liquidadoresController');
Route::get('pdfor/{idordenretiro}/{peticion}', 'ordenretiroController@pdfOR');
Route::post('orasignada', 'ordenretiroController@orasignada');
Route::post('updateBodega', 'existenciasController@updateBodega');
Route::get('fichaingreso/{idingreso}', 'existenciasController@fichaingreso');
Route::get('ultimaacta', 'actasController@ultimaacta');
Route::get('acta/{numeroacta}', 'actasController@show');
Route::get('acta/pdfacta/{numeroacta}', 'actasController@pdfActa');
Route::get('acta/procesar/{numeroacta}', 'actasController@procesar');
Route::get('acta/lotes/{numeroacta}', 'actasController@asignarlotes');
Route::post('acta/lotes/guardar/{numeroacta}', 'actasController@guardarlotes');
Route::get('acta/imprimirlotes/{numeroacta}', 'actasController@imprimirlotes');
Route::get('acta/actamartillero/{numeroacta}', 'actasController@actamartillero');
Route::get('acta/revisionacta/{numeroacta}', 'actasController@revisionacta');
Route::get('acta/boletinremate/{numeroacta}', 'actasController@boletinremate');
Route::get('acta/planillacobranza/{numeroacta}', 'actasController@planillacobranza');
Route::get('acta/resumen/{numeroacta}', 'actasController@resumen');
Route::post('crearCliente', 'ClientesController@store');
Route::get('listaclientes', 'ClientesController@index');
//Route::get('listaclientes', 'ContactosController@listaclientes');
Route::get('verCliente/{idcliente}', 'ClientesController@show');
Route::post('actualizarCliente/{idcliente}', 'ClientesController@update');
Route::get('buscarDatosCliente/{rut}', 'ContactosController@buscarDatosCliente');
Route::get('buscarDatosTransportistas/{rut}', 'transportistaController@buscarDatosTransportistas');
Route::get('buscarDatosLote/{acta}/{lote}', 'actasController@buscarDatosLote');
Route::post('ingresarDatosLote', 'actasController@ingresarDatosLote');
Route::post('cerrarActa', 'actasController@cerrarActa');
Route::get('acta/pdfResumenActa/{numeroacta}', 'actasController@pdfResumenActa');
Route::resource('FacturaTrasladoResource', 'FacturaTrasladoController');
Route::get('facturastraslado/listado', 'FacturaTrasladoController@index');

Route::get('showHistorico/{nproceso}', 'ordenretiroController@showHistorico');
Route::get('showHistoricoExist/{nproceso}', 'existenciasController@showHistoricoExist');
Route::post('crearMarcaDinamico', 'marcasController@crearMarcaDinamico');
Route::post('crearModeloDinamico', 'modelosController@crearModeloDinamico');
Route::post('validarPatenteSiniestroOR', 'ordenretiroController@buscarPatenteVehiculoSiniestro');
Route::post('validarPatenteSiniestroExistencia', 'existenciasController@buscarPatenteVehiculoSiniestro');
Route::get('verExistencia/{nproceso}', 'existenciasController@verExistencia');