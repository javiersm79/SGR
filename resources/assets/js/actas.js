//Vue.component('listausuarios', require('./components/Usuarios.vue'));
import compExistencias from './components/ExistenciasDisponible.vue';
import compActas from './components/Actas.vue';
var appExistencias = new Vue({
components:{
        'listadoexistencias': compExistencias

    },
    el: '#listado_existencias',
    data: {
    },
    created() {
      
    },
  methods: {

    verFicha: function(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/"+idingreso)

    },
    getLastacta: function(idingreso) {

       
  
    },//Fin de getIngreso
    

    

  }//Fin de METHODS del componente
});


var appActas = new Vue({
components:{
        'listadoactas': compActas

    },
    el: '#lista_actas',
    data: {
    },
    created() {
      
    },
  methods: {

    verFicha: function(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/"+idingreso)

    },
    getDatoRemate: function(idingreso) {

       alert("Carga el acta")
  
    },//Fin de getIngreso
    

    

  }//Fin de METHODS del componente
});

var formIngresarData = new Vue({

    el: '#capaModal',
    data: {
      idcliente : 0,
      lote: '',
      acta: '',
      desarme: ''
    },
    created() {
      
    },
  methods: {

    verFicha: function(idingreso) {
      window.location.assign("http://localhost:1606/fichaingreso/"+idingreso)

    },
    ingresarDatos: function(idingreso) {

      var validardatos = 0
        $( ".requerido" ).each(function( index ) {
          if ($( this ).val() == "" || $( this ).val() == "0") {
            swal(
              'FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS',
              'VALIDAR CAMPOS REQUERIDOS',
              'warning'
            )
            //return

            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1

          }
          
        });

      if (validardatos === 0)
      {
        if ($('#accion').val() == 'CREAR ORDEN')
        {
          //this.$refs.formExist._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log("enviando datos basicos")
          var validardatos = 0
          alert("guardando datos")

          //$("#formExist").submit()

        }
        else
        {
          //this.$refs.formExist._method.value="PATCH";
          //this.$refs.formExist.action="/existenciaResource/"+$('#idingreso').val();
         //$("#formExist").submit();
        }
      }
  
    },//Fin de getIngreso

    buscarCliente: function() {
      let vm = this
      var rut = $("#rut").val();

      if (rut != "")
      {
        axios.get("/buscarDatosCliente/"+rut).then(response => {
          if (response.data != 0)
          { 
            $("#nombres").val(response.data.nombre);
            $("#apellidos").val(response.data.apellido);
            $("#telefono").val(response.data.fono1);
            $("#email").val(response.data.email);
            vm.idcliente = response.data.id
          }
          else
          {
            $("#buscarResult").html('RUT No Registrado');
            vm.idcliente = 0
          }

        
        });//FIN AXIOS GET


      }
      else
      {
        swal({
            title: "Validar datos",
            html: true,
            text: "Ingrese el <strong style='color:red'> RUT</strong> a buscar",
            type: "warning"
        }, function() {
            $("#rut").focus();
        });

      }
      

      
    },//Fin de buscarPersona
    

    

  }//Fin de METHODS del componente
});



$(document).ready(function () {

var numeroacta = axios.get('/ultimaacta').then(response => { 
    $('#nacta').val(response.data);
});

$('#tablaVehiculos').dataTable( {

    "language": {
        "url": "htps://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    dom: 'Bfrtip',    buttons: [
        'pdf',
        'excel'
    ]
} );             

$('#tablaProcesar').dataTable( {

    "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    paging: false,
} );             

$('#fecha').val(hoy());
}); // Fin del Document ready




$("#btnAsignarLotes").click(function(){

let formData = new FormData();
var numeroacta = $("#numeroacta").val();
//formData.append('test', nacta);
var str="";
var elem = document.getElementById('frmLotes').elements;
        for(var i = 0; i < elem.length; i++)
        {
          if (elem[i].type == "text") {
            formData.append(elem[i].name, elem[i].value);
            /*str += "<b>Type:</b>" + elem[i].type + "&nbsp&nbsp";
            str += "<b>Name:</b>" + elem[i].name + "&nbsp;&nbsp;";
            str += "<b>Value:</b><i>" + elem[i].value + "</i>&nbsp;&nbsp;";
            str += "<BR>";*/
            }
        } 
        //document.getElementById('lista_actas').innerHTML = str;
      axios.post("/acta/lotes/guardar/"+numeroacta, formData).then(response => {
        swal({
            title: "Lotes",
            text: "Lotes Asignados",
            type: "success"
        }, function() {
            location.reload();
        });
         //$("#lista_actas").html(response.data)

      });

}) //Fin click Crear Acta

$("#btnCrearActa").click(function(){
  let formData = new FormData();
  var nacta = $("#nacta").val()

  formData.append('nacta', nacta)

  axios.post("/actasResource/", formData).then(response => {
    swal({
        title: "Acta",
        text: "Acta creada",
        type: "success"
    }, function() {
        window.location = "listaractas";
    });
     //$("#capaModal").html(response.data)

  });

}) //Fin click Crear Acta

$("#btnCerrarActa").click(function(){
   swal({
      title: "Esta seguro de cerrar el acta?",
      text: "Este proceso no se puede reversar!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si, Carrar Acta",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false
    },
    function(){
      let formData = new FormData();
      var numeroacta = $("#numeroacta").val()
       formData.append('numeroacta', numeroacta)
        axios.post("/cerrarActa", formData).then(response => {
          swal({
              title: "Acta",
              text: "Acta cerrada exitosamente.",
              type: "success"
          }, function() {
              window.location = "/listaractas";
          });

        });
    });

}) //Fin click Crear Acta

window.ingresarDatosLote = function(){
  let formData = new FormData();
  var monto = $("#monto").val()
  
  formData.append('_token', $("input[name*='_token']").val())
  formData.append('lote', formIngresarData.lote)
  formData.append('acta', formIngresarData.acta)
  formData.append('idcliente', formIngresarData.idcliente)
  formData.append('nombre', $("#nombres").val())
  formData.append('apellido', $("#apellidos").val())
  formData.append('rut', $("#rut").val())
  formData.append('email', $("#email").val())
  formData.append('telefono', $("#telefono").val())
  formData.append('monto', monto)
  formData.append('tasacionfiscal', $("#tasacionfiscal").val())
  formData.append('garantia', $("#garantia").val())
  formData.append('desarme', formIngresarData.desarme)
/*
    if (formIngresarData.desarme)
    {
      alert("verdadero")
    }
    else
    {
      alert("falso")
    }*/

var validardatos = 0
        $( ".requerido" ).each(function( index ) {
          if ($( this ).val() == "" || $( this ).val() == "0") {
            swal(
              'FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS',
              'VALIDAR CAMPOS REQUERIDOS',
              'warning'
            )
            //return

            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1

          }
          
        });

      if (validardatos === 0)
      {
        axios.post("/ingresarDatosLote", formData).then(response => {
          swal({
              title: "Datos ingresados",
              text: "Datos de remate ingresados exitosamente",
              type: "success"
          }, function() {
            $("#monto"+formIngresarData.lote).html('$'+parseFloat($("#monto").val()).toLocaleString('es-CL'));
            $("#rut"+formIngresarData.lote).html($("#rut").val());
            $("#estado"+formIngresarData.lote).html("<strong>Ingresado</strong");
            $("#estado"+formIngresarData.lote).removeClass();
            $("#estado"+formIngresarData.lote).addClass("text-success");
          });
           //$("#capaModal").html(response.data)
          

        });
      }

} //Fin click Crear Acta

window.buscarDatosLote = function(lote,acta){
$("form").trigger("reset");
formIngresarData.lote = lote
formIngresarData.acta = acta
/*
let formData = new FormData();

  formData.append('lote', lote)
  formData.append('acta', acta)*/
  axios.get("/buscarDatosLote/"+acta+"/"+lote).then(response => {
    let datosIngreso = response.data[0]
    if (datosIngreso.cliente)
    {
      $("#nombres").val(datosIngreso.cliente.nombre)
      $("#apellidos").val(datosIngreso.cliente.apellido)
      $("#rut").val(datosIngreso.cliente.rut)
      $("#telefono").val(datosIngreso.cliente.fono1)
      $("#email").val(datosIngreso.cliente.email)
    }
    
    $("#monto").val(datosIngreso.monto)
    $("#garantia").val(datosIngreso.garantia)
    $("#tasacionfiscal").val(datosIngreso.tasacionfiscal)
    formIngresarData.idcliente = datosIngreso.idcliente
    if (datosIngreso.estadoingreso == "Ingresado")
    {
        $("#estado"+lote).removeClass();
        $("#estado"+lote).addClass("text-success");
    }
    else
    {
      $("#estado"+lote).addClass("text-danger"); 
    }
     //console.log(response.data)
  });
};

