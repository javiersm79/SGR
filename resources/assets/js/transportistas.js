//Vue.component('listausuarios', require('./components/Usuarios.vue'));
import compTransportistas from './components/Transportistas.vue';
var appTranspComp = new Vue({
components:{
        'listatransportistas': compTransportistas

    },
    el: '#listado_transportista',
    data: {
        transportistaData: ''
    },
  methods: {
    /*actualizarModelo: function (idmodelo) {
       $('#titulomodal').html('ACTUALIZAR MODELO'); 
       $('#accion').val('ACTUALIZAR MODELO'); 
       $('#btnEnviar').html( 'ACTUALIZAR MODELO'); 
    },*/
    getTransportistaData: function(idtransportista) {
       const vm = this;
       //console.log(idmodelo)
       $('#titulomodal').html('ACTUALIZAR TRANSPORTISTA'); 
       $('#accion').val('ACTUALIZAR TRANSPORTISTA'); 
       $('#idModelo').val(idtransportista); 
       $('#btnEnviar').html( 'ACTUALIZAR TRANSPORTISTA');
        var urlTransportista = 'transportistaResource/'+idtransportista;
        axios.get(urlTransportista).then(response => { 
            vm.transportistaData = response.data
            console.log(response.data)
          $('#idTransportista').val(idtransportista);
          $('#rutempresa').val(vm.transportistaData.rutempresa);
          $('#nombreempresa').val(vm.transportistaData.nombreempresa);
          $('#patentecamion').val(vm.transportistaData.patentecamion);
          $('#telefono').val(vm.transportistaData.telefono);
          $('#email').val(vm.transportistaData.email);
          $('#rutchofer').val(vm.transportistaData.rutchofer);
          $('#nombrechofer').val(vm.transportistaData.nombrechofer);
          $('#gruaexterna').val(vm.transportistaData.gruaexterna);
          $('#habilitado').val(vm.transportistaData.habilitado);
        });
               
/*        setTimeout(function(){
          $('#idTransportista').val(idtransportista);
          $('#rutempresa').val(vm.transportistaData.rutempresa);
          $('#nombreempresa').val(vm.transportistaData.nombreempresa);
          $('#patentecamion').val(vm.transportistaData.patentecamion);
          $('#telefono').val(vm.transportistaData.telefono);
          $('#email').val(vm.transportistaData.email);
          $('#rutchofer').val(vm.transportistaData.rutchofer);
          $('#nombrechofer').val(vm.transportistaData.nombrechofer);
          $('#gruaexterna').val(vm.transportistaData.gruaexterna);
          $('#habilitado').val(vm.transportistaData.habilitado);
         


          }, 500);*/
    },
  }
});
var appTranspModal = new Vue({

    el: '#capaModal',
    data: {
        transportistaList: '',

        },
  created: function() {
       // this.getMarcas();
        //this.getTipos();
        },
  methods: {
    crearTransportista: function() {
      $('#titulomodal').html('CREAR TRANSPORTISTA'); 
      $('#accion').val('CREAR TRANSPORTISTA'); 
      $('#btnEnviar').html('CREAR TRANSPORTISTA');
      $("#_method").val("POST");
   
    },
   
    enviarFormulario: function() {
      
        if ($('#accion').val() == 'CREAR TRANSPORTISTA')
        {
          this.$refs.formTransportista._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formTransportista").submit();
        }
        else
        {
          this.$refs.formTransportista._method.value="PATCH";
          this.$refs.formTransportista.action="/transportistaResource/"+$('#idTransportista').val();
         $("#formTransportista").submit();
        }
   
    },
   
  }
});

$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateTransportista").on("hidden.bs.modal", function () {
    document.getElementById("formTransportista").reset();
    //$('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateTransportista').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  

  
}
});
