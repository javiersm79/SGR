
import comptalleres from './components/Talleres.vue';
var appTallerComp = new Vue({
components:{
        'listatalleres': comptalleres

    },
    el: '#listado_talleres',
    data: {
        tallerData: ''
    },
  methods: {
    actualizarTaller: function (idtaller) {
       $('#titulomodal').html('ACTUALIZAR TALLER'); 
       $('#accion').val('ACTUALIZAR TALLER'); 
       $('#btnEnviarTaller').html( 'ACTUALIZAR TALLER'); 
    },
    getTallerData: function(idtaller) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR TALLER'); 
       $('#accion').val('ACTUALIZAR TALLER'); 
       $('#idTaller').val(idtaller); 
       $('#btnEnviarTaller').html( 'ACTUALIZAR TALLER');
        var urlTaller = 'talleresResource/'+idtaller;
        axios.get(urlTaller).then(response => { 
            vm.tallerData = response.data
        });
        setTimeout(function(){
          $('#regiones').val(vm.tallerData.region).change();
          $('#idTaller').val(idtaller);
          $('#nombre').val(vm.tallerData.nombre);
          $('#direccion').val(vm.tallerData.direccion);
          $('#rut').val(vm.tallerData.rut);
          $('#comunas').val(vm.tallerData.comuna);
          
          $('#ciudad').val(vm.tallerData.ciudad);
          $('#telefono').val(vm.tallerData.telefono);
          $('#email').val(vm.tallerData.email);
          $('#nombcontacto').val(vm.tallerData.contacto);
          //$('#habilitado').val(vm.tallerData.fechanac);


          }, 500);
    },
  }
});

var appTallerModal = new Vue({

    el: '#capaModal',
    data: {
        tallerList: ''
        },
  created: function() {
        //this.getCompania();
        },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearTaller: function() {
      $('#titulomodal').html('CREAR TALLER'); 
      $('#accion').val('CREAR TALLER'); 
      $('#btnEnviarTaller').html('CREAR TALLER');
      $("#_method").val("POST");
   
    },
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR TALLER')
        {
          this.$refs.formTaller._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formTaller").submit();
        }
        else
        {
          this.$refs.formTaller._method.value="PATCH";
          this.$refs.formTaller.action="/talleresResource/"+$('#idTaller').val();
         $("#formTaller").submit();
        }
   
    },
   
  }
});

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());



//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateTaller").on("hidden.bs.modal", function () {
    document.getElementById("formTaller").reset();
    $('.text-danger').html("");
});


var RegionesYcomunas = {

  "regiones": [{
      "NombreRegion": "Arica y Parinacota",
      "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
  },
    {
      "NombreRegion": "Tarapacá",
      "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
  },
    {
      "NombreRegion": "Antofagasta",
      "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
  },
    {
      "NombreRegion": "Atacama",
      "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
  },
    {
      "NombreRegion": "Coquimbo",
      "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
  },
    {
      "NombreRegion": "Valparaíso",
      "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
  },
    {
      "NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins",
      "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
  },
    {
      "NombreRegion": "Región del Maule",
      "comunas": ["Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
  },
    {
      "NombreRegion": "Región del Biobío",
      "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
  },
    {
      "NombreRegion": "Región de la Araucanía",
      "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria", ]
  },
    {
      "NombreRegion": "Región de Los Ríos",
      "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
  },
    {
      "NombreRegion": "Región de Los Lagos",
      "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
  },
    {
      "NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo",
      "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
  },
    {
      "NombreRegion": "Región de Magallanes y de la AntárVca Chilena",
      "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
  },
    {
      "NombreRegion": "Región Metropolitana de Santiago",
      "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
  }]
}


$(document).ready(function () {

  var iRegion = 0;
  var htmlRegion = '<option value="">Seleccione región</option>';
  var htmlComunas = '<option value="">Seleccione comuna</option>';

  $.each(RegionesYcomunas.regiones, function () {
    htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
    iRegion++;
  });

  $('#regiones').html(htmlRegion);
  $('#comunas').html(htmlComunas);

  $('#regiones').change(function () {
    var iRegiones = 0;
    var valorRegion = $(this).val();
    var htmlComuna = '<option value="">Seleccione comuna</option>';
    $.each(RegionesYcomunas.regiones, function () {
      if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
        var iComunas = 0;
        $.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
          htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
          iComunas++;
        });
      }
      iRegiones++;
    });
    $('#comunas').html(htmlComuna);
  });
/*  $('#comunas').change(function () {
    if ($(this).val() == 'sin-region') {
      alert('selecciones Región');
    } else if ($(this).val() == 'sin-comuna') {
      alert('selecciones Comuna');
    }
  });
  $('#regiones').change(function () {
    if ($(this).val() == 'sin-region') {
      alert('selecciones Región');
    }
  });*/
//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
var regiones = $('#regiones').data("regionesold");
var comunas = $('#comunas').data("comunasold");
if (haserror == 1){
  $('#regiones').val(regiones).change();
  $('#comunas').val(comunas);
  $('#newUpdateTaller').modal('show');
  $('#btnEnviarTaller').html( $('#accion').val());
  
  
  //setTimeout(function(){
         
         
  //alert(regiones)


          //}, 500);
  
}
});

