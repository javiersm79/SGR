
import compucontactos from './components/Contactos.vue';
var appUserComp = new Vue({
components:{
        'listacontactos': compucontactos

    },
    el: '#listado_contacto',
    data: {
        contactoData: ''
    },
  methods: {
    actualizarContacto: function (idcontacto) {
       $('#titulomodal').html('ACTUALIZAR CONTACTO'); 
       $('#accion').val('ACTUALIZAR CONTACTO'); 
       $('#btnEnviarContacto').html( 'ACTUALIZAR CONTACTO'); 
    },
    getContactoData: function(idcontacto) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR CONTACTO'); 
       $('#accion').val('ACTUALIZAR CONTACTO'); 
       $('#idcontacto').val(idcontacto); 
       $('#btnEnviarContacto').html( 'ACTUALIZAR CONTACTO');
        var urlContacto = 'contactosResource/'+idcontacto;
        axios.get(urlContacto).then(response => { 
            vm.contactoData = response.data
        });
        setTimeout(function(){
          $('#idcontacto').val(idcontacto);
          $('#nombre').val(vm.contactoData.nombre);
          $('#fechanac').val(vm.contactoData.fechanac);
          $('#fono1').val(vm.contactoData.fono1);
          $('#fono2').val(vm.contactoData.fono2);
          $('#apellidos').val(vm.contactoData.apellido);
          $('#email').val(vm.contactoData.email);
          $('#emailalt').val(vm.contactoData.emailalt);
          $("#sexo").val(vm.contactoData.sexo);
          $("#compania").val(vm.contactoData.compania);

          }, 500);
    },
  }
});

var appContactoModal = new Vue({

    el: '#capaModal',
    data: {
        companiaList: ''
        },
  created: function() {
        this.getCompania();
        },
  methods: {
    getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },
    crearContacto: function() {
      $('#titulomodal').html('CREAR CONTACTO'); 
      $('#accion').val('CREAR CONTACTO'); 
      $('#btnEnviarContacto').html('CREAR CONTACTO');
      $("#_method").val("POST");
   
    },
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR CONTACTO')
        {
          this.$refs.formContacto._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formContacto").submit();
        }
        else
        {
          this.$refs.formContacto._method.value="PATCH";
          this.$refs.formContacto.action="/contactosResource/"+$('#idcontacto').val();
         $("#formContacto").submit();
        }
   
    },
   
    },
   

});

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
if (haserror == 1){
  $('#newUpdateContacto').modal('show');
  $('#btnEnviarContacto').html( $('#accion').val());
}

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateContacto").on("hidden.bs.modal", function () {
    document.getElementById("formContacto").reset();
});


