
//Vue.component('listausuarios', require('./components/Usuarios.vue'));
import compModelos from './components/Modelos.vue';
var appModeloComp = new Vue({
components:{
        'listamodelos': compModelos

    },
    el: '#listado_modelos',
    data: {
        modeloData: ''
        
    },
  methods: {
    /*actualizarModelo: function (idmodelo) {
       $('#titulomodal').html('ACTUALIZAR MODELO'); 
       $('#accion').val('ACTUALIZAR MODELO'); 
       $('#btnEnviar').html( 'ACTUALIZAR MODELO'); 
    },*/
    getModeloData: function(idmodelo) {
       const vm = this;
       //console.log(idmodelo)
       $('#titulomodal').html('ACTUALIZAR MODELO'); 
       $('#accion').val('ACTUALIZAR MODELO'); 
       $('#idModelo').val(idmodelo); 
       $('#btnEnviar').html( 'ACTUALIZAR MODELO');
        var urlModelo = 'modeloFullData/'+idmodelo;
        axios.get(urlModelo).then(response => { 
            vm.modeloData = response.data[0]
            //console.log(response.data)
        });
        
        setTimeout(function(){
          $('#idModelo').val(idmodelo);
          $('#nombre').val(vm.modeloData.nombre);
          $('#marca').val(vm.modeloData.marca).trigger('change');
          //alert(vm.modeloData.marca);
          $('#tipo').val(vm.modeloData.tipo).trigger('change');
          //alert($('#tipo').val());
          //$('#habilitado').val(vm.marcaData.habilitado);


          }, 500);
    },
  }
});

var appModeloModal = new Vue({

    el: '#capaModal',
    data: {
        marcasList: '',
        tiposList: '',

        },
  created: function() {
        this.getMarcas();
        this.getTipos();
        },
  methods: {
    getMarcas: function() {
      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(response => {
          this.marcasList = response.data
      });
    },
    getTipos: function() {
      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(response => {
          this.tiposList = response.data
          console.log(response.data)
      });
    },
    crearModelo: function() {
      $('#titulomodal').html('CREAR MODELO'); 
      $('#accion').val('CREAR MODELO'); 
      $('#btnEnviar').html('CREAR MODELO');
      $("#_method").val("POST");
   
    },
   
    enviarFormulario: function() {
      
        if ($('#accion').val() == 'CREAR MODELO')
        {
          this.$refs.formModelo._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formModelo").submit();
        }
        else
        {
          this.$refs.formModelo._method.value="PATCH";
          this.$refs.formModelo.action="/modelosResource/"+$('#idModelo').val();
         $("#formModelo").submit();
        }
   
    },
   
  }
});

//Inicializa Plugin para selects
 $('#marca').select2();
 $('#tipo').select2();
//$('#fcrear').val = hoy();


//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateModelo").on("hidden.bs.modal", function () {
    document.getElementById("formModelo").reset();
    $('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateModelo').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  

  
}
});
