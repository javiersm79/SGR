
import compLiquidador from './components/Liquidadores.vue';
var appLiquidadorComp = new Vue({
components:{
        'listaliquidadores': compLiquidador

    },
    el: '#listado_liquidadores',
    data: {
        liquidadorData: ''
    },
  methods: {
    actualizarLiquidador: function (idliquidador) {
       $('#titulomodal').html('ACTUALIZAR LIQUIDADOR'); 
       $('#accion').val('ACTUALIZAR LIQUIDADOR'); 
       $('#btnEnviar').html( 'ACTUALIZAR LIQUIDADOR'); 
    },
    getLiquidadorData: function(idliquidador) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR LIQUIDADOR'); 
       $('#accion').val('ACTUALIZAR LIQUIDADOR'); 
       $('#idMarca').val(idliquidador); 
       $('#btnEnviar').html( 'ACTUALIZAR LIQUIDADOR');
        var urlLiquidador = 'liquidadoresResource/'+idliquidador;
        axios.get(urlLiquidador).then(response => { 
            vm.liquidadorData = response.data
            $('#idliquidador').val(vm.liquidadorData.id);
            $('#liquidador').val(vm.liquidadorData.liquidador);
            $('#compania').val(vm.liquidadorData.compania);
        });
        //console.log(vm.datamarcas)

    },
    eliminarLiquidador: function(idliquidador) {
      //var idliquidador = $('#idliquidador').val();

       var urlLiquidador = 'liquidadoresResource/'+idliquidador;
        axios.delete(urlLiquidador).then(response => {
          swal(
          'Eliminar',
          'Liquidador eliminado',
          'success'
            )
            $('#btneliminar').closest('tr').remove();
           
          });


    },
  }
});

var appLiquidadorModal = new Vue({

    el: '#capaModal',
    data: {
        marcaData: '',
        companiaList: '',
        },
  created: function() {
        this.getCompania();
        },
  methods: {
    getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },
    crearLiquidador: function() {
      $('#titulomodal').html('CREAR LIQUIDADOR'); 
      $('#accion').val('CREAR LIQUIDADOR'); 
      $('#btnEnviar').html('CREAR LIQUIDADOR');
      $("#_method").val("POST");
   
    },
    
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR LIQUIDADOR')
        {
          this.$refs.formLiquidador._method.value="POST";
          $("#formLiquidador").submit();
        }
        else
        {
          this.$refs.formLiquidador._method.value="PATCH";
          this.$refs.formLiquidador.action="/liquidadoresResource/"+$('#idliquidador').val();
         $("#formLiquidador").submit();
        }
   
    },
   
  }
});


//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateLiquidador").on("hidden.bs.modal", function () {
    document.getElementById("formLiquidador").reset();
    $('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateLiquidador').modal('show');
  $('#btnEnviar').html( $('#accion').val());

  
}




});

