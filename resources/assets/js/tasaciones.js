
//Vue.component('listausuarios', require('./components/Usuarios.vue'));
import compTasaciones from './components/Tasaciones.vue';
var appTasacComp = new Vue({
components:{
        'listatasaciones': compTasaciones

    },
    el: '#listado_tasaciones',
    data: {
        tasacionesData: ''
    },
  methods: {
    getTasacionData: function (idtasacion) {
      const vm = this;
      $('#titulomodal').html('ACTUALIZAR TASACIÓN'); 
      $('#accion').val('ACTUALIZAR TASACIÓN'); 
      $('#btnEnviar').html('ACTUALIZAR TASACIÓN');
      $("#_method").val("POST");
        axios.get('tasacionesResource/'+idtasacion).then(response => { 
          vm.tasacionesData = response.data
          $("#compania").select2("trigger", "select", {
          data: { id: vm.tasacionesData.tasacion.compania }
          });
          $('#idtasacion').val(vm.tasacionesData.tasacion.id)
          $("#email").val(vm.tasacionesData.tasacion.email)
          $("#nombre").val(vm.tasacionesData.tasacion.nombre)
          $("#telefono").val(vm.tasacionesData.tasacion.telefono)
          $("#patente").val(vm.tasacionesData.tasacion.patente)
          $("#nsiniestro").val(vm.tasacionesData.tasacion.siniestro)
          $("#anio").val(vm.tasacionesData.tasacion.anio)
          $("#fecha").val(vm.tasacionesData.tasacion.fecha)         
          $("#marca").select2("trigger", "select", {
          data: { id: vm.tasacionesData.tasacion.marcaid }
          })
          $("#valor").val(vm.tasacionesData.tasacion.valor)

        });


          //Seccion de la orden 
    }
  }
});

var appTasacionModal = new Vue({

    el: '#capaModal',
    data: {
      marcasList: '',
      tiposList: '',
    	companiaList: ''
  	},
  created: function() {
        this.getMarcas();
        //this.getTipos();
        this.getCompania();
        },
  methods: {
    crearTasacion: function (event) {
      $('#titulomodal').html('CREAR TASACIÓN'); 
      $('#accion').val('CREAR TASACION'); 
      $('#btnEnviar').html('CREAR TASACIÓN');
      $("#_method").val("POST");
    },
    enviarDatos: function() {
      
        if ($('#accion').val() == 'CREAR TASACION')
        {
          this.$refs.formTasacion._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log("enviando datos basicos")
          var validardatos = 0

        $( ".requerido" ).each(function( index ) {

          if ($( this ).val() == "") {
            swal(
              'FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS',
              'VALIDAR CAMPOS REQUERIDOS',
              'warning'
            )
            //return

            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1

          }
          
        });

          if (validardatos === 0)
          {
            $("#formTasacion").submit()

          }

        }
        else
        {
          //alert(this.$refs.formTasacion._method)
          this.$refs.formTasacion._method.value="PATCH";
          this.$refs.formTasacion.action="/tasacionesResource/"+$('#idtasacion').val();
         $("#formTasacion").submit();
        }
   
    },
    getMarcas: function() {
      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(response => {
          this.marcasList = response.data
      });
    },
    getTipos: function() {
      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(response => {
          this.tiposList = response.data
          //console.log(response.data)
      });
    },
    getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },
  }
});

$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});
$(document).ready(function () {
//$('#fcrear').val = hoy();
$("#marca").select2({
    dropdownParent: $("#newUpdateTasacion"),
    tags: true,
    createTag: function (params) {
      
      $('.select2-search__field').bind('keyup', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { // ENTER ( TAB doesn't work with code==9, i'll looking for that later
          let formData = new FormData();
          formData.append('nombre', params.term)
          formData.append('habilitado', "SI")
          axios.post("/crearMarcaDinamico/", formData).then(response => {
            var newOption = new Option(params.term, response.data, true, true);
            $('#marca').append(newOption).trigger('change');
          })

        }
      });
  },
  maximumInputLength: 20, // only allow terms up to 20 characters long
  closeOnSelect: true
  });
 $("#modelo").select2({
    dropdownParent: $("#newUpdateTasacion"),
   tags: true,
   createTag: function (params) {
     
     $('.select2-search__field').bind('keyup', function (e) {
       var code = (e.keyCode ? e.keyCode : e.which);
       if (code == 13) { // ENTER ( TAB doesn't work with code==9, i'll looking for that later
         let formData = new FormData();
         formData.append('nombre', params.term)
         formData.append('marca', $("#marca").val())
         axios.post("/crearModeloDinamico/", formData).then(response => {
           var newOption = new Option(params.term, response.data, true, true);
           $('#modelo').append(newOption).trigger('change');
         })
         
       }
     });
   },
  });
 $('#tipo').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
 $('#compania').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
 $('#email').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
 var anio = (new Date()).getFullYear() + 1;
var i = 0;
for (i = anio; i > 1969; i--) {
  // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
  $("#anio").append($("<option></option>").attr("value", i ).text(i ));
};

 $('#anio').select2({
    dropdownParent: $("#newUpdateTasacion")
  });
});

//Evento que ocurre cuando se selecciona una marca para llenar los modelos
$('#marca').on('select2:select', function (e) {
    $('#modelo').html('')
    
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
  //addopt(data.text.toUpperCase(), data.id)
    var urlModelo = 'showFullDataByMarca/'+data.id;
      axios.get(urlModelo).then(response => {
        var modelos = response.data;
        //console.log(modelos)
        $("#modelo").append($("<option></option>").attr("value", "" ).text("Seleccione modelo" ));

        modelos.forEach(function(modelo, key, modelos) {
         // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
          $("#modelo").append($("<option></option>").attr("value", modelo.id ).text(modelo.nombre ));
        
        });
        
/*         if(appTasacComp.tasacionesData.tasacion.modeloid)
        {
          $("#modelo").select2("trigger", "select", {
              data: { id: appTasacComp.tasacionesData.tasacion.modeloid }
          });
        } */
        if (appTasacComp.tasacionesData.hasOwnProperty("tasacion")) {
          $("#modelo").select2("trigger", "select", {
            data: { id: appTasacComp.tasacionesData.tasacion.modeloid }
          });
        }

      });
});// Fin del evento change de Marca

//Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
$('#compania').on('select2:select', function (e) {
    $('#email').html('')
    
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "usuariosPorCompania/"+data.id;
      axios.get(urlContactosEmpresa).then(response => {
        var contactos = response.data;
        //console.log(modelos)
        $("#email").append($("<option></option>").attr("value", "0" ).text("Seleccione contacto"));

        contactos.forEach(function(contacto, key, contactos) {
         // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
          $("#email").append($("<option></option>").attr("nombrecontacto", contacto.nombre ).attr("tlfncontacto", contacto.fono1 ).attr("value", contacto.email ).text(contacto.email ));
        
        });
          
      });


});// Fin del evento change de compania

//Evento que ocurre cuando se selecciona un email de los contactos
$('#email').on('select2:select', function (e) {
    
    var data = e.params.data;
    $("#nombre").val(data.element.attributes.nombrecontacto.value)
    $("#telefono").val(data.element.attributes.tlfncontacto.value)


});// Fin del evento change de compania