
import compBodegas from './components/Bodegas.vue';
var appBodegaComp = new Vue({
components:{
        'listabodegas': compBodegas

    },
    el: '#listado_bodegas',
    data: {
        bodegaData: ''
    },
  methods: {
    actualizarBodega: function (idbodega) {
       $('#titulomodal').html('ACTUALIZAR BODEGA'); 
       $('#accion').val('ACTUALIZAR BODEGA'); 
       $('#btnEnviar').html( 'ACTUALIZAR BODEGA'); 
    },
    getBodegaData: function(idbodega) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR BODEGA'); 
       $('#accion').val('ACTUALIZAR BODEGA'); 
       $('#idBodega').val(idbodega); 
       $('#btnEnviar').html( 'ACTUALIZAR BODEGA');
        var urlBodega = 'bodegasResource/'+idbodega;
        axios.get(urlBodega).then(response => { 
            vm.bodegaData = response.data
        });
        
        setTimeout(function(){
          $('#idBodega').val(idbodega);
          $('#nombre').val(vm.bodegaData.nombre);
          $('#habilitado').val(vm.bodegaData.habilitado);


          }, 500);
    },
  }
});

var appBodegaModal = new Vue({

    el: '#capaModal',
    data: {
        bodegaList: ''
        },
  created: function() {
        //this.getCompania();
        },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearBodega: function() {
      $('#titulomodal').html('CREAR BODEGA'); 
      $('#accion').val('CREAR BODEGA'); 
      $('#btnEnviar').html('CREAR BODEGA');
      $("#_method").val("POST");
   
    },
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR BODEGA')
        {
          this.$refs.formBodega._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formBodega").submit();
        }
        else
        {
          this.$refs.formBodega._method.value="PATCH";
          this.$refs.formBodega.action="/bodegasResource/"+$('#idBodega').val();
         $("#formBodega").submit();
        }
   
    },
   
  }
});

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());



//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateBodega").on("hidden.bs.modal", function () {
    document.getElementById("formBodega").reset();
    $('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateBodega').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  

  
}
});

