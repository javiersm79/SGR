
import compCompania from './components/Companias.vue';
var appMarcaComp = new Vue({
components:{
        'listacompanias': compCompania

    },
    el: '#listado_companias',
    data: {
        companiaData: ''
    },
  methods: {
    getCompaniaData: function(idcompania) {
      
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR COMPAÑIA'); 
       $('#accion').val('ACTUALIZAR COMPAÑIA'); 
       $('#idMarca').val(idcompania); 
       $('#btnEnviar').html( 'ACTUALIZAR COMPAÑIA');
        var urlCompania = 'companiaResource/'+idcompania;
        axios.get(urlCompania).then(response => { 
            vm.companiaData = response.data
            
            $('#idCompania').val(idcompania);
            $('#nombre').val(vm.companiaData.nombre);
            $('#habilitado').val(vm.companiaData.habilitado);
        });
    },
  }
});

var appMarcaModal = new Vue({

    el: '#capaModal',
    data: {
        compania_datos: ''
        },
  created: function() {
        //this.getCompania();
        },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearCompania: function() {
      $('#titulomodal').html('CREAR COMPAÑIA'); 
      $('#accion').val('CREAR COMPAÑIA'); 
      $('#btnEnviar').html('CREAR COMPAÑIA');
      $("#_method").val("POST");
   
    },
    
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR COMPAÑIA')
        {
          this.$refs.formCompania._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formCompania").submit();
        }
        else
        {
          this.$refs.formCompania._method.value="PATCH";
          this.$refs.formCompania.action="/companiaResource/"+$('#idCompania').val();
         $("#formCompania").submit();
        }
   
    },
   
  }
});



//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateCompania").on("hidden.bs.modal", function () {
    document.getElementById("formCompania").reset();
    $('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateCompania').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  

  
}
});

