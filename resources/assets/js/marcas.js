
import compMarcas from './components/Marcas.vue';
var appMarcaComp = new Vue({
components:{
        'listamarcas': compMarcas

    },
    el: '#listado_marcas',
    data: {
        marcaData: ''
    },
  methods: {
    actualizarMarca: function (idmarca) {
       $('#titulomodal').html('ACTUALIZAR MARCA'); 
       $('#accion').val('ACTUALIZAR MARCA'); 
       $('#btnEnviar').html( 'ACTUALIZAR MARCA'); 
    },
    getMarcaData: function(idmarca) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR MARCA'); 
       $('#accion').val('ACTUALIZAR MARCA'); 
       $('#idMarca').val(idmarca); 
       $('#btnEnviar').html( 'ACTUALIZAR MARCA');
        var urlMarca = 'marcasResource/'+idmarca;
        axios.get(urlMarca).then(response => { 
            vm.marcaData = response.data
        });
        //console.log(vm.datamarcas)
        setTimeout(function(){
          $('#idMarca').val(idmarca);
          $('#nombre').val(vm.marcaData.nombre);
          $('#habilitado').val(vm.marcaData.habilitado);


          }, 500);
    },
  }
});

var appMarcaModal = new Vue({

    el: '#capaModal',
    data: {
        marcaData: ''
        },
  created: function() {
        //this.getCompania();
        },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearMarca: function() {
      $('#titulomodal').html('CREAR MARCA'); 
      $('#accion').val('CREAR MARCA'); 
      $('#btnEnviar').html('CREAR MARCA');
      $("#_method").val("POST");
   
    },
    
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR MARCA')
        {
          this.$refs.formMarca._method.value="POST";
          //alert("enviando el contacto nuevo")
          //console.log(this.$refs.formContacto._method)
          $("#formMarca").submit();
        }
        else
        {
          this.$refs.formMarca._method.value="PATCH";
          this.$refs.formMarca.action="/marcasResource/"+$('#idMarca').val();
         $("#formMarca").submit();
        }
   
    },
   
  }
});

/*var appAnioModal = new Vue({

    el: '#anioModal',
    data: {
        anioslist: [ "2007", "2010", "2012" ],
        aniosSelect: []
        },

  created: function() {
        this.anioActual();
        },
  methods: {

    eliminarAnio: function(idanio) {
      alert(idanio)
   
    },
    anioActual: function(idanio) {
      const vm = this;
      var d = new Date();
      var anioActual = d.getFullYear();
      var i=anioActual - 40;

      while (i < anioActual) {
          vm.aniosSelect.push(i);
          i++;
      }

    },

    enviarFormulario: function() {

      this.$refs.formAnio._method.value="POST";
      $("#formAnio").submit();

    },
   
  }
});*/

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());



//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateMarca").on("hidden.bs.modal", function () {
    document.getElementById("formMarca").reset();
    $('.text-danger').html("");
});


$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');

if (haserror == 1){
  $('#newUpdateMarca').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  

  
}
});

