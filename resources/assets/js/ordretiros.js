//Vue.component('listausuarios', require('./components/Usuarios.vue'));
import compOrdRetiro from './components/OrdenesRetiro.vue';
var appOrdRetComp = new Vue({
components:{
        'listaordenesretiro': compOrdRetiro

    },
    el: '#listado_ordenesRetiro',
    data: {
        ordRetiroData: '',
        galeriaFotos: ''
    },
  methods: {

    getOrdRetData: function(idorden) {

       const vm = this;
        $('#collapseTaller').collapse('show') 
        $('#collapseVehiculos').collapse('show') 
        $('#collapseImg').collapse('hide')
        $('#tbodyArchivos').html('')


        $('#accion').val('ACTUALIZAR ORDEN'); 
        $('#idordenretiro').val(idorden); 
        $('#btnEnviar').html( 'ACTUALIZAR ORDEN');
        $('#btnEnviarTraslado').html( 'ACTUALIZAR ORDEN');
        $(".collapseTab").removeClass('disabledTab')

        $(".tipoOR").html('')
        //$('#patente').val(vm.ordRetiroData.vehiculo.patente);
        


      //Obteniendo datos propios de la orden
       axios.get('showFullDataOR/'+idorden).then(response => { 
          vm.ordRetiroData = response.data

          //Seccion de la orden
          $('#titulomodal').html('ACTUALIZAR ORDEN - #' +  vm.ordRetiroData.ordenretiro.nproceso);
          $("#nsiniestro").val(vm.ordRetiroData.ordenretiro.nsiniestro)
          //$("#estado").val(vm.ordRetiroData.ordenretiro.estado)
          $("#estado").select2("trigger", "select", {
            data: { id: vm.ordRetiroData.ordenretiro.estado }
          });
          $("#compania").select2("trigger", "select", {
          data: { id: vm.ordRetiroData.ordenretiro.compania }
          });

          //$('#emailcontac').html('')
          
        
          //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
          var urlContactosEmpresa = "usuariosPorCompania/"+vm.ordRetiroData.ordenretiro.compania;
            axios.get(urlContactosEmpresa).then(response => {
              var contactos = response.data;
              //console.log(contactos)
              $('#emailcontac').html('')
              $("#liquidador").html('')
              $("#emailcontac").append($("<option></option>").attr("value", "0" ).text("Seleccione contacto"));

              contactos.forEach(function(contacto, key, contactos) {
               // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
              //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
                $("#emailcontac").append($("<option></option>").attr("value", contacto.id ).text(contacto.email ));
              
              });

              $("#emailcontac").select2("trigger", "select", {
              data: { id: vm.ordRetiroData.ordenretiro.contacto }
              });
              
                
            });

            var urlLiquidadoresEmpresa = "liquidadoresPorCompania/"+vm.ordRetiroData.ordenretiro.compania;
              axios.get(urlLiquidadoresEmpresa).then(response => {
                var liquidadores = response.data;
                $("#liquidador").html('')
               
                $("#liquidador").append($("<option></option>").attr("value", "0" ).text("Seleccione liquidador"));

                liquidadores.forEach(function(liquidador, key, contactos) {
                  if (vm.ordRetiroData.ordenretiro.liquidador == liquidador.id)
                  {
                    $("#liquidador").append($("<option></option>").attr("value", liquidador.id ).attr("selected", true ).text(liquidador.liquidador ));
                    
                  }
                  else
                  {
                    $("#liquidador").append($("<option></option>").attr("value", liquidador.id ).text(liquidador.liquidador ));
                    
                  }
                  
                
                });
                  
              });

            $("#vtraslado").val(formatearNumero(vm.ordRetiroData.ordenretiro.vtraslado))
            $("#nfacturaproveedor").val(vm.ordRetiroData.ordenretiro.nfacturaproveedor)
             $("#vfacturacia").val(formatearNumero(vm.ordRetiroData.ordenretiro.vfacturacia))
            $("#nfactura").val(vm.ordRetiroData.ordenretiro.nfactura)
            $("#tasacion").val(formatearNumero(vm.ordRetiroData.ordenretiro.tasacion))
            $("#vminremate").val(formatearNumero(vm.ordRetiroData.ordenretiro.vminremate))
            $("#vindem").val(formatearNumero(vm.ordRetiroData.ordenretiro.vindem))






          //Seccion del vehiculo
          $("#patente").val(vm.ordRetiroData.vehiculo.patente)
            
           $("#marca").select2("trigger", "select", {
                data: { id: vm.ordRetiroData.vehiculo.marcaid }
            });
            /*setTimeout(function(){
              $("#modelo").select2("trigger", "select", {
                data: { id: vm.ordRetiroData.vehiculo.modeloid }
              });
            }, 1000);*/


            $('#modelo').html('')
            var urlModelo = 'showFullDataByMarca/'+vm.ordRetiroData.vehiculo.marcaid;
              axios.get(urlModelo).then(response => {
                var modelos = response.data;
                $("#modelo").select2("trigger", "select", {
                        data: { id: vm.ordRetiroData.vehiculo.modeloid }
                    });


              });

            $("#anio").select2("trigger", "select", {
                data: { id: vm.ordRetiroData.vehiculo.anio }
            });
            $("#tipo").select2("trigger", "select", {
                data: { id: vm.ordRetiroData.vehiculo.tipoid }
            });
            $("#color").val(vm.ordRetiroData.vehiculo.color)

            $("#combustible").select2("trigger", "select", {
                data: { id: vm.ordRetiroData.vehiculo.combustible }
            });
            $("#nmotor").val(vm.ordRetiroData.vehiculo.nummotor)
            $("#nchasis").val(vm.ordRetiroData.vehiculo.numchasis)
            $("#vim").val(vm.ordRetiroData.vehiculo.VIM)
            $("#nserie").val(vm.ordRetiroData.vehiculo.serial)
            $("#nombreprop").val(vm.ordRetiroData.vehiculo.nombrepropietario)
            $("#rutprop").val(vm.ordRetiroData.vehiculo.rutpropietario)
            
            //Seccion del taller
            $("#idtaller").val(vm.ordRetiroData.taller.id)
            $("#nombretaller").val(vm.ordRetiroData.taller.nombre)
            $("#ruttaller").val(vm.ordRetiroData.taller.rut)
            $("#direcciontaller").val(vm.ordRetiroData.taller.direccion)
            $("#direcciontaller").val(vm.ordRetiroData.taller.direccion)
            $("#emailtaller").val(vm.ordRetiroData.taller.email)
            $("#nombcontactotaller").val(vm.ordRetiroData.taller.contacto)
            $("#telefonotaller").val(vm.ordRetiroData.taller.telefono)

            //Seccion de traslado
            if (vm.ordRetiroData.transportista)
            {
              $("#idtransportista").val(vm.ordRetiroData.transportista.id)
              $("#emailtransp").val(vm.ordRetiroData.transportista.email)
              $("#telefonotransp").val(vm.ordRetiroData.transportista.telefono)
              $("#ruttransp").val(vm.ordRetiroData.transportista.rutempresa)
              $("#nombtransp").val(vm.ordRetiroData.transportista.nombreempresa)
              $("#nombchofertransp").val(vm.ordRetiroData.transportista.nombrechofer)
              $("#gruaexternatransp").val(vm.ordRetiroData.transportista.gruaexterna)
              $("#patentecamiontransp").val(vm.ordRetiroData.transportista.patentecamion)
              $("#rutchofertransp").val(vm.ordRetiroData.transportista.rutchofer)

            }
            


            axios.get('scanfolderGaleriaVehiculo/'+vm.ordRetiroData.vehiculo.patente).then(response => { 
                //vm.galeriaFotos = response.data  
                $("#galeriaVehiculo").html(response.data)
                $('#galeriaVehiculo').trigger('destroy.owl.carousel');
            $('#galeriaVehiculo').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })

            });


            //Seccion de archivos

            axios.get('scanfolderArchivosOR/'+vm.ordRetiroData.ordenretiro.nproceso).then(response => { 
            //vm.galeriaFotos = response.data  
            $('#tbodyArchivos').html('')
            $("#tbodyArchivos").append(response.data)

            });

            //Seccion de Observaciones

            axios.get('showObservaciones/'+vm.ordRetiroData.ordenretiro.id).then(response => { 
            //vm.galeriaFotos = response.data  
            $('#tbodyObervaciones').html('')
            $("#tbodyObervaciones").append(response.data)

            });

            //Seccion Historico
            axios.get('showHistorico/'+vm.ordRetiroData.ordenretiro.nproceso).then(response => { 

            $('#operacioneshist').html('')
            $("#operacioneshist").append(response.data)

            });





        }); // Fin de recuperar datos de la orden


  
    },//Fin de getOrdRetData
    

    

  }//Fin de METHODS del componente
});


var appOrModal = new Vue({

    el: '#capaModal',
    data: {
        ordenesList: '',
        marcasList: '',
        modelosList: '',
        tiposList: '',
        talleresList: '',
        companiaList: '',
        vehiculo: '',

        },
  created: function() {
        this.getMarcas();
        this.getTipos();
        this.getCompania();
        this.getTalleres();
        },
  methods: {
    crearOR: function() {
      $('#titulomodal').html('CREAR ORDEN'); 
      $('#accion').val('CREAR ORDEN'); 
      $('#btnEnviar').html('CREAR ORDEN');
      $("#_method").val("POST");
      $('.tipoOR').html("OR Nueva");
      $(".collapseTab").addClass('disabledTab')
      $("#galeriaVehiculo").html("")
      
      $('#galeriaVehiculo').trigger('destroy.owl.carousel');
      $('.nav-tabs a[href="#vehiculo"]').tab('show')
      $('.collapse').collapse('hide')
      setTimeout(function(){
        $('#collapseVehiculos').collapse('show')
      }, 500);
      $("#compania").select2("trigger", "select", {
                data: { id: '0' }
            });
      $("#marca").select2("trigger", "select", {
                data: { id: '0' }
            });
      $("#modelo").html("")
      $("#tipo").select2("trigger", "select", {
                data: { id: '0' }
            });
      $("#anio").select2("trigger", "select", {
                data: { id: ((new Date()).getFullYear() + 1) }
            });
      $('#idordenretiro').val('');
      

   
    },
    crearPDF: function() {
      var idorden=$('#idordenretiro').val(); 
      window.location.replace("pdfor/"+idorden+"/descargar");

    },
   
    enviarDatosBasicos: function() {
      
        var validardatos = 0
        var validarpatentesiniestro = 0
        $( ".requerido" ).each(function( index ) {
          if ($( this ).val() == "" || $( this ).val() == "0") {
            swal(
              'FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS',
              'VALIDAR CAMPOS REQUERIDOS',
              'warning'
            )
            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1

          }
          
        });

        if (validardatos == 0)
        {
          let formData = new FormData();
          formData.append('patente', $("#patente").val());
          formData.append('nsiniestro', $("#nsiniestro").val());
          axios.post("/validarPatenteSiniestroOR/", formData).then(response => {
            validarpatentesiniestro = response.data

            if (validarpatentesiniestro == 1 && $('#accion').val() == 'CREAR ORDEN') {
              swal(
                'VALIDAR DATOS',
                'PATENTE Y Nº SINIESTRO REGISTRADO',
                'warning'
              )

            }
            else {
              if ($('#accion').val() == 'CREAR ORDEN') {
                this.$refs.formOrdRet._method.value = "POST";
                //alert("enviando el contacto nuevo")
                //console.log("enviando datos basicos")
                var validardatos = 0

                $("#formOrdRet").submit()

              }
              else {
                this.$refs.formOrdRet._method.value = "PATCH";
                this.$refs.formOrdRet.action = "/ordenretiroResource/" + $('#idordenretiro').val();
                $("#formOrdRet").submit();
              }

            }
          })


          
        }//fin if validardatos
        
      

   
    },
    enviarObservacion: function() {
      let formData = new FormData();
      var nombrearchivo;
      var idordenretiro = $("#idordenretiro").val()
      var observ = $("#obsertxt").val()
      var privada = 0
      if( $("#observprivada").is(":checked") ) {
          //alert('Seleccionado');
          privada = 1

      }

      formData.append('idordenretiro', idordenretiro)
      formData.append('obsertxt', observ)
      formData.append('privada', privada)

      axios.post("/storeObservacion/"+idordenretiro, formData).then(response => {
         swal(
          'Observación',
          'Observación ingresada',
          'success'
          )

         axios.get('showObservaciones/'+idordenretiro).then(response => { 
            //vm.galeriaFotos = response.data  
            $('#tbodyObervaciones').html('')
            $("#tbodyObervaciones").append(response.data)

            });

      });



    },

    enviarTraslado: function() {
      let formData = new FormData();
      var idordenretiro = $("#idordenretiro").val()
      var idtransportista = $("#idtransportista").val()

      formData.append('idordenretiro', idordenretiro)
      formData.append('idtransportista', idtransportista)
      axios.post("/updateTransportista/", formData).then(response => {
         swal(
          'Transportista',
          'Transportista Actualizado',
          'success'
          )

      });

    },
    enviarEmailTraslado: function() {
      let formData = new FormData();
      var idordenretiro = $("#idordenretiro").val()
      var idtransportista = $("#idtransportista").val()

      if (idtransportista != '')
      {
        //this.enviarTraslado();
        formData.append('idordenretiro', idordenretiro)
        formData.append('idtransportista', idtransportista)
        swal(
        'Enviando',
        'Enviando Email...',
        'warning'
        )

        axios.post("/updateTransportista/", formData).then(response => {
          axios.post("/notificarTransportista/", formData).then(response => {
            swal(
            'Transportista',
            'Transportista notificado',
            'success'
            )
            axios.post("/orasignada/", formData)


          });

        });

        

      }
      else
      {
        swal(
          'Validar',
          'Debe seleccionar un transportista',
          'warning'
          )

      }

      

    },
    getMarcas: function() {
      var urlMarcas = 'marcasResource';
      axios.get(urlMarcas).then(response => {
          this.marcasList = response.data
      });
    },
    getTipos: function() {
      var urlTipos = 'getTipos';
      axios.get(urlTipos).then(response => {
          this.tiposList = response.data
          //console.log(response.data)
      });
    },
    getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },
    getTalleres: function() {
      var urlTaller = 'talleresResource';
      axios.get(urlTaller).then(response => {
          this.talleresList = response.data
      });
    },
    getContacto: function(idcontacto) {
      var urlContacto = 'contactosResource/'+idcontacto;
      axios.get(urlContacto).then(response => {
        $('#emailcontac').val(response.data.email);
        $('#tlfcontacto').val(response.data.fono1);
        $('#nombcontacto').val(response.data.nombre + ' ' + response.data.apellido);
          
      });
    },
    
    buscarVehiculo: function() {
      //const vm = this
      patente = $('#patente').val();
      if (!patente)
      {     
        swal(
          'INGRESE UNA PATENTE',
          'DATO REQUERIDO',
          'warning'
        )

      }
      else
      {
        //var urlVehiculo = 'buscarpatente/'+patente;
        var urlVehiculo = 'buscarPatenteTasacion/'+patente;
          axios.get(urlVehiculo).then(response => {
          this.vehiculo = response.data
          //alert (response.data.lenght)
          if (this.vehiculo.length == 0)
          {
            swal(
              'PATENTE NO REGISTRADA',
              '',
              'error'
            )
          }
          else
          {
            swal(
              'PATENTE REGISTRADA',
              'REGISTRADA EN TASACIONES',
              'success'
            )
            /*$("#idvehiculo").val(this.vehiculo[0].id)*/
            
            //$("#idvehiculo").val(this.vehiculo.id)

            $("#marca").select2("trigger", "select", {
                data: { id: this.vehiculo.marcaid }
            });
            $('#modelo').html('')

            //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
            var urlModelo = 'showFullDataByMarca/'+this.vehiculo.marcaid;
              axios.get(urlModelo).then(response => {
                var modelos = response.data;
                //console.log(modelos)

                modelos.forEach(function(modelo, key, modelos) {
                 // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
                //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
                  $("#modelo").append($("<option></option>").attr("value", modelo.id ).text(modelo.nombre ));
                });

                $("#modelo").select2("trigger", "select", {
                        data: { id: this.vehiculo.modeloid }
                    });
              });

            $("#anio").select2("trigger", "select", {
                data: { id: this.vehiculo.anio }
            });
            $("#tipo").select2("trigger", "select", {
                data: { id: this.vehiculo.tipoid }
            });
            $("#tasacion").val(this.vehiculo.valor)

          }
            
            

          //console.log(this.vehiculo[0].patente)


      });

      }
      this.vehiculo =''
      
    },
    
   
  }
});

/*$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});*/

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateOrdenRetiro").on("hidden.bs.modal", function () {
    //document.getElementById("formOrdRet").reset();

  $("form").trigger("reset");
  


    
});

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$('#importarCertificadoModal').on('shown.bs.modal', function (e) {
  $('#datosCertificados').focus()
  
})



$(document).ready(function () {

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
if (haserror == 1){
  $('#newUpdateOrdenRetiro').modal('show');
  $('#btnEnviar').html( $('#accion').val());
  
}

$( '[data-fancybox="images"]' ).fancybox({

});

//Detecta si se ha creado una nueva OR y se seguira 
if ($("#idordenretiro").val() != '')
{

  $('#newUpdateOrdenRetiro').modal('show');
  setTimeout(function(){
    appOrdRetComp.getOrdRetData($("#idordenretiro").val())
  }, 1500);
}

 $('#combustible').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
 $("#marca").select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
 $("#modelo").select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
 $('#tipo').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
 $('#compania').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });

 $('select:not(.normal)').each(function () {
                $(this).select2({
                    dropdownParent: $(this).parent()
                });
            });
 var anio = (new Date()).getFullYear() + 1;
var i = 0;
for (i = anio; i > 1969; i--) {
  // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
  $("#anio").append($("<option></option>").attr("value", i ).text(i ));
};

 $('#anio').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });
$('#emailcontac').select2({
    dropdownParent: $("#newUpdateOrdenRetiro")
  });

/* $('#direcciontaller').select2({
    dropdownParent: $("#newUpdateOrdenRetiro"),
    tags: true,
    createTag: function (tag) {
            return {id: 'nuevotaller', text: tag.term, tag: true};
        }
  });*/
 



var options = {
  url: "talleresResource",

  getValue: "direccion",
  

  list: {
    onClickEvent: function() {
      var nombretaller = $("#direcciontaller").getSelectedItemData().nombre;
      var ruttaller = $("#direcciontaller").getSelectedItemData().rut;
      var telefonotaller = $("#direcciontaller").getSelectedItemData().telefono;
      var emailtaller = $("#direcciontaller").getSelectedItemData().email;
      var contactotaller = $("#direcciontaller").getSelectedItemData().contacto;
      var idtaller = $("#direcciontaller").getSelectedItemData().id;

      $("#nombretaller").val(nombretaller).trigger("change");
      $("#ruttaller").val(ruttaller).trigger("change");
      $("#telefonotaller").val(telefonotaller).trigger("change");
      $("#emailtaller").val(emailtaller).trigger("change");
      $("#nombcontactotaller").val(contactotaller).trigger("change");
      $("#idtaller").val(idtaller).trigger("change");
    },
    match: {
      enabled: true
    },
  }
};

$("#direcciontaller").easyAutocomplete(options);
$('div.easy-autocomplete').removeAttr('style');



//Autocompletar del Transportista
var options = {
              url: "transportistaResource",

              getValue: "nombrechofer",
              

              list: {
                onClickEvent: function() {
                  
                  var idtransportista = $("#nombchofertransp").getSelectedItemData().id;
                  var ruttransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
                  var telefonotransp = $("#nombchofertransp").getSelectedItemData().rutempresa;
                  var emailtransp = $("#nombchofertransp").getSelectedItemData().email;
                  var nombreempresa = $("#nombchofertransp").getSelectedItemData().nombreempresa;
                  var gruaexterna = $("#nombchofertransp").getSelectedItemData().gruaexterna;
                  var patentecamiontransp = $("#nombchofertransp").getSelectedItemData().patentecamion;
                  var rutchofertransp = $("#nombchofertransp").getSelectedItemData().rutchofer;
                  $("#idtransportista").val(idtransportista).trigger("change");
                  $("#emailtransp").val(emailtransp).trigger("change");
                  $("#telefonotransp").val(telefonotransp).trigger("change");
                  $("#ruttransp").val(ruttransp).trigger("change");
                  $("#nombtransp").val(nombreempresa).trigger("change");
                  $("#emailtransp").val(emailtransp).trigger("change");
                  $("#gruaexternatransp").val(gruaexterna).trigger("change");
                  $("#patentecamiontransp").val(patentecamiontransp).trigger("change");
                  $("#rutchofertransp").val(rutchofertransp).trigger("change");
                  $("#btnEnviarTraslado").prop( "disabled", false );
                  $("#btnEnviarEmailTraslado").prop( "disabled", false );

                },
                match: {
                  enabled: true
                },
              }
            };

            $("#nombchofertransp").easyAutocomplete(options);
            $('div.easy-autocomplete').removeAttr('style');   



//Evento que ocurre cuando se selecciona una marca para llenar los modelos
$('#marca').on('select2:select', function (e) {
    $('#modelo').html('')
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlModelo = 'showFullDataByMarca/'+data.id;
      axios.get(urlModelo).then(response => {
        var modelos = response.data;
        //console.log(modelos)
        $("#modelo").append($("<option></option>").attr("value", "0" ).text("Seleccione modelo" ));

        modelos.forEach(function(modelo, key, modelos) {
         // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
          $("#modelo").append($("<option></option>").attr("value", modelo.id ).text(modelo.nombre ));
        
        });
          
      });
});

//Evento que ocurre cuando se selecciona un email de contacto para llenar los modelos
$('#emailcontac').on('select2:select', function (e) {
    //$('#modelo').html('')
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    $('#idcontacto').val(data.id)
    
});



//Evento que ocurre cuando se selecciona una compañia para llenar los email de los contactos
$('#compania').on('select2:select', function (e) {
    
    var data = e.params.data;
    //alert ("Seleccionado el elemento: "+data.text+"Con id: "+data.id)
    var urlContactosEmpresa = "usuariosPorCompania/"+data.id;
      axios.get(urlContactosEmpresa).then(response => {
        var contactos = response.data;
        $('#emailcontac').html('')
        $("#liquidador").html('')
        $("#emailcontac").append($("<option></option>").attr("value", "0" ).text("Seleccione contacto"));

        contactos.forEach(function(contacto, key, contactos) {
         // console.log("id del modelo:"+modelo.id+" Nombre del modelo:"+modelo.nombre)
        //  $("#modelo").select2('data', {id: modelo.id, text: modelo.nombre});  
          $("#emailcontac").append($("<option></option>").attr("value", contacto.id ).text(contacto.email ));
        
        });
          
      });

      var urlLiquidadoresEmpresa = "liquidadoresPorCompania/"+data.id;
      axios.get(urlLiquidadoresEmpresa).then(response => {
        var liquidadores = response.data;
       
        $("#liquidador").append($("<option></option>").attr("value", "0" ).text("Seleccione liquidador"));

        liquidadores.forEach(function(liquidador, key, contactos) {
          $("#liquidador").append($("<option></option>").attr("value", liquidador.id ).text(liquidador.liquidador ));
        
        });
          
      });


});// Fin del evento change de compania


$( "#direcciontaller" ).focusin(function() {
      $("#nombretaller").val('');
      $("#ruttaller").val('');
      $("#telefonotaller").val('');
      $("#emailtaller").val('');
      $("#nombcontactotaller").val('');
      $("#idtaller").val('');
});

$( "#nombchofertransp" ).focusin(function() {
      $("#idtransportista").val('');
      $("#ruttransp").val('');
      $("#nombtransp").val('');
      $("#patentecamiontransp").val('');
      $("#telefonotransp").val('');
      $("#emailtransp").val('');
      $("#rutchofertransp").val('');
      $("#btnEnviarTraslado").prop( "disabled", true );
      $("#btnEnviarEmailTraslado").prop( "disabled", true );
});


}); // Fin del Document ready




window.eliminarArchivo = function(nproceso,nombrearchivo,idarchivoor){
axios.post('eliminararchivo/'+nproceso+'/'+nombrearchivo+'/'+idarchivoor).then(response => { 
    swal(
        'Archivo Eliminado',
        'Se eliminó el archivo: '.nombrearchivo,
        'success'
      )

});
//alert('#doc'+idarchivo)
//document.getElementById("tablaarchivos").deleteRow(0);
//$(this).closest('tr').remove();
}

$("#tablaarchivos").on('click','.btnDelete',function(){
       $(this).closest('tr').remove();
     });


//Funcion que comprueba el estado de la OR
window.eliminarFoto = function(patente,idfoto){
 let galeriaFotos;

  axios.post('eliminarfoto/'+appOrdRetComp.ordRetiroData.vehiculo.patente+'/'+idfoto).then(response => {  

        });

  galeriaFotos = axios.get('scanfolderGaleriaVehiculo/'+appOrdRetComp.ordRetiroData.vehiculo.patente).then(response => { 
           return response.data
           

        });

 
//
    galeriaFotos.then(function(result) {
    swal(
      'Imegen Eliminada',
      'Se eliminó la imagen: '.idfoto,
      'success'
    )

    $("#galeriaVehiculo").html(result)
    $('#galeriaVehiculo').trigger('destroy.owl.carousel');
    $('#galeriaVehiculo').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })//Fin de funcion de inicializacion de OWN CORUSEL


    })

    $.fancybox.close( true );



}




$("#btnObservation").click(function(){


}) //Fin click guardar observacion

$("#cargarArchivo").click(function(){
 
  let formData = new FormData();
  var nombrearchivo;
  var idordenretiro = $("#idordenretiro").val()
  var nproceso = appOrdRetComp.ordRetiroData.ordenretiro.nproceso
  var observ = $("#obserfile").val()


  formData.append('archivo', document.getElementById('archivoOR').files[0])
  formData.append('idordenretiro', idordenretiro)
  formData.append('nproceso', nproceso)
  formData.append('observacion', observ)

  let archivocargado = axios.post('subirarchivo', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then(function (response) {
      
        //return response.data;
        axios.get('scanfolderArchivosOR/'+nproceso).then(response => {
          swal(
          'Cargar Archivo',
          'Archivo cargado',
          'success'
          );
            $('#tbodyArchivos').html('')
            $("#tbodyArchivos").append(response.data)

        });
    });



  

}) //Fin click carga de arechivo

$("#btnImportarCert").click(function(){
  //alert("btnImportarCert");
  var inicio=0
  var fin=0

  var aLineas = document.getElementById("datosCertificados").value.split('\n');
  var datosCertificados = document.getElementById("datosCertificados").value;
  //alert("El textarea tiene " + aLineas.length + " líneas");
  
  //Importador del Dato Patente
  inicio = datosCertificados.indexOf("Inscripción : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var patente = datosCertificados.substring(inicio+14, fin-2);
  patente = patente.replace(".", "-");
  $("#patente").val(patente)

//Importador del Dato Marca
  inicio = datosCertificados.indexOf("Marca : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var marcanombre = datosCertificados.substring(inicio+8, fin);
  const resultMarca = appOrModal.marcasList.filter(marca => marca.nombre ===marcanombre);
  $("#marca").select2("trigger", "select", {
         data: { id: resultMarca[0].id }
         });

  //Importador del Dato Modelo
  inicio = datosCertificados.indexOf("Modelo : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var modelonombre = datosCertificados.substring(inicio+9, fin);
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function(){
  var urlbuscarModeloId = 'buscarModeloPorNombre/'+modelonombre;
      axios.get(urlbuscarModeloId).then(response => {
          $("#modelo").select2("trigger", "select", {
           data: { id: response.data[0].id }
           });
           
      });
    
  }, 1500);
  
//Importador del Dato Año
  inicio = datosCertificados.indexOf("Año : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var anio = datosCertificados.substring(inicio+6, fin);
  $("#anio").val(anio) 

//Importador del Dato Color
  inicio = datosCertificados.indexOf("Color : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var color = datosCertificados.substring(inicio+8, fin);
  $("#color").val(color)
  

//Importador del Dato Combustible
  inicio = datosCertificados.indexOf("Combustible : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var combustible = datosCertificados.substring(inicio+14, fin);
  $("#combustible").select2("trigger", "select", {
         data: { id: combustible }
  });


  
//Importador del Dato TIPO
  inicio = datosCertificados.indexOf("Tipo Vehículo : ");
  fin = datosCertificados.indexOf(' Año : ');
  var nombretipo = datosCertificados.substring(inicio+16, fin);
  
 const resultTipo = appOrModal.tiposList.filter(tipo => tipo.nombre ===nombretipo);
 //console.log(resultTipo)
  $("#tipo").select2("trigger", "select", {
         data: { id: resultTipo[0].id }
         });

//Importador del Dato Nro. Motor 
  inicio = datosCertificados.indexOf("Nro. Motor : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nmotor = datosCertificados.substring(inicio+13, fin);
  $("#nmotor").val(nmotor) 

//Importador del Dato Nro. Chasis
  inicio = datosCertificados.indexOf("Nro. Chasis : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nchasis = datosCertificados.substring(inicio+14, fin);
  $("#nchasis").val(nchasis) 


  //Importador del Dato Nro. Vin
  inicio = datosCertificados.indexOf("Nro. Vin : ");
  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio+11, fin);
  if (inicio < 0)
  {$("#vim").val('')}
  else
  {
    $("#vim").val(nvim)
    $("#nserie").val(nvim)
  }
  

  //Importador del Dato Nombre del propietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");
  
  if (inicioseccion < 0)
  {
    $("#nombreprop").val('')
  }
  else
  {
    inicio = datosCertificados.indexOf("Nombre : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var nombreprorp = datosCertificados.substring(inicio+9, fin);
    $("#nombreprop").val(nombreprorp)
  }


  //Importador del Dato rut del prropietario
  var inicioseccion = datosCertificados.indexOf("DATOS DEL PROPIETARIO");
  
  if (inicioseccion < 0)
  {
    $("#rutprop").val('')
  }
  else
  {
    inicio = datosCertificados.indexOf("R.U.N. : ", inicioseccion);
    fin = datosCertificados.indexOf('\n', inicio);
    var rutprop = datosCertificados.substring(inicio+9, fin);
    $("#rutprop").val(rutprop)
  }



  inicio = datosCertificados.indexOf("DATOS DEL PROPIETARIO");

  fin = datosCertificados.indexOf('\n', inicio);
  var nvim = datosCertificados.substring(inicio+11, fin);
  
  

  


  /*
  $("#anio").val(aLineas[4].substr(-4))
  //Obtiene el ID del marca para seleccionarlo dinamicamente
  const resultMarca = appOrModal.marcasList.filter(marca => marca.nombre ===aLineas[6]);
  $("#marca").select2("trigger", "select", {
         data: { id: resultMarca[0].id }
         });
  //Obtiene el ID del modelo para seleccionarlo dinamicamente
  setTimeout(function(){
  var urlbuscarModeloId = 'buscarModeloPorNombre/'+aLineas[8];
      axios.get(urlbuscarModeloId).then(response => {
          $("#modelo").select2("trigger", "select", {
           data: { id: response.data[0].id }
           });
           
      });
    
  }, 1500);
  //Obtiene el ID del tipo para seleccionarlo dinamicamente
  var n = aLineas[4].indexOf(" ");
  var nombretipo = aLineas[4].substring(0, n);
  const resultTipo = appOrModal.tiposList.filter(tipo => tipo.nombre ===nombretipo);
  $("#tipo").select2("trigger", "select", {
         data: { id: resultTipo[0].id }
         });
  $("#nmotor").val(aLineas[10])
  $("#nchasis").val(aLineas[12])
  
  $("#vin").val(aLineas[14])
  $("#color").val(aLineas[16])
  $("#combustible").select2("trigger", "select", {
         data: { id: aLineas[18] }
         });*/
  //$("#color").val(aLineas[18])
  

  
  
  
  //$("#marca").select2("val",'TOYOTA' );
  //$('#marca').val(3).trigger('change');
  
  
  /*$("#marca").select2("trigger", "select", {
         data: { id: 1 }
         });*/
         //$("#marca option:contains("+aLineas[6]+")").attr('selected', true);

  $('#importarCertificadoModal').modal('hide');

}) //Fin click importar datos del certificado


$("#btnsubirimg").click(function(){
      let formData = new FormData();

        for(var key in document.getElementById('imagenesVehiculo').files){
            formData.append('fotosvehiculo[]', document.getElementById('imagenesVehiculo').files[key]);
        }
        formData.append('patente', appOrdRetComp.ordRetiroData.vehiculo.patente);
        formData.append('nproceso', appOrdRetComp.ordRetiroData.ordenretiro.nproceso);

    axios.post('subirfotosor', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then(response => { 
      axios.get('scanfolderGaleriaVehiculo/'+appOrdRetComp.ordRetiroData.vehiculo.patente).then(response => { 
   
         swal(
          'Cargar Imagen',
          'Imagen(es) Cargada(s)',
          'success'
          )
              $("#galeriaVehiculo").html(response.data)
              $('#galeriaVehiculo').trigger('destroy.owl.carousel');
              $('#galeriaVehiculo').owlCarousel({
                  loop:true,
                  margin:10,
                  nav:true,
                  responsive:{
                      0:{
                          items:1
                      },
                      600:{
                          items:3
                      },
                      1000:{
                          items:5
                      }
                  }
              })//Fin de funcion de inicializacion de OWN CORUSEL
         

      });// Fin de scanfolderGaleriaVehiculo


    });// Fin de subirfotos



     
        
        //alert($("#galeriaVehiculo").html())
        $.fancybox.close( true );
        


}) //Fin click carga de archivos

window.filtrarEstado = function (estado) {
  $("#tablaOrdenesRetiro").dataTable().api().search(estado).draw()
};



