
import compusuarios from './components/Usuarios.vue';
var appUserComp = new Vue({
components:{
        'listausuarios': compusuarios

    },
    el: '#listado_user',
    data: {
        userData: ''
    },
  methods: {
    actualizarUsuario: function (iduser) {
       $('#titulomodal').html('ACTUALIZAR USUARIO'); 
       $('#accion').val('ACTUALIZAR USUARIO'); 
       $('#btnEnviarUser').html( 'ACTUALIZAR USUARIO'); 
    },
    getUserData: function(iduser) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR USUARIO'); 
       $('#accion').val('ACTUALIZAR USUARIO'); 
       $('#idusuario').val(iduser); 
       $('#btnEnviarUser').html( 'ACTUALIZAR USUARIO');
        var urlUsuario = 'userFullData/'+iduser;
        axios.get(urlUsuario).then(response => { 
            vm.userData = response.data[0]
        });
        setTimeout(function(){
          $('#idusuario').val(iduser);
          $('#nombre').val(vm.userData.nombre);
          $('#fechanac').val(vm.userData.fechanac);
          $('#fono1').val(vm.userData.fono1);
          $('#fono2').val(vm.userData.fono2);
          $('#apellidos').val(vm.userData.apellido);
          $('#email').val(vm.userData.email);
          $('#emailalt').val(vm.userData.emailalt);
          $("#sexo").val(vm.userData.sexo);
          $("#compania").val(vm.userData.compania);
          $("#perfil").val(vm.userData.tipo);
          $("#estado").val(vm.userData.estado);

          }, 500);
    },
  }
});

var appUserModal = new Vue({

    el: '#capaModal',
    data: {
        companiaList: ''
        },
  created: function() {
        this.getCompania();
        },
  methods: {
    getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },
    crearUsuario: function() {
      $('#titulomodal').html('CREAR USUARIO'); 
      $('#accion').val('CREAR USUARIO'); 
      $('#btnEnviarUser').html('CREAR USUARIO');
      $("#_method").val("POST");
   
    },
    enviarFormulario: function() {
        if ($('#accion').val() == 'CREAR USUARIO')
        {
          this.$refs.formUsuario._method.value="POST";
          $("#formUsuario").submit();
        }
        else
        {
          this.$refs.formUsuario._method.value="PATCH";
          this.$refs.formUsuario.action="/adminusuarios/"+$('#idusuario').val();
          $("#formUsuario").submit();
        }
   
    },
   
  }
});

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
if (haserror == 1){
  $('#newUpdateUser').modal('show');
  $('#btnEnviarUser').html( $('#accion').val());
}

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateUser").on("hidden.bs.modal", function () {
    document.getElementById("formUsuario").reset();
    $('.text-danger').html("");
});

;

