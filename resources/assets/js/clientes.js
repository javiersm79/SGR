
import compclientes from './components/Clientes.vue';
var appUserComp = new Vue({
components:{
        'listacontactos': compclientes

    },
    el: '#listado_contacto',
    data: {
        contactoData: ''
    },
  methods: {
    actualizarContacto: function (idcontacto) {
       $('#titulomodal').html('ACTUALIZAR CLIENTE'); 
       $('#accion').val('ACTUALIZAR CLIENTE'); 
       $('#btnEnviarContacto').html( 'ACTUALIZAR CLIENTE'); 
    },
    getContactoData: function(idcontacto) {
       const vm = this;
       $('#titulomodal').html('ACTUALIZAR CLIENTE'); 
       $('#accion').val('ACTUALIZAR CLIENTE'); 
       $('#idcontacto').val(idcontacto); 
       $('#btnEnviarContacto').html( 'ACTUALIZAR CLIENTE');
        var urlCliente = 'verCliente/'+idcontacto;
        axios.get(urlCliente).then(response => { 
            vm.contactoData = response.data
        });
        setTimeout(function(){
          $('#idcontacto').val(idcontacto);
          $('#nombre').val(vm.contactoData.nombre);
          $('#rut').val(vm.contactoData.rut);
          $('#telefono').val(vm.contactoData.fono1);
          $('#apellidos').val(vm.contactoData.apellido);
          $('#email').val(vm.contactoData.email);

          }, 500);
    },
  }
});

var appContactoModal = new Vue({

    el: '#capaModal',
    data: {
        //companiaList: ''
        },
  created: function() {
        //this.getCompania();
        },
  methods: {
    /*getCompania: function() {
      var urlCompania = 'companiaResource';
      axios.get(urlCompania).then(response => {
          this.companiaList = response.data
      });
    },*/
    crearContacto: function() {
      $('#titulomodal').html('CREAR CLIENTE'); 
      $('#accion').val('CREAR CLIENTE'); 
      $('#btnEnviarContacto').html('CREAR CLIENTE');
      $("#_method").val("POST");
   
    },
    enviarFormulario: function() {
      
        var validardatos = 0
        $( ".requerido" ).each(function( index ) {
          if ($( this ).val() == "") {
            swal(
              'FAVOR COMPLETAR LOS CAMPOS OBLIGATORIOS',
              'VALIDAR CAMPOS REQUERIDOS',
              'warning'
            )
            //return

            //Asigna 1 si hay un campo requerido vacio
            validardatos = 1

          }
          
        });

      if (validardatos === 0)
      {
        if ($('#accion').val() == 'CREAR CLIENTE')
        {
          this.$refs.formCliente._method.value="POST";
          $("#formCliente").submit();
          var validardatos = 0
        }
        else
        {
          this.$refs.formCliente._method.value="POST";
          this.$refs.formCliente.action="/actualizarCliente/"+$('#idcontacto').val();
         $("#formCliente").submit();
        }
      }
   
    },
   
  }, //Fin de moetodos del componente
    
   
});

//Inicializa los datapicker
$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    startView: 2,
    endDate: hoy() ,
    language: "es"
});

//Colocala fecha actual en los campos
$('#fcrearnew').val(hoy());
$('#fcrearupdate').val(hoy());

//Detecta si se envio un formulario de crear/actualizar y se retorno un error de validación
var haserror;
haserror = $('meta[name=haserror]').attr('content');
if (haserror == 1){
  $('#newUpdateContacto').modal('show');
  $('#btnEnviarContacto').html( $('#accion').val());
}

//Detecta el cierre del fomulario crear/actualizar para acciones de limpieza
$("#newUpdateContacto").on("hidden.bs.modal", function () {
    document.getElementById("formContacto").reset();
});


