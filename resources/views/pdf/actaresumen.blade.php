<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    
    <link href="{{URL::to('/')}}/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-color: white">
    <div id="app" style="font-size: 12px">
        
            <div class="row">
                    <img src='{{URL::to('/')}}/storage/images/vedisa.png'>
                    <span class="pull-right">J. PABLO MONTERO L.<br><em>Martillero Público RNM 836</em></span>

            </div>

             <div class="row"> 
                
                     <table width="100%">
                        <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>

                        </tr>
                        <tr>
                             <td width="33%">Nº Acta</td>
                             <td width="33%">Fecha</td>
                             <td width="33%">Nº de Vehículos</td>
                        </tr>
                        <tr>
                             <td width="33%"><b>{{$datosacta[0]->numeroacta}}</b></td>
                             <td width="33%"><b>{{$datosacta[0]->fecha}}</b></td>
                             <td width="33%"><b>{{count($datosacta)}}</b></td>
                        </tr>
                        <tr>
                             <td width="33%">Total  Remate</td>
                             <td width="33%">Total  Comisiones</td>
                             <td width="33%">Total  IVA</td>
                        </tr>
                        <tr>
                             <td width="33%"><b>{{'$'.number_format( $datosacta->totales['monto'],0,",",".") }}</b></td>
                             <td width="33%"><b>{{'$'.number_format( $datosacta->totales['comision'],0,",",".") }}</b></td>
                             <td width="33%"><b>{{'$'.number_format( $datosacta->totales['iva'],0,",",".") }}</b></td>
                        </tr>
                        <tr>
                             <td width="33%">Total  Garantia</td>
                             <td width="33%">Total  Gastos op</td>
                        </tr>
                        <tr>
                             <td width="33%"><b>{{'$'.number_format( $datosacta->totales['garantia'],0,",",".") }}</b></td>
                             <td width="33%"><b>{{'$'.number_format( $datosacta->totales['gastosoperacionales'],0,",",".") }}</b></td>
                        </tr>
                     </table>
                     <br>            
                     <table width="100%" border="1" style="font-size: 0.7em">
                         <tr>
                            <td colspan="9"><br>LISTADO DE VEHÍCULOS</td>
                         </tr>
  
                         <tr>
                            <td colspan="9" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <th>N° Proceso</th>
                            <th>Patente</th>
                            <th>Compañia</th>
                            <th>N° Siniestro</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Cliente</th>
                            <th>Monto</th>
                            
                         </tr>
                         @foreach ($datosacta as $vehiculo)
                        <tr>
                            <td> {{ $vehiculo->nproceso }}</td>
                            <td> {{ $vehiculo->patente }}</td>
                            <td> {{ $vehiculo->nombrecompania }}</td>
                            <td> {{ $vehiculo->nsiniestro }}</td>
                            <td> {{ $vehiculo->nombremarca }}</td>
                            <td> {{ $vehiculo->nombremodelo }}</td>
                            <td> {{ $vehiculo->anio }}</td>
                            <td> {{ $vehiculo->cliente }}</td>
                            <td> {{'$'.number_format( $vehiculo->monto,0,",",".") }}</td>
                        </tr>
                        @endforeach
                     </table>               
                     

            </div>
        

    </div>

</body>
</html>
