<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    
    <link href="http://localhost/sive/public/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-color: white">
    <div id="app" style="font-size: 12px">
        
            <div class="row">
                   <img src='http://localhost/sive/public/storage/images/vedisa.png'> 
                    <span class="pull-right">J. PABLO MONTERO L.<br><em>Martillero Público RNM 836</em></span>

            </div>

             <div class="row"> 
                
                     <table width="100%">
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>

                         </tr>
                         <tr>
                             <td width="33%">Nº Proceso</td>
                             <td width="33%"></td>
                             <td width="33%">Fecha</td>
                         </tr>
                         <tr>
                             <td width="33%"><b>{{ $ordenRetiro['ordenretiro']['nproceso'] }}</b></td>
                             <td width="33%"></td>
                             <td width="33%"><b>{{ $ordenRetiro['ordenretiro']['fecha'] }}</b></td>
                         </tr>
                     </table>                
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL VEHÍCULO</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>COMPAÑÍA</td>
                            <td width="33%"><br>Nº SINIESTRO</td>
                            <td width="33%"><br>LIQUIDADOR</td>
                            
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['compania']['nombre'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['ordenretiro']['nsiniestro'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['liquidador']['liquidador'] }}</b></td>
                            
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>

                         <tr>
                            <td width="33%"><br>PATENTE</td>
                            <td width="33%">TIPO</td>
                            <td width="33%">COLOR</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['vehiculo']['patente'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['tipovehiculo']->nombre }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['vehiculo']['color'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">MARCA</td>
                            <td width="33%">MODELO</td>
                            <td width="33%">AÑO</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['marcavehiculo']->nombre }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['modelovehiculo']->nombre }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['vehiculo']['anio'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">Nº CHASIS</td>
                            <td colspan="2">Nº MOTOR</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['vehiculo']['numchasis'] }}</b></td>
                            <td colspan="2"><b>{{ $ordenRetiro['vehiculo']['numotor'] }}</b></td>
                         </tr>
                     </table>               
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL TALLER O RETIRO</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>TALLER</td>
                            <td width="33%"><br>DIRECCIÓN</td>
                            <td width="33%"><br>REGIÓN</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['nombre'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['direccion'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['region'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">COMUNA</td>
                            <td width="33%">CIUDAD</td>
                            <td width="33%">TELEFONO</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['comuna'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['ciudad'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['taller']['telefono'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td colspan="3">CONTACTO</td>
                         </tr>
                         <tr>
                            <td colspan="3"><b>{{ $ordenRetiro['taller']['contacto'] }}</b></td>
                         </tr>
                     </table>              
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL TRASNPORTISTA</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>NOMBRE</td>
                            <td width="33%"><br>RUT</td>
                            <td width="33%"><br></td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ordenRetiro['transportista']['nombrechofer'] }}</b></td>
                            <td width="33%"><b>{{ $ordenRetiro['transportista']['rutchofer'] }}</b></td>
                            <td width="33%"><b></b></td>
                         </tr>
                     </table>

            </div>
        

    </div>

</body>
</html>
