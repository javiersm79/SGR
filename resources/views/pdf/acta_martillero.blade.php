<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    
    
    <style>
    table{border-spacing:0;border-collapse:collapse}td,th{padding:0}/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */@media print{*,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}.navbar{display:none}.btn>.caret,.dropup>.btn>.caret{border-top-color:#000!important}.label{border:1px solid #000}.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}.table-bordered td,.table-bordered th{border:1px solid #ddd!important}}@font-face{font-family:'Glyphicons Halflings';src:url(../fonts/glyphicons-halflings-regular.eot);src:url(../fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(../fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(../fonts/glyphicons-halflings-regular.woff) format('woff'),url(../fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')}
    
    header { position: fixed; top: 0px; left: 0px; right: 0px; height: 60px; }
  </style>
</head>
<body style="background-color: white">
    <header>
         <table width="100%" style="font-size: 25px">
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 3px;"></td>

                         </tr>
                         <tr>
                             <td width="33%">Nº Acta: {{$datosacta[0]->numeroacta}}</td>
                             <td width="33%">Fecha: {{$datosacta[0]->fecha}}</td>
                             <td width="33%">Nº de Vehículos: {{count($datosacta)}}</td>
                         </tr>
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 3px;"></td>

                         </tr>
                     </table>
                     
    </header>
<br><br><br>
            

    <table width="100%" border="1">
         @foreach ($datosacta as $vehiculo)
        <tr>
            <td width="13%">
            Tipo Vehiculo<br>
            <b>Marca</b><br>
            <b>Modelo</b><br>
            <b>Año</b><br>
            Patente<br>
            Siniestro
            </td>
            <td width="25%">
            {{ $vehiculo->nombretipo }}<br>
            <b>{{ $vehiculo->nombremarca }}</b><br>
            <b>{{ $vehiculo->nombremodelo }}</b><br>
            <b>{{ $vehiculo->anio }}</b><br>
            {{ $vehiculo->patente }}<br>
            {{ $vehiculo->nsiniestro }}
            </td>
            <td width="12%">
                {{ $vehiculo->nombrecompania }}                                
            </td>
            <td width="25%">
                                                
            </td>
            <td width="25%">
                                                
            </td>
            
        </tr>
        @endforeach
    </table>

</body>
</html>
