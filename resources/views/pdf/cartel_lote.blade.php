<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    
    
    <style type="text/css">
        @page { margin: 2cm; }
        html { margin: 40px }
        table{border-spacing:0;border-collapse:collapse}td,th{padding:0}/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */@media print{*,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}.navbar{display:none}.btn>.caret,.dropup>.btn>.caret{border-top-color:#000!important}.label{border:1px solid #000}.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}.table-bordered td,.table-bordered th{border:1px solid #ddd!important}}@font-face{font-family:'Glyphicons Halflings';src:url(../fonts/glyphicons-halflings-regular.eot);src:url(../fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(../fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(../fonts/glyphicons-halflings-regular.woff) format('woff'),url(../fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')}
        .info {
           border-bottom: 3px solid black;
           font-weight: bold;
        }
        table, td
        {
            width: 100%
        }

        .loteinfo
        {
            font-size: 100px;
            font-weight: bold;

        }
    </style>
</head>
<body>              
    
        @foreach ($datosacta as $vehiculo)
        <span class="loteinfo">LOTE Nº {{ $vehiculo->lote }}</span>
        <table id="tablaLotes" style="font-size: 3em">
        <tr>
            <td >TIPO DE VEHICULO</td>
            <td class="info">{{ $vehiculo->nombretipo }}</td>
        </tr>
        <tr>
            <td></td>
            <td><br></td>
        </tr>
        <tr>
            <td>MARCA</td>
            <td class="info">{{ $vehiculo->nombremarca }}</td>
        </tr>
        <tr>
            <td></td>
            <td><br></td>
        </tr>        
        <tr>
            <td>MODELO</td>
            <td class="info">{{ $vehiculo->nombremodelo }}</td>
        </tr>
        <tr>
            <td></td>
            <td><br></td>
        </tr>
        <tr>
            <td>AÑO</td>
            <td class="info">{{ $vehiculo->anio }}</td>
        </tr>
        <tr>
            <td></td>
            <td><br></td>
        </tr>
        <tr>
            <td>PATENTE</td>
            <td class="info">{{ $vehiculo->patente }}</td>
        </tr>
        <tr>
            <td></td>
            <td><br></td>
        </tr>
        <tr>
            <td>COMISIÓN</td>
            <td class="info">12% + IVA</td>
        </tr>
        </table>
        @if (!$loop->last)
            <div style="page-break-after:always;"></div>
        @endif
        
        @endforeach


</body>
</html>
