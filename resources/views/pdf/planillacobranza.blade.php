<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">

	
	
	<style type="text/css">
		@page { margin: 1cm; }
		html { margin: 20px }
		table th{
			text-align: center;
		}
		table{border-spacing:0;border-collapse:collapse}td,th{padding:0}/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */@media print{*,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}.navbar{display:none}.btn>.caret,.dropup>.btn>.caret{border-top-color:#000!important}.label{border:1px solid #000}.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}.table-bordered td,.table-bordered th{border:1px solid #ddd!important}}@font-face{font-family:'Glyphicons Halflings';src:url(../fonts/glyphicons-halflings-regular.eot);src:url(../fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(../fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(../fonts/glyphicons-halflings-regular.woff) format('woff'),url(../fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')}
	</style>
</head>
<body style="background-color: white; font-size: 10px">

<table width="100%" border="1">
	<tr>
		<td colspan="16"><h3>&nbsp;Planilla de Cobranza | Acta: {{$datosacta[0]->numeroacta}} - Fecha: {{$datosacta[0]->fechafor}}</h3></td>
	</tr>

	<tr>
		<td colspan="16" style="background-color: #004C66; height: 4px;"></td>
	</tr>

	<tr>
		<th width="3%">Lote</th>
		<th>Patente</th>
		<th>Marca</th>
		<th>Modelo</th>
		<th width="3%">Año</th>
		<th>Compañia</th>
		<th>Siniestro</th>
		<th width="5%">Condicionado</th>
		<th width="5%">Desarme</th>
		<th>Valor Remate</th>
		<th>Comisión</th>
		<th>IVA</th>
		<th>Gastos Op.</th>
		<th>Tasación Fisc.</th>
		<th>Impuesto</th>
		<th>Total</th>

	</tr>
	@foreach ($datosacta as $vehiculo)
	<tr>
		<td align="center">{{ $vehiculo->lote }}</td>
		<td align="center">{{ $vehiculo->patente }}</td>
		<td align="center">{{ $vehiculo->nombremarca }}</td>
		<td align="center">{{ $vehiculo->nombremodelo }}</td>
		<td align="center">{{ $vehiculo->anio }}</td>
		<td align="center">{{ $vehiculo->nombrecompania }}</td>
		<td align="center">{{ $vehiculo->nsiniestro }}</td>
		<td align="center">
			@if($vehiculo->condicionado == "on")
			SI
			@else
			NO
			@endif
		</td>
		<td align="center">
			@if($vehiculo->desarme == "on")
			SI
			@else
			NO
			@endif
		</td>
		<td align="right">{{ '$'.number_format($vehiculo->monto,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->comision,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->iva,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->gastosoperacionales,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->tasacionfiscal,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->tasacionfiscal * 0.015,0,",",".") }}</td>
		<td align="right">{{ '$'.number_format($vehiculo->monto + $vehiculo->comision + $vehiculo->iva + $vehiculo->gastosoperacionales + $vehiculo->tasacionfiscal + ($vehiculo->tasacionfiscal * 0.015),0,",",".") }}</td>
	</tr>
	@endforeach
</table>               

</body>
</html>
