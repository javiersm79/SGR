<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    <link href="http://localhost/sive/public/css/bootstrap.min.css" rel="stylesheet">
    
</head>
<body style="background-color: white; margin-left: 2%; margin-right: 2%;">
    <div id="app" style="font-size: 12px">
        <br>
        
            <div class="row">
                    <img src='http://localhost/sive/public/storage/images/vedisa.png'>
                    <span class="pull-right">J. PABLO MONTERO L.<br><em>Martillero Público RNM 836</em></span>

            </div>

             <div class="row">
                
                     <table width="100%">
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>

                         </tr>
                         <tr>
                             <td width="33%">Nº Proceso</td>
                             <td width="33%"></td>
                             <td width="33%">Fecha</td>
                         </tr>
                         <tr>
                             <td width="33%"><b>{{ $ingreso['ingreso']['nproceso'] }}</b></td>
                             <td width="33%"></td>
                             <td width="33%"><b>{{ $ingreso['ingreso']['fecha'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><br>LIQUIDADOR</td>
                         </tr>
                         <tr>
                            <td colspan="3"><b>{{ $ingreso['liquidador']['liquidador'] }}</b></td>
                         </tr>
                     </table>                
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL VEHÍCULO</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>PATENTE</td>
                            <td width="33%"><br>Nº SINIESTRO</td>
                            <td width="33%"><br>COMPAÑÍA</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['vehiculo']['patente'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['ingreso']['nsiniestro'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['compania']['nombre'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">TIPO</td>
                            <td width="33%">MARCA</td>
                            <td width="33%">MODELO</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['tipovehiculo']->nombre }}</b></td>
                            <td width="33%"><b>{{ $ingreso['marcavehiculo']->nombre }}</b></td>
                            <td width="33%"><b>{{ $ingreso['modelovehiculo']->nombre }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">COLOR</td>
                            <td width="33%">AÑO</td>
                            <td width="33%">Nº MOTOR</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['vehiculo']['color'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['vehiculo']['anio'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['vehiculo']['numotor'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td colspan="3">Nº CHASIS</td>
                         </tr>
                         <tr>
                            <td colspan="3"><b>{{ $ingreso['vehiculo']['numchasis'] }}</b></td>
                         </tr>
                     </table>               
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL TALLER O RETIRO</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>TALLER</td>
                            <td width="33%"><br>DIRECCIÓN</td>
                            <td width="33%"><br>REGIÓN</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['taller']['nombre'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['taller']['direccion'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['taller']['region'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td width="33%">COMUNA</td>
                            <td width="33%">CIUDAD</td>
                            <td width="33%">TELEFONO</td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['taller']['comuna'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['taller']['ciudad'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['taller']['telefono'] }}</b></td>
                         </tr>
                         <tr>
                            <td colspan="3"><hr></td>
                         </tr>
                         <tr>
                            <td colspan="3">CONTACTO</td>
                         </tr>
                         <tr>
                            <td colspan="3"><b>{{ $ingreso['taller']['contacto'] }}</b></td>
                         </tr>
                     </table>              
                     <table width="100%">
                         <tr>
                            <td colspan="3"><br>DATOS DEL TRASNPORTISTA</td>
                         </tr>
  
                         <tr>
                            <td colspan="3" style="background-color: #004C66; height: 5px;"></td>
                         </tr>
                        
                         <tr>
                            <td width="33%"><br>NOMBRE</td>
                            <td width="33%"><br>RUT</td>
                            <td width="33%"><br></td>
                         </tr>
                         <tr>
                            <td width="33%"><b>{{ $ingreso['transportista']['nombrechofer'] }}</b></td>
                            <td width="33%"><b>{{ $ingreso['transportista']['rutchofer'] }}</b></td>
                            <td width="33%"><b></b></td>
                         </tr>
                     </table>
                     <br><br><br><br>

            </div>
        

    </div>

</body>
</html>
