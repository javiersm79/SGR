<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">

    
    
    <style type="text/css">
        table {border: 4px solid black;}
        table{border-spacing:0;border-collapse:collapse}td,th{padding:0}/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */@media print{*,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}.navbar{display:none}.btn>.caret,.dropup>.btn>.caret{border-top-color:#000!important}.label{border:1px solid #000}.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}.table-bordered td,.table-bordered th{border:1px solid #ddd!important}}@font-face{font-family:'Glyphicons Halflings';src:url(../fonts/glyphicons-halflings-regular.eot);src:url(../fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(../fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(../fonts/glyphicons-halflings-regular.woff) format('woff'),url(../fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')}
    </style>
</head>
<body style="background-color: white; font-size: 12px">
 @foreach ($datosacta as $vehiculo) 
<b>VEDISA REMATES LTDA</b><br>
R.U.T.: 76.114.336-0 - IRNM 1619<br>
San Gerardo 913 - Fono 2795820 - Fax 27958620<br>
Recoleta - Santiago<br>

<table width="20%" align="right" border="1">
    <tr>
        <td align="center" colspan="3">FECHA DE REMATE</td>
       
    </tr>
    <tr>
        <td align="center">{{ Carbon\Carbon::parse($datosacta[0]->fecha)->format('d') }}</td>
        <td align="center">{{ Carbon\Carbon::parse($datosacta[0]->fecha)->format('m') }}</td>
        <td align="center">{{ Carbon\Carbon::parse($datosacta[0]->fecha)->format('Y') }}</td>
    </tr>

</table><br><br><br>

<h2>Boletín de Remate</h2>

<table width="65%" align="left">
    <tr>
        <td colspan="2">&nbsp;Juan Pablo Montero Lira</td>
    </tr>
    <tr>
        <td><b>&nbsp;Sr(a/ita)</b>___________________________<br><br></td>
        <td><b>FONO</b>______________________<br><br></td>
    </tr>
    <tr>
        <td><b>&nbsp;RUT</b>______________________________<br></td>
        <td><b>E-MAIL</b>_____________________</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;Ha comprado Lote Nº {{$vehiculo->lote}}<br></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;<b>{{strtoupper ($vehiculo->nombretipo." ".$vehiculo->nombremarca." ".$vehiculo->nombremodelo." ".$vehiculo->color)}} </b></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;<b>{{strtoupper ($vehiculo->anio." ".$vehiculo->patente)}}</b></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;Y declara haberlo aceptado a su entera satisfacción SiSi</td>
    </tr>

</table>
<table width="30%" align="right" border="1">
    <tr>
        <td colspan="2" style="text-align: center">PRECIO</td>
    </tr>
    <tr>
        <td colspan="2"><b><br><br><br><br>&nbsp;&nbsp;&nbsp;DEPOSITAR SI / NO</b></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;Nº CHEQUE</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;BANCO</td>
        <td>&nbsp;&nbsp;&nbsp;MONTO</td>
    </tr>
    

</table>
<br><br><br><br><br><br><br>
<table style="border: 0px" width="103%">
    <tr>
        <td width="65%">
            <p style="font-weight: bold; ">DEPOSITAR EN CUENTA CORRIENTE 86856502 BANCO BCI O TRANSFERENCIA<br>
            ELECTRONICA Y ENVIAR COMPROBANTE A <u>pagos@vedisaremates.cl</u>,<br><u>jmunoz@vedisaremates.cl</u>
            </p>

            <p style="font-size: 11px">
                <u>NOTA IMPORTANTE</u><br>
                -PASADO 3 DIAS DESDE LA FECHA DEL REMATE, TODA MERCANCÍA NO RETTIRADA, SE ENTENDERÁCOMO QUE EL CLIENTE RENUNCIA A ESTA, PERDIENDO TODO DERECHO A LA DEVOLUCIÓN DE LA GARNTÍA.<br>
                -PASADO 48 HORAS DESDE LA FECHA DEL REMATE, SU GARNTÍA SERÁ DEPOSITADA.<br>
                -A PARTIR DEL 3º DÍA SE COBRARÁ NODEGA DE 10.000 + IVA.<br>

            </p>
        </td>
        <td style="font-weight: bold;">
                <br><br>
                <p align="center">GASTOS</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comisión: 12% mas IVA</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gastos Trasnferencia: $49.000</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Impuesto: 1.5%</p><br><br>
                <p align="center">___________________________</p>
                <p align="center">FIRMA Y TIMBRE CAJA</p>
            
        </td>
    </tr>
</table>
@if (!$loop->last)
            <div style="page-break-after:always;"></div>
        @endif
        
        @endforeach
</body>
</html>
