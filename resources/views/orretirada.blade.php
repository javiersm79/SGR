@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Estatus - Orden de Retiro</div>

                <div class="panel-body" style="font-family: 'Varela Round', sans-serif;">
                    <h3>La Orden de Retiro  <b>#{{ $nproceso }}</b> ha sido procesada existosamente</h3>


                    <h3>Estatus: Retirado</h3>

                    Gracias.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
