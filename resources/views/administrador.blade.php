@extends('layouts.app')
@section('menu')
<!-- Left Side Of Navbar -->


    <ul class="nav navbar-nav">
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inicio<b class="caret"></b></a>
            <ul class="dropdown-menu multi-level">
                <li class=""><a href="#">Bienvenida</a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administrador<b class="caret"></b></a>
            <ul class="dropdown-menu multi-level">
                <li><a href="#">Usuarios</a></li>
                <li class="dropdown-submenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenedores</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Talleres</a></li>
                        <li><a href="#">Bodega</a></li>
                        <li><a href="#">Transportistas</a></li>
                        <li><a href="#">Marcas</a></li>
                        <li><a href="#">Modelos</a></li>
                        <!-- <li class="dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Procesos<b class="caret"></b></a>
            <ul class="dropdown-menu multi-level">
                <li class=""><a href="#">Tasación</a></li>
                <li class=""><a href="#">O.R. Interna</a></li>
                <li class=""><a href="#">Ingreso a Bodega</a></li>
                <li class=""><a href="#">O.R. Externa</a></li>
                <li class=""><a href="#">Existencia</a></li>
                <li class=""><a href="#">Inventario</a></li>
            </ul>
        </li>
         <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consulta<b class="caret"></b></a>
            <ul class="dropdown-menu multi-level">
                <li class=""><a href="#">Historicos</a></li>
                <li class=""><a href="#">Por Cia. de Seguro</a></li>
            </ul>
        </li>
         <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Soporte<b class="caret"></b></a>
            <ul class="dropdown-menu multi-level">
                <li class=""><a href="#">Contacto</a></li>
                <li class=""><a href="#">Descargas</a></li>
                <li class=""><a href="#">Contraseña</a></li>
            </ul>
        </li>

    </ul>




<!-- Right Side Of Navbar -->

<ul class="nav navbar-nav navbar-right">


    <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ route('login') }}">Login</a></li>
        <li><a href="{{ route('register') }}">Register</a></li>
    @else
    
        <li >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
</ul>

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Administrador</div>

                <div class="panel-body">
                    You are logged in as: <b>{{ Auth::user()->tipo }}</b>
                </div>
                <!-- <div id='app' style="width: 90%; margin-left: 10px">
                    <pre>@{{ $data }}</pre>
                    
                </div> -->
                <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:90%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p class="">Some text in the modal.</p>
       <div id='app' style="width: 90%; margin-left: 10px">
                <componente></componente>
                    
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div id='app' style="width: 90%; margin-left: 10px">
                <componente></componente>
                    
            </div>
        </div>
    </div>
</div>
@endsection
