@extends('administrador.masteradmin')



@section('content')
<div id="tasacionesApp" class="container" style="font-family: sans-serif;">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Tasaciones</div>
            <div id="tasacionesSearchNew" class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>

                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav">

                                <li><a href="#">Tasaciones a buscar</a></li>
                        
                            </ul>
                            <form class="navbar-form navbar-left">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="">
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <div class="navbar-form">
                                <button id="btnNuevaTasacion" type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUpdateTasacion" v-on:click="rutaCrear">Nuevo</button>
                            </div>
                        </div>
                    </nav> <!-- FIN CAMPO DE BUSQUEDA DEL TASACIONES -->
                   
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_tasaciones" style="font-size: 13px">  
                        <listatasaciones class="listatabla" @view-tasaciones="rutaEditar"></listatasaciones>
                    
                    </div>

                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateTasacion" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:95%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title "><b id="titulomodal"></b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            CONTACTO
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="compania">Compañia:</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="compania" name="compania"><option value="">Seleccione</option>
                                                        <option value="3">CARDIF</option>
                                                        <option value="4">CHILENA CONSOLIDADA</option>
                                                        <option value="7">HDI</option>
                                                        <option value="5">MAPFRE</option>
                                                        <option value="6">RENTA NACIONAL</option>
                                                        <option value="2">SURA</option>
                                                        <option value="1">VEDISA</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nombre">Nombre:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre">
                                                    </div>
                                                </div>
                                                
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="email">Email:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="email" placeholder="" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="telefono">Telefono:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="telefono" placeholder="" name="telefono">
                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body">
                                            VEHICULO
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px;">
                                            <form class="form-horizontal" action="/action_page.php">
                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE TASACIONES -->

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="patente">Patente:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="patente" placeholder="" name="patente">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="marca">Marca:</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="marca" name="marca" >
                                                        
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="tipo">Tipo:</label>
                                                    <div class="col-sm-7">          
                                                        <select class="form-control" id="tipo" name="tipo">
                                                        <option value="">Seleccione</option>
                                                        <option value="2">AUTOMOVIL</option>
                                                       
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nsiniestro">Nº Siniestro:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nsiniestro" placeholder="" name="nsiniestro">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="modelo">Modelo:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="modelo" placeholder="" name="modelo">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="tasacion">Tasación:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="tasacion" placeholder="" name="tasacion">
                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="anio">Año:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="anio" placeholder="" name="anio">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="fecha">Fecha:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="fecha" placeholder="" name="fecha" readonly value="@php echo date("d/m/Y"); @endphp"> 
                                                        
                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <button class="btn btn-success btn-lg pull-right">Enviar</button>
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div> <!-- Fin - Modal nuevo usuario -->                  
            </div>
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
@endsection
@section('scriptsmodules')
<script src="js/tasaciones.js"></script>
@endsection