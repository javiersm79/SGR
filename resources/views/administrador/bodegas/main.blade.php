@extends('administrador.masteradmin')

@section('content')
<div id="bodegasApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/bodegas.png') }}"> Mantenedor de Bodegas</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.bodegas.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateBodega" v-on:click="crearBodega">Nueva Bodega</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Bodegas</div>
                   
                <div class="panel-body">
                    <div id="listado_bodegas">  
                        <listabodegas class="listatabla" @view-bodega="getBodegaData"></listabodegas>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/bodegas.js"></script>
@endsection

 