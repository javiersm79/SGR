<!-- Modal -->
<div id="newUpdateBodega" class="modal fade" role="dialog">
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles de la Bodega</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formBodega" ref="formBodega" method="post" class="form-horizontal" action="/bodegasResource">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idBodega" placeholder="" name="idBodega" value="{{ old('idBodega') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="col-md-6"> 
                            <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombre"><strong class="text-danger">(*)</strong> Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="nombre" placeholder="" name="nombre" value="{{ old('nombre') }}" >
                                     @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            
                            
                               
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-6" > 
                            <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                            
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="habilitado"><strong class="text-danger"></strong> Habilitado:</label>
                                <div class="col-sm-5">
                                    <select id="habilitado" name="habilitado" class="form-control" value="{{ old('habilitado') }}">
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>

                                    </select>

                                </div>

                            </div>

        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
