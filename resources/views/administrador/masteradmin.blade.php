<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="haserror" content="{{ Session::has('errors') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIVE') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    @yield('stylemodules')
    <style type="text/css">
  
        
    </style>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/sweetalert.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.es.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script>
    
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> --}}

</head>
<body>
    <div id="appMaster" style="font-family: 'Varela Round', sans-serif;">
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="{{ asset('storage/images/vedisa.png') }}">
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenedores<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="{{ url('usuarios') }}">Usuarios</a></li>
                                <li><a href="{{ url('contactos') }}">Contactos</a></li>
                                <li><a href="{{ url('clientes') }}">Clientes</a></li>
                                <li><a href="{{ url('companias') }}">Compañias</a></li>
                                <li><a href="{{ url('liquidadores') }}">Liquidadores</a></li>
                                <li><a href="{{ url('talleres') }}">Talleres</a></li>
                                <li><a href="{{ url('bodegas') }}">Bodegas</a></li>
                                <li><a href="{{ url('marcas') }}">Marcas</a></li>
                                <li><a href="{{ url('modelos') }}">Modelos</a></li>
                                <li><a href="{{ url('transportistas') }}">Transportistas</a></li>
                                {{-- <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenedores</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('talleres') }}">Talleres</a></li>
                                        <li><a href="{{ url('bodegas') }}">Bodegas</a></li>
                                        
                                        
                                        <li><a href="{{ url('marcas') }}">Marcas</a></li>
                                        <li><a href="{{ url('modelos') }}">Modelos</a></li> --}}
                                        
                                        <!-- <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                        <li><a href="#">Something else here</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Separated link</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">One more separated link</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li> -->
                                    </ul>
                            {{--     </li>
                            </ul>
                        </li>--}}

                        <li><a href="{{ url('tasaciones') }}">Tasación</a></li>
                        <li><a href="{{ url('ordretiros') }}">Orden de Retiro</a></li>
                        <li><a href="{{ url('existencias') }}">Existencia</a></li>
                         <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actas<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class=""><a href="{{ url('actanueva') }}">Crear Acta</a></li>
                                <li class=""><a href="{{ url('listaractas') }}">Ver Actas</a></li>
                                
                            </ul>
                        </li>
                         <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Proveedores<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class=""><a href="/crearfacturatraslado">Facturacion Traslados</a></li>
                                <li class=""><a href="#">Facturacion otros</a></li>
                                
                            </ul>
                        </li>
                         <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Factura<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class=""><a href="#">Factura Afecta</a></li>
                                <li class=""><a href="#">Factura Excenta</a></li>
                                <li class=""><a href="#">Liquidación Factura</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reportes<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class=""><a href="#">Históricos</a></li>
                            </ul>
                        </li>
                        {{-- 
                         <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Soporte<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class=""><a href="#">Contacto</a></li>
                                <li class=""><a href="#">Descargas</a></li>
                                <li class=""><a href="#">Contraseña</a></li>
                            </ul>
                        </li> --}}
                    </ul>
                    <!-- Right Side Of Navbar -->

                    <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                        
                            <li >
                                  {{-- {{ Auth::user()->name }} <span class="caret"></span> --}}
                                    <a href="{{ route('logout') }}" 
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                            </li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        @yield('content')
    </div>

@yield('scriptsmodules')
@include('sweet::alert')

</body>
</html>
