@extends('administrador.masteradmin')



@section('content')
<div id="tasacionesApp" class="container" style="font-family: sans-serif;">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/ordenret.png') }}"> Mantenedor de Ordenes de retiro</div>
            <div id="tasacionesSearchNew" class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>

                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav">

                                <li><a href="#">Ordenes de retiro a buscar</a></li>
                        
                            </ul>
                            <form class="navbar-form navbar-left">
                                <div class="form-group">
                                  <input type="text" class="form-control input-sm" placeholder="">
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <div class="navbar-form">
                                <button id="btnNuevaTasacion" type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUpdateOR" v-on:click="rutaCrear">Nuevo</button>
                            </div>
                        </div>
                    </nav> <!-- FIN CAMPO DE BUSQUEDA DEL TASACIONES -->
                   
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_tasaciones" style="font-size: 13px">  
                        <table id="demoOR" class="table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>
                                    N° Proceso
                                </th>
                                <th>
                                    Patente
                                </th>
                                <th>
                                    Compañia
                                </th>
                                <th>
                                N° Siniestro
                                </th>
                                <th>
                                Marca
                                </th>
                                <th>
                                Modelo
                                </th>
                                <th>
                                Año
                                </th>
                                <th>
                                Transportista
                                </th>
                                <th>
                                Taller
                                </th>
                                <th>
                                Dias en Retiro
                                </th>
                                <th>
                                Estado
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr >
                                            <td>                                    
                                                00010361
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10361', 'O.R. NUEVA', '0', 
                                                        '19/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010360
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10360', 'O.R. NUEVA', '0', 
                                                        '19/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010359
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10359', 'O.R. NUEVA', '0', 
                                                        '19/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010358
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10358', 'O.R. NUEVA', '0', 
                                                        '19/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010354
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10354', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010346
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10346', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010345
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10345', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010344
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10344', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010343
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                   <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10343', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row" style="display: none;">
                                            <td>                                    
                                                00010342
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10342', 'O.R. NUEVA', '0', 
                                                        '13/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00010333
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10333', 'O.R. NUEVA', '0', 
                                                        '08/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00010332
                                            </td>
                                            <td>
                                                ACXD95
                                            </td>
                                            <td>
                                                CARDIF
                                            </td>
                                            <td>
                                                123123123
                                            </td>
                                            <td>
                                                TOYOTA
                                            </td>
                                            <td>
                                                COROLLA 1.6
                                            </td>
                                            <td>
                                                1993
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10332', 'TASACION', '3', 
                                                        '08/09/2017', 'ACXD95', 
                                                        '123123123', '', '178', 'COROLLA 1.6', '13987', 
                                                        '1993', 'STATION WAGON', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '10040', '6345629', 'jjlion79@gmail.com', 'javier', 
                                                        'salazar', '10060');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00010331
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10331', 'O.R. NUEVA', '0', 
                                                        '08/09/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00010330
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10330', 'O.R. NUEVA', '0', 
                                                        '31/08/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00010327
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                
                                            </td>
                                            <td>
                                                0
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                Sin Asignar
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">0</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-danger">OR NUEVA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '10327', 'O.R. NUEVA', '0', 
                                                        '19/06/2017', '', 
                                                        '', '', '0', '', '0', 
                                                        '0', '', '', '', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '999', '999', '0', '0', '', '', 
                                                        '', '0');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00000332
                                            </td>
                                            <td>
                                                VHET34
                                            </td>
                                            <td>
                                                CARDIF
                                            </td>
                                            <td>
                                                5435847538
                                            </td>
                                            <td>
                                                ACADIAN
                                            </td>
                                            <td>
                                                BEAUMONT
                                            </td>
                                            <td>
                                                1976
                                            </td>
                                            <td>
                                                TRANSPORTITSTA 2
                                            </td>
                                            <td>
                                                TALLER 1
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">12</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-success">OR ASIGNADA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '332', 'O. R. ASIGNADA', '3', 
                                                        '09/06/2017', 'VHET34', 
                                                        '5435847538', 'JOSE GONZALEZ', '5', 'BEAUMONT', '1', 
                                                        '1976', 'AUTOMOVIL', 'blanco', '33', '', '',
                                                        '', '', '', '', 'False', 'False', 
                                                        '0', '5000000', '0', '0',
                                                        '1', '2', '44', '6000000', 'gcribillero@gmail.com', 'Ginos C', 
                                                        '92790451', '63');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr><tr role="row">
                                            <td>                                    
                                                00000324
                                            </td>
                                            <td>
                                                DYWJ-18
                                            </td>
                                            <td>
                                                CARDIF
                                            </td>
                                            <td>
                                                165465116846848
                                            </td>
                                            <td>
                                                KIA MOTORS
                                            </td>
                                            <td>
                                                MORNING EX 1.1 AUT DH AB
                                            </td>
                                            <td>
                                                2006
                                            </td>
                                            <td>
                                                TRANSPORTITSTA 2
                                            </td>
                                            <td>
                                                TALLER 3
                                            </td>
                                            <td>
                                                <div class="retirementDays">
                                                    <label class="label label-danger">110</label>
                                                </div>                                    
                                            </td>
                                            <td>
                                                <div class="status">
                                                    <label class="label label-success">OR ASIGNADA</label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="btnDetails" class="btn-link" style="width:80px;" onclick="CreateOrUpdate(false, '324', 'O. R. ASIGNADA', '3', 
                                                        '12/04/2017', 'DYWJ-18', 
                                                        '165465116846848', 'LIQUIDADOR', '1', 'MORNING EX 1.1 AUT DH AB', '7116', 
                                                        '2006', 'AUTOMOVIL', 'plata', '34', '3523523f523v5235', '3523523f523v5235',
                                                        '3523523f523v5235', '3523523f523v5235', '1751119330-2', 'test', 'False', 'False', 
                                                        '0', '0', '0', '0',
                                                        '3', '2', '39', '12312311', 'jopsuey@gmail.com', 'www', 
                                                        'w', '57');">
                                                    VER
                                                </button>
                                            </td>
                                        </tr></tbody>
                    </table>
                    
                    </div>

                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateOR" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:95%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title "><b id="titulomodal">Detalles de la OR</b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="container">
                                    <div class="row">{{-- Inicio Tab row --}}
                                        <div class="col-md-12"> 
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#vehiculo" aria-controls="vehiculo" role="tab" data-toggle="tab">Vehiculo</a></li>
                                                    <li role="presentation"><a href="#traslado" aria-controls="traslado" role="tab" data-toggle="tab">Traslado</a></li>
                                                    <li role="presentation"><a href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab">Archivos</a></li>
                                                    <li role="presentation"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Historial</a></li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content" style="font-size: 11px">
                                                    <div role="tabpanel" class="tab-pane active" id="vehiculo">
                                                        
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingVehiculos" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseVehiculos" aria-expanded="true" aria-controls="collapseOne">
                                                                  Ficha<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="collapseVehiculos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingVehiculos">
                                                                <div class="panel-body">
                                                                    <form class="form-horizontal" action="/action_page.php">
                                                                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="estado">Estado:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="estado" name="estado" placeholder="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="patente">Patente:</label>
                                                                                <div class="col-sm-4">          
                                                                                    <input type="text" class="form-control input-sm" id="patente" placeholder="" name="patente">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="marca">Marca:</label>
                                                                                <div class="col-sm-7">
                                                                                    <select class="form-control input-sm" id="marca" name="marca" >
                                                                                    <option value="">Seleccione</option>
                                                                                    <option value="5">ACADIAN</option>
                                                                                    <option value="6">ACURA</option>
                                                                                    <option value="203">AGRALE</option>
                                                                                    <option value="7">ALFA ROMEO</option>
                                                                                    <option value="8">AMERICAN MOTORS</option>
                                                                                    <option value="204">AMERITRUCK</option>
                                                                                    <option value="9">APRILIA</option>
                                                                                    <option value="10">ARO</option>
                                                                                    <option value="205">ASIA</option>
                                                                                    <option value="11">ASIA MOTORS</option>
                                                                                    <option value="12">ASTON MARTIN</option>
                                                                                    <option value="13">AUDI</option>
                                                                                    <option value="14">AUSTIN</option>
                                                                                    <option value="206">AUTOCAR</option>
                                                                                    <option value="15">AUTORRAD</option>
                                                                                    <option value="207">B.M.C.</option>
                                                                                    <option value="16">BAIC</option>
                                                                                    <option value="17">BAJAJ</option>
                                                                                    <option value="18">BASHAN</option>
                                                                                    <option value="208">BAW</option>
                                                                                    <option value="209">BEDFORD</option>
                                                                                    <option value="19">BEIGING</option>
                                                                                    <option value="210">BELAVTOMAZ</option>
                                                                                    <option value="20">BENELLI</option>
                                                                                    <option value="21">BENTLEY</option>
                                                                                    <option value="22">BENYI</option>
                                                                                    <option value="23">BENZHOU</option>
                                                                                    <option value="211">BERLIET</option>
                                                                                    <option value="24">BIGFOOT</option>
                                                                                    <option value="25">BIMOTA</option>
                                                                                    <option value="26">BMW</option>
                                                                                    <option value="27">BOATIAN</option>
                                                                                    <option value="212">BORGWARD</option>
                                                                                    <option value="28">BRILLIANCE</option>
                                                                                    <option value="29">BUELL</option>
                                                                                    <option value="30">BUICK</option>
                                                                                    <option value="31">BULTACO</option>
                                                                                    <option value="213">BUSSING</option>
                                                                                    <option value="32">BYD</option>
                                                                                    <option value="33">CADILLAC</option>
                                                                                    <option value="34">CAGIVA</option>
                                                                                    <option value="214">CAKY-CHANGAN</option>
                                                                                    <option value="215">CAMC</option>
                                                                                    <option value="35">CATERHAM</option>
                                                                                    <option value="36">CFMOTO</option>
                                                                                    <option value="37">CHANGAN</option>
                                                                                    <option value="38">CHANGHE</option>
                                                                                    <option value="39">CHERY</option>
                                                                                    <option value="2">CHEVROLET</option>
                                                                                    <option value="40">CHRYSLER</option>
                                                                                    <option value="41">CITROEN</option>
                                                                                    <option value="216">COMIL GALLEGIANTE</option>
                                                                                    <option value="42">COMMER</option>
                                                                                    <option value="43">DACIA</option>
                                                                                    <option value="44">DAELIM</option>
                                                                                    <option value="45">DAEWOO</option>
                                                                                    <option value="217">DAF</option>
                                                                                    <option value="46">DAIHATSU</option>
                                                                                    <option value="47">DATSUN</option>
                                                                                    <option value="48">DAYUN</option>
                                                                                    <option value="218">DIAMOND</option>
                                                                                    <option value="219">DIMEX</option>
                                                                                    <option value="49">DODGE</option>
                                                                                    <option value="50">DONGFENG</option>
                                                                                    <option value="51">DUCATI</option>
                                                                                    <option value="52">DUNNA</option>
                                                                                    <option value="53">DYNA</option>
                                                                                    <option value="220">EBRO</option>
                                                                                    <option value="54">EUROMOT</option>
                                                                                    <option value="55">F.S.O.</option>
                                                                                    <option value="221">FARGO</option>
                                                                                    <option value="56">FAW</option>
                                                                                    <option value="222">FBM</option>
                                                                                    <option value="223">FEDERAL</option>
                                                                                    <option value="57">FERRARI</option>
                                                                                    <option value="58">FIAT</option>
                                                                                    <option value="59">FLSTF</option>
                                                                                    <option value="60">FORD</option>
                                                                                    <option value="224">FORD TAUNUS</option>
                                                                                    <option value="225">FORD THAMES</option>
                                                                                    <option value="61">FOTON</option>
                                                                                    <option value="62">FOX</option>
                                                                                    <option value="226">FREIGHTLINER</option>
                                                                                    <option value="63">FXA</option>
                                                                                    <option value="64">G.M.C.</option>
                                                                                    <option value="65">GAC GONOW</option>
                                                                                    <option value="66">GARELLI</option>
                                                                                    <option value="67">GASGAS</option>
                                                                                    <option value="68">GEELY</option>
                                                                                    <option value="276">GELLY</option>
                                                                                    <option value="69">GILERA</option>
                                                                                    <option value="227">GOLDEN DRAGON</option>
                                                                                    <option value="70">GREAT WALL</option>
                                                                                    <option value="228">GREYHOUND</option>
                                                                                    <option value="71">GUZZI</option>
                                                                                    <option value="72">HAFEI</option>
                                                                                    <option value="73">HAIMA</option>
                                                                                    <option value="74">HAO JUE</option>
                                                                                    <option value="75">HARLEY</option>
                                                                                    <option value="76">HARLEY-DAVIDSON</option>
                                                                                    <option value="77">HARTFORD</option>
                                                                                    <option value="78">HAVAL</option>
                                                                                    <option value="229">HENSCHEL</option>
                                                                                    <option value="79">HENSIM</option>
                                                                                    <option value="80">HERO-PUCH</option>
                                                                                    <option value="230">HIGER</option>
                                                                                    <option value="81">HILLMAN</option>
                                                                                    <option value="231">HINO</option>
                                                                                    <option value="82">HONDA</option>
                                                                                    <option value="232">HUANGHAI</option>
                                                                                    <option value="83">HUSABERG</option>
                                                                                    <option value="84">HUSQVARNA</option>
                                                                                    <option value="85">HYOSUNG</option>
                                                                                    <option value="86">HYUNDAI</option>
                                                                                    <option value="87">INDIAN</option>
                                                                                    <option value="88">INFINITI</option>
                                                                                    <option value="89">INTERNATIONAL</option>
                                                                                    <option value="90">ISUZU</option>
                                                                                    <option value="233">IVECO</option>
                                                                                    <option value="91">JAC</option>
                                                                                    <option value="92">JAGUAR</option>
                                                                                    <option value="93">JAWA</option>
                                                                                    <option value="234">JBC JINBEI</option>
                                                                                    <option value="94">JEEP</option>
                                                                                    <option value="95">JIANSHE</option>
                                                                                    <option value="235">JIE FANG</option>
                                                                                    <option value="96">JINBEI</option>
                                                                                    <option value="97">JMC</option>
                                                                                    <option value="236">KAMAZ</option>
                                                                                    <option value="98">KAWASAKI</option>
                                                                                    <option value="99">KAYAK</option>
                                                                                    <option value="100">KEEWAY</option>
                                                                                    <option value="237">KENWORTH</option>
                                                                                    <option value="238">KIA</option>
                                                                                    <option value="1">KIA MOTORS</option>
                                                                                    <option value="239">KING LONG</option>
                                                                                    <option value="101">KINLON</option>
                                                                                    <option value="102">KTM</option>
                                                                                    <option value="103">KYMCO</option>
                                                                                    <option value="104">LADA</option>
                                                                                    <option value="105">LAMBORGHINI</option>
                                                                                    <option value="106">LAMBRETTA</option>
                                                                                    <option value="107">LANCIA</option>
                                                                                    <option value="108">LAND ROVER</option>
                                                                                    <option value="109">LANDWIND</option>
                                                                                    <option value="110">LEXUS</option>
                                                                                    <option value="240">LEYLAND</option>
                                                                                    <option value="241">LIAZ</option>
                                                                                    <option value="111">LIFAN</option>
                                                                                    <option value="112">LINCOLN</option>
                                                                                    <option value="113">LML</option>
                                                                                    <option value="114">LONCIN</option>
                                                                                    <option value="115">LOTUS</option>
                                                                                    <option value="116">LUOJIA</option>
                                                                                    <option value="242">MACK</option>
                                                                                    <option value="243">MAGIRUS</option>
                                                                                    <option value="244">MAGIRUS DEUTZ</option>
                                                                                    <option value="117">MAHINDRA</option>
                                                                                    <option value="118">MAICO</option>
                                                                                    <option value="245">MAN</option>
                                                                                    <option value="246">MARCOPOLO</option>
                                                                                    <option value="119">MASERATI</option>
                                                                                    <option value="247">MAXUS</option>
                                                                                    <option value="3">MAZDA</option>
                                                                                    <option value="120">MCLAREN</option>
                                                                                    <option value="121">MERCEDES BENZ</option>
                                                                                    <option value="122">MERCURY</option>
                                                                                    <option value="123">MG</option>
                                                                                    <option value="124">MINI</option>
                                                                                    <option value="125">MITSUBISHI</option>
                                                                                    <option value="126">MONTELLI</option>
                                                                                    <option value="127">MONTESA</option>
                                                                                    <option value="128">MORGAN</option>
                                                                                    <option value="129">MORRIS</option>
                                                                                    <option value="130">MOTO BECANE</option>
                                                                                    <option value="131">MOTORRAD</option>
                                                                                    <option value="132">MSK</option>
                                                                                    <option value="133">MV AGUSTA</option>
                                                                                    <option value="4">NISSAN</option>
                                                                                    <option value="134">NSU</option>
                                                                                    <option value="248">O.M.</option>
                                                                                    <option value="135">ODES</option>
                                                                                    <option value="136">OLDSMOBILE</option>
                                                                                    <option value="137">OPEL</option>
                                                                                    <option value="138">OSSA</option>
                                                                                    <option value="249">PEGASO</option>
                                                                                    <option value="139">PEUGEOT</option>
                                                                                    <option value="140">PGO</option>
                                                                                    <option value="141">PIAGGIO</option>
                                                                                    <option value="142">PIONNER</option>
                                                                                    <option value="143">PLYMOUTH</option>
                                                                                    <option value="144">POLARIS</option>
                                                                                    <option value="145">POLSKI FIAT</option>
                                                                                    <option value="146">PONTIAC</option>
                                                                                    <option value="147">PORSCHE</option>
                                                                                    <option value="148">PROTON</option>
                                                                                    <option value="250">PULLMAN STD AMERIC</option>
                                                                                    <option value="149">PUMA</option>
                                                                                    <option value="150">REGAL RAPTOR</option>
                                                                                    <option value="151">RENAULT</option>
                                                                                    <option value="251">RENAULT(Berliet)</option>
                                                                                    <option value="252">REO</option>
                                                                                    <option value="152">RIZATO</option>
                                                                                    <option value="153">ROLLS ROYCE</option>
                                                                                    <option value="253">ROMAN</option>
                                                                                    <option value="154">ROVER</option>
                                                                                    <option value="254">S/M</option>
                                                                                    <option value="155">SAAB</option>
                                                                                    <option value="156">SACHS</option>
                                                                                    <option value="157">SAEHAN</option>
                                                                                    <option value="158">SAMSUNG</option>
                                                                                    <option value="159">SANFU</option>
                                                                                    <option value="160">SANLG</option>
                                                                                    <option value="161">SANYA</option>
                                                                                    <option value="162">SANYANG SYM</option>
                                                                                    <option value="255">SCANIA</option>
                                                                                    <option value="163">SEAT</option>
                                                                                    <option value="256">SETRA (Mercedes Benz)</option>
                                                                                    <option value="164">SG</option>
                                                                                    <option value="257">SHACMAN</option>
                                                                                    <option value="258">SHENFENG</option>
                                                                                    <option value="259">SHENYANG JINBEI</option>
                                                                                    <option value="165">SHINERAY</option>
                                                                                    <option value="166">SIMCA</option>
                                                                                    <option value="260">SINOTRUK</option>
                                                                                    <option value="167">SKODA</option>
                                                                                    <option value="168">SKYGO</option>
                                                                                    <option value="169">SMA</option>
                                                                                    <option value="170">SPITZ</option>
                                                                                    <option value="171">SSANGYONG</option>
                                                                                    <option value="261">STUDEBAKER</option>
                                                                                    <option value="172">SUBARU</option>
                                                                                    <option value="262">SUNLONG</option>
                                                                                    <option value="173">SUZUKI</option>
                                                                                    <option value="174">TAKASAKI</option>
                                                                                    <option value="175">TATA</option>
                                                                                    <option value="263">TATRA</option>
                                                                                    <option value="176">TM</option>
                                                                                    <option value="177">TORITO</option>
                                                                                    <option value="178">TOYOTA</option>
                                                                                    <option value="179">TRIUMPH</option>
                                                                                    <option value="180">TVS</option>
                                                                                    <option value="181">UNITED MOTORS</option>
                                                                                    <option value="182">URAL</option>
                                                                                    <option value="183">VERONA</option>
                                                                                    <option value="184">VESPA</option>
                                                                                    <option value="264">VOLARE</option>
                                                                                    <option value="185">VOLKSWAGEN</option>
                                                                                    <option value="186">VOLVO</option>
                                                                                    <option value="265">WABCO</option>
                                                                                    <option value="187">WANGYE</option>
                                                                                    <option value="266">WESTERN STAR</option>
                                                                                    <option value="267">WHITE</option>
                                                                                    <option value="188">WILLYS</option>
                                                                                    <option value="189">WOLKEN</option>
                                                                                    <option value="190">XGJAO</option>
                                                                                    <option value="191">XINGYUE</option>
                                                                                    <option value="192">XMOTORS</option>
                                                                                    <option value="193">YAMAHA</option>
                                                                                    <option value="194">YAMAMOTO</option>
                                                                                    <option value="268">YOUYI</option>
                                                                                    <option value="269">YUE JIN</option>
                                                                                    <option value="195">YUGO</option>
                                                                                    <option value="270">YUTONG</option>
                                                                                    <option value="196">ZANELLAS</option>
                                                                                    <option value="271">ZANELLO</option>
                                                                                    <option value="197">ZASTAVA</option>
                                                                                    <option value="198">ZNEN GROUP</option>
                                                                                    <option value="199">ZONGSHEN</option>
                                                                                    <option value="200">ZOTYE</option>
                                                                                    <option value="201">ZUNDAPP</option>
                                                                                    <option value="202">ZX</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="tipo">Tipo:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <select class="form-control input-sm" id="tipo" name="tipo">
                                                                                    <option value="">Seleccione</option>
                                                                                    <option value="2">AUTOMOVIL</option>
                                                                                    <option value="3">STATION WAGON</option>
                                                                                    <option value="4">TODO TERRENO</option>
                                                                                    <option value="5">CAMIONETA</option>
                                                                                    <option value="6">MOTO</option>
                                                                                    <option value="7">MINIBUS</option>
                                                                                    <option value="8">FURGON</option>
                                                                                    <option value="9">MOTOR HOME</option>
                                                                                    <option value="10">CAMIONES</option>
                                                                                    <option value="11">BUSES Y TAXIBUSES</option>
                                                                                    <option value="12">TROLEBUSES</option>
                                                                                    <option value="13">TOLVAS HIDRAULICAS (S/TOMA FUERZA)</option>
                                                                                    <option value="14">ESTANQUES METALICOS FIERRO (SIN CHASSIS)</option>
                                                                                    <option value="15">BASURERO</option>
                                                                                    <option value="16">AMPLIROLL</option>
                                                                                    <option value="17">ESTANQUES METALICOS DE ACERO INOXIDABLE (SIN CHASSIS)</option>
                                                                                    <option value="18">SEMIREMOLQUE CARGA TODO</option>
                                                                                    <option value="19">REMOLQUE CARROCERIA PLANA</option>
                                                                                    <option value="20">SEMIREMOLQUE FORESTAL</option>
                                                                                    <option value="21">SEMIREMOLQUE CARROCERIA PLANA</option>
                                                                                    <option value="22">SEMIREMOLQUE BATEA</option>
                                                                                    <option value="23">CARROCERÍAS TERMICAS CON UNIDAD REFRIGERANTE</option>
                                                                                    <option value="24">CARROCERÍAS TERMICAS</option>
                                                                                    <option value="25">FURGON CARGA GENERAL</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nmotor">Nro. Motor:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nmotor" placeholder="" name="nmotor">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nserie">Nro. Serie:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nserie" placeholder="" name="nserie">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="condicion">Condicionado</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="checkbox" class="" id="condicion" placeholder="" name="condicion" checked="false">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                    <label class="" for="condicion">Desarme</label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                    <input type="checkbox" class="" id="desarme" placeholder="" name="desarme" checked="false">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontacto">Nombre Contacto:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontacto" placeholder="" name="nombcontacto">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="valortraslado">Valor Traslado:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="valortraslado" placeholder="" name="valortraslado">
                                                                                </div>
                                                                            </div>      
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombre">Compañia:</label>
                                                                                <div class="col-sm-7">
                                                                                    <select class="form-control" id="compania" name="compania"><option value="">Seleccione</option>
                                                                                    <option value="3">CARDIF</option>
                                                                                    <option value="4">CHILENA CONSOLIDADA</option>
                                                                                    <option value="7">HDI</option>
                                                                                    <option value="5">MAPFRE</option>
                                                                                    <option value="6">RENTA NACIONAL</option>
                                                                                    <option value="2">SURA</option>
                                                                                    <option value="1">VEDISA</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nsiniestro">Nro. Siniestro:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nsiniestro" placeholder="" name="nsiniestro">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="modelo">Modelo:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="modelo" placeholder="" name="modelo">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="color">Color:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="color" placeholder="" name="color">
                                                                                </div>
                                                                            </div>  
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nchasis">Nro. Chasis:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nchasis" placeholder="" name="nchasis">
                                                                                </div>
                                                                            </div>  
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="rutprop">Rut Propietario:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="rutprop" placeholder="" name="rutprop">
                                                                                </div>
                                                                            </div>   
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="tasacion">Tasación:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="tasacion" placeholder="" name="tasacion">
                                                                                </div>
                                                                            </div>  
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="tlfcontacto">Teléfono Contacto:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="tlfcontacto" placeholder="" name="tlfcontacto">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="valorfactcia">Valor a facturar CIA. Seguro:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="valorfactcia" placeholder="" name="valorfactcia">
                                                                                </div>
                                                                            </div>     
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="fecha">Fecha:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="fecha" placeholder="" name="fecha" value="@php echo date('d-m-Y') @endphp" readonly>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="liquidador">Liquidador:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="liquidador" placeholder="" name="liquidador">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="anio">Año:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="anio" placeholder="" name="anio">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="combustible">Costumble:</label>
                                                                                <div class="col-sm-4">          
                                                                                    <select id="combustible" name="combustible" class="form-control input-sm" style="width: 210px">
                                                                                        <option value="33">Bencina</option>
                                                                                        <option value="34">Diesel</option>
                                                                                        <option value="35">Eléctrico</option>
                                                                                    </select>

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="vim">VIM:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="vim" placeholder="" name="vim">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombreprop">Nombre Propietario:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="nombreprop" placeholder="" name="nombreprop">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="emailcontac">Email Contacto:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="emailcontac" placeholder="" name="emailcontac">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="valorminrem">Valor Mínimo Remate:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="valorminrem" placeholder="" name="valorminrem">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="valorindem">Valor Indemnización:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="valorindem" placeholder="" name="valorindem">
                                                                                </div>
                                                                            </div>
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    </form>



                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingImg" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseImg" aria-expanded="false" aria-controls="collapseTwo">
                                                                  Imagenes<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="collapseImg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingImg">
                                                                <div class="panel-body">
                                                                    <form class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="archivo">Seleccionar archivo:</label>
                                                                            <input type="file" class="form-control" id="archivo" name="archivo">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <button type="submit" class="btn btn-success">Cargar</button>
                                                                        </div>
                                                                    </form>
                                                                    <br>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">

                                                                            <div class='row'>
                                                                                <div class='col-md-12'>
                                                                                  <div class="carousel slide media-carousel" id="media">
                                                                                    <div class="carousel-inner">
                                                                                      <div class="item  active">
                                                                                        <div class="row"> 
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 0.JPG') }}"></a>
                                                                                          </div> 
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 2.JPG') }}"></a>
                                                                                          </div>          
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 3.JPG') }}"></a>
                                                                                          </div>
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 4.JPG') }}"></a>
                                                                                          </div>        
                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="item">
                                                                                        <div class="row">
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 5.JPG') }}"></a>
                                                                                          </div> 
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 6.JPG') }}"></a>
                                                                                          </div>          
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 7.JPG') }}"></a>
                                                                                          </div>
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 8.JPG') }}"></a>
                                                                                          </div>        
                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="item">
                                                                                        <div class="row">
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 9.JPG') }}"></a>
                                                                                          </div>  
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 5.JPG') }}"></a>
                                                                                          </div>          
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 11.JPG') }}"></a>
                                                                                          </div>
                                                                                          <div class="col-md-3">
                                                                                            <a class="thumbnail" href="#"><img alt="" src="{{ asset('storage/images/vehiculo/DB-5170 10.JPG') }}"></a>
                                                                                          </div>      
                                                                                        </div>
                                                                                      </div>
                                                                                    </div>
                                                                                    <a data-slide="prev" href="#media" class="pull-left "><img src="{{ asset('storage/images/prev.png') }}"></a>
                                                                                    <a data-slide="next" href="#media" class="pull-right"><img src="{{ asset('storage/images/next.png') }}"></a>
                                                                                  </div>                          
                                                                                </div>
                                                                            </div>
                                                                        </div> {{-- fin panel body --}}
                                                                    </div>


                                                                </div>
                                                            </div>
                                                          </div>
                                                          <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingThree" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseObs" aria-expanded="false" aria-controls="collapseThree">
                                                                    Observaciones<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="collapseObs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                              <div class="panel-body">
                                                                    <fieldset class="fieldsetPrincipal">
                                                                        <table>
                                                                            <tbody><tr>                
                                                                                <td>
                                                                                    Observación: &nbsp;&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <textarea id="observation" cols="40" rows="5" maxlength="100" style="background-color:#dee6eb;"></textarea>
                                                                                </td>
                                                                                <td>
                                                                                   &nbsp;&nbsp;Observación privada&nbsp;&nbsp;&nbsp;&nbsp;<input id="chkPrivateObservation" name="chkPrivateObservation" type="checkbox">
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="btnObservation" class="btn btn-success">GRABAR OBSERVACIÓN</button>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </fieldset>
                                                              </div>
                                                            </div>
                                                          </div>

                                                          
                                                        </div><!--  FIN ACORDEON VEHICULOS -->
                                                    </div><!--  FIN TAB VEHICULOS -->
                                                    
                                                    <div role="tabpanel" class="tab-pane" id="traslado">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingTaller" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTaller" aria-expanded="true" aria-controls="collapseOne">
                                                                  Taller<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="collapseTaller" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingVehiculos">
                                                                <div class="panel-body">
                                                                    <form class="form-horizontal" action="/action_page.php">
                                                                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="estado">Nombre:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="estado" name="estado" placeholder="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="patente">Direccion:</label>
                                                                                <div class="col-sm-4">          
                                                                                    <input type="text" class="form-control input-sm" id="patente" placeholder="" name="patente">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nmotor">Comuna:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nmotor" placeholder="" name="nmotor">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nserie">Telefono:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nserie" placeholder="" name="nserie">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontacto">Nombre Contacto:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontacto" placeholder="" name="nombcontacto">
                                                                                </div>
                                                                            </div>     
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontacto">RUT:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontacto" placeholder="" name="nombcontacto">
                                                                                </div>
                                                                            </div> 
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nsiniestro">Region:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nsiniestro" placeholder="" name="nsiniestro">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="modelo">Ciudad:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="modelo" placeholder="" name="modelo">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="color">Email:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="color" placeholder="" name="color">
                                                                                </div>
                                                                            </div>  
                                                                            
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    </form>



                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingTransp" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#transportistaCollapse" aria-expanded="false" aria-controls="collapseThree">
                                                                    Transportista<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="transportistaCollapse" class="" role="tabpanel" aria-labelledby="headingTransp">
                                                              <div class="panel-body">
                                                                    <form class="form-horizontal" action="/action_page.php">
                                                                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="estado">Nombre:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="estado" name="estado" placeholder="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="patente">Direccion:</label>
                                                                                <div class="col-sm-4">          
                                                                                    <input type="text" class="form-control input-sm" id="patente" placeholder="" name="patente">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nmotor">Comuna:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nmotor" placeholder="" name="nmotor">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nserie">Telefono:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nserie" placeholder="" name="nserie">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontacto">Nombre Contacto:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontacto" placeholder="" name="nombcontacto">
                                                                                </div>
                                                                            </div>     
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontacto">RUT:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontacto" placeholder="" name="nombcontacto">
                                                                                </div>
                                                                            </div> 
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nsiniestro">Region:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nsiniestro" placeholder="" name="nsiniestro">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="modelo">Ciudad:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="modelo" placeholder="" name="modelo">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="color">Email:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="color" placeholder="" name="color">
                                                                                </div>
                                                                            </div>  
                                                                            
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    </form>
                                                              </div>
                                                            </div>
                                                          </div>

                                                    </div>{{-- FIN TAB TRASLADO --}}
                                                    <div role="tabpanel" class="tab-pane" id="archivos">
                                                        <div class="panel panel-default">
                                                            
                                                           
                                                                <div class="panel-body">
                                                                    <form class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="archivo">Seleccionar archivo:</label>
                                                                            <input type="file" class="form-control" id="archivo" name="archivo">
                                                                        </div>
                                                                        &nbsp; &nbsp; &nbsp;

                                                                        <div class="form-group">
                                                                            <label for="archivo">Observacion:</label>
                                                                            <input type="text" class="form-control" id="obserfile" name="obserfile">
                                                                        </div>
                                                                        &nbsp; &nbsp; &nbsp;
                                                                        <div class="form-group">
                                                                            <button type="submit" class="btn btn-success">Cargar</button>
                                                                        </div>
                                                                    </form>
                                                                    <br>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">

                                                                            <div class='row'>
                                                                                <div class='col-md-12'>
                                                                                  <table class="table table-striped table-hover" >
                                                                                      <thead>
                                                                                          <tr>
                                                                                              <th>Nombre</th>
                                                                                              <th>Empresa</th>
                                                                                              <th>Fecha Creacion</th>
                                                                                              <th>Creado por</th>
                                                                                              <th>Creado Observación</th>
                                                                                              <th>Archivo</th>
                                                                                              <th>Acciones</th>
                                                                                          </tr>
                                                                                      </thead>
                                                                                      <tbody>
                                                                                          <tr>
                                                                                            <td>
                                                                                                09062017084638.pdf
                                                                                            </td>
                                                                                            <td>
                                                                                                VEDISA
                                                                                            </td>
                                                                                            <td>
                                                                                                09-06-2017 8:46:40
                                                                                            </td>
                                                                                            <td>
                                                                                                Roberto Quiroz
                                                                                            </td>
                                                                                            <td>
                                                                                                Generación automática de Orden de Retiro
                                                                                            </td>
                                                                                            <td align="center">
                                                                                                <img src="{{ asset('storage/images/pdf.png') }}">
                                                                                            </td>
                                                                                            <td align="center">
                                                                                                <button class="btn btn-danger btn-sm">Eliminar</button>
                                                                                            </td>
                                                                                        </tr>
                                                                                      </tbody>
                                                                                  </table>
                                                                                </div>
                                                                            </div>
                                                                        </div> {{-- fin panel body --}}
                                                                    </div>


                                                                </div>
                                                            
                                                          </div>

                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="historial">
                                                        <div class='row'>
                                                                                <div class='col-md-12'>
                                                                                  <table class="table table-striped table-hover" >
                                                                                      <thead>
                                                                                          <tr>
                                                                                              <th>Fecha</th>
                                                                                              <th>Empresa</th>
                                                                                              <th>Creado por</th>
                                                                                              <th>Descripción</th>
                                                                                          </tr>
                                                                                      </thead>
                                                                                      <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                19-09-2017 0:52:05
                                                                                            </td> 
                                                                                            <td>
                                                                                                VEDISA
                                                                                            </td> 
                                                                                            <td>
                                                                                                Roberto Quiroz
                                                                                            </td>
                                                                                            <td>
                                                                                                Ingreso de imagen
                                                                                            </td>                                                                                                                      
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                09-06-2017 8:46:41
                                                                                            </td> 
                                                                                            <td>
                                                                                                VEDISA
                                                                                            </td> 
                                                                                            <td>
                                                                                                Roberto Quiroz
                                                                                            </td>
                                                                                            <td>
                                                                                                Asignación de Orden de Retiro
                                                                                            </td>                                                                                                                      
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                09-06-2017 8:26:47
                                                                                            </td> 
                                                                                            <td>
                                                                                                VEDISA
                                                                                            </td> 
                                                                                            <td>
                                                                                                Roberto Quiroz
                                                                                            </td>
                                                                                            <td>
                                                                                                Ingreso de Tasación
                                                                                            </td>                                                                                                                      
                                                                                        </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </div>
                                                                            </div>
                                                    </div>
                                               
                                            </div>{{-- Fin card tab --}}
                                        </div>{{-- Fin colmuna tab --}}
                                    </div>{{-- Fin row tab --}}
                                    </div>{{-- Fin tab Container --}}
                                    
                                </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div> <!-- Fin - Modal nueva OR -->                  
            </div>
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
@endsection
@section('scriptsmodules')
<script src="js/ordretiros.js"></script>
@endsection