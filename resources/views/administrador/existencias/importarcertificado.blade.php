<!-- Modal -->
<div id="importarCertificadoModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:70%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Importar Certificado</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        
                        <textarea id="datosCertificados" rows="15" cols="70"></textarea>
                        <br>
                      
                        <button id="btnImportarCert" class="btn btn-danger pull-right">Importar</button>
                    </div>

                </div>
                
            
                
          </div>

         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>

</div> <!-- Fin - Modal nuevo usuario --> 
