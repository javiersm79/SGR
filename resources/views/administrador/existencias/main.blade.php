@extends('administrador.masteradmin')
@section('stylemodules')

<link href="css/owl.carousel.min.css" rel="stylesheet" />
<link href="css/owl.theme.default.min.css" rel="stylesheet" />
<link href="css/jquery.fancybox.min.css" rel="stylesheet" />
<link href="css/easy-autocomplete.min.css" rel="stylesheet" />
<link href="css/easy-autocomplete.themes.min.css" rel="stylesheet" />
@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/vehicles.png') }}"> Mantenedor de Existencias</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.existencias.formulario')
        @include('administrador.existencias.importarcertificado')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateExistencias" v-on:click="crearIngreso">Ingreso por asistencia</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
        
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de Existencias
                    <div class="pull-right">
                        <select id="filtrarEstado" class="input-sm" style="width: 200px; border: black" >
                            <optgroup label="Bodega">
                            <option value="Bodega-Normal">Bodega Normal</option>
                            <option value="Bodega-Para desarme">Bodega Para desarme</option>
                            <option value="Bodega-Prohibiciones">Bodega Prohibiciones</option>
                            </optgroup>
                            <optgroup label="Propuesto">
                            <option value="Propuesto-Sin Revisar">Propuesto Sin Revisar</option>
                            <option value="Propuesto-Proceso Desarme">Propuesto Proceso Desarme</option>
                            <option value="Propuesto-Prohibiciones">Propuesto Con Prohibiciones</option>
                            <option value="Propuesto-Listo para Remate">Propuesto Listo para Remate</option>
                            <option value="Propuesto-Otros Problemas">Propuesto Otros Problemas</option>
                            </optgroup>
                            <optgroup label="Remate">
                                <option value="Remate-Vehiculo">Remate Vehiculo</option>
                                <option value="Remate-Desarme">Remate Desarme</option>
                                </optgroup>
                                <optgroup label="Rechazado">
                                <option value="Rechazado-Sin Retirar">Rechazado-Sin Retirar</option>
                                <option value="Rechazado-Retirado">Rechazado-Retirado</option>
                            </optgroup>
                        </select>
                        <button id="limpiarfiltro" class="btn btn-dafault btn-sm" style="border-color: grey">Limpiar</button>
                    </div>

                </div>
                   
                <div class="panel-body">
                    <div id="listado_existencias">  
                        <listadoexistencias class="listatabla" @view-ingreso="getIngreso"></listadoexistencias>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/existencias.js"></script>

<script src="js/jquery.fancybox.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.easy-autocomplete.min.js"></script>


@endsection

 