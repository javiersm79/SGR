<!-- Modal -->
<div id="newUpdateExistencias" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles de la Ingreso a Bodega</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formExist" ref="formExist" method="post" class="form-horizontal" action="/existenciaResource" v-on:submit.prevent>
                            {{csrf_field()}}
                       
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idingreso" placeholder="" name="idordenesretiro" value="{{ old('idordenesretiro') }}">
                        <input type="hidden" class="form-control" id="patentecargada" placeholder="" name="patentecargada" value="{{ old('patentecargada') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">

                        <div class="row"> 
                            <div class="col-md-12" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE OR -->
                                <!-- Nav tabs -->
                                <div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" id="tabVehiculo" class="active"><a href="#vehiculo" aria-controls="vehiculo" role="tab" data-toggle="tab">Vehiculo</a></li>
                                        
                                        <li role="presentation" class="disabledTab collapseTab"><a href="#traslado" aria-controls="traslado" role="tab" data-toggle="tab">Traslado</a></li>
                                        <li role="presentation" class="disabledTab collapseTab"><a href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab">Archivos</a></li>
                                        <li role="presentation" class="disabledTab collapseTab"><a href="#bodega" aria-controls="bodega" role="tab" data-toggle="tab">Inventario</a></li>
                                        <li role="presentation" class="disabledTab collapseTab"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Historial</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="font-size: 11px">
                                        <div role="tabpanel" class="tab-pane active" id="vehiculo">
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingVehiculos" style="background-color: #ededed">
                                                      <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseVehiculos" aria-expanded="true" aria-controls="collapseOne">
                                                          Ficha<span class="caret"></span> <span class="alert-danger tipoOR">Ingreso Nuevo</span>
                                                        </a>

                                                      </h4>

                                                    </div>
                                                    <div id="collapseVehiculos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingVehiculos">
                                                        <div class="panel-body">
                                                           
                                                                <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="estado">Estado:</label>
                                                                        <div class="col-md-3" > 
                                                                            <select id="estado" name="estado" class="form-control input-sm disabledTab collapseTab" style="width: 200px">
                                                                                <optgroup label="Bodega">
                                                                                <option value="Bodega-Normal" selected>Bodega Normal</option>
                                                                                <option value="Bodega-Para desarme">Bodega Para desarme</option>
                                                                                <option value="Bodega-Prohibiciones">Bodega Prohibiciones</option>
                                                                                </optgroup>
                                                                                <optgroup label="Propuesto">
                                                                                <option value="Propuesto-Sin Revisar">Propuesto Sin Revisar</option>
                                                                                <option value="Propuesto-Proceso Desarme">Propuesto Proceso Desarme</option>
                                                                                <option value="Propuesto-Prohibiciones">Propuesto Con Prohibiciones</option>
                                                                                <option value="Propuesto-Listo para Remate">Propuesto Listo para Remate</option>
                                                                                <option value="Propuesto-Otros Problemas">Propuesto Otros Problemas</option>
                                                                                </optgroup>
                                                                                <optgroup label="Remate">
                                                                                 <option value="Remate-Vehiculo">Remate Vehiculo</option>
                                                                                 <option value="Remate-Desarme">Remate Desarme</option>
                                                                                 </optgroup>
                                                                                 <optgroup label="Rechazado">
                                                                                 <option value="Rechazado-Sin Retirar">Rechazado-Sin Retirar</option>
                                                                                 <option value="Rechazado-Retirado">Rechazado-Retirado</option>
                                                                                </optgroup>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5 text-danger" for="patente">DATOS DEL VEHICULO</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="patente"><strong class="text-danger">(*)</strong> Patente:</label>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm requerido" id="patente" placeholder="" name="patente" value="{{ old('patente') }}" onkeyup="this.value = this.value.toUpperCase();">
                                                                                {{-- <input type="text" class="form-control input-sm" id="idvehiculo" placeholder="idvehiculo" name="idvehiculo" value="{{ old('idvehiculo') }}"> --}}
                                                                                <span class="input-group-btn ">
                                                                                <button class="btn btn-info btn-sm" type="button" @click="buscarVehiculo()"><span class="fas fa-search"></span></button>
                                                                                </span>
                                                                                <button class="btn btn-sm btn-danger pull-right"  data-toggle="modal" data-target="#importarCertificadoModal" ><span class="fas fa-upload"></button>
                                                                                @if ($errors->has('patente'))
                                                                                <span class="help-block">
                                                                                    <strong class="text-danger">{{ $errors->first('patente') }}</strong>
                                                                                </span>

                                                                                @endif
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="marca">Marca:</label>
                                                                        <div class="col-sm-7">
                                                                            <select  class="form-control" id="marca" name="marca"style="width: 200px">
                                                                            <option value="0">Seleccione Marca</option>
                                                                            <option v-for="marca in marcasList" 
                                                                            :value="marca.id" :selected="marca.id=={{json_encode(old('marca'))}}?true : false">@{{ marca.nombre }}</option>

                                                                            </select>
                                                                            @if ($errors->has('marca'))
                                                                            <span class="help-block">
                                                                                <strong class="text-danger">{{ $errors->first('marca') }}</strong>
                                                                            </span>

                                                                            @endif

                                                                        </div>

                                                                    </div>

                                                                     <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="tipo"><strong class="text-danger"></strong> Tipo:</label>
                                                                        <div class="col-sm-7" >
                                                                            <select  class="form-control" id="tipo" name="tipo"style="width: 200px">
                                                                                <option value="0">Seleccione Tipo</option>
                                                                            <option v-for="tipo in tiposList" 
                                                                            :value="tipo.id" :selected="tipo.id=={{json_encode(old('tipo'))}}?true : false">@{{ tipo.nombre }}</option>
                                                                            

                                                                            </select>
                                                                            @if ($errors->has('tipo'))
                                                                            <span class="help-block">
                                                                                <strong class="text-danger">{{ $errors->first('tipo') }}</strong>
                                                                            </span>

                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nmotor">Nro. Motor:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="nmotor" placeholder="" name="nmotor">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nserie">Nro. Serie:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="nserie" placeholder="" name="nserie">
                                                                        </div>
                                                                    </div>
                                                                     
                                                                      
                                                                     <div class="form-group">
                                                                        <label class="control-label col-sm-5 text-danger" for="nmotor">TALLER</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="direcciontaller">Nombre Taller:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="hidden" class="form-control input-sm requerido" id="idtaller" name="idtaller" placeholder="idtaller">
                                                                                    <input type="text" class="form-control input-sm" id="nombretaller" name="nombretaller" placeholder="">
                                                                                </div>
                                                                            </div>
                                                                    <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="direcciontaller"><strong class="text-danger">(*)</strong> Dirección Taller:</label>
                                                                                <div class="col-sm-7">
                                                                                
                                                                                    <input type="text" class="form-control input-sm" id="direcciontaller" name="direcciontaller" placeholder="" >
                                                                                </div>
                                                                            </div>                                                    

                                                                            
                                                                            
                                                                            {{-- <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="comunataller">Comuna:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="comunataller" placeholder="" name="comunataller">
                                                                                </div>
                                                                            </div> --}}
                                                                             
                                                
                                                                </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                     

                                                                <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="compania"><strong class="text-danger">(*)</strong> Compa&#241;&#237;a:</label>
                                                                        <div class="col-sm-7">
                                                                            {{-- <input type="text" name="compania" id="compania" value="VEDISA"> --}}
                                                                            <select  class="form-control requerido" id="compania" name="compania"style="width: 200px">
                                                                            <option value="0">Seleccione Compa&#241;&#237;a</option>
                                                                            <option v-for="empresa in companiaList" 
                                                                            :value="empresa.id" :selected="empresa.id=={{json_encode(old('compania'))}}?true : false">@{{ empresa.nombre }}</option>

                                                                            </select>
                                                                            @if ($errors->has('compania'))
                                                                            <span class="help-block">
                                                                                <strong class="text-danger">{{ $errors->first('compania') }}</strong>
                                                                            </span>
                                                                            

                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="">&nbsp;</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nsiniestro">Nro. Siniestro:</label>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm " id="nsiniestro" placeholder="" name="nsiniestro">
                                                                                <span class="input-group-btn">
                                                                                <button class="btn btn-danger btn-sm" type="button"><span class="fas fa-search"></span></button>
                                                                                </span>
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="modelo">Modelo:</label>
                                                                        <div class="col-sm-7">          
                                                                            <select  class="form-control" id="modelo" name="modelo"style="width: 200px">

                                                                            </select>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="color">Color:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="color" placeholder="" name="color">
                                                                        </div>
                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nchasis">Nro. Chasis:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="nchasis" placeholder="" name="nchasis">
                                                                        </div>
                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="rutprop">Rut Propietario:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="rutprop" placeholder="" name="rutprop">
                                                                        </div>
                                                                    </div> 
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="">&nbsp;</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="telefonotaller">Telefono Taller:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="telefonotaller" placeholder="" name="telefonotaller">
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombcontactotaller"> Contacto Taller:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombcontactotaller" placeholder="" name="nombcontactotaller">
                                                                                </div>
                                                                            </div>    
                                                
                                                                </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                    {{-- <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="fecha">Fecha:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control input-sm" id="fecha" placeholder="" name="fecha" value="@php echo date('d-m-Y') @endphp" readonly>
                                                                        </div>
                                                                    </div> --}}
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="emailcontac">Email Contacto:</label>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                {{-- <span class="form-control input-sm">{{ Auth::user()->email }}</span> --}}
                                                                                
                                                                                <select type="text" class="form-control input-sm requerido" style="width: 200px" id="emailcontac" placeholder="" name="emailcontac" value="" >
                                                                                </select>
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="">&nbsp;</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="liquidador">Liquidador:</label>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                
                                                                                <select type="text" class="form-control input-sm requerido" style="width: 200px" id="liquidador" placeholder="" name="liquidador" value="" >
                                                                                </select>
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="anio">Año:</label>
                                                                        <div class="col-sm-4">
                                                                            <select class="form-control input-sm" id="anio" placeholder="Año" name="anio">
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="combustible">Combustible:</label>
                                                                        <div class="col-sm-7">
                                                                            <select id="combustible" name="combustible" class="form-control input-sm" style="width: 200px">
                                                                                <option value="(NO INFORMADO)">(NO INFORMADO)</option>
                                                                                <option value="GASOLINA">GASOLINA</option>
                                                                                <option value="BENCINA">BENCINA</option>
                                                                                <option value="DIESEL">DIESEL</option>
                                                                                <option value="ELECTRICO">ELECTRICO</option>
                                                                            </select>
                                                                            @if ($errors->has('combustible'))
                                                                            <span class="help-block">
                                                                                <strong class="text-danger">{{ $errors->first('combustible') }}</strong>
                                                                            </span>

                                                                            @endif

                                                                        </div>

                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vim">VIN:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control input-sm" id="vim" placeholder="" name="vim">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nombreprop">Nombre Propietario:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control input-sm" id="nombreprop" placeholder="" name="nombreprop">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="">&nbsp;</label>
                                                                        <div class="col-sm-7">          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="emailtaller">Email Taller:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="emailtaller" placeholder="" name="emailtaller">
                                                                                </div>
                                                                            </div> 
                                                                    <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="ruttaller">RUT Taller:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="ruttaller" placeholder="" name="ruttaller">
                                                                                </div>
                                                                            </div> 
                                                                </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                    

                                                </div>

                                            
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingVehiculos" style="background-color: #ededed">
                                                      <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseValores" aria-expanded="true" aria-controls="collapseOne">
                                                          Valores<span class="caret"></span>
                                                        </a>

                                                      </h4>

                                                    </div>
                                                    <div id="collapseValores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingVehiculos">
                                                        <div class="panel-body">
                                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL VALORES -->
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vtraslado">Valor Traslado:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="vtraslado" placeholder="" name="vtraslado" value=0>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="nfacturaproveedor">Nº Factura Proveedor:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="nfacturaproveedor" placeholder="" name="nfacturaproveedor" value=0>
                                                                        </div>
                                                                    </div> 
                                                                    

                                                            </div> {{-- FIN DE LA PRIMERA COLUMNA DE VALORES --}}
                                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                     
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vfacturacia">Valor de la Factura:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="vfacturacia" placeholder="" name="vfacturacia" value=0>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vfacturacia">Nº Factura:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="nfactura" placeholder="" name="nfactura" value=0>
                                                                        </div>
                                                                    </div>
                                                            


                                                            </div> {{-- FIN DE LA SEGUNDA COLUMNA DE VALORES --}}

                                                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="tasacion">Tasación:</label>
                                                                        <div class="col-sm-7">          
                                                                            <input type="text" class="form-control input-sm" id="tasacion" placeholder="" name="tasacion" value=0>
                                                                        </div>
                                                                    </div> 
                                                                <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vminremate">Valor Mínimo Remate:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control input-sm" id="vminremate" placeholder="" name="vminremate" value=0>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-5" for="vindem">Valor Indemnización:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control input-sm" id="vindem" placeholder="" name="vindem" value=0>
                                                                        </div>
                                                                    </div>


                                                            </div> {{-- FIN DE LA TERCERA COLUMNA DE VALORES --}}




                                                        </div>
                                                       
                                                    </div>
                                                    

                                                </div> <!-- FIN DEL PANEL DE VALORES -->
                                                </form><!-- FIN DEL FORM DE DATOS BASICOS -->

                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingImg" style="background-color: #ededed">
                                                      <h4 class="panel-title">
                                                        <a class="collapsed disabledTab collapseTab" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseImg" aria-expanded="false" aria-controls="collapseTwo">
                                                          Imagenes<span class="caret"></span>
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapseImg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingImg">
                                                        <div class="panel-body">
                                                           
                                                                <div class="col-md-4">
                                                                    {{-- <label for="archivo">Seleccionar archivo:</label>
                                                                    <input type="file" class="form-control" id="archivo" name="archivo"> --}}
                                                                    <form id="subirimg" action="subirfotos" method="post" enctype="multipart/form-data">
                                                                        {{csrf_field()}}

                                                                      
                                                                        <input class="form-control" id="imagenesVehiculo" name="fotosvehiculo[]" type="file" multiple />
                                                                        
                                                                     
                                                                       
                                                                    </form>
                                                                </div>
                                                                <div class="">
                                                                   <button type="button" id="btnsubirimg" class="btn btn-success">Cargar</button>
                                                                </div>
                                                            
                                                            <br>
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">

                                                                    <div class='row'>
                                                                        <div class='col-md-12'>
                                                                            <div id="galeriaVehiculo" class="owl-carousel owl-theme">
                                                                               
                                                                            </div>
                
                                                                        </div>
                                                                    </div>
                                                                </div> {{-- fin panel body --}}
                                                            </div>


                                                        </div>
                                                    </div>

                                                  </div>
                                                  <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingThree" style="background-color: #ededed">
                                                      <h4 class="panel-title">
                                                        <a class="collapsed disabledTab collapseTab" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseObs" aria-expanded="false" aria-controls="collapseThree">
                                                            Observaciones<span class="caret"></span>
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapseObs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                      <div class="panel-body">
                                                        <form id="formObserv" ref="formObserv" method="post" class="form-horizontal" action="/ordenretiroResource" v-on:submit.prevent>
                                                            {{ csrf_field()}}
                                                            <fieldset class="fieldsetPrincipal">
                                                                <table>
                                                                    <tbody><tr>                
                                                                        <td>
                                                                            Observación: &nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <textarea id="obsertxt" name="obsertxt" cols="40" rows="5" maxlength="100" style="background-color:#dee6eb;"></textarea>
                                                                        </td>
                                                                        <td>
                                                                           &nbsp;&nbsp;Observación privada&nbsp;&nbsp;&nbsp;&nbsp;<input id="observprivada" name="observprivada" type="checkbox">
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="btnObservation" class="btn btn-success" @click="enviarObservacion()">GRABAR OBSERVACIÓN</button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </fieldset>
                                                        </form> {{-- FIN FORM OBSERVACION --}}
                                                        <div class='row'>
                                                                        <div class='col-md-12'>
                                                                          <table id="tablaobervaciones" class="table table-striped table-hover" >
                                                                              <thead>
                                                                                  <tr>
                                                                                      
                                                                                      <th style="width: 20%">Empresa</th>
                                                                                      <th style="width: 5%">Fecha</th style="width: 20%">
                                                                                      <th style="width: 20%">Usuario</th>
                                                                                      <th style="width: 40%">Observación</th>
                                                                                  </tr>
                                                                              </thead>
                                                                              <tbody id="tbodyObervaciones">
                                                                                  
                                                                              </tbody>
                                                                          </table>
                                                                        </div>
                                                                    </div>
                                                      </div>
                                                    </div>
                                                  </div><br>
                                                    <div class="row">
                                                        <div class="col-md-12" >


                                                            <div class="pull-right">
                                                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarDatosBasicos()"></button>
                                                                {{-- <button id="btnPdf" type="button" class="btn btn-warning btn-md" @click="crearPDF()">Descargar</button> --}}
                                                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>

                                                                </div>
                                                                <br>
                                                                <div class="pull-left">
                                                                <strong class="text-danger">(*) Campos Obligatorios</strong>

                                                            </div>




                                                        </div>
                                                    </div>
                                                  
                                                </div><!--  FIN ACORDEON VEHICULO -->
                                            </div><!--  FIN TAB VEHICULO -->
                                            <div role="tabpanel" class="tab-pane" id="traslado">
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingTransp" style="background-color: #ededed">
                                                              <h4 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#transportistaCollapse" aria-expanded="false" aria-controls="collapseThree">
                                                                    Transportista<span class="caret"></span>
                                                                </a>
                                                              </h4>
                                                            </div>
                                                            <div id="transportistaCollapse" class="" role="tabpanel" aria-labelledby="headingTransp">
                                                              <div class="panel-body">
                                                                    <form id="formTransportista" ref="formTransportista" method="post" class="form-horizontal" action="/ordenretiroResource" v-on:submit.prevent>
                                                                        {{csrf_field()}}
                                                                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="ruttransp">Rut:</label>
                                                                                <div class="col-sm-7">
                                                                                    <input type="text" class="form-control input-sm" id="ruttransp" name="ruttransp" placeholder="" disabled>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="telefonotransp">Telefono:</label>
                                                                                <div class="col-sm-4">          
                                                                                    <input type="text" class="form-control input-sm" id="telefonotransp" placeholder="" name="telefonotransp" disabled>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombchofertransp">Nombre del Chofer:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="hidden" class="form-control input-sm" id="idtransportista" placeholder="idtransportista" name="idtransportista">
                                                                                    <input type="text" class="form-control input-sm" id="nombchofertransp" placeholder="" name="nombchofertransp">
                                                                                </div>
                                                                            </div>    
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                                                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="nombtransp">Nombre:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="nombtransp" placeholder="" name="nombtransp" disabled>
                                                                                </div>
                                                                            </div> 
                                                                            
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="emailtransp">Email:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="emailtransp" placeholder="" name="emailtransp" disabled>
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="gruaexternatransp">Grua Externa:</label>
                                                                                <div class="col-md-3" > 
                                                                                    <select id="gruaexternatransp" name="gruaexternatransp" class="form-control input-sm disabledTab collapseTab" style="width: 200px" disabled>
                                                                                        <option value="SI" >SI</option>
                                                                                        <option value="NO">NO</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>  
                                                                            
                                                        
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE TRASLADO -->

                                                                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE TRASLADO -->
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="patentecamiontransp">Patente Camion:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="patentecamiontransp" placeholder="" name="patentecamiontransp" disabled>
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group">
                                                                                <label class="control-label col-sm-5" for="rutchofertransp">Rut Chofer:</label>
                                                                                <div class="col-sm-7">          
                                                                                    <input type="text" class="form-control input-sm" id="rutchofertransp" placeholder="" name="rutchofertransp" disabled>
                                                                                </div>
                                                                            </div> 
                                                                            <div class="form-group ">
                                                                                
                                                                                {{-- <div class="col-sm-6">          
                                                                                    <button id="btnEnviarEmailTraslado" type="button" class="btn btn-primary btn-md form-control" @click="enviarEmailTraslado()">Notificar Transportista</button>
                                                                                </div> --}}
                                                                            </div> 
                                                                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE TRASLADO -->
                                                                    </form> {{-- FIN FORM TRANSPORTISTA --}}
                                                            
                                                              </div>
                                                            </div>
                                                          </div>
                                                            <div class="row">
                                                                <div class="col-md-12" >


                                                                    <div class="pull-right">
                                                                        <button id="btnEnviarTraslado" type="button" class="btn btn-success btn-md" @click="enviarTraslado()"></button>
                                                                        
                                                                        <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>

                                                                        </div>




                                                                </div>
                                                            </div>


                                                    </div>{{-- FIN TAB TRASLADO --}}
                                                    <div role="tabpanel" class="tab-pane" id="archivos">
                                                        <div class="panel panel-default">
                                                            
                                                            <form id="subirarchivo" action="subirfotos" method="post" enctype="multipart/form-data">
                                                                <div class="panel-body">
                                                                    <div class='row'>
                                                                        <div class='col-md-12'>

                                                                    
                                                                            <div class="form-group col-sm-4">
                                                                                
                                                                               
                                                                                {{csrf_field()}}

                                                                                    <input type="hidden" class="form-control" id="usuarioid" name="usuarioid" value="{{ Auth::user()->id }}">
                                                                                    <label for="archivo">Seleccionar archivo:</label>
                                                                                    <input type="file" class="form-control" id="archivoOR" name="archivoOR">
                                                                                    <p class="help-block"><em>Verificar nombre duplicado de archivo</em></p>
                                                                                
                                                                            </div>
                                                                            
                                                                            <div class="form-group col-sm-1">
                                                                                
                                                                            </div>

                                                                            <div class="form-group col-sm-4">
                                                                                <label for="archivo">Observacion:</label>
                                                                                <input type="text" class="form-control" id="obserfile" name="obserfile">
                                                                            </div>
                                                                </form>   
                                                                        </div>

                                                                    </div>
                                                                    <div class='row'>
                                                                        <div class='col-md-12'>
                                                                            <button type="button" id="cargarArchivo" class="btn btn-success">Cargar</button>
                                                                            <br><br>
                                                                        </div>
                                                                    </div>
                                                        
                                                                    <div class='row'>
                                                                        <div class='col-md-12'>
                                                                          <table id="tablaarchivos" class="table table-striped table-hover" >
                                                                              <thead>
                                                                                  <tr>
                                                                                      <th>Nombre</th>
                                                                                      <th>Empresa</th>
                                                                                      <th>Fecha Creacion</th>
                                                                                      <th>Creado por</th>
                                                                                      <th>Observación</th>
                                                                                      <th>Archivo</th>
                                                                                      <th>Acciones</th>
                                                                                  </tr>
                                                                              </thead>
                                                                              <tbody id="tbodyArchivos">
                                                                                  
                                                                              </tbody>
                                                                          </table>
                                                                        </div>
                                                                    </div>
                                                                </div> {{-- fin panel body --}}
                                                            </div>

                                                </div>{{-- FIN TAB ARCHIVO --}}
                                                <div role="tabpanel" class="tab-pane" id="bodega">
                                                    <div class="panel panel-default">
                                                        <form class="navbar-form navbar-left" id="frmBodega" ref="frmBodega" method="post" action="/updateBodega" v-on:submit.prevent>
                                                            {{csrf_field()}}
                                                            <div class='row'>
                                                                        <div class='col-md-12'>
                                                                            DOCUMENTOS:&nbsp;
                                                                          
                                                                            Permiso
                                                                            <input type="checkbox" class="form-control input-sm" name="permiso" id="permiso">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          
                                                                            Seguro
                                                                            <input type="checkbox" class="form-control input-sm" name="seguro" id="seguro">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          
                                                                            Padron
                                                                            <input type="checkbox" class="form-control input-sm" name="padron" id="padron">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          
                                                                            Revisión Técnica
                                                                            <input type="checkbox" class="form-control input-sm" name="revtec" id="revtec">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          
                                                                            Gases
                                                                            <input type="checkbox" class="form-control input-sm" name="gases" id="gases">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          <hr>
                                                                          Condicionado
                                                                            <input type="checkbox" class="form-control input-sm" name="condicionado" id="condicionado">&nbsp;
                                                                          &nbsp;&nbsp;
                                                                          
                                                                            Desarme
                                                                            <input type="checkbox" class="form-control input-sm" name="desarme" id="desarme">&nbsp;
                                                                          &nbsp;&nbsp;

                                                                          <hr>
                                                                          
                                                                            Llaves:
                                                                            <select class="form-control input-sm" id="llaves" name="llaves">
                                                                            <option value="SI">SI</option>
                                                                            <option value="NO">NO</option>
                                                                            </select>
                                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                          
                                                                            Placas:
                                                                            <select class="form-control input-sm" id="placas" name="placas">
                                                                            <option value="DELANTERA">DELANTERA</option>
                                                                            <option value="TRASERA">TRASERA</option>
                                                                            <option value="AMBOS">AMBOS</option>
                                                                            </select>
                                                                           
                                                                          <hr>
                                                                          
                                                                            TAG:
                                                                            <select class="form-control input-sm" id="tag" name="tag">
                                                                            <option value="SI">SI</option>
                                                                            <option value="NO">NO</option>
                                                                            </select>
                                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                          
                                                                            Computador:
                                                                            <select class="form-control input-sm" id="computador" name="computador">
                                                                            <option value="SI">SI</option>
                                                                            <option value="NO">NO</option>
                                                                            </select>
                                                                          <hr>
                                                                          
                                                                            Motor arranca:
                                                                            <select class="form-control input-sm" id="motorarranca" name="motorarranca">
                                                                            <option value="SI">SI</option>
                                                                            <option value="NO">NO</option>
                                                                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                          
                                                                            Airbag:
                                                                            <select class="form-control input-sm" id="airbag" name="airbag">
                                                                            <option value="SIN ACTIVAR">SIN ACTIVAR</option>
                                                                            <option value="ACTIVADO">ACTIVADO</option>
                                                                            <option value="NO USA">NO USA</option>
                                                                            </select>
                                                                          <hr>
                                                                          Kilometraje: <input type="text" id="kilometraje" name="kilometraje">
                                                                          <hr>
                                                                          
                                                                            Bodega destino:
                                                                            <select class="form-control input-sm" id="bodegas" name="bodegas">
                                                                                <option value="0">Seleccione Bodega</option>
                                                                            <option v-for="bodega in bodegalist" 
                                                                            :value="bodega.id" :selected="bodega.id=={{json_encode(old('bodegas'))}}?true : false">@{{ bodega.nombre }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
      
                                                                </form>
                                                                        

                                                           
                                                    </div>

                                                                    <div class='row'>
                                                                        <div class='col-md-12'>
                                                                            <button type="button" id="btnActBdg" class="btn btn-success pull-right">ACTUALIZAR BODEGA</button>
                                                                            <br><br>
                                                                        </div>
                                                                    </div>



                                                </div>{{-- FIN TAB ARCHIVO --}}
                                                <div role="tabpanel" class="tab-pane" id="historial">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table class="table table-hover table-striped">
                                                                        <thead>
                                                                            <th>Operacion</th>
                                                                            <th>Usuario</th>
                                                                            <th>Fecha</th>
                                                                        </thead>
                                                                        <tbody id="operacioneshist">
                                                                            
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        
                                                </div> <!-- FIN DE HISTORIAL" -->
                                        
                                    </div> <!-- FIN DE class="tab-content" -->
                                
                                </div> <!-- FIN DE class="card" -->
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->


                        </div> <!-- FIN FILA DEL FORMULARIO -->
                        

                        
                        


                        

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>

         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>

</div> <!-- Fin - Modal nuevo usuario --> 
