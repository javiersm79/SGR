@extends('administrador.masteradmin')

@section('content')
<div id="tasacionApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/invoice.png') }}"> Mantenedor de Tasaciones</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.tasaciones.formulario')
        <button id="btnNuevaTasacion" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateTasacion" v-on:click="crearTasacion">Nueva Tasación</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Tasaciones</div>
                   
                <div class="panel-body">
                    <div id="listado_tasaciones">  
                        <listatasaciones class="listatabla" @view-tasacion="getTasacionData"></listatasaciones>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/tasaciones.js"></script>
@endsection

 