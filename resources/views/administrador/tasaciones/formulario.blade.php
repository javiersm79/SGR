<!-- Modal -->
<div id="newUpdateTasacion" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:85%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles de la Tasación</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                    </div>
                </div>
                <form id="formTasacion" ref="formTasacion" method="post" class="form-horizontal" action="/tasacionesResource">
                         {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idtasacion" placeholder="" name="idtasacion" value="{{ old('idtasacion') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">

                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-heading" id="headingTransp" style="background-color: #ededed">
                        <div class="panel-title">Contacto</div>
                    </div>
                    <div class="panel-body" style="font-size: 12px">
                        
                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="compania"><strong class="text-danger">(*)</strong> Compañia:</label>
                                    <div class="col-sm-7">
                                        <select  class="form-control requerido" id="compania" name="compania" style="width: 200px">
                                        <option value="">Seleccione Compa&#241;&#237;a</option>
                                        <option v-for="empresa in companiaList" 
                                        :value="empresa.id" :selected="empresa.id=={{json_encode(old('compania'))}}?true : false">@{{ empresa.nombre }}</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nombre">Nombre:</label>
                                    <div class="col-sm-7">          
                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre" readonly>
                                    </div>
                                </div>
                                
                                
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                            <div class="col-md-8" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email"><strong class="text-danger">(*)</strong> Email:</label>
                                    <div class="col-sm-6">
                                        <select class="form-control requerido" id="email" placeholder="" name="email" style="width: 300px">
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="telefono">Telefono:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="telefono" placeholder="" name="telefono" readonly>
                                    </div>
                                </div>
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                    </div><!-- FIN body panel Contacto-->
                    <div class="panel-heading" id="headingTransp" style="background-color: #ededed">
                        <div class="panel-title">Vehiculo</div>
                    </div>
                    <div class="panel-body" style="font-size: 12px">
                        
                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE TASACIONES -->

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="patente"><strong class="text-danger">(*)</strong> Patente:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control requerido" id="patente" placeholder="" name="patente">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="marca">Marca:</label>
                                                    <div class="col-sm-7">
                                                        <select  class="form-control" id="marca" name="marca"style="width: 200px">
                                                        <option value="">Seleccione Marca</option>
                                                        <option v-for="marca in marcasList" 
                                                        :value="marca.id" :selected="marca.id=={{json_encode(old('marca'))}}?true : false">@{{ marca.nombre }}</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="valor"><strong class="text-danger">(*)</strong> Valor:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control requerido" id="valor" placeholder="" name="valor">
                                                    </div>
                                                </div>
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nsiniestro"> Nº Siniestro:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nsiniestro" placeholder="" name="nsiniestro">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="modelo">Modelo:</label>
                                                    <div class="col-sm-7">
                                                        <select  class="form-control" id="modelo" name="modelo"style="width: 200px">
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE TASACIONES -->
                                                
                                                
   
                                            <div class="form-group">
                                                <label class="control-label col-sm-5" for="fecha">Fecha:</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="fecha" placeholder="" name="fecha" readonly value="@php echo date("d/m/Y"); @endphp"> 
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="control-label col-sm-5" for="anio">Año:</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="anio" placeholder="" name="anio">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TASACIONES -->

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarDatos()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>
                    </div><!-- FIN body panel -->

                </form>

                    
                </div><!-- FIN body defeault -->
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
