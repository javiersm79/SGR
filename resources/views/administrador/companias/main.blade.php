@extends('administrador.masteradmin')

@section('content')
<div id="companiaApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/building.png') }}"> Mantenedor de Compañias</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.companias.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateCompania" v-on:click="crearCompania">Nueva Compañia</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
 
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Compañias</div>
                   
                <div class="panel-body">
                    <div id="listado_companias">  
                        <listacompanias class="listatabla" @view-compania="getCompaniaData"></listacompanias>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/companias.js"></script>
@endsection

 