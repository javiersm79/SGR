@extends('administrador.masteradmin')



@section('content')
<div id="modelosApp" class="container" style="font-family: sans-serif;">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Modelos</div>
            <div id="modeloSearchNew" class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>

                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav">

                                <li><a href="#">Modelos a buscar</a></li>
                        
                            </ul>
                            <form class="navbar-form navbar-left">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="">
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <div class="navbar-form">
                                <button id="btnNuevoModelo" type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUpdateModelo" v-on:click="rutaCrear">Nuevo</button>
                            </div>
                        </div>
                    </nav> <!-- FIN CAMPO DE BUSQUEDA DEL TRANSPORTISTA -->
                   
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_modelos">  
                        <listamodelos class="listatabla" @view-modelo="rutaEditar"></listamodelos>
                    
                    </div>

                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateModelo" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:95%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title ">Detalles del modelo</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <b id="titulomodal"></b>
                                            
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO-->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="marca">Marca:</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="marca" name="marca" >
                                                        <option value="">Seleccione</option>
                                                        <option value="5">ACADIAN</option>
                                                        <option value="6">ACURA</option>
                                                        <option value="203">AGRALE</option>
                                                        <option value="7">ALFA ROMEO</option>
                                                        <option value="8">AMERICAN MOTORS</option>
                                                        <option value="204">AMERITRUCK</option>
                                                        <option value="9">APRILIA</option>
                                                        <option value="10">ARO</option>
                                                        <option value="205">ASIA</option>
                                                        <option value="11">ASIA MOTORS</option>
                                                        <option value="12">ASTON MARTIN</option>
                                                        <option value="13">AUDI</option>
                                                        <option value="14">AUSTIN</option>
                                                        <option value="206">AUTOCAR</option>
                                                        <option value="15">AUTORRAD</option>
                                                        <option value="207">B.M.C.</option>
                                                        <option value="16">BAIC</option>
                                                        <option value="17">BAJAJ</option>
                                                        <option value="18">BASHAN</option>
                                                        <option value="208">BAW</option>
                                                        <option value="209">BEDFORD</option>
                                                        <option value="19">BEIGING</option>
                                                        <option value="210">BELAVTOMAZ</option>
                                                        <option value="20">BENELLI</option>
                                                        <option value="21">BENTLEY</option>
                                                        <option value="22">BENYI</option>
                                                        <option value="23">BENZHOU</option>
                                                        <option value="211">BERLIET</option>
                                                        <option value="24">BIGFOOT</option>
                                                        <option value="25">BIMOTA</option>
                                                        <option value="26">BMW</option>
                                                        <option value="27">BOATIAN</option>
                                                        <option value="212">BORGWARD</option>
                                                        <option value="28">BRILLIANCE</option>
                                                        <option value="29">BUELL</option>
                                                        <option value="30">BUICK</option>
                                                        <option value="31">BULTACO</option>
                                                        <option value="213">BUSSING</option>
                                                        <option value="32">BYD</option>
                                                        <option value="33">CADILLAC</option>
                                                        <option value="34">CAGIVA</option>
                                                        <option value="214">CAKY-CHANGAN</option>
                                                        <option value="215">CAMC</option>
                                                        <option value="35">CATERHAM</option>
                                                        <option value="36">CFMOTO</option>
                                                        <option value="37">CHANGAN</option>
                                                        <option value="38">CHANGHE</option>
                                                        <option value="39">CHERY</option>
                                                        <option value="2">CHEVROLET</option>
                                                        <option value="40">CHRYSLER</option>
                                                        <option value="41">CITROEN</option>
                                                        <option value="216">COMIL GALLEGIANTE</option>
                                                        <option value="42">COMMER</option>
                                                        <option value="43">DACIA</option>
                                                        <option value="44">DAELIM</option>
                                                        <option value="45">DAEWOO</option>
                                                        <option value="217">DAF</option>
                                                        <option value="46">DAIHATSU</option>
                                                        <option value="47">DATSUN</option>
                                                        <option value="48">DAYUN</option>
                                                        <option value="218">DIAMOND</option>
                                                        <option value="219">DIMEX</option>
                                                        <option value="49">DODGE</option>
                                                        <option value="50">DONGFENG</option>
                                                        <option value="51">DUCATI</option>
                                                        <option value="52">DUNNA</option>
                                                        <option value="53">DYNA</option>
                                                        <option value="220">EBRO</option>
                                                        <option value="54">EUROMOT</option>
                                                        <option value="55">F.S.O.</option>
                                                        <option value="221">FARGO</option>
                                                        <option value="56">FAW</option>
                                                        <option value="222">FBM</option>
                                                        <option value="223">FEDERAL</option>
                                                        <option value="57">FERRARI</option>
                                                        <option value="58">FIAT</option>
                                                        <option value="59">FLSTF</option>
                                                        <option value="60">FORD</option>
                                                        <option value="224">FORD TAUNUS</option>
                                                        <option value="225">FORD THAMES</option>
                                                        <option value="61">FOTON</option>
                                                        <option value="62">FOX</option>
                                                        <option value="226">FREIGHTLINER</option>
                                                        <option value="63">FXA</option>
                                                        <option value="64">G.M.C.</option>
                                                        <option value="65">GAC GONOW</option>
                                                        <option value="66">GARELLI</option>
                                                        <option value="67">GASGAS</option>
                                                        <option value="68">GEELY</option>
                                                        <option value="276">GELLY</option>
                                                        <option value="69">GILERA</option>
                                                        <option value="227">GOLDEN DRAGON</option>
                                                        <option value="70">GREAT WALL</option>
                                                        <option value="228">GREYHOUND</option>
                                                        <option value="71">GUZZI</option>
                                                        <option value="72">HAFEI</option>
                                                        <option value="73">HAIMA</option>
                                                        <option value="74">HAO JUE</option>
                                                        <option value="75">HARLEY</option>
                                                        <option value="76">HARLEY-DAVIDSON</option>
                                                        <option value="77">HARTFORD</option>
                                                        <option value="78">HAVAL</option>
                                                        <option value="229">HENSCHEL</option>
                                                        <option value="79">HENSIM</option>
                                                        <option value="80">HERO-PUCH</option>
                                                        <option value="230">HIGER</option>
                                                        <option value="81">HILLMAN</option>
                                                        <option value="231">HINO</option>
                                                        <option value="82">HONDA</option>
                                                        <option value="232">HUANGHAI</option>
                                                        <option value="83">HUSABERG</option>
                                                        <option value="84">HUSQVARNA</option>
                                                        <option value="85">HYOSUNG</option>
                                                        <option value="86">HYUNDAI</option>
                                                        <option value="87">INDIAN</option>
                                                        <option value="88">INFINITI</option>
                                                        <option value="89">INTERNATIONAL</option>
                                                        <option value="90">ISUZU</option>
                                                        <option value="233">IVECO</option>
                                                        <option value="91">JAC</option>
                                                        <option value="92">JAGUAR</option>
                                                        <option value="93">JAWA</option>
                                                        <option value="234">JBC JINBEI</option>
                                                        <option value="94">JEEP</option>
                                                        <option value="95">JIANSHE</option>
                                                        <option value="235">JIE FANG</option>
                                                        <option value="96">JINBEI</option>
                                                        <option value="97">JMC</option>
                                                        <option value="236">KAMAZ</option>
                                                        <option value="98">KAWASAKI</option>
                                                        <option value="99">KAYAK</option>
                                                        <option value="100">KEEWAY</option>
                                                        <option value="237">KENWORTH</option>
                                                        <option value="238">KIA</option>
                                                        <option value="1">KIA MOTORS</option>
                                                        <option value="239">KING LONG</option>
                                                        <option value="101">KINLON</option>
                                                        <option value="102">KTM</option>
                                                        <option value="103">KYMCO</option>
                                                        <option value="104">LADA</option>
                                                        <option value="105">LAMBORGHINI</option>
                                                        <option value="106">LAMBRETTA</option>
                                                        <option value="107">LANCIA</option>
                                                        <option value="108">LAND ROVER</option>
                                                        <option value="109">LANDWIND</option>
                                                        <option value="110">LEXUS</option>
                                                        <option value="240">LEYLAND</option>
                                                        <option value="241">LIAZ</option>
                                                        <option value="111">LIFAN</option>
                                                        <option value="112">LINCOLN</option>
                                                        <option value="113">LML</option>
                                                        <option value="114">LONCIN</option>
                                                        <option value="115">LOTUS</option>
                                                        <option value="116">LUOJIA</option>
                                                        <option value="242">MACK</option>
                                                        <option value="243">MAGIRUS</option>
                                                        <option value="244">MAGIRUS DEUTZ</option>
                                                        <option value="117">MAHINDRA</option>
                                                        <option value="118">MAICO</option>
                                                        <option value="245">MAN</option>
                                                        <option value="246">MARCOPOLO</option>
                                                        <option value="119">MASERATI</option>
                                                        <option value="247">MAXUS</option>
                                                        <option value="3">MAZDA</option>
                                                        <option value="120">MCLAREN</option>
                                                        <option value="121">MERCEDES BENZ</option>
                                                        <option value="122">MERCURY</option>
                                                        <option value="123">MG</option>
                                                        <option value="124">MINI</option>
                                                        <option value="125">MITSUBISHI</option>
                                                        <option value="126">MONTELLI</option>
                                                        <option value="127">MONTESA</option>
                                                        <option value="128">MORGAN</option>
                                                        <option value="129">MORRIS</option>
                                                        <option value="130">MOTO BECANE</option>
                                                        <option value="131">MOTORRAD</option>
                                                        <option value="132">MSK</option>
                                                        <option value="133">MV AGUSTA</option>
                                                        <option value="4">NISSAN</option>
                                                        <option value="134">NSU</option>
                                                        <option value="248">O.M.</option>
                                                        <option value="135">ODES</option>
                                                        <option value="136">OLDSMOBILE</option>
                                                        <option value="137">OPEL</option>
                                                        <option value="138">OSSA</option>
                                                        <option value="249">PEGASO</option>
                                                        <option value="139">PEUGEOT</option>
                                                        <option value="140">PGO</option>
                                                        <option value="141">PIAGGIO</option>
                                                        <option value="142">PIONNER</option>
                                                        <option value="143">PLYMOUTH</option>
                                                        <option value="144">POLARIS</option>
                                                        <option value="145">POLSKI FIAT</option>
                                                        <option value="146">PONTIAC</option>
                                                        <option value="147">PORSCHE</option>
                                                        <option value="148">PROTON</option>
                                                        <option value="250">PULLMAN STD AMERIC</option>
                                                        <option value="149">PUMA</option>
                                                        <option value="150">REGAL RAPTOR</option>
                                                        <option value="151">RENAULT</option>
                                                        <option value="251">RENAULT(Berliet)</option>
                                                        <option value="252">REO</option>
                                                        <option value="152">RIZATO</option>
                                                        <option value="153">ROLLS ROYCE</option>
                                                        <option value="253">ROMAN</option>
                                                        <option value="154">ROVER</option>
                                                        <option value="254">S/M</option>
                                                        <option value="155">SAAB</option>
                                                        <option value="156">SACHS</option>
                                                        <option value="157">SAEHAN</option>
                                                        <option value="158">SAMSUNG</option>
                                                        <option value="159">SANFU</option>
                                                        <option value="160">SANLG</option>
                                                        <option value="161">SANYA</option>
                                                        <option value="162">SANYANG SYM</option>
                                                        <option value="255">SCANIA</option>
                                                        <option value="163">SEAT</option>
                                                        <option value="256">SETRA (Mercedes Benz)</option>
                                                        <option value="164">SG</option>
                                                        <option value="257">SHACMAN</option>
                                                        <option value="258">SHENFENG</option>
                                                        <option value="259">SHENYANG JINBEI</option>
                                                        <option value="165">SHINERAY</option>
                                                        <option value="166">SIMCA</option>
                                                        <option value="260">SINOTRUK</option>
                                                        <option value="167">SKODA</option>
                                                        <option value="168">SKYGO</option>
                                                        <option value="169">SMA</option>
                                                        <option value="170">SPITZ</option>
                                                        <option value="171">SSANGYONG</option>
                                                        <option value="261">STUDEBAKER</option>
                                                        <option value="172">SUBARU</option>
                                                        <option value="262">SUNLONG</option>
                                                        <option value="173">SUZUKI</option>
                                                        <option value="174">TAKASAKI</option>
                                                        <option value="175">TATA</option>
                                                        <option value="263">TATRA</option>
                                                        <option value="176">TM</option>
                                                        <option value="177">TORITO</option>
                                                        <option value="178">TOYOTA</option>
                                                        <option value="179">TRIUMPH</option>
                                                        <option value="180">TVS</option>
                                                        <option value="181">UNITED MOTORS</option>
                                                        <option value="182">URAL</option>
                                                        <option value="183">VERONA</option>
                                                        <option value="184">VESPA</option>
                                                        <option value="264">VOLARE</option>
                                                        <option value="185">VOLKSWAGEN</option>
                                                        <option value="186">VOLVO</option>
                                                        <option value="265">WABCO</option>
                                                        <option value="187">WANGYE</option>
                                                        <option value="266">WESTERN STAR</option>
                                                        <option value="267">WHITE</option>
                                                        <option value="188">WILLYS</option>
                                                        <option value="189">WOLKEN</option>
                                                        <option value="190">XGJAO</option>
                                                        <option value="191">XINGYUE</option>
                                                        <option value="192">XMOTORS</option>
                                                        <option value="193">YAMAHA</option>
                                                        <option value="194">YAMAMOTO</option>
                                                        <option value="268">YOUYI</option>
                                                        <option value="269">YUE JIN</option>
                                                        <option value="195">YUGO</option>
                                                        <option value="270">YUTONG</option>
                                                        <option value="196">ZANELLAS</option>
                                                        <option value="271">ZANELLO</option>
                                                        <option value="197">ZASTAVA</option>
                                                        <option value="198">ZNEN GROUP</option>
                                                        <option value="199">ZONGSHEN</option>
                                                        <option value="200">ZOTYE</option>
                                                        <option value="201">ZUNDAPP</option>
                                                        <option value="202">ZX</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="tipo">Tipo:</label>
                                                    <div class="col-sm-7">          
                                                        <select class="form-control" id="tipo" name="tipo">
                                                        <option value="">Seleccione</option>
                                                        <option value="2">AUTOMOVIL</option>
                                                        <option value="3">STATION WAGON</option>
                                                        <option value="4">TODO TERRENO</option>
                                                        <option value="5">CAMIONETA</option>
                                                        <option value="6">MOTO</option>
                                                        <option value="7">MINIBUS</option>
                                                        <option value="8">FURGON</option>
                                                        <option value="9">MOTOR HOME</option>
                                                        <option value="10">CAMIONES</option>
                                                        <option value="11">BUSES Y TAXIBUSES</option>
                                                        <option value="12">TROLEBUSES</option>
                                                        <option value="13">TOLVAS HIDRAULICAS (S/TOMA FUERZA)</option>
                                                        <option value="14">ESTANQUES METALICOS FIERRO (SIN CHASSIS)</option>
                                                        <option value="15">BASURERO</option>
                                                        <option value="16">AMPLIROLL</option>
                                                        <option value="17">ESTANQUES METALICOS DE ACERO INOXIDABLE (SIN CHASSIS)</option>
                                                        <option value="18">SEMIREMOLQUE CARGA TODO</option>
                                                        <option value="19">REMOLQUE CARROCERIA PLANA</option>
                                                        <option value="20">SEMIREMOLQUE FORESTAL</option>
                                                        <option value="21">SEMIREMOLQUE CARROCERIA PLANA</option>
                                                        <option value="22">SEMIREMOLQUE BATEA</option>
                                                        <option value="23">CARROCERÍAS TERMICAS CON UNIDAD REFRIGERANTE</option>
                                                        <option value="24">CARROCERÍAS TERMICAS</option>
                                                        <option value="25">FURGON CARGA GENERAL</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO-->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO-->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nombre">Nombre:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="gruaexterna">Habilitado:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="habilitado" name="habilitado" class="form-control" style="width: 120px">
                                                            <option selected="selected" value="">Seleccione</option>
                                                            <option value="si">SI</option>
                                                            <option value="no">NO</option>
                                                        </select>

                                                    </div>
                                                </div>
                                                
                            
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO-->

                                           
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <button class="btn btn-success btn-lg pull-right">Enviar</button>
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div>                     
                </div><!-- Fin - Modal nuevo modelo -->
                <!-- Modal Admin años-->
                    <div id="adminAnio" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title ">Detalles</h4>
                                </div>
                                <div class="modal-body">
                                    <br>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="anio">Año:</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="anio" name="anio">
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        <option value="2009">2009</option>
                                                        <option value="2008">2008</option>
                                                        <option value="2007">2007</option>
                                                        <option value="2006">2006</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option value="1989">1989</option>
                                                        <option value="1988">1988</option>
                                                        <option value="1987">1987</option>
                                                        <option value="1986">1986</option>
                                                        <option value="1985">1985</option>
                                                        <option value="1984">1984</option>
                                                        <option value="1983">1983</option>
                                                        <option value="1982">1982</option>
                                                        <option value="1981">1981</option>
                                                        <option value="1980">1980</option>
                                                        <option value="1979">1979</option>
                                                        <option value="1978">1978</option>
                                                        <option value="1977">1977</option>
                                                        <option value="1976">1976</option>
                                                        <option value="1975">1975</option>
                                                        <option value="1974">1974</option>
                                                        <option value="1973">1973</option>
                                                        <option value="1972">1972</option>
                                                        <option value="1971">1971</option>
                                                        <option value="1970">1970</option>
                                                        <option value="1969">1969</option>
                                                        <option value="1968">1968</option>
                                                        <option value="1967">1967</option>
                                                        <option value="1966">1966</option>
                                                        <option value="1965">1965</option>
                                                        <option value="1964">1964</option>
                                                        <option value="1963">1963</option>
                                                        <option value="1962">1962</option>
                                                        <option value="1961">1961</option>
                                                        <option value="1960">1960</option>
                                                        <option value="1959">1959</option>
                                                        <option value="1958">1958</option>
                                                        <option value="1957">1957</option>
                                                        <option value="1956">1956</option>
                                                        <option value="1955">1955</option>
                                                        <option value="1954">1954</option>
                                                        <option value="1953">1953</option>
                                                        <option value="1952">1952</option>
                                                        <option value="1951">1951</option>
                                                        <option value="1950">1950</option>
                                                        <option value="1949">1949</option>
                                                        <option value="1948">1948</option>
                                                        <option value="1947">1947</option>
                                                        <option value="1946">1946</option>
                                                        <option value="1945">1945</option>
                                                        <option value="1944">1944</option>
                                                        <option value="1943">1943</option>
                                                        <option value="1942">1942</option>
                                                        <option value="1941">1941</option>
                                                        <option value="1940">1940</option>
                                                        <option value="1939">1939</option>
                                                        <option value="1938">1938</option>
                                                        <option value="1937">1937</option>
                                                        <option value="1936">1936</option>
                                                        <option value="1935">1935</option>
                                                        <option value="1934">1934</option>
                                                        <option value="1933">1933</option>
                                                        <option value="1932">1932</option>
                                                        <option value="1931">1931</option>
                                                        <option value="1930">1930</option>
                                                        <option value="1929">1929</option>
                                                        <option value="1928">1928</option>
                                                        <option value="1927">1927</option>
                                                        <option value="1926">1926</option>
                                                        <option value="1925">1925</option>
                                                        <option value="1924">1924</option>
                                                        <option value="1923">1923</option>
                                                        <option value="1922">1922</option>
                                                        <option value="1921">1921</option>
                                                        <option value="1920">1920</option>
                                                        <option value="1919">1919</option>
                                                        <option value="1918">1918</option>
                                                        <option value="1917">1917</option>
                                                        <option value="1916">1916</option>
                                                        <option value="1915">1915</option>
                                                        <option value="1914">1914</option>
                                                        <option value="1913">1913</option>
                                                        <option value="1912">1912</option>
                                                        <option value="1911">1911</option>
                                                        <option value="1910">1910</option>
                                                        <option value="1909">1909</option>
                                                        <option value="1908">1908</option>
                                                        <option value="1907">1907</option>
                                                        <option value="1906">1906</option>
                                                        <option value="1905">1905</option>
                                                        <option value="1904">1904</option>
                                                        <option value="1903">1903</option>
                                                        <option value="1902">1902</option>
                                                        <option value="1901">1901</option>
                                                        <option value="1900">1900</option>
                                                        </select>
                                                    </div>
                                                    <button type="button" class="btn btn-success pull-left">Agregar</button>
                                                </div>
                                                
                                              {{--   <div class="form-group">
                                                    <label class="control-label col-sm-2" for="estado">Habilitado:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="habilitado" name="abilitado" class="form-control">
                                                        <option value="si">SI</option>
                                                        <option value="no">NO</option>

                                                        </select>

                                                    </div>
                                                </div> --}}
                            
                                            
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><b>Años del Modelo</b></div>
                                            <div class="panel-body">
                                                <table id="demoAnio">
                                                    <thead>
                                                        <tr>
                                                            <th>Años</th>
                                                            <th>Fecha creacíon</th>
                                                            <th>Creado por</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>2015</td>
                                                        <td>15/10/16</td>
                                                        <td>Javier Salazar</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2003</td>
                                                        <td>15/11/16</td>
                                                        <td>Mario Salas</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1999</td>
                                                        <td>15/09/17</td>
                                                        <td>Vicente Valdéz</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1995</td>
                                                        <td>15/03/17</td>
                                                        <td>Mario Salas</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                 


                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" >
                                        
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div>                   
      
            </div><!-- Fin - Modal admin Año --> 
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
@endsection
@section('scriptsmodules')
<script src="js/modelos.js"></script>
@endsection