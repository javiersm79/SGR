<!-- Modal -->
<div id="newUpdateUser" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del usuario</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formUsuario" ref="formUsuario" method="post" class="form-horizontal" action="/adminusuarios">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idusuario" placeholder="" name="idusuario" value="{{ old('idusuario') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombre"><strong class="text-danger">(*)</strong> Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="nombre" placeholder="" name="nombre" value="{{ old('nombre') }}">
                                     @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fechanac">Fecha Nacimiento:</label>
                                <div class="col-sm-4">          
                                    <input type="text" class="form-control datepicker" id="fechanac" placeholder="" name="fechanac" readonly value="{{ old('fechanac') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fono1">Fono 1:</label>
                                <div class="col-sm-7">          
                                    <input type="text" class="form-control" id="fono1" placeholder="" name="fono1" value="{{ old('fono1') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="perfil"><strong class="text-danger">(*)</strong> Perfil:</label>
                                <div class="col-sm-7">          
                                    <select id="perfil" name="perfil" class="form-control" style="width: 210px">
                                    <option selected="selected" value="">Seleccione Perfil</option>
                                    <option value="Administrador" @if (old('perfil') == 'Administrador') selected="selected" @endif>Administrador</option>
                                    <option value="Supervisor" @if (old('perfil') == 'Supervisor') selected="selected" @endif>Supervisor</option>
                                    <option value="Usuario" @if (old('perfil') == 'Usuario') selected="selected" @endif>Usuario</option>

                                    </select>
                                    @if ($errors->has('perfil'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('perfil') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fcrear">Fecha de creacion:</label>
                                <div class="col-sm-4">          
                                    <input type="text" class="form-control" id="fcrearnew" placeholder="" name="fcrearnew" readonly>
                                </div>
                            </div>     
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="apellidos">Apellidos:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="apellidos" placeholder="" name="apellidos" value="{{ old('apellidos') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="sexo">Sexo:</label>
                                <div class="col-sm-4">          
                                    <select id="sexo" name="sexo" class="form-control" style="width: 210px">
                                        {{-- <option selected="selected" value="">Seleccione Sexo</option> --}}
                                        <option value="MASCULINO">MASCULINO</option>
                                        <option value="FEMENINO">FEMENINO</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fono2">Fono 2:</label>
                                <div class="col-sm-7">          
                                    <input type="text" class="form-control" id="fono2" placeholder="" name="fono2" pattern="" value="{{ old('fono2') }}">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="password"><strong class="text-danger">(*)</strong> Password:</label>
                                <div class="col-sm-7">          
                                    <input type="text" class="form-control" id="password" placeholder="" name="password">
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                    </span>

                                    @endif
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="ultlogin">Ultimo Login:</label>
                                <div class="col-sm-4">          
                                    <input type="text" class="form-control" id="ultlogin" placeholder="" name="ultlogin" readonly>
                                </div>
                            </div>     
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="email"><strong class="text-danger">(*)</strong> Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>

                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="emailalt">Email alterno:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="emailalt" placeholder="" name="emailalt" value="{{ old('emailalt') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5" for="estado"><strong class="text-danger">(*)</strong> Estado:</label>
                                <div class="col-sm-7">          
                                    <select id="estado" name="estado" class="form-control" style="width: 210px">
                                        <option selected="selected" value="">Seleccione Estado</option>
                                        <option value="INACTIVO" @if (old('estado') == 'INACTIVO') selected="selected" @endif>INACTIVO</option>
                                        <option value="ACTIVO" @if (old('estado') == 'ACTIVO') selected="selected" @endif>ACTIVO</option>                            
                                        <option value="DESHABILITADO" @if (old('estado') == 'DESHABILITADO') selected="selected" @endif>DESHABILITADO</option>
                                        <option value="BLOQUEADO" @if (old('estado') == 'BLOQUEADO') selected="selected" @endif>BLOQUEADO</option>
                                        <option value="RECUPERACIÓN" @if (old('estado') == 'RECUPERACIÓN') selected="selected" @endif>RECUPERACIÓN</option>
                                    </select>
                                    @if ($errors->has('estado'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('estado') }}</strong>
                                    </span>

                                    @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="compania"><strong class="text-danger">(*)</strong> Compa&#241;&#237;a:</label>
                                <div class="col-sm-7">  

                                   
                                    <select  class="form-control" id="compania" name="compania"style="width: 210px">
                                    <option value="">Seleccione Compa&#241;&#237;a</option>
                                    <option v-for="empresa in companiaList" 
                                    :value="empresa.id" :selected="empresa.id=={{json_encode(old('compania'))}}?true : false">@{{ empresa.nombre }}</option>

                                    </select>
                                    @if ($errors->has('compania'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('compania') }}</strong>
                                    </span>
                                    

                                    @endif
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="control-label col-sm-5" for="compania"><strong class="text-danger">(*)</strong> Compañia:</label>
                                <div class="col-sm-7">          
                                    <select  class="form-control" id="compania" name="compania"style="width: 210px">
                                    <option value="">Seleccione Compa&#241;&#237;a</option>
                                    <option value="CARDIF" @if (old('compania') == 'CARDIF') selected="selected" @endif>CARDIF</option>
                                    <option value="CHILENA CONSOLIDADA" @if (old('compania') == 'CHILENA CONSOLIDADA') selected="selected" @endif>CHILENA CONSOLIDADA</option>
                                    <option value="HDI" @if (old('compania') == 'HDI') selected="selected" @endif>HDI</option>
                                    <option value="MAPFRE" @if (old('compania') == 'MAPFRE') selected="selected" @endif>MAPFRE</option>
                                    <option value="RENTA NACIONAL" @if (old('compania') == 'RENTA NACIONAL') selected="selected" @endif>RENTA NACIONAL</option>
                                    <option value="SURA" @if (old('compania') == 'SURA') selected="selected" @endif>SURA</option>
                                    <option value="VEDISA" @if (old('compania') == 'VEDISA') selected="selected" @endif>VEDISA</option>
                                    </select>
                                    @if ($errors->has('compania'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('compania') }}</strong>
                                    </span>

                                    @endif


                                </div> 
                            </div>--}}
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviarUser" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
