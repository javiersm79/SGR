@extends('administrador.masteradmin')

@section('content')
<div id="usuariosApp" class="container">
        <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Usuarios</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.usuarios.formulario')
        <button id="btnNuevoUsuario" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateUser" v-on:click="crearUsuario">Nuevo Usuario</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Usuarios</div>
                   
                <div class="panel-body">
                    <div id="listado_user">  
                        <listausuarios class="listatabla" @view-user="getUserData"></listausuarios>
                        
                    </div>

                </div>
            </div>
            

        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
{{-- @include('administrador.usuarios.footer')
@endsection --}}

@section('scriptsmodules')
<script src="js/usuarios.js"></script>
@endsection

 