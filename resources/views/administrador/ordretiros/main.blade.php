@extends('administrador.masteradmin')
@section('stylemodules')

<link href="css/owl.carousel.min.css" rel="stylesheet" />
<link href="css/owl.theme.default.min.css" rel="stylesheet" />
<link href="css/jquery.fancybox.min.css" rel="stylesheet" />
<link href="css/easy-autocomplete.min.css" rel="stylesheet" />
<link href="css/easy-autocomplete.themes.min.css" rel="stylesheet" />
@endsection

@section('content')
<div id="modelosApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Ordenes de Retiro</div>
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.ordretiros.formulario')
        @include('administrador.ordretiros.importarcertificado')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateOrdenRetiro" v-on:click="crearOR">Nueva Orden de Retiro</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
        
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Ordenes de Retiro
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-danger btn-sm" onclick="filtrarEstado('Sin asignar')">Sin asignar</button>
                        <button type="button" class="btn btn-primary btn-sm" onclick="filtrarEstado('Asignada')">Asignada</button>
                        <button type="button" class="btn btn-warning btn-sm" onclick="filtrarEstado('Con Problemas')">Con Problemas</button>
                        <button type="button" class="btn btn-success btn-sm" onclick="filtrarEstado('Retirado')">Retirado</button>
                        <button type="button" class="btn btn-default btn-sm" onclick="filtrarEstado('')">TODOS</button>
                    </div>
                </div>
                                   
                <div class="panel-body" >
                    <div id="listado_ordenesRetiro">  
                        <listaordenesretiro class="listatabla" @view-orden="getOrdRetData"></listaordenesretiro>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/ordretiros.js"></script>

<script src="js/jquery.fancybox.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.easy-autocomplete.min.js"></script>


@endsection

 