@extends('administrador.masteradmin')



@section('content')
<div id="usuariosApp" class="container" style="font-family: sans-serif;">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/bodegas.png') }}"> Mantenedor de Bodegas</div>
            <div id="bodegaSearchNew"  class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>

                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav">

                                <li><a href="#">Bodega a buscar</a></li>
                        
                            </ul>
                            <form class="navbar-form navbar-left">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="">
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <div class="navbar-form">
                                <button id="btnNuevaBodega" type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUpdateBodega" v-on:click="rutaCrear">Nuevo</button>
                            </div>
                        </div>
                    </nav> <!-- FIN CAMPO DE BUSQUEDA DE BODEGA -->
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_bodegas">  
                        <listabodegas class="listatabla" @view-bodega="rutaEditar"></listabodegas>
                   
                    </div>
                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateBodega" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title ">Detalles de la Marca</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <b id="titulomodal"></b>
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="nombrebodega">Nombre:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nombrebodega" name="nombrebodega" placeholder="">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="estado">Habilitado:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="habilitado" name="abilitado" class="form-control">
                                                        <option value="si">SI</option>
                                                        <option value="no">NO</option>

                                                        </select>

                                                    </div>
                                                </div>
                            
                                            
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <button class="btn btn-success btn-lg pull-right">Enviar</button>
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div>                   
                </div><!-- Fin - Modal nueva bodega -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
@endsection
@section('scriptsmodules')
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.4/vue-resource.min.js"></script>
<script src="js/bodegas.js"></script>
@endsection