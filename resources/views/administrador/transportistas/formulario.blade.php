<!-- Modal -->
<div id="newUpdateTransportista" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del Trasnportista</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formTransportista" ref="formTransportista" method="post" class="form-horizontal" action="/transportistaResource" v-on:submit.prevent>
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idTransportista" placeholder="" name="idTransportista" value="{{ old('idTransportista') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="row"> 
                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="rutempresa"><strong class="text-danger">(*)</strong> RUT:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="rutempresa" name="rutempresa" placeholder="" value="{{ old('rutempresa') }}">
                                        @if ($errors->has('rutempresa'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('rutempresa') }}</strong>
                                        </span>

                                        @endif

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="telefono"><strong class="text-danger">(*)</strong> Telefono:</label>
                                    <div class="col-sm-7">          
                                        <input type="text" class="form-control" id="telefono" placeholder="" name="telefono" value="{{ old('telefono') }}">
                                        @if ($errors->has('telefono'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('telefono') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nombrechofer"> <strong class="text-danger">(*)</strong> Nombre Chofer:</label>
                                    <div class="col-sm-7">          
                                        <input type="text" class="form-control" id="nombrechofer" placeholder="" name="nombrechofer" value="{{ old('nombrechofer') }}">
                                        @if ($errors->has('nombrechofer'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('nombrechofer') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>
                                
                                
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nombreempresa"> <strong class="text-danger">(*)</strong> Nombre:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="nombreempresa" placeholder="" name="nombreempresa" value="{{ old('nombreempresa') }}">
                                        @if ($errors->has('nombreempresa'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('nombreempresa') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="email"><strong class="text-danger">(*)</strong> Email:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="gruaexterna"><strong class="text-danger">(*)</strong> Grua Externa:</label>
                                    <div class="col-sm-4">          
                                        <select id="gruaexterna" name="gruaexterna" class="form-control" style="width: 120px">
                                         
                                            <option selected="selected" value="">Seleccione</option>
                                            <option value="SI" @if (old('gruaexterna') == 'SI') selected="selected" @endif>SI</option>
                                            <option value="NO" @if (old('gruaexterna') == 'NO') selected="selected" @endif>NO</option>
                                        </select
                                        </select>
                                        @if ($errors->has('gruaexterna'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('gruaexterna') }}</strong>
                                        </span>

                                        @endif

                                    </div>
                                </div>
                                
            
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="patentecamion"><strong class="text-danger">(*)</strong> Patente Camión:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="patentecamion" placeholder="" name="patentecamion" value="{{ old('patentecamion') }}">
                                         @if ($errors->has('patentecamion'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('patentecamion') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="rutchofer"><strong class="text-danger">(*)</strong> RUT Chofer:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="rutchofer" placeholder="" name="rutchofer" value="{{ old('rutchofer') }}">
                                        @if ($errors->has('rutchofer'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('rutchofer') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="habilitado"><strong class="text-danger">(*)</strong> Habilitado:</label>
                                    <div class="col-sm-4">          
                                        <select id="habilitado" name="habilitado" class="form-control" style="width: 120px">
                                            <option selected="selected" value="">Seleccione</option>
                                            <option value="SI" @if (old('habilitado') == 'SI') selected="selected" @endif>SI</option>
                                            <option value="NO" @if (old('habilitado') == 'NO') selected="selected" @endif>NO</option>
                                        </select>
                                        @if ($errors->has('habilitado'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('habilitado') }}</strong>
                                        </span>

                                        @endif

                                    </div>
                                </div>
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->


                        </div> <!-- FIN FILA DEL FORMULARIO -->
                        

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                                <div class="pull-right">
                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>

                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                            
                                </div>
                                <br>
                                <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                            
                                </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>

         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>

</div> <!-- Fin - Modal nuevo usuario --> 
