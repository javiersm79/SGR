<!-- Modal -->
<div id="anioMarcaModal" class="modal fade" role="dialog">
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Gestionar años del Modelo</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">AGREGAR</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formAnio" ref="formAnio" method="post" class="form-horizontal" action="/">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idAnio" placeholder="" name="idAnio" value="{{ old('idAnio') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="row">
                            <div class="col-md-4"> 
                                <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="anio"><strong class="text-danger"></strong> Año:</label>
                                    <div class="col-sm-3">
                                        <select  class="form-control" id="anio" name="anio"style="width: 150px">
                                        <option value="">Seleccione a&#241;o</option>
                                        <option v-for="anio in aniosSelect" 
                                        :value="anio" :selected="anio=={{json_encode(old('anio'))}}?true : false">@{{ anio }}</option>

                                        </select>
                                         @if ($errors->has('anio'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('anio') }}</strong>
                                        </span>

                                        @endif

                                    </div>

                                </div>
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="col-md-5 pull-left" >
                                
                                  
                                  <div class="pull-right">
                                    <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarFormulario()">Agregar</button>
                                    
                                    <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                    <br>
                                    
                                                    
                                  </div>
                                  
                                
                         


                            </div>
                        </div> <!-- FIN DE LA FILA SELEC Y BOTONES DE ACCIÓN -->



                        </form>
                

                        <span class="alalert alert-warning" v-if='anioslist.length === 0'>Sin Años registrados para el modelo</span>        
                        <table id="tablaMarcas" v-if='anioslist.length > 0' class="table table-striped table-hover" >
                            <thead>
                                <tr>
                                    
                                    <th>Modelo</th>
                                    <th>Marca</th>
                                    <th>Año</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="anio in anioslist" >
                                    <td>Toyota</td>
                                    <td>RAV4</td>
                                    <td>@{{anio}}</td>
                                     <!-- <td><button @click="viewUser(usuario.id, usuario.email)" class="btn btn-success">Editar</button> </td> -->
                                     <td>
                                        <button  type="button" class="btn btn-danger"  v-on:click="eliminarAnio(anio)">Eliminar</button>
                                     </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
