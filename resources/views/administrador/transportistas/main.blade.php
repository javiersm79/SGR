@extends('administrador.masteradmin')

@section('content')
<div id="modelosApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Transportistas</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.transportistas.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateTransportista" v-on:click="crearTransportista">Nuevo Transportista</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Transportistas</div>
                   
                <div class="panel-body">
                    <div id="listado_transportista">  
                        <listatransportistas class="listatabla" @view-transportista="getTransportistaData"></listatransportistas>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/transportistas.js"></script>
@endsection

 