@extends('administrador.masteradmin')

@section('content')
<div id="marcasApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/vehicles.png') }}"> Mantenedor de Marcas</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.marcas.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateMarca" v-on:click="crearMarca">Nueva Marca</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
 
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Marcas</div>
                   
                <div class="panel-body">
                    <div id="listado_marcas">  
                        <listamarcas class="listatabla" @view-marca="getMarcaData"></listamarcas>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>
<div id='anioModal'>
    {{-- @include('administrador.usuarios.update') --}}
    @include('administrador.marcas.anio')
    
                
    <br><br>
    {{-- @{{ $data || json}}  --}}
</div>
@endsection

@section('scriptsmodules')
<script src="js/marcas.js"></script>
@endsection

 