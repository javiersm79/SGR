<!-- Modal -->
<div id="newUpdateTaller" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del Taller</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formTaller" ref="formTaller" method="post" class="form-horizontal" action="/talleresResource">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idTaller" placeholder="" name="idTaller" value="{{ old('idTaller') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="col-md-4" > 
                            <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombre"><strong class="text-danger">(*)</strong> Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="nombre" placeholder="" name="nombre" value="{{ old('nombre') }}">
                                     @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="direccion"><strong class="text-danger">(*)</strong> Dirección:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="direccion" placeholder="" name="direccion" value="{{ old('direccion') }}">
                                     @if ($errors->has('direccion'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('direccion') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="comunas"><strong class="text-danger">(*)</strong> Comuna:</label>
                                <div class="col-sm-7">
                                    <select id="comunas" name="comunas" class="form-control" style="width: 210px" data-comunasold="{{ old('comunas') }}"></select>
                                     @if ($errors->has('comunas'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('comunas') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5" for="telefono"><strong class="text-danger">(*)</strong> Teléfono:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="telefono" placeholder="" name="telefono" value="{{ old('telefono') }}">
                                     @if ($errors->has('telefono'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('telefono') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombcontacto"><strong class="text-danger">(*)</strong> Nombre Contacto:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="nombcontacto" placeholder="" name="nombcontacto" value="{{ old('nombcontacto') }}">
                                     @if ($errors->has('nombcontacto'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('nombcontacto') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            
                               
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > 
                            <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="rut"><strong class="text-danger">(*)</strong> RUT:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="rut" placeholder="" name="rut" value="{{ old('rut') }}">
                                     @if ($errors->has('rut'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('rut') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="regiones"><strong class="text-danger">(*)</strong> Region:</label>
                                <div class="col-sm-7">
                                    <select id="regiones" name="regiones" class="form-control" style="width: 210px" data-regionesold="{{ old('regiones') }}"></select>
                                     @if ($errors->has('regiones'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('regiones') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="ciudad"><strong class="text-danger">(*)</strong> Ciudad:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="ciudad" placeholder="" name="ciudad" value="{{ old('ciudad') }}">
                                     @if ($errors->has('ciudad'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('ciudad') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email"><strong class="text-danger">(*)</strong> Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ old('email') }}">
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviarTaller" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
