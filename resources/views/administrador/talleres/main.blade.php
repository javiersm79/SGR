@extends('administrador.masteradmin')

@section('content')
<div id="talleresApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.talleres.formulario')
        <button id="btnNuevoTalleres" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateTaller" v-on:click="crearTaller">Nuevo Taller</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Talleres</div>
                   
                <div class="panel-body">
                    <div id="listado_talleres">  
                        <listatalleres class="listatabla" @view-taller="getTallerData"></listatalleres>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/talleres.js"></script>
@endsection

 