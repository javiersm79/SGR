@extends('administrador.masteradmin')

@section('content')
<div id="modelosApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/vehicles.png') }}"> Mantenedor de Modelos</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.modelos.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateModelo" v-on:click="crearModelo">Nuevo Modelo</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Modelos</div>
                   
                <div class="panel-body">
                    <div id="listado_modelos">  
                        <listamodelos class="listatabla" @view-modelo="getModeloData"></listamodelos>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/modelos.js"></script>
@endsection

 