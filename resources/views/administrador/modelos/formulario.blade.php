<!-- Modal -->
<div id="newUpdateModelo" class="modal fade" role="dialog">
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del Modelo</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formModelo" ref="formModelo" method="post" class="form-horizontal" action="/modelosResource" v-on:submit.prevent>
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idModelo" placeholder="" name="idModelo" value="{{ old('idModelo') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nombre"><strong class="text-danger">(*)</strong> Nombre:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre" value="{{ old('nombre') }}" >
                                         @if ($errors->has('nombre'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
                                        </span>

                                        @endif

                                    </div>

                                </div>
                                
                                
                                   
            
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-6" > 
                                <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="marca"><strong class="text-danger"></strong> Marca:</label>
                                    <div class="col-sm-5">
                                        <select  class="form-control" id="marca" name="marca"style="width: 190px">
                                        <option value="">Seleccione Marca</option>
                                        <option v-for="marca in marcasList" 
                                        :value="marca.id" :selected="marca.id=={{json_encode(old('marca'))}}?true : false">@{{ marca.nombre }}</option>

                                        </select>
                                        @if ($errors->has('marca'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('marca') }}</strong>
                                        </span>

                                        @endif

                                    </div>

                                </div>

            
                            </div> <!-- FIN COLUMNA DEL FORMULARIO -->
                        </div> <!-- FIN FILA DEL FORMULARIO -->
                       {{--  <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="tipo"><strong class="text-danger"></strong> Tipo:</label>
                                    <div class="col-sm-6" >
                                        <select  class="form-control" id="tipo" name="tipo"style="width: 150px">
                                            <option value="">Seleccione Tipo</option>
                                        <option v-for="tipo in tiposList" 
                                        :value="tipo.id" :selected="tipo.id=={{json_encode(old('tipo'))}}?true : false">@{{ tipo.nombre }}</option>
                                        

                                        </select>
                                        @if ($errors->has('tipo'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('tipo') }}</strong>
                                        </span>

                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div> --}}

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
