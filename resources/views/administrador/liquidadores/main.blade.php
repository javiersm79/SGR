@extends('administrador.masteradmin')

@section('content')
<div id="marcasApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/vehicles.png') }}"> Mantenedor de Liquidadores</div>
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.liquidadores.formulario')
        <button id="btnNuevo" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateLiquidador" v-on:click="crearLiquidador">Nuevo Liquidador</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
 
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Liquidadores</div>
                   
                <div class="panel-body">
                    <div id="listado_liquidadores">  
                        <listaliquidadores class="listatabla" @view-liquidador="getLiquidadorData" @eliminar-liquidador="eliminarLiquidador"></listaliquidadores>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('scriptsmodules')
<script src="js/liquidadores.js"></script>
@endsection

 