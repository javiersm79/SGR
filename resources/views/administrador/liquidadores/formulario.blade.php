<!-- Modal -->
<div id="newUpdateLiquidador" class="modal fade" role="dialog">
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles de Liquidadores</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                      
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formLiquidador" ref="formLiquidador" method="post" class="form-horizontal" action="/liquidadoresResource" v-on:submit.prevent>
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idliquidador" placeholder="" name="idliquidador" value="{{ old('idliquidador') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="row">
                            <div class="col-md-12"> 
                                <!-- PRIMERA COLUMNA DEL FORMULARIO DE MARCA -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="liquidador"><strong class="text-danger">(*)</strong> Nombre/Apellido:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="liquidador" placeholder="" name="liquidador" value="{{ old('nombre') }}" >
                                         @if ($errors->has('liquidador'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('liquidador') }}</strong>
                                        </span>

                                        @endif

                                    </div>

                                </div>
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-12" > 
                                <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="compania"><strong class="text-danger">(*)</strong> Compa&#241;&#237;a:</label>
                                    <div class="col-sm-7">
                                        <select  class="form-control requerido" id="compania" name="compania"style="width: 200px">
                                        <option value="0">Seleccione Compa&#241;&#237;a</option>
                                        <option v-for="empresa in companiaList" 
                                        :value="empresa.id" :selected="empresa.id=={{json_encode(old('compania'))}}?true : false">@{{ empresa.nombre }}</option>

                                        </select>
                                        @if ($errors->has('compania'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('compania') }}</strong>
                                        </span>
                                        

                                        @endif
                                    </div>
                                </div>
                            </div> <!-- FIN COLUMNA DEL FORMULARIO  -->
                        </div> <!-- FIN FILA SUPERIOR DEL FORMULARIO  -->
                        

                        
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviar" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">CANCELAR</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
