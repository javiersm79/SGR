@extends('administrador.masteradmin')



@section('content')
<div id="transportistasApp" class="container" style="font-family: sans-serif;">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/transportista.png') }}"> Mantenedor de Usuarios</div>
            <div id="trasnportistaSearchNew" class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>

                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">

                            <ul class="nav navbar-nav">

                                <li><a href="#">Transportistas a buscar</a></li>
                        
                            </ul>
                            <form class="navbar-form navbar-left">
                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="">
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <div class="navbar-form">
                                <button id="btnNuevoUsuario" type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUpdateTransportista" v-on:click="rutaCrear">Nuevo</button>
                            </div>
                        </div>
                    </nav> <!-- FIN CAMPO DE BUSQUEDA DEL TRANSPORTISTA -->
                   
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_transportista">  
                        <listatransportistas class="listatabla" @view-transportista="rutaEditar"></listatransportistas>
                    
                    </div>

                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateTransportista" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:95%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title ">Detalles del transportista</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <b id="titulomodal"></b>
                                            
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="iduser">RUT:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="iduser" name="iduser" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="telefono">Telefono:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="telefono" placeholder="" name="telefono">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nombrechofer">Nombre Chofer:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="nombrechofer" placeholder="" name="nombrechofer">
                                                    </div>
                                                </div>
                                                
                                                
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nombre">Nombre:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="email">Email:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="email" placeholder="" name="email">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="gruaexterna">Grua Externa:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="gruaexterna" name="gruaexterna" class="form-control" style="width: 120px">
                                                            <option selected="selected" value="">Seleccione</option>
                                                            <option value="si">SI</option>
                                                            <option value="no">NO</option>
                                                        </select>

                                                    </div>
                                                </div>
                                                
                            
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="camion">Camión:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="camion" placeholder="" name="camion">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="rutchofer">RUT Chofer:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="rutchofer" placeholder="" name="rutchofer">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="habilitado">Habilitado:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="habilitado" name="habilitado" class="form-control" style="width: 120px">
                                                            <option selected="selected" value="">Seleccione</option>
                                                            <option value="si">SI</option>
                                                            <option value="no">NO</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <button class="btn btn-success btn-lg pull-right">Enviar</button>
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div> <!-- Fin - Modal nuevo usuario -->                  
            </div>
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->
@endsection
@section('scriptsmodules')
<script src="js/transportistas.js"></script>
@endsection