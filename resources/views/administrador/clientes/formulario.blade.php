<!-- Modal -->
<div id="newUpdateContacto" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <b id="titulomodal">{{ old('accion') }}</b>
                        
                    </div>
                </div>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <form id="formCliente" ref="formCliente" method="post" class="form-horizontal" action="/crearCliente">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="{{ old('accion') }}">
                        <input type="hidden" class="form-control" id="idcontacto" placeholder="idcontacto" name="idcontacto" value="{{ old('idcontacto') }}">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="col-md-4" > 
                            <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombre"><strong class="text-danger">(*)</strong> Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="nombre" placeholder="" name="nombre" value="{{ old('nombre') }}">
                                     @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
                                    </span>

                                    @endif

                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="RUT">(*) RUT:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="rut" placeholder="" name="rut" value="{{ old('rut') }}">
                                    @if ($errors->has('rut'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('rut') }}</strong>
                                    </span>

                                    @endif
                                </div>
                            </div>
                               
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="apellidos">(*) Apellidos:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="apellidos" placeholder="" name="apellidos" value="{{ old('apellidos') }}">
                                    @if ($errors->has('apellidos'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('apellidos') }}</strong>
                                    </span>

                                    @endif
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="telefono">Telefono:</label>
                                <div class="col-sm-7">          
                                    <input type="text" class="form-control" id="telefono" placeholder="" name="telefono" value="{{ old('telefono') }}">
                                </div>
                            </div>   
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                                        
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="email"> Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" placeholder="" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>

                                    @endif
                                </div>
                            </div>

                            
                            
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                        <div class="row">
                            <div class="col-md-12" >
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviarContacto" type="button" class="btn btn-success btn-md" @click="enviarFormulario()"></button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     


                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         <!--  @{{ $data || json}}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div> -->

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
