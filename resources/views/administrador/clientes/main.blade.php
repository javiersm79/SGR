@extends('administrador.masteradmin')

@section('content')
<div id="contactosApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            
        </div>
    </div>
    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.clientes.formulario')
        <button id="btnNuevoContacto" type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#newUpdateContacto" v-on:click="crearContacto">Nuevo Cliente</button>
        <br><br>
        {{-- @{{ $data || json}}  --}}
    </div>
    <div class="row">
        @isset($cliente)
            {{ var_dump($cliente) }}
            
        @endisset

    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Clientes</div>
                   
                <div class="panel-body">
                    <div id="listado_contacto">  
                        <listacontactos class="listatabla" @view-contacto="getContactoData"></listacontactos>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>

</div>



@endsection

@section('scriptsmodules')
<script src="js/clientes.js"></script>
@endsection

 