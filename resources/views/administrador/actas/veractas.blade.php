@extends('administrador.masteradmin')
@section('stylemodules')

@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Mantenedor de Actas</div>

 
    </div>
    <div id="capaModal"></div>
    <div class="row" style="margin: auto; width: 95%;">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Listado de actas</div>
                   
                <div class="panel-body">
                    <div id="lista_actas">  
                        <listadoactas class="listatabla"></listadoactas>
                        
                    </div>
                    <div id="listado_existencias"></div>


                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/actas.js"></script>



@endsection

 