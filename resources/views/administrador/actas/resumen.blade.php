@extends('administrador.masteradmin')
@section('stylemodules')

@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Detalle de Acta</div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Datos del acta
                        <a href="/acta/pdfResumenActa/{{$datosacta[0]->numeroacta}}" class="btn btn-success btn-sm pull-right" style="margin-right: 5px">Descargar</a>
                        
                </div>

                <div class="panel-body ">
                    
                        <label class="control-label col-sm-1" for="actafecha">Fecha:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="actafecha" placeholder="" name="actafecha" style="width: 90px" value="{{  $datosacta[0]->fechafor}}" readonly>
                        </div>
                        <label class="control-label col-sm-1" for="numeroacta">Nro. Acta:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="numeroacta" name="numeroacta" value="{{$datosacta[0]->numeroacta}}" readonly>
                        </div>
                        <label class="control-label col-sm-2" for="totalvehiculos">Total Vehiculos en Acta:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="totalvehiculos" name="totalvehiculos" value="{{count($datosacta)}}" readonly>
                        </div>
                        

                </div>{{--Fin panel body superior --}}
            </div>
        </div>
 
    </div>
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="panel panel-default">
                <div class="panel-heading">Totales del acta</div>
                <div class="panel-body ">
                    <div class="row" >
                        <label class="control-label col-sm-1">Monto:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm"  value="{{'$'.number_format( $datosacta->totales['monto'],0,",",".") }}" readonly>
                        </div>
                        <label class="control-label col-sm-1">Comisión:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" value="{{'$'.number_format( $datosacta->totales['comision'],0,",",".") }}" readonly>
                        </div>
                        <label class="control-label col-sm-1">IVA:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" value="{{'$'.number_format( $datosacta->totales['iva'],0,",",".") }}" readonly>
                        </div>
                    </div>
                    <hr>
                    <div class="row" >
                        <label class="control-label col-sm-1">Garantia:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" value="{{'$'.number_format( $datosacta->totales['garantia'],0,",",".") }}" readonly>
                        </div>
                        <label class="control-label col-sm-1">Gastos Op:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" value="{{'$'.number_format( $datosacta->totales['gastosoperacionales'],0,",",".") }}" readonly>
                        </div>
                    </div>
                        

                </div>{{--Fin panel body superior --}}
            </div>
        </div>
 
    </div>
    <div id='listado_existencias'></div>
    <div id='lista_actas'></div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Vehículos en Acta </div>
                   
                <div class="panel-body">
                    <div id="listado_vehiculos">  
                        <table id="tablaProcesar" class="table table-striped table-hover" style="font-size: 13px">
                    <thead>
                        <tr>
                            <th style="text-align: center">Patente</th>
                            <th style="text-align: center">Compañia</th>
                            <th style="text-align: center">N° Siniestro</th>
                            <th style="text-align: center">Marca</th>
                            <th style="text-align: center">Modelo</th>
                            <th style="text-align: center">Año</th>
                            <th style="text-align: center">Lote</th>
                            <th style="text-align: center">RUT</th>
                            <th style="text-align: center">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datosacta as $vehiculo)
                        <tr>
                           
                            <td>{{ $vehiculo->patente }}</td>
                            <td>{{ $vehiculo->nombrecompania }}</td>
                            <td>{{ $vehiculo->nsiniestro }}</td>
                            <td>{{ $vehiculo->nombremarca }}</td>
                            <td>{{ $vehiculo->nombremodelo }}</td>
                            <td>{{ $vehiculo->anio }}</td>
                            <td align="center">{{ $vehiculo->lote }}</td>
                            <td id="rut{{ $vehiculo->lote }}">{{ $vehiculo->cliente }}</td>
                            <td id="monto{{ $vehiculo->lote }}">{{ '$'.number_format($vehiculo->monto,0,",",".") }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

    <div id='capaModal'>
        {{-- @include('administrador.usuarios.update') --}}
        @include('administrador.actas.formulario')
    </div>


@endsection

@section('scriptsmodules')
<script src="{{ asset('js/actas.js') }}"></script>
@endsection

 