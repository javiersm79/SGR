@extends('administrador.masteradmin')
@section('stylemodules')

@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Detalle de Acta</div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Datos del acta
                    @if ($datosacta[0]->estado ==    "Activa")
                        
                        
                        <a href="/acta/actamartillero/{{$datosacta[0]->numeroacta}}" id="actamartillero" class="btn btn-primary btn-sm pull-right" style="margin-right: 5px">Acta Martillero</a>
                        <a href="/acta/revisionacta/{{$datosacta[0]->numeroacta}}" id="actamartillero" class="btn btn-primary btn-sm pull-right" style="margin-right: 5px">Revisión Acta</a>
                        <a href="/acta/imprimirlotes/{{$datosacta[0]->numeroacta}}" id="imprimirlotes" class="btn btn-primary btn-sm pull-right" style="margin-right: 5px">Carteles de Lotes</a>
                        <a href="/acta/boletinremate/{{$datosacta[0]->numeroacta}}" id="boletinremate" class="btn btn-primary btn-sm pull-right" style="margin-right: 5px">Boletines remate</a>
                        <a href="/acta/lotes/{{$datosacta[0]->numeroacta}}" id="asignarlotes" class="btn btn-danger btn-sm pull-right" style="margin-right: 5px">Asignar Lotes</a>
                        <a href="/acta/procesar/{{$datosacta[0]->numeroacta}}" id="procesaracta" class="btn btn-success btn-sm pull-right" style="margin-right: 5px">Procesar</a>
                        
                        
                    @endif
                </div>

                <div class="panel-body ">
                    
                        <label class="control-label col-sm-1" for="actafecha">Fecha:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="actafecha" placeholder="" name="actafecha" style="width: 90px" value="{{  $datosacta[0]->fechafor}}" readonly>
                        </div>
                        <label class="control-label col-sm-1" for="numeroacta">Nro. Acta:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="numeroacta" name="numeroacta" value="{{$datosacta[0]->numeroacta}}" readonly>
                        </div>
                        <label class="control-label col-sm-2" for="totalvehiculos">Total Vehiculos en Acta:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="totalvehiculos" name="totalvehiculos" value="{{count($datosacta)}}" readonly>
                        </div>
                        

                </div>{{--Fin panel body superior --}}
            </div>
            </div>
 
    </div>
    <div id='capaModal'></div>
    <div id='listado_existencias'></div>
    <div id='lista_actas'></div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Vehículos en Acta
                    @if ($datosacta[0]->estado ==    "Activa")
                    <button id="btnCerrarActa" class="btn btn-danger btn-sm pull-right" >Cerrar Acta</button>
                    @endif
                </div>
                   
                <div class="panel-body">
                    <div id="listado_vehiculos">  
                        <table id="tablaVehiculos" class="table table-striped table-hover" style="font-size: 13px">
                    <thead>
                        <tr>
                            <th>N° Proceso</th>
                            <th>Patente</th>
                            <th>Compañia</th>
                            <th>N° Siniestro</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Lote</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datosacta as $vehiculo)
                        <tr>
                            <td><a href="/verExistencia/{{ $vehiculo->nproceso }}">{{ $vehiculo->nproceso }}<a></td>
                            <td>{{ $vehiculo->patente }}</td>
                            <td>{{ $vehiculo->nombrecompania }}</td>
                            <td>{{ $vehiculo->nsiniestro }}</td>
                            <td>{{ $vehiculo->nombremarca }}</td>
                            <td>{{ $vehiculo->nombremodelo }}</td>
                            <td>{{ $vehiculo->anio }}</td>
                            <td>{{ $vehiculo->lote }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                        
                    </div>

                </div>
            </div>
            <a href="pdfacta/{{$datosacta[0]->numeroacta}}" class="btn btn-danger pull-right">Descargar PDF</a><br><br><br>
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="{{ asset('js/actas.js') }}"></script>
@endsection

 