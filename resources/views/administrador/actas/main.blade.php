@extends('administrador.masteradmin')
@section('stylemodules')
@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Mantenedor de Actas</div>
            <div class="panel panel-default">
                <div class="panel-heading">Datos del acta</div>

                <div class="panel-body ">
                    
                        <label class="control-label col-sm-1" for="fecha">Fecha:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="fecha" placeholder="" name="fecha" style="width: 90px" readonly>
                        </div>
                        <label class="control-label col-sm-1" for="nacta">Nro. Acta:</label>
                        <div class="col-sm-2">          
                            <input type="text" class="form-control input-sm" id="nacta" name="nacta" readonly>
                        </div>
                        
                        <div class="col-sm-2">          
                            <button id="btnCrearActa" type="button" class="btn btn-danger">Crear Acta</button>
                        </div>


                </div>{{--Fin panel body superior --}}
            </div>
            </div>
 
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Vehículos en Acta</div>
                   
                <div class="panel-body">
                    <div id="listado_existencias">  
                        <listadoexistencias class="listatabla" ></listadoexistencias>
                        
                    </div>

                    <div id="lista_actas"></div>
                    <div id="capaModal"></div>

                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/actas.js"></script>


@endsection

 