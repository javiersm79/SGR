@extends('administrador.masteradmin')



@section('content')
<div id="usuariosApp" class="container">
    <div class="row" >
        <div class="col-md-12">
            <div class="titlemodule"><img src="{{ asset('storage/images/talleres.png') }}"> Mantenedor de Talleres</div>
            <div class="panel panel-default">
                
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Resultado</div>
                    
                <div class="panel-body">
                    <div id="listado_user">  
                    
                    </div>
                </div>
            <div id='capaModal' style="width: 90%; margin-left: 10px">
                    

                    <!-- Modal -->
                    <div id="newUpdateUser" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:95%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header gradientegris">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title ">Detalles del Taller</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <b>@{{ formdata.accion }}</b>
                                        </div>
                                    </div>
                                    <div class="panel panel-default" style="margin-top: -22px">
                                        <div class="panel-body" style="font-size: 12px">
                                            <form class="form-horizontal" action="/action_page.php">
                                            <div class="col-md-4" > <!-- PRIMERA COLUMNA DEL FORMULARIO DE TALLER -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="iduser">ID:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="iduser" name="iduser" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="fechanac">Fecha Nacimiento:</label>
                                                    <div class="col-sm-4">          
                                                        <input type="date" class="form-control datepicker" id="fechanac" placeholder="" name="fechanac" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="fono1">Fono 1:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="fono1" placeholder="" name="fono1">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="estado">Perfil:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="estado" name="estado" class="form-control" style="width: 210px">
                                                        <option selected="selected" value="">Seleccione Perfil</option>
                                                        <option value="Administrador">Administrador</option>
                                                        <option value="Supervisor">Supervisor</option>
                                                        <option value="Usuario">Usuario</option>

                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="fcrear">Fecha de creacion:</label>
                                                    <div class="col-sm-4">          
                                                        <input type="text" class="form-control" id="fcrear" placeholder="" name="fcrear" readonly>
                                                    </div>
                                                </div>     
                            
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TALLER -->

                                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE TALLER -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="nombre">Nombre:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="nombre" placeholder="" name="nombre">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="sexo">Sexo:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="sexo" name="sexo" class="form-control" style="width: 210px">
                                                            <option selected="selected" value="">Seleccione Sexo</option>
                                                            <option value="MASCULINO">MASCULINO</option>
                                                            <option value="FEMENINO">FEMENINO</option>
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="fono2">Fono 2:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="fono2" placeholder="" name="fono2" pattern="\d{1,5}">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="password">Password:</label>
                                                    <div class="col-sm-7">          
                                                        <input type="text" class="form-control" id="password" placeholder="" name="password">
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="ultlogin">Ultimo Login:</label>
                                                    <div class="col-sm-4">          
                                                        <input type="text" class="form-control" id="ultlogin" placeholder="" name="ultlogin" readonly>
                                                    </div>
                                                </div>     
                            
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TALLER -->

                                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE TALLER -->
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="apellidos">Apellidos:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="apellidos" placeholder="" name="apellidos">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="email">Email:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="email" placeholder="" name="email">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="emailalt">Email alterno:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="emailalt" placeholder="" name="emailalt">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="estado">Estado:</label>
                                                    <div class="col-sm-4">          
                                                        <select id="estado" name="estado" class="form-control" style="width: 210px">
                                                            <option selected="selected" value="">Seleccione Estado</option>
                                                            <option value="INACTIVO">INACTIVO</option>
                                                            <option value="ACTIVO">ACTIVO</option>                            
                                                            <option value="DESHABILITADO">DESHABILITADO</option>
                                                            <option value="BLOQUEADO">BLOQUEADO</option>
                                                            <option value="RECUPERACIÓN">RECUPERACIÓN</option>
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" for="compania">Compañia:</label>
                                                    <div class="col-sm-4">          
                                                        <select  class="form-control" id="compania" name="compania"style="width: 210px">
                                                        <option value="">Seleccione Compa&#241;&#237;a</option>
                                                        <option value="3">CARDIF</option>
                                                        <option value="4">CHILENA CONSOLIDADA</option>
                                                        <option value="7">HDI</option>
                                                        <option value="5">MAPFRE</option>
                                                        <option value="6">RENTA NACIONAL</option>
                                                        <option value="2">SURA</option>
                                                        <option value="1">VEDISA</option>
                                                        </select>


                                                    </div>
                                                </div>
                                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE TALLER -->
                                            </form>

                                        </div><!-- FIN body panel -->
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12" >
                                        <button class="btn btn-success btn-lg pull-right">Enviar</button>
                                 


                                        </div>
                                    </div>
                              </div>
                             <!--  @{{ $data || json}}
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div> -->

                        </div>
                    </div> <!-- Fin - Modal nuevo usuario -->                  
            </div>
        </div>
    </div>
</div>

<!-- <div id="app">
<example></example>
</div> -->

@endsection
@section('scriptsmodules')
<script src="js/usuarios.js"></script>
@endsection