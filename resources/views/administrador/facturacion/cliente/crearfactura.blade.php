@extends('administrador.masteradmin')
@section('stylemodules')
@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Mantenedor de Facturas</div>

        </div>
 
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">Vehículos en Acta</div>
                   
                <div class="panel-body">
                    <div id="listado_existencias">  
                        <listadoexistencias class="listatabla" ></listadoexistencias>
                        
                    </div>

                    <div id="lista_actas"></div>
                    <div id="capaModal"></div>

                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/actas.js"></script>


@endsection

 