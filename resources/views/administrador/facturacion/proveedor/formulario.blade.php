<!-- Modal -->
<div id="datosRemate" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:95%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header gradientegris">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Detalles del Contacto</h4>
            </div>
            <div class="modal-body"><br>
                <div class="panel panel-default" style="margin-top: -22px">
                    <div class="panel-body" style="font-size: 12px">
                        <div class="row">    
                        <form id="formDatosLote" ref="formDatosLote" method="post" class="form-horizontal" action="/ingresarDatosLote">
                            {{csrf_field()}}
                        <input type="hidden" class="form-control" id="lote" placeholder="" name="lote" value="">
                        <input type="hidden" class="form-control" id="acta" placeholder="" name="acta" value="">
                        
                        <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="">
                        <input name="_method" type="hidden" value="{{ old('_method') }}">
                        <div class="col-md-4" > 
                            <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nombres"><strong class="text-danger">(*)</strong> Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="nombres" name="nombres">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="apellidos">(*) Apellidos:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="apellidos" placeholder="" name="apellidos">
                                    
                                </div>
                                
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-5" for="rut"><strong class="text-danger">(*)</strong> RUT:</label>
                            
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="text" class="form-control requerido" id="rut" placeholder="" name="rut" value="" onkeyup="this.value = this.value.toUpperCase();">
                                        
                                        <span class="input-group-btn ">
                                        <button class="btn btn-info" type="button" @click="buscarCliente()"><span class="glyphicon glyphicon-search"></span></button>
                                        </span>
                                    </div>

                                        <span class="help-block">
                                            <strong id="buscarResult" class="text-danger"></strong>
                                        </span>  
                                </div>  
                            </div>
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                            
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="email"><strong class="text-danger">(*)</strong> Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" placeholder="" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fono">Telefono:</label>
                                <div class="col-sm-7">          
                                    <input type="text" class="form-control" placeholder="" id="telefono">
                                </div>
                            </div>
        
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                        <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                            
                            <div class="form-group">
                                    <label class="control-label col-sm-5" for="monto"><strong class="text-danger">(*)</strong> Monto Rematado:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control requerido" id="monto" placeholder="" name="monto" value="0">
                                    </div>
                                </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="garantia"><strong class="text-danger">(*)</strong> Garantia:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control requerido" id="garantia" placeholder="" name="garantia" value="0">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                    <label class="control-label col-sm-5" for="tasacionfiscal"><strong class="text-danger">(*)</strong> Tasacion Fiscal:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control requerido" id="tasacionfiscal" placeholder="" name="tasacionfiscal" value="0">
                                    </div>
                                </div>
                        </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                        </div>
                        

                        <div class="row">    
                            <div class="col-md-12">    
                            
                              
                              <div class="pull-right">
                                <button id="btnEnviarDatos" type="button" class="btn btn-success btn-md" onclick="ingresarDatosLote()">Enviar</button>
                                
                                <button type="button" class="btn btn-danger btn-md " data-dismiss="modal">Cancelar</button>
                                                
                              </div>
                              <br>
                              <div class="pull-left">
                                <strong class="text-danger">(*) Campos Obligatorios</strong>
                                                
                              </div>
                            
                     

                            </div>
                        </div>


                        </form>

                    </div><!-- FIN body panel -->
                </div>
            
                
          </div>
         
          

        </div>
    </div>
</div> <!-- Fin - Modal nuevo usuario --> 
