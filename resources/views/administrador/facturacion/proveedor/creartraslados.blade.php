@extends('administrador.masteradmin')
@section('stylemodules')
@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/invoice.png') }}"> Facturas de Trasalados de Proveedores</div>
        </div>
 
    </div>
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="nproceso"><strong class="text-danger">(*)</strong> Nº Proceso Existencia:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control requerido" id="nproceso" placeholder="" name="nproceso">
                        </div>
                        <div class="col-sm-1">
                            <button id="btnbscarnproceso" class="btn btn-info" onclick="buscarIngreso()">Buscar</button>

                        </div>
                        <div class="col-sm-6">
                            <span class="help-block">
                                <strong id="buscarResultNproceso" class="text-danger"></strong>
                            </span> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
 
    </div>

    <div id="ingresoFactura" class="hidden">
        <div class="row" >
            <div class="col-sm-12"> 
                <div class="panel panel-default">
                <div class="panel-heading"><strong>Datos del Vehiculo</strong></div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <strong>Marca: </strong><div id="patente"></div>
                        </div>
                        <div class="col-sm-3" >
                            <strong>Marca: </strong><div id="marca"></div>
                        </div>
                        <div class="col-sm-3" >
                            <strong>Modelo: </strong><div id="modelo"></div>
                        </div>
                        <div class="col-sm-3" >
                            <strong>Año: </strong><div id="anio"></div>
                        </div>
                    </div>
                </div>
            </div>
     
        </div>
        
        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">    
                            <form id="formDatosLote" method="post" class="form-horizontal" action="/ingresarDatosLote">
                                {{csrf_field()}}                        
                            <input type="hidden" class="form-control" id="accion" placeholder="" name="accion" value="">
                            <input name="_method" type="hidden" value="{{ old('_method') }}">
                            <div class="col-md-4" > 
                                <!-- PRIMERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="valor"><strong class="text-danger">(*)</strong> Valor:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control requerido" id="valor" name="nombres">
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nfactura"><strong class="text-danger">(*)</strong> Nº Factura Proveedor:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control requerido" id="nfactura" placeholder="" name="nfactura">
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="concepto"><strong class="text-danger">(*)</strong> Concepto:</label>
                                    <div class="col-sm-7">          
                                        <textarea class="form-control" rows="4" placeholder="" id="concepto" name="concepto"></textarea>
                                    </div>
                                </div>
            
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-4" > <!-- SEGUNDA COLUMNA DEL FORMULARIO DE USUARIO -->
                                
                                
                                <div class="form-group">
                                <label class="control-label col-sm-5" for="rut"><strong class="text-danger">(*)</strong> RUT:</label>
                                
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="hidden" class="form-control requerido" id="idrut" placeholder="" name="idrut" >
                                            
                                            <input type="text" class="form-control requerido" id="rut" placeholder="" name="rut" value="" onkeyup="this.value = this.value.toUpperCase();">
                                            
                                            <span class="input-group-btn ">
                                            <button class="btn btn-info" type="button" id="btnBuscarProveedor" onclick="buscarproveedor()"><span class="glyphicon glyphicon-search"></span></button>
                                            </span>
                                        </div>

                                            <span class="help-block">
                                                <strong id="buscarResult" class="text-danger"></strong>
                                            </span>  
                                    </div>  
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="rsocial"><strong class="text-danger">(*)</strong> Razón Social:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control requerido" id="rsocial" placeholder="" name="rsocial">
                                    </div> 
                                </div>
            
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->

                            <div class="col-md-4" > <!-- TERCERA COLUMNA DEL FORMULARIO DE USUARIO -->
                                
                                <div class="form-group">
                                        <label class="control-label col-sm-5" for="fecha"><strong class="text-danger">(*)</strong> Fecha:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control datepicker requerido" id="fecha" placeholder="" name="fecha" readonly>
                                        </div>
                                    </div>
                                
                            </div> <!-- FIN COLUMNA DEL FORMULARIO DE USUARIO -->
                            </form>
                        </div>
                        <div class="row">
                             <div class="col-md-12" >
                            <button id="btnIngresar" class="btn btn-info pull-right">Ingresar</button>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/facturaproveedor.js"></script>


@endsection

 