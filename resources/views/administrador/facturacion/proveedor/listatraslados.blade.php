@extends('administrador.masteradmin')
@section('stylemodules')
@endsection

@section('content')
<div id="mainApp" class="container">
    <div class="row" >
        <div class="col-sm-12"> 
            <div class="titlemodule"><img src="{{ asset('storage/images/actasdoc.png') }}"> Lista de Facturas de Traslados</div>

        </div>
 
    </div>
    
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                
                   
                <div class="panel-body">
                    <div id="listado_facturas" class="listatabla">
                        <table id="tablaProcesar" class="table table-striped table-hover" style="font-size: 13px">
                            <thead>
                                <tr>
                                    <th style="text-align: center">Nº Proceso</th>
                                    <th style="text-align: center">Nº Factura</th>
                                    <th style="text-align: center">Empresa</th>
                                    <th style="text-align: center">RUT</th>
                                    <th style="text-align: center">Patente</th>
                                    <th style="text-align: center">Marca</th>
                                    <th style="text-align: center">Modelo</th>
                                    <th style="text-align: center">Año</th>
                                    <th style="text-align: center">Valor</th>
                                    <th style="text-align: center">Operacion</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($facturas as $factura)
                                <tr>
                                    <td>{{ $factura->nproceso }}</td>
                                    <td>{{ $factura->nfactura }}</td>
                                    <td>{{ $factura->razonsocial }}</td>
                                    <td>{{ $factura->rut }}</td>
                                    <td>{{ $factura['datosingreso']['vehiculo']->patente}}</td>
                                    <td>{{ $factura['datosingreso']['marcavehiculo']->nombre}}</td>
                                    <td>{{ $factura['datosingreso']['modelovehiculo']->nombre}}</td>
                                    <td>{{ $factura['datosingreso']['vehiculo']->anio}}</td>
                                    <td>{{ '$'.number_format($factura->valor,0,",",".") }}</td>
                                    <td><button onclick="">Ingresar</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>                        
                        
                    </div>

                    <div id="lista_factura"></div>
                    <div id="capaModal"></div>

                </div>
            </div>
            
        </div>
    </div>
</div>



@endsection

@section('scriptsmodules')
<script src="js/facturaproveedor.js"></script>


@endsection

 