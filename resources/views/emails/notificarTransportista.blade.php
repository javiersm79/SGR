@component('mail::message')
# Introduction

Estimado {{ $ordenRetiro['transportista']['nombrechofer'] }}, 

{{-- Se le notifica que se le ha asignado la Orden de Retiro #{{ $ordenRetiro['ordenretiro']['nproceso'] }} --}}

Se adjunta en este correo orden de retiro de referencia.

Una vez que sea retirado, favor realizar click en el siguiente link:
@component('mail::button', ['url' => 'http://localhost:1606/validaror/'.$ordenRetiro['ordenretiro']['nproceso'] ])
Validar Retiro
@endcomponent

Saludos atte.
Remates Montero
@endcomponent