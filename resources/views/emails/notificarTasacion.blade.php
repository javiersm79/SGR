@component('mail::message')
# Introduction

Estimado {{ $_POST['nombre'] }},

Acorde a lo solicitado, nuestra tasación del vehículo de referencia, acorde a los daños observados, es de {{ '$'.number_format($_POST['valor'] ,0,",",".")}}<br>
@component('mail::panel')
PATENTE:  <b>{{ $_POST['patente'] }}</b><br> 
Nº SINIESTRO:  <b>{{ $_POST['nsiniestro'] }}</b>
@endcomponent
<b>Notas a la tasación</b>
<ul>
<li>Tasación válida por 60 días y considera el valor mínimo a liquidar hasta ese plazo de remate</li>
<li>Las características del vehículo deberán corresponder a lo informado en la solicitud de tasación</li>
<li>Las fotografías y/o información entregada del vehículo, considera la totalidad del daño del vehículo producto del siniestro</li>
<li>El monto tasado corresponde al vehículo con toda su documentación al día y entregada a la Casa de Remates al momento de la subasta, por parte de la Compañía de Seguros</li>
<li>El kilometraje del vehículo de uso particular, a excepción de que se informe, no deberá exceder los 25.000 kilómetros recorridos por año desde su fabricación. En caso que se supere este acorde al año, la tasación quedará sin efecto y se le informará a la Compañía de Seguros.
<li>El vehículo debe ser entregado en las mismas condiciones de las fotografías, en caso de que presente faltantes o piezas cambiadas, la tasación quedará sin efecto y se le informará a la Compañía de Seguros</li>
<li>El vehículo deberá presentar número de chasis y de motor originales, deberán coincidir a los del Certificado de Anotaciones de este (incluyendo vehículos por siniestro de robo)</li>
<li>La información entregada es confidencial para exclusivo uso de la Compañía de Seguros y la Casa de Remates</li>
</ul>
Saludos atte.
Remates Montero
@endcomponent