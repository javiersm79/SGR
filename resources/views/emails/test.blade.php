@component('mail::message')
# Introduction

Mi contenido de correo<br>
Realizado por: {{ $usuario->name }}<br>
PD: {{ $notas }}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
