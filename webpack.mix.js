let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/usuarios.js', 'public/js');
mix.js('resources/assets/js/app.js', 'public/js')
.js('resources/assets/js/usuarios.js', 'public/js')
.js('resources/assets/js/contactos.js', 'public/js')
.js('resources/assets/js/clientes.js', 'public/js')
.js('resources/assets/js/bodegas.js', 'public/js')
.js('resources/assets/js/transportistas.js', 'public/js')
.js('resources/assets/js/modelos.js', 'public/js')
.js('resources/assets/js/tasaciones.js', 'public/js')
.js('resources/assets/js/talleres.js', 'public/js')
.js('resources/assets/js/ordretiros.js', 'public/js')
.js('resources/assets/js/marcas.js', 'public/js')
.js('resources/assets/js/liquidadores.js', 'public/js')
.js('resources/assets/js/companias.js', 'public/js')
.js('resources/assets/js/actas.js', 'public/js')
.js('resources/assets/js/facturaproveedor.js', 'public/js')
.js('resources/assets/js/existencias_reportes.js', 'public/js')
.js('resources/assets/js/existencias.js', 'public/js');
