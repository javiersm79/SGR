<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('123123'),
        //'type' => $faker->randomElement(['admin' ,'user']),
        'tipo' => $faker->randomElement($array = array ('administrador' ,'socio')),
        'remember_token' => str_random(10),
    ];
 });

$factory->define(App\Bodega::class, function (Faker\Generator $faker) {


    return [
        'nombre' => $faker->text($maxNbChars = 15),
        'habilitado' => $faker->randomElement($array = array ('SI' ,'NO')),
    ];
 });

$factory->define(App\Marca::class, function (Faker\Generator $faker) {


    return [
        'nombre' => $faker->text($maxNbChars = 15),
        'habilitado' => $faker->randomElement($array = array ('SI' ,'NO')),
    ];
 });



$factory->define(App\Tasacion::class, function (Faker\Generator $faker) {


    return [
        'nproceso' => $faker->numerify('##########'),
        'patente' => strtoupper($faker->bothify('????-##')),
        'compania' => $faker->randomElement($array = array ('CARDIF','CHILENA CONSOLIDADA','HDI','MAPFRE','RENTA NACIONAL','SURA','VEDISA')),
        'email' => $faker->unique()->safeEmail,
        'nombre' => $faker->name,
        'telefono' => $faker->numerify('9########'),
        'siniestro' => $faker->numerify('##########'),
        'marca' => $faker->randomElement($array = array ('ACADIAN','ACURA','AGRALE','ALFA ROMEO','AMERICAN MOTORS','AMERITRUCK','APRILIA','ARO','ASIA','ASIA MOTORS','ASTON MARTIN','AUDI','AUSTIN','AUTOCAR','AUTORRAD','B.M.C.','BAIC','BAJAJ','BASHAN','BAW','BEDFORD','BEIGING','BELAVTOMAZ','BENELLI','BENTLEY','BENYI','BENZHOU','BERLIET','BIGFOOT','BIMOTA','BMW','BOATIAN','BORGWARD','BRILLIANCE','BUELL','BUICK','BULTACO','BUSSING','BYD','CADILLAC','CAGIVA','CAKY-CHANGAN','CAMC','CATERHAM','CFMOTO','CHANGAN','CHANGHE','CHERY','CHEVROLET','CHRYSLER','CITROEN','COMIL GALLEGIANTE','COMMER','DACIA','DAELIM','DAEWOO','DAF','DAIHATSU','DATSUN','DAYUN','DIAMOND','DIMEX','DODGE','DONGFENG','DUCATI','DUNNA','DYNA','EBRO','EUROMOT','F.S.O.','FARGO','FAW','FBM','FEDERAL','FERRARI','FIAT','FLSTF','FORD','FORD TAUNUS','FORD THAMES','FOTON','FOX','FREIGHTLINER','FXA','G.M.C.','GAC GONOW','GARELLI','GASGAS','GEELY','GELLY','GILERA','GOLDEN DRAGON','GREAT WALL','GREYHOUND','GUZZI','HAFEI','HAIMA','HAO JUE','HARLEY','HARLEY-DAVIDSON','HARTFORD','HAVAL','HENSCHEL','HENSIM','HERO-PUCH','HIGER','HILLMAN','HINO','HONDA','HUANGHAI','HUSABERG','HUSQVARNA','HYOSUNG','HYUNDAI','INDIAN','INFINITI','INTERNATIONAL','ISUZU','IVECO','JAC','JAGUAR','JAWA','JBC JINBEI','JEEP','JIANSHE','JIE FANG','JINBEI','JMC','KAMAZ','KAWASAKI','KAYAK','KEEWAY','KENWORTH','KIA','KIA MOTORS','KING LONG','KINLON','KTM','KYMCO','LADA','LAMBORGHINI','LAMBRETTA','LANCIA','LAND ROVER','LANDWIND','LEXUS','LEYLAND','LIAZ','LIFAN','LINCOLN','LML','LONCIN','LOTUS','LUOJIA','MACK','MAGIRUS','MAGIRUS DEUTZ','MAHINDRA','MAICO','MAN','MARCOPOLO','MASERATI','MAXUS','MAZDA','MCLAREN','MERCEDES BENZ','MERCURY','MG','MINI','MITSUBISHI','MONTELLI','MONTESA','MORGAN','MORRIS','MOTO BECANE','MOTORRAD','MSK','MV AGUSTA','NISSAN','NSU','O.M.','ODES','OLDSMOBILE','OPEL','OSSA','PEGASO','PEUGEOT','PGO','PIAGGIO','PIONNER','PLYMOUTH','POLARIS','POLSKI FIAT','PONTIAC','PORSCHE','PROTON','PULLMAN STD AMERIC','PUMA','REGAL RAPTOR','RENAULT','RENAULT(Berliet)','REO','RIZATO','ROLLS ROYCE','ROMAN','ROVER','S/M','SAAB','SACHS','SAEHAN','SAMSUNG','SANFU','SANLG','SANYA','SANYANG SYM','SCANIA','SEAT','SETRA (Mercedes Benz)','SG','SHACMAN','SHENFENG','SHENYANG JINBEI','SHINERAY','SIMCA','SINOTRUK','SKODA','SKYGO','SMA','SPITZ','SSANGYONG','STUDEBAKER','SUBARU','SUNLONG','SUZUKI','TAKASAKI','TATA','TATRA','TM','TORITO','TOYOTA','TRIUMPH','TVS','UNITED MOTORS','URAL','VERONA','VESPA','VOLARE','VOLKSWAGEN','VOLVO','WABCO','WANGYE','WESTERN STAR','WHITE','WILLYS','WOLKEN','XGJAO','XINGYUE','XMOTORS','YAMAHA','YAMAMOTO','YOUYI','YUE JIN','YUGO','YUTONG','ZANELLAS','ZANELLO','ZASTAVA','ZNEN GROUP','ZONGSHEN','ZOTYE','ZUNDAPP','ZX')),
        'modelo' => $faker->text($maxNbChars = 15),
        'anio' => $faker->numberBetween($min = 1990, $max = 2017),
        'valor' => '$'.number_format(($faker->numerify('########'))),
        'fecha' => $faker->date($format = 'd-m-Y', $max = 'now'),
        'estado' => $faker->randomElement($array = array ('ANULADO','O RETIRO','REMATADO')),

    ];
 });


$factory->define(App\Modelo::class, function (Faker\Generator $faker) {


    return [
        'nombre' => $faker->text($maxNbChars = 15),
        'marca' => $faker->randomElement($array = array ('ACADIAN','ACURA','AGRALE','ALFA ROMEO','AMERICAN MOTORS','AMERITRUCK','APRILIA','ARO','ASIA','ASIA MOTORS','ASTON MARTIN','AUDI','AUSTIN','AUTOCAR','AUTORRAD','B.M.C.','BAIC','BAJAJ','BASHAN','BAW','BEDFORD','BEIGING','BELAVTOMAZ','BENELLI','BENTLEY','BENYI','BENZHOU','BERLIET','BIGFOOT','BIMOTA','BMW','BOATIAN','BORGWARD','BRILLIANCE','BUELL','BUICK','BULTACO','BUSSING','BYD','CADILLAC','CAGIVA','CAKY-CHANGAN','CAMC','CATERHAM','CFMOTO','CHANGAN','CHANGHE','CHERY','CHEVROLET','CHRYSLER','CITROEN','COMIL GALLEGIANTE','COMMER','DACIA','DAELIM','DAEWOO','DAF','DAIHATSU','DATSUN','DAYUN','DIAMOND','DIMEX','DODGE','DONGFENG','DUCATI','DUNNA','DYNA','EBRO','EUROMOT','F.S.O.','FARGO','FAW','FBM','FEDERAL','FERRARI','FIAT','FLSTF','FORD','FORD TAUNUS','FORD THAMES','FOTON','FOX','FREIGHTLINER','FXA','G.M.C.','GAC GONOW','GARELLI','GASGAS','GEELY','GELLY','GILERA','GOLDEN DRAGON','GREAT WALL','GREYHOUND','GUZZI','HAFEI','HAIMA','HAO JUE','HARLEY','HARLEY-DAVIDSON','HARTFORD','HAVAL','HENSCHEL','HENSIM','HERO-PUCH','HIGER','HILLMAN','HINO','HONDA','HUANGHAI','HUSABERG','HUSQVARNA','HYOSUNG','HYUNDAI','INDIAN','INFINITI','INTERNATIONAL','ISUZU','IVECO','JAC','JAGUAR','JAWA','JBC JINBEI','JEEP','JIANSHE','JIE FANG','JINBEI','JMC','KAMAZ','KAWASAKI','KAYAK','KEEWAY','KENWORTH','KIA','KIA MOTORS','KING LONG','KINLON','KTM','KYMCO','LADA','LAMBORGHINI','LAMBRETTA','LANCIA','LAND ROVER','LANDWIND','LEXUS','LEYLAND','LIAZ','LIFAN','LINCOLN','LML','LONCIN','LOTUS','LUOJIA','MACK','MAGIRUS','MAGIRUS DEUTZ','MAHINDRA','MAICO','MAN','MARCOPOLO','MASERATI','MAXUS','MAZDA','MCLAREN','MERCEDES BENZ','MERCURY','MG','MINI','MITSUBISHI','MONTELLI','MONTESA','MORGAN','MORRIS','MOTO BECANE','MOTORRAD','MSK','MV AGUSTA','NISSAN','NSU','O.M.','ODES','OLDSMOBILE','OPEL','OSSA','PEGASO','PEUGEOT','PGO','PIAGGIO','PIONNER','PLYMOUTH','POLARIS','POLSKI FIAT','PONTIAC','PORSCHE','PROTON','PULLMAN STD AMERIC','PUMA','REGAL RAPTOR','RENAULT','RENAULT(Berliet)','REO','RIZATO','ROLLS ROYCE','ROMAN','ROVER','S/M','SAAB','SACHS','SAEHAN','SAMSUNG','SANFU','SANLG','SANYA','SANYANG SYM','SCANIA','SEAT','SETRA (Mercedes Benz)','SG','SHACMAN','SHENFENG','SHENYANG JINBEI','SHINERAY','SIMCA','SINOTRUK','SKODA','SKYGO','SMA','SPITZ','SSANGYONG','STUDEBAKER','SUBARU','SUNLONG','SUZUKI','TAKASAKI','TATA','TATRA','TM','TORITO','TOYOTA','TRIUMPH','TVS','UNITED MOTORS','URAL','VERONA','VESPA','VOLARE','VOLKSWAGEN','VOLVO','WABCO','WANGYE','WESTERN STAR','WHITE','WILLYS','WOLKEN','XGJAO','XINGYUE','XMOTORS','YAMAHA','YAMAMOTO','YOUYI','YUE JIN','YUGO','YUTONG','ZANELLAS','ZANELLO','ZASTAVA','ZNEN GROUP','ZONGSHEN','ZOTYE','ZUNDAPP','ZX')),
        'tipo' => $faker->randomElement($array = array ('AUTOMOVIL', 'STATION WAGON', 'TODO TERRENO', 'CAMIONETA', 'MOTO', 'MINIBUS', 'FURGON', 'MOTOR HOME', 'CAMIONES', 'BUSES Y TAXIBUSES', 'TROLEBUSES')),
        'habilitado' => $faker->randomElement($array = array ('SI' ,'NO')),
    ];
 });

$factory->define(App\Vehiculo::class, function (Faker\Generator $faker) {


    return [
        'patente' => strtoupper($faker->bothify('????-##')),
        //'patente' => $faker->lexify('????')."-".$faker->numberBetween($min = 10, $max = 99),
        'marcaid' => $faker->randomDigitNotNull,
        'modeloid' => $faker->randomDigitNotNull,
        'tipoid' => $faker->randomDigitNotNull,
        'anio' => $faker->numberBetween($min = 1990, $max = 2017),
        'combustible' => strtoupper($faker->randomElement($array = array ('bencina' ,'disiel'))),
        'nummotor' => $faker->numerify('##########'),
        'numchasis' => $faker->numerify('##########'),
        'serial' => strtoupper($faker->bothify('##??#?#???####?')),
        'VIM' => strtoupper($faker->bothify('##??#?#???####?')),
        'color' => $faker->safeColorName,
        'rutpropietario' => $faker->numerify('########')."-".$faker->randomDigitNotNull,
        'nombrepropietario' => $faker->name,
        'observacion' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'actualizadopor' => $faker->numberBetween($min = 1, $max = 150),
        
    ];
 });

$factory->define(App\Transportista::class, function (Faker\Generator $faker) {


    return [
        'rutempresa' => $faker->numerify('#########-#'),
        'nombreempresa' => $faker->name,
        'camion' => $faker->realText($maxNbChars = 10, $indexSize = 2),
        'telefono' => $faker->tollFreePhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'rutchofer' => $faker->numerify('#########-#'),
        'nombrechofer' => $faker->name($gender = 'male'),
        'gruaexterna' => strtoupper($faker->randomElement($array = array ('SI' ,'NO'))),
        'habilitado' => strtoupper($faker->randomElement($array = array ('SI' ,'NO'))),
        
        
    ];
 });

$factory->state(App\User::class, 'administrador', function (\Faker\Generator $faker) {
  return [
    'tipo' => 'administrador',
  ];
});

$factory->state(App\User::class, 'socio', function (\Faker\Generator $faker) {
  return [
    'tipo' => 'socio',
  ];
});
