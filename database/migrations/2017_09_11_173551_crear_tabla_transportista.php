<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTransportista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportistas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rutempresa',30);
            $table->string('nombreempresa',250);
            $table->string('patentecamion',200);
            $table->string('telefono',50);
            $table->string('email',150);
            $table->string('rutchofer',30);
            $table->string('nombrechofer',250);
            $table->string('gruaexterna',5);
            $table->string('habilitado',5);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportistas');
    }
}
