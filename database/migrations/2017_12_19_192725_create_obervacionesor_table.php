<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObervacionesorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observacionesor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idordenretiro');
            $table->integer('companiaid');
            $table->date('fecha');
            $table->integer('userid');
            $table->string('observacion',300);
            $table->integer('privada');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observacionesor');
    }
}
