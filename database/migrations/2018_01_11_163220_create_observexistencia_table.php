<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservexistenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observexistencia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idingreso');
            $table->integer('companiaid');
            $table->date('fecha');
            $table->integer('userid');
            $table->string('observacion',300);
            $table->integer('privada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observexistencia');
    }
}
