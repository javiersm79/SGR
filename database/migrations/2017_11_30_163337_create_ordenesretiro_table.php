<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesretiroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenesretiro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nproceso',150);
            $table->string('estado');
            $table->date('fecha');
            $table->integer('compania');
            $table->integer('vehiculo');
            $table->string('nsiniestro',150);
            $table->integer('contacto');
            $table->integer('tasacion');
            $table->integer('vminremate')->default(0);
            $table->integer('vtraslado')->default(0);
            $table->string('nfacturaproveedor',150)->default('N/R');
            $table->integer('vfacturacia')->default(0);
            $table->string('nfactura',150)->default('N/R');
            $table->integer('vindem')->default(0);
            $table->integer('liquidador')->default(0);
            $table->integer('transportista');
            $table->integer('taller');
            $table->integer('creadopor');
            $table->integer('actualizadopor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenesretiro');
    }
}
