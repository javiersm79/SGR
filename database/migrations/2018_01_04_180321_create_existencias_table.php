<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nproceso',150);
            $table->string('estado');
            $table->integer('acta')->default(0);
            $table->date('fecha');
            $table->integer('compania');
            $table->integer('vehiculo');
            $table->string('nsiniestro',150);
            $table->integer('contacto');
            $table->integer('tasacion');
            $table->integer('vminremate')->default(0);
            $table->integer('vtraslado')->default(0);
            $table->string('nfacturaproveedor',150)->default('N/R');
            $table->integer('vfacturacia')->default(0);
            $table->string('nfactura',150)->default('N/R');
            $table->integer('vindem')->default(0);
            $table->integer('liquidador')->default(0);
            $table->integer('transportista');
            $table->integer('taller');
            $table->string('permiso',150)->nullable()->default(null);
            $table->string('seguro',150)->nullable()->default(null);
            $table->string('padron',150)->nullable()->default(null);
            $table->string('revtec',150)->nullable()->default(null);
            $table->string('gases',150)->nullable()->default(null);
            $table->string('llaves',150)->nullable()->default(null);
            $table->string('placas',150)->nullable()->default(null);
            $table->string('tag',150)->nullable()->default(null);
            $table->string('computador',150)->nullable()->default(null);
            $table->string('motorarranca',150)->nullable()->default(null);
            $table->integer('kilometraje')->nullable()->default(0);
            $table->string('airbag',150)->nullable()->default(null);
            $table->integer('bodegas')->default(0)->nullable()->default(null);
            $table->string('condicionado')->default(0)->nullable()->default(null);
            $table->string('desarme')->default(0)->nullable()->default(null);

            $table->integer('creadopor');
            $table->integer('actualizadopor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existencias');
    }
}
