<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActasExitenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actas_existencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numeroacta');
            $table->integer('idexistencia');
            $table->integer('lote')->nullable();
            $table->integer('cliente')->nullable();
            $table->string('estado',25)->nullable();
            $table->integer('monto')->nullable();
            $table->integer('comision')->nullable();
            $table->integer('iva')->nullable();
            $table->integer('gastosoperacionales')->nullable();
            $table->integer('tasacionfiscal')->nullable();
            $table->integer('garantia')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actas_existencias');
    }
}
