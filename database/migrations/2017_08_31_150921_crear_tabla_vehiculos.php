<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaVehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patente',10);
            $table->integer('marcaid');
            $table->integer('modeloid');
            $table->integer('tipoid');
            $table->integer('anio');
            $table->string('combustible', 50);
            $table->string('nummotor', 50);
            $table->string('numchasis', 50);
            $table->string('serial', 50);
            $table->string('VIM', 50);
            $table->string('color', 100);
            //$table->integer('llaves');
            //$table->integer('tag');
            //$table->integer('computador');
            //$table->integer('motorarranca');
            //$table->string('aribag');
            //$table->string('placas');
            $table->string('rutpropietario',15);
            $table->string('nombrepropietario',250);
            $table->string('observacion', 250);
            $table->integer('actualizadopor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
