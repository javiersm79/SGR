<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturatrasladoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturatraslado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nfactura',250);
            $table->integer('valor');
            $table->date('fecha');
            $table->string('rut',25);
            $table->string('razonsocial',250);
            $table->string('concepto',250);
            $table->string('nproceso',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturatraslado');
    }
}
