<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patente',25);
            $table->integer('compania');
            $table->string('email',150);
            $table->string('nombre',150);
            $table->string('telefono',150);
            $table->string('siniestro',150);
            $table->integer('marcaid');
            $table->integer('modeloid');
            $table->integer('tipoid');
            $table->integer('anio');
            $table->string('valor',150);
            $table->string('fecha',25);
            $table->string('estado',55);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasaciones');
    }
}
