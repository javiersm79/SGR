<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',250);
            $table->string('apellido',250);
            $table->string('rut',250)->nullable();
            $table->string('email',250);
            $table->string('emailalt',250)->nullable();
            $table->string('fechanac',20)->nullable();
            $table->string('sexo',250);
            $table->string('fono1',50)->nullable();
            $table->string('fono2',50)->nullable();
            $table->string('compania',50);
            $table->integer('userid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
