<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasacion extends Model
{
    protected $table = 'tasaciones';
    protected $fillable = [
        'patente','compania','email','nombre','telefono','siniestro','marcaid','modeloid','tipoid','anio','valor','fecha','estado',
    ];
}
