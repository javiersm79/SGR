<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Existencia extends Model
{
    protected $table = 'existencias';
    protected $fillable = [
         'nproceso', 'estado','fecha','acta', 'compania', 'vehiculo',  'nsiniestro', 'contacto', 'tasacion','vminremate', 'vtraslado', 'nfacturaproveedor','vfacturacia', 'nfactura','vindem', 'liquidador', 'transportista',  'taller',  'creadopor', 'actualizadopor', 
    ];


}
