<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{

    protected $table = 'contactos';
    protected $fillable = [
         'nombre', 'apellido', 'rut', 'email', 'emailalt', 'fechanac', 'sexo', 'fono1', 'fono2', 'compania', 'userid',
    ];
}
