<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observor extends Model
{
    protected $table = 'observacionesor';
    protected $fillable = [
          'idordenretiro', 'fecha', 'companiaid', 'userid', 'observacion', 'privada',
    ];
}
