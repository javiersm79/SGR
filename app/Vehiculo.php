<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
	protected $table = 'vehiculos';
	protected $fillable = ['patente', 'marcaid', 'modeloid', 'tipoid', 'anio', 'combustible', 'nummotor', 'numchasis', 'serial', 'VIM', 'color', 'rutpropietario', 'nombrepropietario', 'observacion', 'actualizadopor'];
	//protected $fillable = ['patente', 'marcaid', 'modeloid', 'tipoid', 'anio', 'combustible', 'nummotor', 'numchasis', 'serial', 'VIM', 'color', 'llaves', 'tag', 'computador', 'motorarranca', 'aribag', 'placas', 'rutpropietario', 'nombrepropietario', 'observacion', 'actualizadopor'];

}
