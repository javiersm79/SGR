<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordenretiro extends Model
{

    protected $table = 'ordenesretiro';
    protected $fillable = [
         'nproceso', 'estado','fecha', 'compania', 'vehiculo',  'nsiniestro', 'contacto', 'tasacion','vminremate', 'vtraslado', 'nfacturaproveedor','vfacturacia', 'nfactura','vindem', 'liquidador', 'transportista',  'taller',  'creadopor', 'actualizadopor', 
    ];
    
}