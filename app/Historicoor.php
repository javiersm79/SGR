<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historicoor extends Model
{
    protected $table = 'historicoor';
    protected $fillable = [
        'nproceso', 'operacion', 'usuario'
    ];
}