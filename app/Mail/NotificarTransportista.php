<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarTransportista extends Mailable
{
    public $ordenRetiro;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ordenRetiroData)
    {
        //var_dump($ordenRetiroData);
        $this->ordenRetiro = $ordenRetiroData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('emails.notificarTransportista')->from('sive@vedisaremates.cl', 'Sistema SIVE')->subject('Asignación de Orden de Retiro #'.$this->ordenRetiro['ordenretiro']['nproceso'])->attach('storage/files/or/'.$this->ordenRetiro['ordenretiro']['nproceso'].'/OR'.$this->ordenRetiro['ordenretiro']['nproceso'].'.pdf');
    }
}
