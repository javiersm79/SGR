<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observexistencia extends Model
{
    protected $table = 'observexistencia';
    protected $fillable = [
          'idingreso', 'fecha', 'companiaid', 'userid', 'observacion', 'privada',
    ];
}
