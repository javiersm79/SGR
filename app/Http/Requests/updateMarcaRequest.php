<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Marca;
class updateMarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $marca = \App\Marca::where('id', '=' ,(int)$this->get('idMarca'))->firstOrFail();
        return [
            'nombre' => 'required|min:3|max:200|unique:marcas,nombre,'.$marca->id,
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'nombre.unique' => 'Nombre de Marca registrado',
            'nombre.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 200 caracteres',
        ];
    }
}
