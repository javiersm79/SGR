<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newModeloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|unique:modelos|min:3|max:200',
            'marca' => 'required',
            //'tipo' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'marca.required' => 'Este campo es obligatorio',
            //'tipo.required' => 'Este campo es obligatorio',
            'nombre.unique' => 'Nombre de Modelo registrado',
            'nombre.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 200 caracteres',
        ];
    }
}
