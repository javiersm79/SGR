<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateCompaniaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $compania = \App\Compania::where('id', '=' ,(int)$this->get('idCompania'))->firstOrFail();
        //dd($compania);
        return [
            'nombre' => 'required|min:3|max:200|unique:companias,nombre,'.$compania->id,
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'nombre.unique' => 'Nombre de Compañia registrado',
            'nombre.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 200 caracteres',
        ];
    }
}
