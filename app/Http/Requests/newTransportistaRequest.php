<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newTransportistaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rutempresa' => 'required|unique:transportistas|min:9|max:12',
            'nombreempresa' => 'required|min:3|max:200',
            'telefono' => 'required|min:9|max:15',
            'email' => 'required|email|unique:transportistas|max:100',
            'rutchofer' => 'required|unique:transportistas|min:9|max:12',
            'nombrechofer' => 'required|min:3|max:200',
            'gruaexterna' => 'required',
            'patentecamion' => 'required|unique:transportistas',
            'habilitado' => 'required',
        ];
    }
    public function messages()
    {
        //'rutempresa', 'nombreempresa','camion','telefono','email','rutchofer','nombrechofer','gruaexterna','habilitado',
        return [
            'rutempresa.required' => 'Este campo es obligatorio',
            'rutempresa.unique' => 'Rut de empresa registrado',
            'rutempresa.min' => 'Mínimo 9 caracteres',
            'rutempresa.max' => 'Máximo 12 caracteres',
            'telefono.required' => 'Este campo es obligatorio',
            'telefono.min' => 'Mínimo 9 caracteres',
            'telefono.max' => 'Máximo 12 caracteres',
            'email.required' => 'Este campo es obligatorio',
            'email.email' => 'Formato no valido',
            'email.unique' => 'Email registrado',
            'email.max' => 'Máximo 100 caracteres',
            'nombreempresa.required' => 'Este campo es obligatorio',
            'nombreempresa.min' => 'Mínimo 9 caracteres',
            'nombreempresa.max' => 'Máximo 200 caracteres',
            'rutchofer.required' => 'Este campo es obligatorio',
            'rutchofer.unique' => 'Rut Chofer  registrado',
            'rutchofer.min' => 'Mínimo 9 caracteres',
            'rutchofer.max' => 'Máximo 12 caracteres',
            'patentecamion.required' => 'Este campo es obligatorio',
            'patentecamion.unique' => 'Patente duplicada',
            'nombrechofer.required' => 'Este campo es obligatorio',
            'nombrechofer.min' => 'Mínimo 3 caracteres',
            'nombrechofer.max' => 'Máximo 200 caracteres',
            'gruaexterna.required' => 'Este campo es obligatorio',
            'habilitado.required' => 'Este campo es obligatorio',
        ];
    }
}
