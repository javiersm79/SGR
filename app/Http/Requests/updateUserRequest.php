<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:5|max:190',
            'apellidos' => 'required|min:5|max:190',
            
            'perfil' => 'required',
            'password' => 'required|min:4|max:8',
            'email' => 'required|email|max:100',
            'estado' => 'required',
            'compania' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'apellidos.required' => 'Este campo es obligatorio',
            
            'nombre.min' => 'Mínimo 5 caracteres',
            'apellidos.min' => 'Mínimo 5 caracteres',
            'nombre.max' => 'Máximo 190 caracteres',
            'apellidos.max' => 'Máximo 190 caracteres',
            'perfil.required' => 'Este campo es obligatorio',
            'password.required' => 'Este campo es obligatorio  (min: 4 / max: 8)',
            'password.min' => 'Mínimo 4 caracteres',
            'password.max' => 'Máximo 8 caracteres',
            'email.required' => 'Este campo es obligatorio',           
            'email.email' => 'Formato de email erroneo',           
            'email.unique' => 'Email registrado en el sistema',           
            'estado.required' => 'Este campo es obligatorio',           
            'compania.required' => 'Este campo es obligatorio',           
        ];
    }
}
