<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:190',
            'apellidos' => 'required|min:3|max:190',
            //'nombreUpdate' => 'required|min:5|max:10',
            'perfil' => 'required',
            'password' => 'required|min:4|max:8',
            'email' => 'required|email|unique:users|max:100',
            'estado' => 'required',
            'compania' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'apellidos.required' => 'Este campo es obligatorio',
            'nombre.min' => 'Mínimo 3 caracteres',
            'apellidos.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 190 caracteres',
            'apellidos.max' => 'Máximo 190 caracteres',
            'perfil.required' => 'Este campo es obligatorio',
            'password.required' => 'Este campo es obligatorio  (min: 4 / max: 8)',
            'password.min' => 'Mínimo 4 caracteres',
            'password.max' => 'Máximo 8 caracteres',
            'email.required' => 'Este campo es obligatorio',           
            'email.email' => 'Formato de email erroneo',           
            'email.unique' => 'Email registrado en el sistema',           
            'estado.required' => 'Este campo es obligatorio',           
            'compania.required' => 'Este campo es obligatorio',           
        ];
    }
}
