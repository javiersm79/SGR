<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateBodegaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bodega = \App\Bodega::where('id', '=' ,(int)$this->get('idBodega'))->firstOrFail();
        return [
            'nombre' => 'required|min:3|max:200|unique:bodegas,nombre,'.$bodega->id,
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'nombre.unique' => 'Nombre de Bodega registrado',
            'nombre.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 200 caracteres',
        ];
    }
}
