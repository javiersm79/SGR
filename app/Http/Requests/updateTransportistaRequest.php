<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateTransportistaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        $transportista = \App\Transportista::where('id', '=' ,(int)$this->get('idTransportista'))->firstOrFail();
        return [
            'rutempresa' => 'required||min:9|max:12|unique:transportistas,rutempresa,'.$transportista->id,
            'nombreempresa' => 'required|min:3|max:200',
            'telefono' => 'required|min:9|max:15',
            'email' => 'required|email|max:100|unique:transportistas,rutempresa,'.$transportista->id,
            'rutchofer' => 'required|min:9|max:12|unique:transportistas,rutchofer,'.$transportista->id,
            'patentecamion' => 'required|min:7|max:12|unique:transportistas,patentecamion,'.$transportista->id,
            'nombrechofer' => 'required|min:3|max:200',
            'gruaexterna' => 'required',
            'habilitado' => 'required',
        ];
    }
    public function messages()
    {
        //'rutempresa', 'nombreempresa','camion','telefono','email','rutchofer','nombrechofer','gruaexterna','habilitado',
        return [
            'rutempresa.required' => 'Este campo es obligatorio',
            'rutempresa.unique' => 'Rut de empresa registrado',
            'rutempresa.min' => 'Mínimo 9 caracteres',
            'rutempresa.max' => 'Máximo 12 caracteres',
            'telefono.required' => 'Este campo es obligatorio',
            'telefono.min' => 'Mínimo 9 caracteres',
            'telefono.max' => 'Máximo 12 caracteres',
            'email.required' => 'Este campo es obligatorio',
            'email.email' => 'Formato no valido',
            'email.unique' => 'Email registrado',
            'email.max' => 'Máximo 100 caracteres',
            'nombreempresa.required' => 'Este campo es obligatorio',
            'nombreempresa.min' => 'Mínimo 9 caracteres',
            'nombreempresa.max' => 'Máximo 200 caracteres',
            'rutchofer.required' => 'Este campo es obligatorio',
            'rutchofer.unique' => 'Rut Chofer  registrado',
            'rutchofer.min' => 'Mínimo 9 caracteres',
            'rutchofer.max' => 'Máximo 12 caracteres',
            'nombrechofer.required' => 'Este campo es obligatorio',
            'nombrechofer.min' => 'Mínimo 3 caracteres',
            'nombrechofer.max' => 'Máximo 200 caracteres',
            'gruaexterna.required' => 'Este campo es obligatorio',
            'patentecamion.unique' => 'Patente registrada',
            'patentecamion.min' => 'Mínimo 7 caracteres',
        ];
    }
}
