<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newTallerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|unique:talleres|min:3|max:200',
            'direccion' => 'required|unique:talleres|min:3|max:200',
            'rut' => 'required|unique:talleres|min:9|max:15',
            'comunas' => 'required',
            'regiones' => 'required',
            'ciudad' => 'required|min:3|max:200',
            'telefono' => 'required|min:3|max:200',
            'email' => 'required|unique:talleres|min:3|max:200',
            'nombcontacto' => 'required|min:3|max:200',
            //
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'nombre.unique' => 'Nombre de Taller registrado',
            'direccion.required' => 'Este campo es obligatorio',
            'direccion.unique' => 'Dirección de Taller registrado',
            'rut.required' => 'Este campo es obligatorio',
            'rut.unique' => 'RUT de Taller registrado',
            'comunas.required' => 'Este campo es obligatorio',
            'regiones.required' => 'Este campo es obligatorio',
            'ciudad.required' => 'Este campo es obligatorio',
            'telefono.required' => 'Este campo es obligatorio',
            'email.required' => 'Este campo es obligatorio',
            'email.unique' => 'Email de Taller registrado',
            'nombcontacto.required' => 'Este campo es obligatorio',
            'nombre.min' => 'Mínimo 3 caracteres',
            'rut.min' => 'Mínimo 9 caracteres',
            'direccion.min' => 'Mínimo 3 caracteres',
            'ciudad.min' => 'Mínimo 3 caracteres',
            'telefono.min' => 'Mínimo 3 caracteres',
            'email.min' => 'Mínimo 3 caracteres',
            'nombcontacto.min' => 'Mínimo 3 caracteres',
            'nombre.max' => 'Máximo 200 caracteres',
            'rut.max' => 'Máximo 15 caracteres',
            'direccion.max' => 'Máximo 200 caracteres',
            'ciudad.max' => 'Máximo 200 caracteres',
            'telefono.max' => 'Máximo 200 caracteres',
            'email.max' => 'Máximo 200 caracteres',
            'nombcontacto.max' => 'Máximo 200 caracteres',
            
            'email.required' => 'Este campo es obligatorio',           
            'email.email' => 'Formato de email erroneo',           
        ];
    }
}