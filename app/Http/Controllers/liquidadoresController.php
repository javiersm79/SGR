<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Liquidador;

class liquidadoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$liquidadores = Liquidador::get();
        
        //return $liquidadores;
        $liquidadores = DB::table('liquidadores')
            ->join('companias', 'liquidadores.compania', '=', 'companias.id')
            ->select('liquidadores.*', 'companias.nombre as empresa')
            ->get();
         //return User::find($id);
         return $liquidadores;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $liquidador = new Liquidador([
          'liquidador' => $request->get('liquidador'),
          'compania' => $request->get('compania')
          
        ]);
        $liquidador->save();
        alert()->success('Registros creado exitosamente', 'Liquidador Creado')->autoclose(3000);
        return redirect()->to('liquidadores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Liquidador::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $liquidador = Liquidador::find($id);

        $liquidador->liquidador = $request->get('liquidador');
        $liquidador->compania = $request->get('compania');
        $liquidador->save();
        alert()->success('Registros actualizado exitosamente', 'Liquidador Actualizado')->autoclose(3000);

        return redirect()->to('liquidadores');

    }

    /**
     * Lista de liquidadores por empresa
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function liquidadoresPorCompania($compania)
    {
        $liquidadores = DB::table('liquidadores')
            ->join('companias', 'liquidadores.compania', '=', 'companias.id')
            ->select('liquidadores.*', 'companias.nombre as empresa')
            ->where('liquidadores.compania', '=', $compania)
            ->get();
         //return User::find($id);
         return $liquidadores;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $liquidador = Liquidador::find($id);
        $liquidador->delete();
        return;
        //alert()->success('Registros eliminado exitosamente', 'Liquidador eliminado')->autoclose(3000);
        //return redirect()->to('liquidadores');
    }
}
