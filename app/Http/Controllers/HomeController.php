<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Marca;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        $test= new Marca;
        $mensaje = $test->imprimir();
        echo $mensaje;



    }
    public function index()
    {
        return view('home');
    }



     public function archivotxt()
    {
       //$content = \View::make('txt.facturaliquidacion')->with('nombre', 'JAVIER');
        $texto ="1~33\r\n
2~2728\r\n
3~2018-02-20\r\n
6~1\r\n
7~2018-02-20\r\n
9~76114336-0\r\n
10~VEDISA REMATES LIMITADA\r\n
11~ACTIVIDADES DE SUBASTA (MARTILLEROS)\r\n
12~749950\r\n
13~SAN GERARDO 913\r\n
14~RECOLETA\r\n
15~SANTIAGO\r\n
19~14018460-8\r\n
20~FLAVIO MAURICIO HERNANDEZ QUEZADA\r\n
21~PARTICULAR\r\n
22~19 NORTE B 3141 PARQUE BICENTENARIO\r\n
23~TALCA\r\n
24~TALCA\r\n
35~128333\r\n
37~19.00\r\n
38~24383\r\n
39~152716\r\n
43~1~~LOTE 7    COMISION POR REMATE VEHICULO WF 42 77   ~~1~~86400~~~86400~~~~~\r\n
43~2~~GTOS ADMINISTRATIVOS FACT 1753          ~~1~~31933~~~31933~~~~~\r\n
43~3~~GASTOS TAG                              ~~1~~10000~~~10000~~~~~\r\n";
//dd($content);
  // Set the name of the text file
    $content = nl2br(e($texto));
  $filename = 'WhateverYouWant.txt';

  // Set headers necessary to initiate a download of the textfile, with the specified name
  $headers = array(
      'Content-Type' => 'plain/txt',
      'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
      'Content-Length' => strlen($texto),
  );

  return \Response::make($texto, 200, $headers);
    }    
}
