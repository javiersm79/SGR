<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacturaTraslado;
use App\Existencia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class FacturaTrasladoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ingreso = new existenciasController();

        $facturas = FacturaTraslado::all();

        for ($x = 0; $x < count($facturas); $x++) {
            //echo "The number is: $facturas[$x] <br>";
            $facturas[$x]["datosingreso"] = $ingreso->showFullDataIngresoNproceso($facturas[$x]->nproceso);
        } 
//dd($facturas);
       // $ingreso = $ingreso->showFullDataIngresoNproceso($facturas[0]->nproceso);

        //return $ingreso;
        return view("administrador.facturacion.proveedor.listatraslados")->with('facturas', $facturas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ingreso = new Existencia();
        //$ingreso = $ingreso::find(1);
        $ingreso = $ingreso::where('nproceso','like',"$request->nproceso")->first();
        //return $ingreso->vtraslado;
        //$ingreso->save();
//return $ingreso;
        $date = Carbon::createFromFormat('d/m/Y', $request->fecha)->toDateString();
        $factura= new FacturaTraslado();
        $factura->nproceso = $request->nproceso;
        $factura->valor = $request->valor;
        $factura->fecha = $date;
        $factura->nfactura = $request->nfactura;
        $factura->rut = $request->rut;
        $factura->razonsocial = $request->razonsocial;
        $factura->concepto = $request->concepto;
        //$factura->save();
        //return $request->all();
        $validator = Validator::make($request->all(), [
            'rut' => 'required|string|min:6|max:10',
            'nfactura' => 'required|string|min:1|unique:facturatraslado',
            'razonsocial' => 'required|string|min:3|max:200',
            'concepto' => 'required|string|min:10|max:200',
            'valor' => 'required|numeric|between:1,99999999999999999999999999999',
          ], [
            'rut.required' => '<b>RUT:</b> Es un campo Requerido',
            'rut.max' => '<b>RUT:</b> Maximo 4 caracteres!',
            'rut.min' => '<b>RUT:</b> Minimo 3 caracteres!',
            'nfactura.required' => '<b>Nº Factura:</b> Es un campo Requerido',
            'nfactura.unique' => '<b>Nº Factura:</b> Valor igresado anteriormente',
            'nfactura.min' => '<b>Nº Factura:</b> Minimo 6 caracteres!',
            'razonsocial.required' => '<b>Razon Social:</b> Es un campo Requerido',
            'concepto.required' => '<b>Concepto:</b> Es un campo Requerido',
            'concepto.min' => '<b>Concepto:</b> Minimo 10 caracteres!',
            'valor.required' => '<b>Valor:</b> Es un campo Requerido',
            'valor.between' => '<b>Valor:</b> Debe ser mayor a 0 (cero)',
          ]);

        if($validator->fails()) {
            
            return $validator->messages();
        }
        else
        {
            $ingreso->vtraslado = $request->valor;
            $ingreso->nfacturaproveedor = $request->nfactura;
            $ingreso->transportista = $request->idrut;
            $ingreso->save();
            $factura->save();
            return 0;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factura = FacturaTraslado::findorfail($id);
        return $factura;
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
