<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\newTallerRequest;
use App\Http\Requests\updateTallerRequest;
use App\Taller;

class talleresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $talleres = Taller::get();
        //return [$usuarios, $companias];
        return $talleres;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newTallerRequest $request)
    {
         
        $taller = new Taller([
          'nombre' => $request->get('nombre'),
          'rut' => $request->get('rut'),
          'direccion' => $request->get('direccion'),
          'region' => $request->get('regiones'),
          'comuna' => $request->get('comunas'),
          'ciudad' => $request->get('ciudad'),
          'telefono' => $request->get('telefono'),
          'email' => $request->get('email'),
          'contacto' => $request->get('nombcontacto')
        ]);
        $taller->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Taller Creado')->autoclose(3000);
        //dd($taller);

        return redirect()->to('talleres');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Taller::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateTallerRequest $request, $id)
    {
        //dd($request);
        $taller = Taller::find($id);
        //dd($taller);

        $taller->nombre = $request->get('nombre');
        $taller->rut = $request->get('rut');
        $taller->direccion = $request->get('direccion');
        $taller->region = $request->get('regiones');
        $taller->comuna = $request->get('comunas');
        $taller->ciudad = $request->get('ciudad');
        $taller->telefono = $request->get('telefono');
        $taller->email = $request->get('email');
        $taller->contacto = $request->get('nombcontacto');
        $taller->save();        
        
        alert()->success('Registros actualizados exitosamente', 'Taller actualizado')->autoclose(3000);

        return redirect()->to('talleres');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
