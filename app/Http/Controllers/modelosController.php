<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\newModeloRequest;
use App\Http\Requests\updateModeloRequest;
use App\Modelo;

class modelosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = Modelo::get();
        return $modelos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newModeloRequest $request)
    {
        $modelo = new Modelo([
          'nombre' => strtoupper($request->get('nombre')),
          'marca' => $request->get('marca'),
          //'tipo' => $request->get('tipo'),
          'tipo' => 1,
          //'habilitado' => $request->get('habilitado')
          'habilitado' => 'SI'
          
        ]);
        $modelo->save();
        
        alert()->success('Registros ingresado exitosamente', 'Modelo Creado')->autoclose(3000);

        return redirect()->to('modelos'); 
        
    }

    public function crearModeloDinamico(Request $request)
    {
        $modelo = new Modelo([
          'nombre' => strtoupper($request->get('nombre')),
          'marca' => $request->get('marca'),
          'tipo' => 1,
          //'tipo' => $request->get('tipo'),
          //'habilitado' => $request->get('habilitado')
          'habilitado' => 'SI'
          
        ]);
        $modelo->save();

        return $modelo->id; 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Modelo::find($id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateModeloRequest $request, $id)
    {
        $modelo = Modelo::find($id);
        

        $modelo->nombre = $request->get('nombre');
        $modelo->marca = $request->get('marca');
        $modelo->tipo = 1;
        //$modelo->tipo = $request->get('tipo');
        
        $modelo->save();        
        
        alert()->success('Registros actualizados exitosamente', 'Modelo actualizado')->autoclose(3000);

        return redirect()->to('modelos');
    }

    public function showFullData($id)
    {
        $modelo = DB::table('modelos')
            ->join('tipos', 'modelos.tipo', '=', 'tipos.id')
            ->join('marcas', 'modelos.marca', '=', 'marcas.id')
            ->select('modelos.*', 'tipos.nombre as tipodesc', 'marcas.nombre as marcadesc')
            ->where('modelos.id', '=', $id)
            ->get();
         //return User::find($id);
         return $modelo;
    }

    public function showFullDataAll()
    {
        $modelo = DB::table('modelos')
            ->join('tipos', 'modelos.tipo', '=', 'tipos.id')
            ->join('marcas', 'modelos.marca', '=', 'marcas.id')
            ->select('modelos.*', 'tipos.nombre as tipodesc', 'marcas.nombre as marcadesc')
            ->get();
         //return User::find($id);
         return $modelo;
    }

    public function showFullDataByMarca($idmarca)
    {
        $modelo = DB::table('modelos')
            ->join('tipos', 'modelos.tipo', '=', 'tipos.id')
            ->join('marcas', 'modelos.marca', '=', 'marcas.id')
            ->select('modelos.*', 'tipos.nombre as tipodesc', 'marcas.nombre as marcadesc')
            ->where('modelos.marca', '=', $idmarca)
            ->get();
         //return User::find($id);
         return $modelo;
    }
    public function getTipos()
    {
        $tipos = DB::table('tipos')
            ->select('tipos.*')
            ->get();
         //return User::find($id);
         return $tipos;
    }
    public function buscarModeloPorNombre($nombremodelo)
    {
        $modelo = DB::table('modelos')
            ->select('modelos.*')
            ->where('modelos.nombre', '=', $nombremodelo)
            ->get();
         //return User::find($id);
         return $modelo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
