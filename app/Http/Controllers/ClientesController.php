<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;


use App\User;
use App\Cliente;
//use \App\Http\Requests\updateUserRequest;
use \App\Http\Requests\newClienteRequest;
use \App\Http\Requests\updateClienteRequest;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::get();
        return $clientes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newClienteRequest $request)
    {
        $cliente = new Cliente([
          'nombre' => $request->get('nombre'),
          'apellido' => $request->get('apellidos'),
          'rut' => $request->get('rut') ?? '',
          'email' => $request->get('email'),
          'telefono' => $request->get('telefono')
          
        ]);

        
        
        $valida = count(Cliente::where('rut', $request->get('rut'))->get());

        if ($valida > 0) {
            alert()->error('Registros sin ingresar', 'RUT EXISTENTE')->autoclose(3000);
            return redirect()->to('clientes');
                
        }
        else
        {
            alert()->success('Registro ingresado exitosamente', 'Cliente Ingresado')->autoclose(3000);
            return redirect()->to('clientes');
        }

        /*
        alert()->success('Registros ingresado exitosamente', 'Cliente Creado')->autoclose(3000);

        return redirect()->to('clientes');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cliente::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rut=str_replace('.', '', $request->get('rut'));
        $cliente = Cliente::find($id);
        $cliente->nombre = $request->get('nombre');
        $cliente->apellido = $request->get('apellidos');
        $cliente->email = $request->get('email');
        $cliente->rut = $rut;
        $cliente->telefono = $request->get('telefono');

        $valida = Cliente::where('rut', $request->get('rut'))->first();
        
        if ($valida->id  == $id) {
            $cliente->save();
            alert()->success('Registros actualizados exitosamente', 'Cliente actualizado')->autoclose(3000);
            return redirect()->to('clientes');
                
        }
        else
        {
            alert()->error('Registros sin actualizar', 'RUT EXISTENTE')->autoclose(3000);
            return redirect()->to('clientes');
        }
        //$contacto->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
