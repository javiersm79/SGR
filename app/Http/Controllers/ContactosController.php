<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;


use App\User;
use App\Contacto;
//use \App\Http\Requests\updateUserRequest;
use \App\Http\Requests\newContactoRequest;
use \App\Http\Requests\updateContactoRequest;
class ContactosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contacto::get();
        //return [$usuarios, $companias];
        return $contactos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(newContactoRequest $request)
    {
        $contacto = new Contacto([
          'nombre' => $request->get('nombre'),
          'apellido' => $request->get('apellidos'),
          'rut' => $request->get('rut') ?? '',
          'email' => $request->get('email'),
          'emailalt' => $request->get('emailalt'),
          'fechanac' => $request->get('fechanac'),
          'sexo' => $request->get('sexo'),
          'fono1' => $request->get('fono1'),
          'fono2' => $request->get('fono2'),
          'compania' => $request->get('compania'),
          'userid' => '0'
          
        ]);
        $contacto->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Contacto Creado')->autoclose(3000);

        return redirect()->to('contactos');
    }

    public function storeCliente(Request $request)
    {
        $rut=str_replace('.', '', $request->get('rut'));

        $cliente = new Contacto([
          'nombre' => $request->get('nombre'),
          'apellido' => $request->get('apellidos'),
          'rut' => str_replace(".","",$rut),
          'email' => $request->get('email'),
          'emailalt' => '',
          'fechanac' => '',
          'sexo' => 'MASCULINO',
          'fono1' => $request->get('telefono') ?? "N/R",
          'fono2' => '',
          'compania' => 0,
          'userid' => '0'
          
        ]);

        $valida = Contacto::where('rut', $rut)->first();
        
        if (count($valida) > 0) {
            alert()->error('Registros sin actualizar', 'RUT EXISTENTE')->autoclose(3000);
            return redirect()->to('clientes');
                
        }
        else
        {
            
            $cliente->save();
            alert()->success('Registros ingresado exitosamente', 'Cliente Creado')->autoclose(3000);
            return redirect()->to('clientes');
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Contacto::find($id);
    }

    public function verCliente($id)
    {
        return Contacto::find($id);
    }

    public function buscarDatosCliente($rut)
    {
        $cliente = Contacto::where('rut', $rut)->first();

        if (count($cliente) > 0)
        {
            return $cliente;
        }
        else
        {
            return 0;
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listaclientes()
    {
        return Contacto::where('compania', 0)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateContactoRequest $request, $id)
    {
        $contacto = Contacto::find($id);

        $contacto->nombre = $request->get('nombre');
        $contacto->apellido = $request->get('apellidos');
        $contacto->email = $request->get('email');
        $contacto->emailalt = $request->get('emailalt');
        $contacto->fechanac = $request->get('fechanac');
        $contacto->sexo = $request->get('sexo');
        $contacto->fono1 = $request->get('fono1');
        $contacto->fono2 = $request->get('fono2');
        $contacto->compania = $request->get('compania');
        $contacto->save();

        if ($contacto->userid > 0 ) {
            $usuario = User::where('id', '=' ,$contacto->userid)->firstOrFail();
            $usuario->name = $request->get('nombre')." ".$request->get('apellidos');
            $usuario->email = $request->get('email');
            $usuario->save();

            
        }

        
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros actualizados exitosamente', 'Contacto actualizado')->autoclose(3000);

        return redirect()->to('contactos');
    }

    //Actualiza los datos del cliente
    public function updateCliente(Request $request, $id)
    {
        $rut=str_replace('.', '', $request->get('rut'));
        $contacto = Contacto::find($id);
        $contacto->nombre = $request->get('nombre');
        $contacto->apellido = $request->get('apellidos');
        $contacto->email = $request->get('email');
        $contacto->rut = $rut;
        $contacto->fono1 = $request->get('telefono');

        $valida = Contacto::where('rut', $request->get('rut'))->first();
        
        if ($valida->id  == $id) {
            $contacto->save();
            alert()->success('Registros actualizados exitosamente', 'Cliente actualizado')->autoclose(3000);
            return redirect()->to('clientes');
                
        }
        else
        {
            alert()->error('Registros sin actualizar', 'RUT EXISTENTE')->autoclose(3000);
            return redirect()->to('clientes');
        }
        //$contacto->save();
        
    }
    //Obtiene los datos con las tablas relacionadas y asi mostrar sus descripcion y no su id
    public function showFullDataAll()
    {
        $contactos = DB::table('contactos')
            ->join('companias', 'contactos.compania', '=', 'companias.id')
            ->select('contactos.*', 'companias.nombre as companiadesc')
            ->get();
         //return User::find($id);
         return $contactos;
    }

    //Obtiene los datos con las tablas relacionadas y asi mostrar sus descripcion y no su id
    public function buscarContactoEmail($email)
    {
        $contacto = DB::table('contactos')
            ->select('contactos.*')
            ->where('contactos.email', '=', $email)
            ->get();
            return $contacto;
    }

    //Obtiene los datos con las tablas relacionadas y asi mostrar sus descripcion y no su id
    public function usuariosPorCompania($compania)
    {
        $contacto = DB::table('contactos')
            ->select('contactos.*')
            //->where('contactos.userid', '>', 0)
            ->where('contactos.compania', '=', $compania)
            ->get();
            return $contacto;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
