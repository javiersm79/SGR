<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bodega;
use App\Http\Requests\newBodegaRequest;
use App\Http\Requests\updateBodegaRequest;
class bodegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bodegas = Bodega::get();
        return $bodegas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newBodegaRequest $request)
    {
       $bodega = new Bodega([
          'nombre' => $request->get('nombre'),
          'habilitado' => $request->get('habilitado')
          
        ]);
        $bodega->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Bodega Creada')->autoclose(3000);
        //dd($taller);

        return redirect()->to('bodegas'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Bodega::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateBodegaRequest $request, $id)
    {
        //dd($request);
        $bodega = Bodega::find($id);
        //dd($taller);

        $bodega->nombre = $request->get('nombre');
        $bodega->habilitado = $request->get('habilitado');
        
        $bodega->save();        
        
        alert()->success('Registros actualizados exitosamente', 'Bodega actualizado')->autoclose(3000);

        return redirect()->to('bodegas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
