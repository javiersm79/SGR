<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Mail\NotificarTransportista;
//use App\Ordenretiro;
use App\Existencia;
use App\Vehiculo;
use App\Tasacion;
use App\Taller;
use App\Transportista;
use App\Archivoexistencia;
use App\Observexistencia;
use App\Historicoexistencia;
use App\Compania;
use App\Liquidador;
use Auth;

class existenciasController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        //$matches = preg_replace('/[^0-9]/', '', $str);
        //dd($matches);
  //      dd($request->get('color') ?? 'Sin color');
        
        $dt = date('Y-m-d H:i:s');
        $anio = date('Y');

        //Valida si el vehiculo existe en la base de datos (Tabla Vehiculos)
        $buscarPatente = $this->buscarPatenteVehiculo($request->get('patente'));
        
        //Obtiene los datos de la compàñia y asi obtener el correlativo para dicha empresa (Tabla compania)
        $compania = Compania::find($request->get('compania'));

        if ($buscarPatente)
        {
            $vehiculo = array(
            'marcaid' => $request->get('marca'),
            'modeloid' => $request->get('modelo'),
            'tipoid' => $request->get('tipo'),
            'anio' => $request->get('anio'),
            'combustible' => $request->get('combustible'),
            'nummotor' => $request->get('nmotor') ?? '',
            'numchasis' => $request->get('nchasis') ?? '',
            'serial' => $request->get('nserie') ?? '',
            'VIM' => $request->get('vim') ?? '',
            'color' => $request->get('color') ?? '',
            'rutpropietario' => $request->get('rutprop') ?? '',
            'nombrepropietario' => $request->get('nombreprop') ?? '',
            'observacion' => 'N/A',
            'actualizadopor' => \Auth::user()->id,          
            );
            Vehiculo::where(['patente' => $request->get('patente')])->update($vehiculo);
            //$accion = "Vehiculo actualizado";
            $idvehiculo = $buscarPatente->id;


        }
        else
        {

            $vehiculo = new Vehiculo([
            'patente' => $request->get('patente'),
            'marcaid' => $request->get('marca'),
            'modeloid' => $request->get('modelo'),
            'tipoid' => $request->get('tipo'),
            'anio' => $request->get('anio'),
            'combustible' => $request->get('combustible'),
            'nummotor' => $request->get('nmotor') ?? '',
            'numchasis' => $request->get('nchasis') ?? '',
            'serial' => $request->get('nserie') ?? '',
            'VIM' => $request->get('vim') ?? '',
            'color' => $request->get('color') ?? '',
            'rutpropietario' => $request->get('rutprop') ?? '',
            'nombrepropietario' => $request->get('nombreprop') ?? '',
            'observacion' => 'N/A',
            'actualizadopor' => \Auth::user()->id,

            ]);
            $vehiculo->save();
            $idvehiculo = $vehiculo->id;
            //$accion = "Vehiculo creado";

        }

        $ingreso = new Existencia([
          //'nproceso' => time(),
          'nproceso' => $request->get('compania').$anio.$compania->correlativobodega,
          'estado' => $request->get('estado'),
          'acta' => 0,
          'fecha' => $dt,
          'compania' => $request->get('compania'),
          //'vehiculo' => $request->get('idvehiculo'),
          'vehiculo' => $idvehiculo,
          'nsiniestro' => $request->get('nsiniestro')  ?? "S/N",
          'liquidador' => $request->get('liquidador') ?? "Sin liquidador",
          'tasacion' => $request->get('tasacion')  ?? "0",
          'contacto' => $request->get('emailcontac'),
          'vminremate' => $request->get('vminremate')  ?? "0",
          'vtraslado' => $request->get('vtraslado')  ?? "0",
          'nfacturaproveedor' => $request->get('nfacturaproveedor')  ?? "S/N",
          'vfacturacia' => $request->get('vfacturacia')  ?? "0",
          'nfactura' => $request->get('nfactura')  ?? "S/N",
          'vindem' => $request->get('vindem')  ?? "0",
          'transportista' => 0,
          'taller' => $request->get('idtaller'),
          'creadopor' => \Auth::user()->id,
          'actualizadopor' => \Auth::user()->id,
          
        ]);
        $ingreso->save();
        $LastInsert = $ingreso->nproceso;

        $historico = new Historicoexistencia;
        $historico->nproceso = $ingreso->nproceso;
        $historico->operacion = "Vehiculo ingresado";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();
        
        
        if ($buscarPatente)
        {
            alert()->warning('Registros ingresado', '**Vehículo ingresado**  Patente ingresada anteriormente')->persistent('Close');
        }
        else
        {
            alert()->success('Registros ingresado exitosamente', 'Vehículo ingresado')->autoclose(3000);
            
        }

        $compania->correlativobodega = $compania->correlativobodega + 1;
        $compania->save();


        return redirect()->to('existencias')->with('status', $LastInsert);
    }

    /**
     * Verfica la existencia de una patente en la tabla de tasaciones.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscarPatenteTasacion($patente)
    {
        //dd($patente);
        //Busca el ultimo registro segun el ID de la patente en la tabla tasaciones
       return Tasacion::where('patente', '=' ,$patente)->latest('id')->first();
    }
    /**
     * Verfica la existencia de una patente en la tabla de Vehiculos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscarPatenteVehiculo($patente)
    {
        //dd($patente);
        //Busca el ultimo registro segun el ID de la patente en la tabla tasaciones
       return Vehiculo::where('patente', '=' ,$patente)->latest('id')->first();
    }

    public function buscarPatenteVehiculoSiniestro(Request $request)
        {
            //$vehiculo_siniestro = 0;

            $vehiculo = Vehiculo::where('patente', '=' ,$request->patente)->latest('id')->first();

            if (!is_null($vehiculo))
            {
                $vehiculo_siniestro = DB::table('existencias')->where([
                    ['vehiculo', '=', $vehiculo->id],
                    ['nsiniestro', '=',$request->nsiniestro],
                ])->get();
            }
            else {
                return 0;
            }

            
            if (count($vehiculo_siniestro) > 0)
            {
                return 1;
            }
            
            return 0;
            
            
        }
    
    /**
     * Actualiza el traslado de una OR
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Valida si el vehiculo existe en la base de datos (Tabla Vehiculos)
        $buscarPatente = $this->buscarPatenteVehiculo($request->get('patente'));

        //Actualiza los datos del vehiculo
        $vehiculo = array(
            'marcaid' => $request->get('marca'),
            'modeloid' => $request->get('modelo'),
            'tipoid' => $request->get('tipo'),
            'anio' => $request->get('anio'),
            'combustible' => $request->get('combustible'),
            'nummotor' => $request->get('nmotor') ?? '',
            'numchasis' => $request->get('nchasis') ?? '',
            'serial' => $request->get('nserie') ?? '',
            'VIM' => $request->get('vim') ?? '',
            'color' => $request->get('color') ?? '',
            'rutpropietario' => $request->get('rutprop') ?? '',
            'nombrepropietario' => $request->get('nombreprop') ?? '',
            'observacion' => 'N/A',
            'actualizadopor' => \Auth::user()->id,          
            );
            Vehiculo::where(['patente' => $request->get('patente')])->update($vehiculo);
            //$accion = "Vehiculo actualizado";
            $idvehiculo = $buscarPatente->id;

            //Actualiza los datos de la orden de retiro
            if (($request->get('estado') != 'Remate-Vehiculo') and ($request->get('estado') != 'Remate-Desarme'))
            {
              $ingreso = array(
                //'nproceso' => time(),
                //'nproceso' => $request->get('compania').$anio.$compania->correlativoor,
                'estado' => $request->get('estado'),
                'acta' => 0,
                'compania' => $request->get('compania'),
                //'vehiculo' => $request->get('idvehiculo'),
                'vehiculo' => $idvehiculo,
                'nsiniestro' => $request->get('nsiniestro')  ?? "S/N",
                'liquidador' => $request->get('liquidador') ?? "Sin liquidador",
                'tasacion' => $request->get('tasacion')  ?? "0",
                'contacto' => $request->get('emailcontac'),
                'vminremate' => $request->get('vminremate')  ?? "0",
                'vtraslado' => $request->get('vtraslado')  ?? "0",
                'nfacturaproveedor' => $request->get('nfacturaproveedor')  ?? "S/N",
                'vfacturacia' => $request->get('vfacturacia')  ?? "0",
                'nfactura' => $request->get('nfactura')  ?? "S/N",
                'vindem' => $request->get('vindem')  ?? "0",
                //'transportista' => 0,
                'taller' => $request->get('idtaller'),
                'creadopor' => \Auth::user()->id,
                'actualizadopor' => \Auth::user()->id,
                
              );
              DB::table('actas_existencias')->where('idexistencia', $id)->delete();


            }
            else
            {
              $ingreso = array(
                //'nproceso' => time(),
                //'nproceso' => $request->get('compania').$anio.$compania->correlativoor,
                'estado' => $request->get('estado'),
                'compania' => $request->get('compania'),
                //'vehiculo' => $request->get('idvehiculo'),
                'vehiculo' => $idvehiculo,
                'nsiniestro' => $request->get('nsiniestro')  ?? "S/N",
                'liquidador' => $request->get('liquidador') ?? "Sin liquidador",
                'tasacion' => $request->get('tasacion')  ?? "0",
                'contacto' => $request->get('emailcontac'),
                'vminremate' => $request->get('vminremate')  ?? "0",
                'vtraslado' => $request->get('vtraslado')  ?? "0",
                'nfacturaproveedor' => $request->get('nfacturaproveedor')  ?? "S/N",
                'vfacturacia' => $request->get('vfacturacia')  ?? "0",
                'nfactura' => $request->get('nfactura')  ?? "S/N",
                'vindem' => $request->get('vindem')  ?? "0",
                //'transportista' => 0,
                'taller' => $request->get('idtaller'),
                'creadopor' => \Auth::user()->id,
                'actualizadopor' => \Auth::user()->id,
                
              );

            }

        
        
        Existencia::where(['id' => $id])->update($ingreso);

        $ingresoupdate = new Existencia;
        $ingresoupdate = $ingresoupdate->findorfail($id);
        //dd($ingresoupdate->nproceso);
        $historico = new Historicoexistencia;
        $historico->nproceso = $ingresoupdate->nproceso;
        $historico->operacion = "Ingreso actualizado";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();

            alert()->success('Registros actualizados exitosamente - '.$ingresoupdate->nproceso, 'Ingreso Actualizado')->autoclose(3000);



        return redirect()->to('existencias');

        
    }
/**
     * Scan folder for image car
     *
     * @return \Illuminate\Http\Response
     */
    public function scanfolderGaleriaVehiculo($patente)
    {

        $html = '';
        $images = '';
        $img_dir = "/storage/app/public/images/vehiculo/".$patente."/";

        if(\File::exists($img_dir)) {
            $images = scandir($img_dir);
            foreach($images as $img)    { 
                if($img === '.' || $img === '..') {continue;}       

                    if (  (preg_match('/.JPG/',$img)  || (preg_match('/.jpg/',$img))  ||  (preg_match('/.JEPG/',$img)) || (preg_match('/.jepg/',$img)) || (preg_match('/.gif/',$img)) || (preg_match('/.tiff/',$img)) || (preg_match('/.png/',$img)) ) || (preg_match('/.PNG/',$img)) ) {

                    $html .= '<div class="item"><a class="thumbnail" href="'.$img_dir.$img.'" data-fancybox="images" data-caption="<button style=\'color:black\' onclick='."eliminarFoto('$patente','".urlencode($img)."')".'>ELIMINAR</button>"><img alt="" src="'.$img_dir.$img.'"></a></div>';               

                    
                    /*$html .='<li> 
                            <img src="'.$img_dir.$img.'" ></li>' ; */
                    } else { continue; }    
            }
        }


        return $html ;
       
    }

    public function scanfolderArchivosIngreso($nproceso)
    {
 
         $archivo_dir = "storage/files/existencias/".$nproceso."/";
            $html = '';
            $archivosOR = DB::table('archivosexistencia')
            ->join('contactos', 'contactos.userid', '=', 'archivosexistencia.userid')
            ->join('companias', 'companias.id', '=', 'archivosexistencia.companiaid')
            ->select('archivosexistencia.*', 'contactos.nombre as nombrecontacto', 'contactos.apellido as apellidocontacto','companias.nombre as nombrecompania' )
            ->where('archivosexistencia.nproceso', '=', $nproceso)
            ->get();

            foreach($archivosOR as $archivo)    { 
                if($archivo === '.' || $archivo === '..') {continue;}       

                    $extencion=preg_split('[\.]', $archivo->nombre, null, PREG_SPLIT_OFFSET_CAPTURE);

                   $html .= '<tr id="doc'.$archivo_dir.'/'.$archivo->nombre.'"><td><a href="/storage/files/existencias/'.$nproceso.'/'.$archivo->nombre.'" target="blank">'.$archivo->nombre.'</a></td><td>'.$archivo->nombrecompania.'</td><td>'.$archivo->fecha.'</td><td>'.$archivo->nombrecontacto.' '.$archivo->apellidocontacto.'</td><td>'.$archivo->observacion.'</td><td align="center"><img src="storage/images/filetype/'.$extencion[1][0].'.png"></td><td align="center"><button class="btn btn-danger btn-sm btnDelete" onclick="eliminarArchivo(\''.$nproceso.'\',\''.$archivo->nombre.'\',\''.$archivo->id.'\')" >Eliminar</button></td></tr>'; 
    
            }

        return  $html;
       
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $idcan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * SUBE LOS ARCHIVOS DE IMAGENES DEL VEHICULO
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subirfotos(Request $request)
    {
        //dd($request);

         $rutaimg = "storage/images/vehiculo/".$request->patente."/";

        if(\File::exists($rutaimg)) {
            $images = scandir($rutaimg);

            $i=count($images) - 2;
            //dd($i);
            //$path = $request->file('fotosvehiculo')->store('uploads');
            

        }
        else
        {
            $i=0;

        }


        if (count($request->fotosvehiculo) <= 2 )
          {
          $extension = $request->fotosvehiculo[0]->getClientOriginalExtension();
          $path = $request->fotosvehiculo[0]->storeAs(
                          'public/images/vehiculo/'.$request->patente, $request->patente." ".$i.".".$extension
                      );
          }
          else
          {
                  foreach ($request->fotosvehiculo as $photo) {
                      $extension = $photo->getClientOriginalExtension();
                      echo $extension;
                      //$filename = $photo->store('public/images/vehiculo');
                      $path = $photo->storeAs(
                          'public/images/vehiculo/'.$request->patente, $request->patente." ".$i.".".$extension
                      );
                      $i++;

                  }
          }
        
        
        $historico = new Historicoexistencia;
        $historico->nproceso = $request->nproceso;
        $historico->operacion = "Imagenes de Vehiculos cargadas";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();

        return;
    }

    /**
     * SUBE LOS ARCHIVOS DE DOCUMENTOS DE LA OR
     *
     */
    public function subirarchivoExist(Request $request)
    {
    
        

        //$narchivos = scandir('storage/files/or/'.$request->idordenretiro."/", $nombrearchivo);
        //cuenta el numero de archivos
        //$i=count($narchivos);

        //crea la carpeta si no existe
        if(!\File::exists('storage/files/existencias/'.$request->nproceso)) {
            \File::makeDirectory('storage/files/existencias/'.$request->nproceso);
        }
        
        
        //obtiene la extencion del archivo a subir
        $extension = $request->archivo->getClientOriginalExtension();
        //obtiene la extencion del archivo a subir
        $nombrearchivo = $request->archivo->getClientOriginalName();
        $nombrearchivo = str_replace(" ","_",$nombrearchivo);

        //sube el archivo al servidor
        $path = $request->archivo->storeAs(
                //'public/files/or/'.$request->idordenretiro."/", "OR-".$i.".".$extension
                'public/files/existencias/'.$request->nproceso."/", $nombrearchivo //.".".$extension
            );


        //Datos del archivo generado para retornar al la vista/front
        $datafile = array();
        $datafile[0] = $nombrearchivo; //.".".$extension;
        //$datafile[0] = "OR-".$i.".".$extension;
        $datafile[1] = $extension;
        $contacto = \App\Contacto::where('userid', '=' ,\Auth::user()->id)->firstOrFail();

        $nuevoarchivo = new Archivoexistencia([
          'nproceso' => $request->nproceso,
          'nombre' => $nombrearchivo,
          'fecha' => date('Y-m-d H:i:s'),
          //'companiaid' => $companiaid,
          'companiaid' => $contacto->compania,
          'userid' => \Auth::user()->id,
          'observacion' => $request->observacion,
          
        ]);
        $nuevoarchivo->save();

        $historico = new Historicoexistencia;

        $historico->nproceso = $request->nproceso;
        $historico->operacion = "Archivo:  $nombrearchivo cargado";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();


        return $datafile;
    }

    /**
     * ELIMINA UNA IMAGEN DEL VEHICULO
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminarfoto($patente, $idfoto)
    {

        /*$images = scandir('storage/images/vehiculo/DB-1515/');*/
        \File::delete('storage/images/vehiculo/'.$patente.'/'.urldecode($idfoto));
        

        return;
        //return "Imagen eliminada: ".$idfoto;
        //return "Patente: ".$patente." Foto: ".$idfoto;
    }

    public function showFullDataAll()
    {
        $ordenesretiro = DB::table('existencias')
            ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
            ->join('companias', 'existencias.compania', '=', 'companias.id')
            ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
            ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
            ->select('existencias.*', 'vehiculos.marcaid as modelo', 'vehiculos.modeloid as modelo', 'vehiculos.patente as patente', 'companias.nombre as nombrecompania', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio as anio')
            ->get();
         //return User::find($id);
            foreach($ordenesretiro as $ordenretiro => $value) {
                //echo $value->name . ", " . $value->email . "<br>";
                $fecha1 = new \DateTime($value->fecha);
                $fecha2 = new \DateTime();
                $fecha = $fecha1->diff($fecha2);
                $value->diasenretiro = $fecha->format('%a días');;
                //$usuarios->setAttribute('test', 'blablabla');;
              }


         return $ordenesretiro;
    }

    /*public function showFullDataAllActa()
    {
        $ordenesretiro = DB::table('existencias')
            ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
            ->join('companias', 'existencias.compania', '=', 'companias.id')
            ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
            ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
            ->select('existencias.*', 'vehiculos.marcaid as modelo', 'vehiculos.modeloid as modelo', 'vehiculos.patente as patente', 'companias.nombre as nombrecompania', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio as anio')
            ->where('estado', '=', 'Remate-Vehiculo')
            ->orWhere('estado', '=', 'Remate-Desarme')
            ->get();
         //return User::find($id);
            foreach($ordenesretiro as $ordenretiro => $value) {
                //echo $value->name . ", " . $value->email . "<br>";
                $fecha1 = new \DateTime($value->fecha);
                $fecha2 = new \DateTime();
                $fecha = $fecha1->diff($fecha2);
                $value->diasenretiro = $fecha->format('%a días');;
                //$usuarios->setAttribute('test', 'blablabla');;
              }


         return $ordenesretiro;
    }*/

    public function showFullDataIngreso($idingreso)
    {
        $ingreso = Existencia::find($idingreso);
        $vehiculo = Vehiculo::find($ingreso->vehiculo);
        $compania = Compania::find($ingreso->compania);
        $taller = Taller::find($ingreso->taller);
        $transportista = Transportista::find($ingreso->transportista);
        $liquidador = Liquidador::find($ingreso->liquidador);
        $tipovehiculo = DB::table('tipos')
            ->where('id', '=', $vehiculo->tipoid)
            ->first();
        $marcavehiculo = DB::table('marcas')
            ->where('id', '=', $vehiculo->marcaid)
            ->first();
        $modelovehiculo = DB::table('modelos')
            ->where('id', '=', $vehiculo->modeloid)
            ->first();
           
        $fulldata = array();
        $fulldata["ingreso"]=$ingreso;
        $fulldata["compania"]=$compania;
        $fulldata["liquidador"]=$liquidador;
        $fulldata["vehiculo"]=$vehiculo;
        $fulldata["taller"]=$taller;
        $fulldata["transportista"]=$transportista;
        $fulldata["tipovehiculo"]=$tipovehiculo;
        $fulldata["marcavehiculo"]=$marcavehiculo;
        $fulldata["modelovehiculo"]=$modelovehiculo;
        
        return $fulldata;
    }


    public function showFullDataIngresoNproceso($nproceso)
    {
        $ingreso = Existencia::where('nproceso',$nproceso)->first();
        if ($ingreso) {
        $vehiculo = Vehiculo::find($ingreso->vehiculo);
        $compania = Compania::find($ingreso->compania);
        $taller = Taller::find($ingreso->taller);
        $transportista = Transportista::find($ingreso->transportista);
        $liquidador = Liquidador::find($ingreso->liquidador);
        $tipovehiculo = DB::table('tipos')
            ->where('id', '=', $vehiculo->tipoid)
            ->first();
        $marcavehiculo = DB::table('marcas')
            ->where('id', '=', $vehiculo->marcaid)
            ->first();
        $modelovehiculo = DB::table('modelos')
            ->where('id', '=', $vehiculo->modeloid)
            ->first();
           
        $fulldata = array();
        $fulldata["ingreso"]=$ingreso;
        $fulldata["compania"]=$compania;
        $fulldata["liquidador"]=$liquidador;
        $fulldata["vehiculo"]=$vehiculo;
        $fulldata["taller"]=$taller;
        $fulldata["transportista"]=$transportista;
        $fulldata["tipovehiculo"]=$tipovehiculo;
        $fulldata["marcavehiculo"]=$marcavehiculo;
        $fulldata["modelovehiculo"]=$modelovehiculo;
        
        
        return $fulldata;  
        }
        else {
        return 0;  
        }
        
    }

    /**
     * ELIMINA UN ARCHIVO DE LA OR
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function eliminararchivoExist($nproceso, $nombrearchivo, $idarchivoexist)
    {

        \File::delete("storage/files/existencias/$nproceso/$nombrearchivo");
        
        \App\Archivoexistencia::destroy($idarchivoexist);

        $historico = new Historicoexistencia;
        $historico->nproceso = $nproceso;
        $historico->operacion = "Archivo: $nombrearchivo ELIMINADO";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();
        return ;

        return ;
    }   

     /**
     * Guarda una observación asociada a la OR
     */
     public function storeObservacionExist(Request $request, $idingreso)
    {
        $contacto = \App\Contacto::where('userid', '=' ,\Auth::user()->id)->firstOrFail();

        /*if ($request->privada) {
            $privada = 1;
        }
        else
        {
            $privada = 0;
        }*/



        $observacioningreso = new Observexistencia([
          'idingreso' => $request->idingreso,
          'fecha' => date('Y-m-d H:i:s'),
          'companiaid' => $contacto->compania,
          'userid' => \Auth::user()->id,
          'observacion' => $request->obsertxt,
          'privada' => $request->privada,
          
        ]);
        $observacioningreso->save();
        //var_export($request->all());
        return;

        
    }
    /**
     * Muestra las observaciones de una OR (No muestra las privadas si no es el usuario logeado)
     */
     public function showObservacionesExist($idingreso)
    {

        $observaciones = DB::table('observexistencia')
            ->join('contactos', 'contactos.userid', '=', 'observexistencia.userid')
            ->join('companias', 'companias.id', '=', 'observexistencia.companiaid')
            ->select('observexistencia.*', 'contactos.nombre as nombrecontacto', 'contactos.apellido as apellidocontacto','companias.nombre as nombrecompania' )
            ->where('observexistencia.idingreso', '=', $idingreso)
            ->where('observexistencia.privada', '=', 0)
            ->orWhere(function ($query) use ($idingreso) {
                $query->where('observexistencia.privada', '=', 1)
                ->where('observexistencia.userid', '=', \Auth::user()->id)
                ->where('observexistencia.idingreso', '=', $idingreso);
                })
            ->get();

        //return $observaciones;



        $html='';
        $i=0;
        foreach ($observaciones as $observacion) {
            $html .= '<tr><td>'.$observacion->nombrecompania.'</td><td>'.$observacion->fecha.'</td><td>'.$observacion->nombrecontacto.' '.$observacion->apellidocontacto.'</td><td>'.nl2br(e($observacion->observacion)).'</td></tr>';
            $i++;

        }

        return $html;
 
    }
    /**
     * Actualiza el transportista de un Ingreso/Existencia
     */
     public function updateTransportistaExist(Request $request)
    {
        //dd($request->all());
        $ordenTraslado = Existencia::find($request->idingreso);

        $ordenTraslado->transportista = $request->idtransportista;
        $ordenTraslado->save();
        $historico = new Historicoexistencia;
        $historico->nproceso = $ordenTraslado->nproceso;
        $historico->operacion = "Datos de Transportista Actualizado";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();

        return;

    }
    /**
     * Actualiza los datos de bodega de un Ingreso/Existencia
     */
     public function updateBodega(Request $request)
    {
        //var_export($request->all());
        $ingreso = Existencia::find($request->idingreso);

        $ingreso->permiso = $request->permiso ?? null;
        $ingreso->seguro = $request->seguro  ?? null;
        $ingreso->padron = $request->padron  ?? null;
        $ingreso->revtec = $request->revtec  ?? null;
        $ingreso->gases = $request->gases  ?? null;
        $ingreso->condicionado = $request->condicionado  ?? null;
        $ingreso->desarme = $request->desarme  ?? null;
        $ingreso->llaves = $request->llaves;
        $ingreso->placas = $request->placas;
        $ingreso->tag = $request->tag;
        $ingreso->computador = $request->computador;
        $ingreso->motorarranca = $request->motorarranca;
        $ingreso->kilometraje = $request->kilometraje;
        $ingreso->airbag = $request->airbag;
        $ingreso->bodegas = $request->bodegas;
        //dd($ingreso->all());
        $ingreso->save();

        $historico = new Historicoexistencia;
        $historico->nproceso = $request->nproceso;
        $historico->operacion = "Datos de Inventario Actualizado";
        $historico->usuario = Auth::user()->name." - ".Auth::user()->email;
        $historico->save();


        return;

    }
    /**
     * Notifica al transportista por email
     */
     public function notificarTransportista(Request $request)
    {
        //$ordenRetiro = Ordenretiro::find($request->idordenretiro);
        $ordenRetiro = $this->showFullDataIngreso($request->idingreso);
        $ruta=$this->pdfOR($request->idordenretiro, "email");

        //dd($ordenRetiro['transportista']);

        //Mail::to('jjlion79@localhost')->send(new NotificarTransportista($ordenRetiro));
        Mail::to($ordenRetiro['transportista']['email'])->send(new NotificarTransportista($ordenRetiro));



        return;

    }

    /**
     * Valida que la OR fue retirada
     */
    public function validarRetiroOR($nproceso)
    {
        $ordenretiro = Existencia::where('nproceso', '=' ,$nproceso)->first();

        $ordenretiro->estado = 'Retirado';

        $ordenretiro->save();

        //return json_encode($ordenRetiro);
        return view('orretirada')->with('nproceso', $nproceso);

    }

    /**
     * Cambia la OR a Estado Asignada
     */
    public function orasignada(Request $request)
    {
        $ordenRetiro = Existencia::find($request->idordenretiro);

        $ordenRetiro->estado = "Asignada";
        $ordenRetiro->save();
        return;

    }

    /**
     * Genera el PDF de la OR
     */
    public function fichaingreso($idingreso)
    {
        $ingreso = $this->showFullDataIngreso($idingreso);

         return view('pdf.ingresobodega')->with(compact('ingreso'));


    }   
    /**
     * Genera el PDF de la OR
     */
    public function pdfOR($idordenretiro, $peticion)
    {
        $ordenRetiro = $this->showFullDataIngreso($idordenretiro);

        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.ordenretiro', compact('ordenRetiro'));
        //$pdf = PDF::loadHTML("<img src='http://localhost:1606/storage/images/vedisa.png'>");

        //crea la carpeta si no existe
        if(!\File::exists('storage/files/existencias/'.$ordenRetiro['ordenretiro']['nproceso'])) {
            \File::makeDirectory('storage/files/existencias/'.$ordenRetiro['ordenretiro']['nproceso']);
        }

        $rutaPDF="storage/files/existencias/".$ordenRetiro['ordenretiro']['nproceso']."/ING".$ordenRetiro['ordenretiro']['nproceso'].".pdf";

        
        if ($peticion == "email")
        {
            $pdf->save($rutaPDF);
            return $rutaPDF;

        }
        else
        {
            return $pdf->download('invoice.pdf');
        }


    }
    /**
     * Muestra las observaciones de una OR (No muestra las privadas si no es el usuario logeado)
     */
     public function showHistoricoExist($nproceso)
    {

        $historicos = DB::table('historicoexist')
            ->select('*')
            ->where('nproceso', '=', $nproceso)
            ->get();

        $html='';
        $i=0;
        foreach ($historicos as $historico) {
            $html .= '<tr><td>'.$historico->operacion.'</td><td>'.$historico->usuario.'</td><td>'.$historico->created_at.'</td></tr>';
            $i++;

        }

        return $html;
 
    }
    
    //public function verExistencia($idingreso)
    public function verExistencia($nproceso)
    {
        $ingresoNP = $this->showFullDataIngresoNproceso($nproceso);
        $ingreso = $this->showFullDataIngreso($ingresoNP["ingreso"]->id);
        return view('administrador.existencias.resumen')->with(compact('ingreso'));


    }  
}
