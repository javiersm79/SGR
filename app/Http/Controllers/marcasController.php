<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
use App\Http\Requests\newMarcaRequest;
use App\Http\Requests\updateMarcaRequest;

class marcasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = Marca::get();
        return $marcas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newMarcaRequest $request)
    {
        $marca = new Marca([
          'nombre' => strtoupper($request->get('nombre')),
          'habilitado' => $request->get('habilitado')
          
        ]);
        $marca->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Marca Creada')->autoclose(3000);
        //dd($taller);

        return redirect()->to('marcas'); 
    }

    public function crearMarcaDinamico(Request $request)
    {
        $marca = new Marca([
          'nombre' => strtoupper($request->get('nombre')),
          'habilitado' => $request->get('habilitado')
          
        ]);
        $marca->save();

        return $marca->id; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Marca::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateMarcaRequest $request, $id)
    {
        $marca = Marca::find($id);
        //dd($taller);

        $marca->nombre = $request->get('nombre');
        $marca->habilitado = $request->get('habilitado');
        
        $marca->save();        
        
        alert()->success('Registros actualizados exitosamente', 'Marca actualizada')->autoclose(3000);

        return redirect()->to('marcas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
