<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;


use App\User;
use App\Contacto;
use \App\Http\Requests\updateUserRequest;
use \App\Http\Requests\newUserRequest;

use Menu;

class usuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @m_returnstatus(conn, identifier) \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        

        $usuarios = User::get();
        //return [$usuarios, $companias];
        return $usuarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newUserRequest $request)
    {
        $dt = date('Y-m-d H:i:s');
        $usuario = new User([
          'name' => $request->get('nombre')." ".$request->get('apellidos'),
          'email' => $request->get('email'),
          'tipo' => $request->get('perfil'),
          'estado' => $request->get('estado'),
          'password' => Hash::make($request->get('password'))
          /*'created_at' => $dt,
          'updated_at' => $dt*/
        ]);

        $usuario->save();
        $LastInsertUserId = $usuario->id;

        
       
        $contacto = new Contacto([
          'nombre' => $request->get('nombre'),
          'apellido' => $request->get('apellidos'),
          'email' => $request->get('email'),
          'emailalt' => $request->get('emailalt'),
          //'emailalt' => $request->get('emailalt'),
          'fechanac' => $request->get('fechanac'),
          'sexo' => $request->get('sexo'),
          'fono1' => $request->get('fono1'),
          'fono2' => $request->get('fono2'),
          'compania' => $request->get('compania'),
          'userid' => $LastInsertUserId
          
        ]);
        $contacto->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Usuario Creado')->autoclose(3000);

        return redirect()->to('usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return User::find($id);
    }
    
    public function showFullData($id)
    {
        $user = DB::table('users')
            ->join('contactos', 'users.email', '=', 'contactos.email')
            ->join('companias', 'contactos.compania', '=', 'companias.id')
            ->select('users.*', 'contactos.*', 'companias.nombre as companiadesc')
            ->where('users.id', '=', $id)
            ->get();
         //return User::find($id);
         return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateUserRequest $request, $id)
    {
        
        
        $usuario = User::find($id);

        $usuario->name = $request->get('nombre')." ".$request->get('apellidos');
        $usuario->email = $request->get('email');
        $usuario->tipo = $request->get('perfil');
        $usuario->estado = $request->get('estado');
        $usuario->password = Hash::make($request->get('password'));
        $usuario->save();

        $contacto = Contacto::where('userid', '=' ,$id)->firstOrFail();
        $contacto->nombre = $request->get('nombre');
        $contacto->apellido = $request->get('apellidos');
        $contacto->email = $request->get('email');
        $contacto->emailalt = $request->get('emailalt');
        $contacto->fechanac = $request->get('fechanac');
        $contacto->sexo = $request->get('sexo');
        $contacto->fono1 = $request->get('fono1');
        $contacto->fono2 = $request->get('fono2');
        $contacto->compania = $request->get('compania');
        $contacto->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros actualizado exitosamente', 'Usuario Actualizado')->autoclose(3000);

        return redirect()->to('usuarios');
    }

    //Obtiene los datos con las tablas relacionadas y asi mostrar sus descripcion y no su id
    public function showFullDataAll()
    {
        $usuarios = DB::table('users')
            ->join('contactos', 'contactos.userid', '=', 'users.id')
            ->join('companias', 'contactos.compania', '=', 'companias.id')
            ->select('users.*', 'companias.nombre as companiadesc')
            ->get();
         //return User::find($id);
            foreach($usuarios as $usuario => $value) {
                //echo $value->name . ", " . $value->email . "<br>";
                $value->diasenretiro = 5;
                //$usuarios->setAttribute('test', 'blablabla');;
              }


         return $usuarios;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $usuario = User::findOrFail($id);
        $usuario->delete();
    }
}
