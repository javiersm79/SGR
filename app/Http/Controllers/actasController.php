<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Acta;
use App\Existencia;
use App\Contacto;
use Exception;
use PDF;
use Illuminate\Database\QueryException;
class actasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actas = Acta::get();
        return $actas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    try {
        date_default_timezone_set('America/Santiago');
            $dt = date('Y-m-d H:i:s');
            $acta = new Acta([
              'numeroacta' => $request->get('nacta'),
              'fecha' => $dt,
              'estado' => 'Activa'
            ]);
            $acta->save();


        $existencia = DB::table('existencias')
            ->select('existencias.*')
            ->Where(function($query) {
                $query->where('existencias.estado', '=', 'Remate-Vehiculo')
                    ->where('existencias.acta', '=', 0);   
            })
            ->orWhere(function($query) {
                $query->where('existencias.estado', '=', 'Remate-Desarme')
                    ->where('existencias.acta', '=', 0);   
            })
            ->get();
        foreach($existencia as $ingreso) {
            Existencia::where('id', '=', $ingreso->id)->update(['acta' => $request->get('nacta')]);
            DB::table('actas_existencias')->insert(['numeroacta' => $request->get('nacta'), 'idexistencia' => $ingreso->id, 'created_at' => $dt, 'updated_at' => $dt]);
        }

         DB::table('sivebase')->where('nombre', '=', 'acta')->update(['valor' => $request->get('nacta')]);


    } catch (QueryException $e) {
        echo 'Mensaje de error: ' .$e->getMessage();
    } catch (Exception $e) {
        echo 'Mensaje de error: ' .$e->getMessage();
    }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($numeroacta)
    {
        try {
            $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
            if (count($datosacta) > 0)
            {return view('administrador.actas.acta')->with('datosacta',$datosacta);}
            else
            {
                alert()->error('', 'Numero de acta no registrado')->autoclose(5000);

                return redirect()->to('listaractas');
            }
            //return $datosacta;
            //dd($datosacta);
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }
    }


    public function procesar($numeroacta)
    {
        try {
            
            $datosacta = DB::table('actas_existencias')
                    ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                    ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                    ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                    ->join('companias', 'companias.id', '=', 'existencias.compania')
                    ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                    ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                    ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                    ->select('actas.*', 'actas_existencias.lote', 'actas_existencias.monto','actas_existencias.estado as estadoingreso', 'actas_existencias.monto','actas_existencias.cliente','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                    ->where('actas_existencias.numeroacta', '=', $numeroacta)
                    ->get();

                foreach ($datosacta as $dato) {
                    if ($dato->cliente == 0)
                    {
                        $dato->cliente = "S/D";
                    }
                    else
                    {
                        $rut=Contacto::find($dato->cliente)->rut;
                        $dato->cliente = $rut;
                    }
                }
            //dd($datosacta);
            if (count($datosacta) > 0)
            {
                return view('administrador.actas.procesar')->with('datosacta',$datosacta);
            }
            else
            {
                alert()->error('', 'Numero de acta no registrado')->autoclose(5000);

                return redirect()->to('listaractas');
            }
            //return $datosacta;
            //dd($datosacta);
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }
    }

    public function asignarlotes($numeroacta)
    {
        try {
            $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
                
            if (count($datosacta) > 0)
            {return view('administrador.actas.lotes')->with('datosacta',$datosacta);}
            else
            {
                alert()->error('', 'Numero de acta no registrado')->autoclose(5000);

                return redirect()->to('listaractas');
            }
            //return $datosacta;
            //dd($datosacta);
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }
    }


    public function guardarlotes(Request $request)
    {
        try {

            /*foreach ($request->all() as $clave => $valor) {
                // $array[3] se actualizará con cada valor de $array...
                echo "{$clave} => {$valor}<br>";
                echo "****************************************<br>";
                //print_r($request->all());
            }*/
            
            foreach ($request->all() as $actas_existencia => $lote) {
                if ($lote){
                    $ref = explode("-",$actas_existencia);
                    //print_r($ref);
                    DB::table('actas_existencias')
                    ->where('idexistencia', '=', $ref[0])
                    ->where('numeroacta', '=', $ref[1])
                    ->update(['lote' => $lote]);

                }

                


            }
            return "<h3>Datos de lote Actualizados</h3>";
            //dd($request->all());
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ultimaacta()
    {

        $ultimaActa=DB::table('sivebase')
        ->select('sivebase.*')
        ->where('nombre', '=', 'acta')
        ->get()->pluck('valor');
        return $ultimaActa[0] + 1;
        
    }


    public function showFullDataAllActa()
    {
        $existencia = DB::table('existencias')
            ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
            ->join('companias', 'existencias.compania', '=', 'companias.id')
            ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
            ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
            ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
            ->select('existencias.*', 'vehiculos.marcaid as modelo', 'vehiculos.modeloid as modelo', 'vehiculos.patente as patente', 'companias.nombre as nombrecompania', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio as anio')
            ->Where(function($query) {
                $query->where('existencias.estado', '=', 'Remate-Vehiculo')
                    ->where('existencias.acta', '=', 0);   
            })
            ->orWhere(function($query) {
                $query->where('existencias.estado', '=', 'Remate-Desarme')
                    ->where('existencias.acta', '=', 0);   
            })
            ->get();


         return $existencia;
    }


    public function pdfActa($numeroacta)
    {
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.acta', compact('datosacta'));      
        
            return $pdf->download('acta.pdf');
  

    }   

    public function imprimirlotes($numeroacta)
    {
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.cartel_lote', compact('datosacta'));      
        
        return $pdf->download("carteles_lote_".$datosacta[0]->numeroacta.".pdf");
        //    return view('pdf.cartel_lote')->with('datosacta',$datosacta);
  

    }   

    public function actamartillero($numeroacta)
    {
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
                //dd($datosacta);
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.acta_martillero', compact('datosacta'));      
        
        return $pdf->download("acta_martilero_".$datosacta[0]->numeroacta.".pdf");
        //    return view('pdf.cartel_lote')->with('datosacta',$datosacta);
  

    }

    public function revisionacta($numeroacta)
    {
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
                //dd($datosacta);
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.revisionacta', compact('datosacta'))->setPaper('letter', 'landscape');      
        
        return $pdf->download("revisionacta".$datosacta[0]->numeroacta.".pdf");
        //    return view('pdf.cartel_lote')->with('datosacta',$datosacta);
  

    }  

    public function boletinremate($numeroacta)
    {
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.lote','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
                //dd($datosacta);
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.boletinremate', compact('datosacta'));      
        
        return $pdf->download("boletinremate".$datosacta[0]->numeroacta.".pdf");
        //    return view('pdf.boletinremate')->with('datosacta',$datosacta);
  

    } 

    public function planillacobranza($numeroacta)
    {
        
        $datosacta = DB::table('actas_existencias')
                ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                ->join('companias', 'companias.id', '=', 'existencias.compania')
                ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                ->select('actas.*', 'actas_existencias.*','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                ->where('actas_existencias.numeroacta', '=', $numeroacta)
                ->get();
                //dd($datosacta);
        //$pdf = PDF::loadView('pdf.ordenretiro');
        $pdf = PDF::loadView('pdf.planillacobranza', compact('datosacta'))->setPaper('letter', 'landscape');      
        
        return $pdf->download("planillacobranza".$datosacta[0]->numeroacta.".pdf");
        //return view('pdf.planillacobranza')->with('datosacta',$datosacta);
  
  

    } 

    public function buscarDatosLote($numeroacta, $lote)
    {
        
     
                    $datosacta = DB::table('actas_existencias')
                    ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                    ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                    ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                    ->join('companias', 'companias.id', '=', 'existencias.compania')
                    ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                    ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                    ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                    ->select('actas.*', 'actas_existencias.lote', 'actas_existencias.monto','actas_existencias.garantia','actas_existencias.tasacionfiscal','actas_existencias.estado as estadoingreso','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"), 'actas_existencias.cliente as idcliente')
                    ->where('actas_existencias.numeroacta', '=', $numeroacta)
                    ->where('actas_existencias.lote', '=', $lote)
                    ->get();
                    $datosacta[0]->cliente=Contacto::find($datosacta[0]->idcliente);

                
        return $datosacta;

    }  

    public function ingresarDatosLote(Request $request)
    {
        $gastosoperacionales=DB::table('sivebase')
        ->select('sivebase.*')
        ->where('nombre', '=', 'gastosoperacionales')
        ->get()->pluck('valor');
  
        $rut=str_replace('.', '', $request->get('rut'));
        $cliente = Contacto::find($request->get('idcliente'));
        $cliente->nombre = $request->get('nombre');
        $cliente->apellido = $request->get('apellido');
        $cliente->email = $request->get('email');
        $cliente->rut = $rut;
        $cliente->fono1 = $request->get('telefono');

        $valida = Contacto::where('rut', $rut)->first();
        
        if ($valida->id  == $cliente->id) {
            $cliente->update();
        }

        $lote = DB::table('actas_existencias')
                    ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                    ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                    ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                    ->join('companias', 'companias.id', '=', 'existencias.compania')
                    ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                    ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                    ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                    ->select('actas.*', 'actas_existencias.lote', 'actas_existencias.monto','actas_existencias.estado as estadoingreso','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"), 'actas_existencias.cliente as idcliente')
                    ->where('actas_existencias.numeroacta', '=', $request->acta)
                    ->where('actas_existencias.lote', '=', $request->lote)
                    ->first();
                    $comision = $request->monto * (0.12);
            if ($lote->desarme)
            {
                $iva = ($request->monto + $comision) * (0.19);
            }
            else
            {
                $iva = $comision * 0.19;   
            }

            
            //->update(['estado' => 'Ingresado','cliente' => $request->idcliente,'monto' => $request->monto,'tasacionfiscal' => $request->tasacionfiscal,'garantia' => $request->garantia,'gastosoperacionales' => $gastosoperacionales[0], 'comision' => ()]);
           //return json_encode($lote);
           DB::table('actas_existencias')
            ->select('actas_existencias.*')
            ->where('lote', '=', $request->lote)
            ->where('numeroacta', '=', $request->acta)
            ->update(['estado' => 'Ingresado','cliente' => $request->idcliente,'monto' => $request->monto,'tasacionfiscal' => $request->tasacionfiscal,'garantia' => $request->garantia,'gastosoperacionales' => $gastosoperacionales[0], 'comision' => $comision, 'iva' => $iva]);

        return;
  

    }

    public function cerrarActa(Request $request)
    {
        Acta::where("numeroacta",$request->numeroacta)->update(['estado' => "Cerrada"]);
        
        return;
    } 

    public function resumen($numeroacta)
    {
        try {
            
            $datosacta = DB::table('actas_existencias')
                    ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                    ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                    ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                    ->join('companias', 'companias.id', '=', 'existencias.compania')
                    ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                    ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                    ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                    ->select('actas.*', 'actas_existencias.*','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                    ->where('actas_existencias.numeroacta', '=', $numeroacta)
                    ->get();

                

                

            
            if (count($datosacta) > 0)
            {

                $totales=  array("monto"=>0, "comision"=>0, "iva"=>0, "garantia"=>0, "gastosoperacionales"=>0, "tasacionfiscal"=>0);

                foreach ($datosacta as $dato) {
                    $totales["monto"]= $totales["monto"] + $dato->monto;
                    $totales["comision"]= $totales["comision"] + $dato->comision;
                    $totales["iva"]= $totales["iva"] + $dato->iva;
                    $totales["garantia"]= $totales["garantia"] + $dato->garantia;
                    $totales["gastosoperacionales"]= $totales["gastosoperacionales"] + $dato->gastosoperacionales;
                    $totales["tasacionfiscal"]= $totales["tasacionfiscal"] + $dato->tasacionfiscal;
                    if ($dato->cliente == 0)
                    {
                        $dato->cliente = "S/D";
                    }
                    else
                    {
                        $rut=Contacto::find($dato->cliente)->rut;
                        $dato->cliente = $rut;
                    }
                }
                $datosacta->totales =$totales;
//dd($datosacta);
                return view('administrador.actas.resumen')->with('datosacta',$datosacta);
            }
            else
            {
                alert()->error('', 'Numero de acta no registrado')->autoclose(5000);

                return redirect()->to('listaractas');
            }
            //return $datosacta;
            //dd($datosacta);
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }
    } 


    public function pdfResumenActa($numeroacta)
    {
        try {
            
            $datosacta = DB::table('actas_existencias')
                    ->join('actas', 'actas_existencias.numeroacta', '=', 'actas.numeroacta')
                    ->join('existencias', 'actas_existencias.idexistencia', '=', 'existencias.id')
                    ->join('vehiculos', 'vehiculos.id', '=', 'existencias.vehiculo')
                    ->join('companias', 'companias.id', '=', 'existencias.compania')
                    ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
                    ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
                    ->join('tipos', 'vehiculos.tipoid', '=', 'tipos.id')
                    ->select('actas.*', 'actas_existencias.*','companias.nombre as nombrecompania','existencias.nproceso','existencias.id as idexistencia' ,'existencias.nsiniestro','existencias.condicionado','existencias.desarme','existencias.motorarranca', 'tipos.nombre as nombretipo', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio','vehiculos.patente','vehiculos.color', DB::raw("DATE_FORMAT(actas.fecha, '%d/%m/%Y') as fechafor"))
                    ->where('actas_existencias.numeroacta', '=', $numeroacta)
                    ->get();

                

                

            
            if (count($datosacta) > 0)
            {

                $totales=  array("monto"=>0, "comision"=>0, "iva"=>0, "garantia"=>0, "gastosoperacionales"=>0, "tasacionfiscal"=>0);

                foreach ($datosacta as $dato) {
                    $totales["monto"]= $totales["monto"] + $dato->monto;
                    $totales["comision"]= $totales["comision"] + $dato->comision;
                    $totales["iva"]= $totales["iva"] + $dato->iva;
                    $totales["garantia"]= $totales["garantia"] + $dato->garantia;
                    $totales["gastosoperacionales"]= $totales["gastosoperacionales"] + $dato->gastosoperacionales;
                    $totales["tasacionfiscal"]= $totales["tasacionfiscal"] + $dato->tasacionfiscal;
                    if ($dato->cliente == 0)
                    {
                        $dato->cliente = "S/D";
                    }
                    else
                    {
                        $rut=Contacto::find($dato->cliente)->rut;
                        $dato->cliente = $rut;
                    }
                }
                $datosacta->totales =$totales;
       
                 $pdf = PDF::loadView('pdf.actaresumen', compact('datosacta'));      
        
                return $pdf->download('actaresumen.pdf');
            }
            else
            {
                alert()->error('', 'Numero de acta no registrado')->autoclose(5000);

                return redirect()->to('listaractas');
            }
            //return $datosacta;
            //dd($datosacta);
            
        } catch (QueryException $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        } catch (Exception $e) {
            echo 'Mensaje de error: ' .$e->getMessage();
        }




  

    }   

}   
  
