<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\newTransportistaRequest;
use App\Http\Requests\updateTransportistaRequest;
use App\Transportista;
class transportistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportista = Transportista::get();
        return $transportista;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newTransportistaRequest $request)
    {
        
        $transportista = new Transportista([
          'rutempresa' => $request->get('rutempresa'),
          'nombreempresa' => $request->get('nombreempresa'),
          'patentecamion' => $request->get('patentecamion'),
          'telefono' => $request->get('telefono'),
          'email' => $request->get('email'),
          'rutchofer' => $request->get('rutchofer'),
          'nombrechofer' => $request->get('nombrechofer'),
          'transportista' =>  $request->get('patentecamion'),
          'gruaexterna' => $request->get('gruaexterna'),
          'habilitado' => $request->get('habilitado')
          
        ]);
        $transportista->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Transportista Creado')->autoclose(3000);
        //dd($taller);

        return redirect()->to('transportistas'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Transportista::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateTransportistaRequest $request, $id)
    {
       //dd($request);
        $transportista = Transportista::find($id);
        $transportista->rutempresa = $request->get('rutempresa');
        $transportista->nombreempresa = $request->get('nombreempresa');
        $transportista->patentecamion = $request->get('patentecamion');
        $transportista->telefono = $request->get('telefono');
        $transportista->email = $request->get('email');
        $transportista->rutchofer = $request->get('rutchofer');
        $transportista->nombrechofer = $request->get('nombrechofer');
        $transportista->gruaexterna = $request->get('gruaexterna');
        $transportista->habilitado = $request->get('habilitado');
        
        $transportista->save();      
        
        alert()->success('Registros actualizados exitosamente', 'Transportista actualizado')->autoclose(3000);

        return redirect()->to('transportistas');
    }


    public function buscarDatosTransportistas($rut)
    {
        $rutbuscar = str_replace(".", "", $rut);
        $transportista = Transportista::where('rutempresa', $rutbuscar)->first();

        if (count($transportista) > 0)
        {
            return $transportista;
        }
        else
        {
            return 0;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
