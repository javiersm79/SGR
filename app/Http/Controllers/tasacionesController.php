<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Tasacion;
use App\Compania;
use Illuminate\Support\Facades\Mail;
use App\Mail\notificarTasacion;
class tasacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       $tasaciones = DB::table('tasaciones')
            ->join('companias', 'companias.id', '=', 'tasaciones.compania')
            ->join('marcas', 'tasaciones.marcaid', '=', 'marcas.id')
            ->join('modelos', 'tasaciones.modeloid', '=', 'modelos.id')
            ->select('tasaciones.*', 'companias.nombre as nombrecompania', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', DB::raw("DATE_FORMAT(tasaciones.fecha, '%d-%m-%Y') as fechafor"))
            ->get();
        return $tasaciones;
        
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ($request->get('nsiniestro') == ''){
            $siniestro = "S/N";
        }
        else
        {
            $siniestro = strtoupper($request->get('nsiniestro'));
        }

        
        $dt = date('Y-m-d H:i:s');
        $tasacion = new Tasacion([
          'patente' => $request->get('patente'),
          'estado' => 'CREADA',
          'fecha' => $dt,
          'compania' => $request->get('compania'),
          'siniestro' => $siniestro,
          'nombre' => $request->get('nombre') ?? "No reportado",
          'email' => $request->get('email') ?? "No reportado",
          'telefono' => $request->get('telefono') ?? "No reportado",
          'marcaid' => $request->get('marca') ?? 0,
          'modeloid' => $request->get('modelo') ?? 0,
          'tipoid' => 1,
          //'tipoid' => $request->get('tipo'),
          'anio' => $request->get('anio'),
          'valor' => $request->get('valor')
          
        ]);


        $tasacion->save();
        //dd($tasacion);
        Mail::to($request->get('email'))->send(new notificarTasacion($tasacion));

        alert()->success('Registros ingresados exitosamente', 'Tasación Creada')->autoclose(3000);
        return redirect()->to('tasaciones');
    }
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasacion = Tasacion::find($id);
        $tasacion->fecha = DATE_FORMAT(date_create($tasacion->fecha), 'd-m-Y');
        $compania = Compania::find($tasacion->compania);
        
           
        $fulldata = array();
        $fulldata["tasacion"]=$tasacion;
        $fulldata["compania"]=$compania;
        
        return $fulldata;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $tasacion = array(
        'patente' => $request->get('patente'),
        //'estado' => 'CREADA',
        //'fecha' => $dt,
        'compania' => $request->get('compania'),
        'siniestro' => strtoupper($request->get('nsiniestro'))  ?? "S/N",
        'nombre' => $request->get('nombre') ?? "No reportado",
        'email' => $request->get('email') ?? "No reportado",
        'telefono' => $request->get('telefono') ?? "No reportado",
        'marcaid' => $request->get('marca'),
        'modeloid' => $request->get('modelo'),
        'tipoid' => 1,
        //'tipoid' => $request->get('tipo'),
        'anio' => $request->get('anio'),
        'valor' => $request->get('valor')

        );
        Tasacion::where(['id' => $id])->update($tasacion);
        
        alert()->success('Registros actualizados exitosamente', 'Tasación Actualizada')->autoclose(3000);
        return redirect()->to('tasaciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
