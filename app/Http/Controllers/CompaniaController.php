<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compania;
use App\Http\Requests\newCompaniaRequest;
use App\Http\Requests\updateCompaniaRequest;

/*use App\Http\Requests\newMarcaRequest;
use App\Http\Requests\updateMarcaRequest;*/
class CompaniaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $companias = Compania::get();
        //return [$usuarios, $companias];
        return $companias;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(newCompaniaRequest $request)
    {
        $compania = new Compania([
          'nombre' => strtoupper($request->get('nombre')),
          'habilitado' => $request->get('habilitado')
          
        ]);
        $compania->save();
        
        //alert()->success('Registros ingresado exitosamente', 'Usuario Creado: '.$request->get('compania'))->autoclose(3000);
        alert()->success('Registros ingresado exitosamente', 'Compania Creada')->autoclose(3000);
        //dd($taller);

        return redirect()->to('companias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return Compania::find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateCompaniaRequest $request, $id)
    {
        $compania = Compania::find($id);
        //dd($taller);

        $compania->nombre = strtoupper($request->get('nombre'));
        $compania->habilitado = $request->get('habilitado');
        
        $compania->save();        
        
        alert()->success('Registros actualizados exitosamente', 'Compania actualizada')->autoclose(3000);

        return redirect()->to('companias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
