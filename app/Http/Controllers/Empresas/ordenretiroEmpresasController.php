<?php

namespace App\Http\Controllers\Empresas;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\Testemail;
use App\Mail\NotificarTransportista;
use App\Ordenretiro;
use App\Vehiculo;
use App\Tasacion;
use App\Taller;
use App\Transportista;
use App\Archivoor;
use App\Observor;
use App\Compania;

class ordenretiroEmpresasController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        //$matches = preg_replace('/[^0-9]/', '', $str);
        //dd($matches);
  //      dd($request->get('color') ?? 'Sin color');
        
        $dt = date('Y-m-d H:i:s');
        $anio = date('Y');

        //Valida si el vehiculo existe en la base de datos (Tabla Vehiculos)
        $buscarPatente = $this->buscarPatenteVehiculo($request->get('patente'));
        
        //Obtiene los datos de la compàñia y asi obtener el correlativo para dicha empresa (Tabla compania)
        $compania = Compania::find($request->get('compania'));

        if ($buscarPatente)
        {
            $vehiculo = array(
            'marcaid' => $request->get('marca'),
            'modeloid' => $request->get('modelo'),
            'tipoid' => $request->get('tipo'),
            'anio' => $request->get('anio'),
            'combustible' => $request->get('combustible'),
            'nummotor' => $request->get('nmotor') ?? '',
            'numchasis' => $request->get('nchasis') ?? '',
            'serial' => $request->get('nserie') ?? '',
            'VIM' => $request->get('vim') ?? '',
            'color' => $request->get('color') ?? '',
            'rutpropietario' => $request->get('rutprop') ?? '',
            'nombrepropietario' => $request->get('nombreprop') ?? '',
            'observacion' => 'N/A',
            'actualizadopor' => \Auth::user()->id,          
            );
            Vehiculo::where(['patente' => $request->get('patente')])->update($vehiculo);
            //$accion = "Vehiculo actualizado";
            $idvehiculo = $buscarPatente->id;


        }
        else
        {

            $vehiculo = new Vehiculo([
            'patente' => $request->get('patente'),
            'marcaid' => $request->get('marca'),
            'modeloid' => $request->get('modelo'),
            'tipoid' => $request->get('tipo'),
            'anio' => $request->get('anio'),
            'combustible' => $request->get('combustible'),
            'nummotor' => $request->get('nmotor') ?? '',
            'numchasis' => $request->get('nchasis') ?? '',
            'serial' => $request->get('nserie') ?? '',
            'VIM' => $request->get('vim') ?? '',
            'color' => $request->get('color') ?? '',
            'rutpropietario' => $request->get('rutprop') ?? '',
            'nombrepropietario' => $request->get('nombreprop') ?? '',
            'observacion' => 'N/A',
            'actualizadopor' => \Auth::user()->id,

            ]);
            $vehiculo->save();
            $idvehiculo = $vehiculo->id;
            //$accion = "Vehiculo creado";

        }
        //dd($accion);



        $ordenretiro = new Ordenretiro([
          //'nproceso' => time(),
          'nproceso' => $request->get('compania').$anio.$compania->correlativoor,
          'estado' => $request->get('estado'),
          'fecha' => $dt,
          'compania' => $request->get('compania'),
          //'vehiculo' => $request->get('idvehiculo'),
          'vehiculo' => $idvehiculo,
          'nsiniestro' => $request->get('nsiniestro')  ?? "S/N",
          'liquidador' => $request->get('liquidador') ?? "Sin liquidador",
          'tasacion' => $request->get('tasacion')  ?? "0",
          'contacto' => $request->get('emailcontac'),
          'vminremate' => $request->get('vminremate')  ?? "0",
          'vtraslado' => $request->get('vtraslado')  ?? "0",
          'nfacturaproveedor' => $request->get('nfacturaproveedor')  ?? "S/N",
          'vfacturacia' => $request->get('vfacturacia')  ?? "0",
          'nfactura' => $request->get('nfactura')  ?? "S/N",
          'vindem' => $request->get('vindem')  ?? "0",
          'transportista' => 0,
          'taller' => $request->get('idtaller'),
          'creadopor' => \Auth::user()->id,
          'actualizadopor' => \Auth::user()->id,
          
        ]);
        $ordenretiro->save();
        $LastInsertOR = $ordenretiro->nproceso;


        
        
        if ($buscarPatente)
        {
            alert()->warning('Registros ingresado', '**Orden de Retiro Creada EMPRESA**  Patente ingresada anteriormente')->persistent('Close');
        }
        else
        {
            alert()->success('Registros ingresado exitosamente', 'Orden de Retiro Creada')->autoclose(3000);
            
        }

        $compania->correlativoor = $compania->correlativoor + 1;
        $compania->save();


        return redirect()->to('ordretiros')->with('status', $LastInsertOR);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Verfica la existencia de una patente en la tabla de tasaciones.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscarPatenteTasacion($patente)
    {
        //dd($patente);
        //Busca el ultimo registro segun el ID de la patente en la tabla tasaciones
       return Tasacion::where('patente', '=' ,$patente)->latest('id')->first();
    }
    /**
     * Verfica la existencia de una patente en la tabla de Vehiculos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscarPatenteVehiculo($patente)
    {
        //dd($patente);
        //Busca el ultimo registro segun el ID de la patente en la tabla tasaciones
       return Vehiculo::where('patente', '=' ,$patente)->latest('id')->first();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTrasladoOR(Request $request, $id)
    {
        dd($request->all());
        
    }
    
    /**
     * Actualiza el traslado de una OR
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
/**
     * Scan folder for image car
     *
     * @return \Illuminate\Http\Response
     */
    public function scanfolderGaleriaVehiculo($patente)
    {

        $html = '';
        $images = '';
        $img_dir = "storage/images/vehiculo/".$patente."/";

        if(\File::exists($img_dir)) {
            $images = scandir($img_dir);
            foreach($images as $img)    { 
                if($img === '.' || $img === '..') {continue;}       

                    if (  (preg_match('/.JPG/',$img)  || (preg_match('/.jpg/',$img))  ||  (preg_match('/.JEPG/',$img)) || (preg_match('/.jepg/',$img)) || (preg_match('/.gif/',$img)) || (preg_match('/.tiff/',$img)) || (preg_match('/.png/',$img)) ) || (preg_match('/.PNG/',$img)) ) {

                    $html .= '<div class="item"><a class="thumbnail" href="'.$img_dir.$img.'" data-fancybox="images" data-caption="<button style=\'color:black\' onclick='."eliminarFoto('$patente','".urlencode($img)."')".'>ELIMINAR</button>"><img alt="" src="'.$img_dir.$img.'"></a></div>';               

                    
                    /*$html .='<li> 
                            <img src="'.$img_dir.$img.'" ></li>' ; */
                    } else { continue; }    
            }
        }


        return $html ;
       
    }

    public function scanfolderArchivosOR($nproceso)
    {
 
         $archivo_dir = "storage/files/or/".$nproceso."/";
            $html = '';
            $archivosOR = DB::table('archivosor')
            ->join('contactos', 'contactos.userid', '=', 'archivosor.userid')
            ->join('companias', 'companias.id', '=', 'archivosor.companiaid')
            ->select('archivosor.*', 'contactos.nombre as nombrecontacto', 'contactos.apellido as apellidocontacto','companias.nombre as nombrecompania' )
            ->where('archivosor.nproceso', '=', $nproceso)
            ->get();

            foreach($archivosOR as $archivo)    { 
                if($archivo === '.' || $archivo === '..') {continue;}       

                    $extencion=preg_split('[\.]', $archivo->nombre, null, PREG_SPLIT_OFFSET_CAPTURE);

                   $html .= '<tr id="doc'.$archivo_dir.'/'.$archivo->nombre.'"><td><a href="/storage/files/or/'.$nproceso.'/'.$archivo->nombre.'" target="blank">'.$archivo->nombre.'</a></td><td>'.$archivo->nombrecompania.'</td><td>'.$archivo->fecha.'</td><td>'.$archivo->nombrecontacto.' '.$archivo->apellidocontacto.'</td><td>'.$archivo->observacion.'</td><td align="center"><img src="storage/images/filetype/'.$extencion[1][0].'.png"></td><td align="center"><button class="btn btn-danger btn-sm btnDelete" onclick="eliminarArchivo(\''.$nproceso.'\',\''.$archivo->nombre.'\',\''.$archivo->id.'\')" >Eliminar</button></td></tr>'; 
    
            }

        return  $html;
       
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $idcan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * SUBE LOS ARCHIVOS DE IMAGENES DEL VEHICULO
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subirfotos(Request $request)
    {
        //dd($request);

        $rutaimg = "storage/images/vehiculo/".$request->patente."/";

        if(\File::exists($rutaimg)) {
            $images = scandir($rutaimg);

            $i=count($images) - 2;
            //dd($i);
            //$path = $request->file('fotosvehiculo')->store('uploads');
            

        }
        else
        {
            $i=0;

        }


        foreach ($request->fotosvehiculo as $photo) {
            $extension = $photo->getClientOriginalExtension();
            //$filename = $photo->store('public/images/vehiculo');
            $path = $photo->storeAs(
                'public/images/vehiculo/'.$request->patente, $request->patente." ".$i.".".$extension
            );
            $i++;

        }
        
        //return $path;
        return ;
    }

    /**
     * SUBE LOS ARCHIVOS DE DOCUMENTOS DE LA OR
     *
     */
    public function subirarchivo(Request $request)
    {
    
        

        //$narchivos = scandir('storage/files/or/'.$request->idordenretiro."/", $nombrearchivo);
        //cuenta el numero de archivos
        //$i=count($narchivos);

        //crea la carpeta si no existe
        if(!\File::exists('storage/files/or/'.$request->nproceso)) {
            \File::makeDirectory('storage/files/or/'.$request->nproceso);
        }
        
        
        //obtiene la extencion del archivo a subir
        $extension = $request->archivo->getClientOriginalExtension();
        //obtiene la extencion del archivo a subir
        $nombrearchivo = $request->archivo->getClientOriginalName();
        $nombrearchivo = str_replace(" ","_",$nombrearchivo);

        //sube el archivo al servidor
        $path = $request->archivo->storeAs(
                //'public/files/or/'.$request->idordenretiro."/", "OR-".$i.".".$extension
                'public/files/or/'.$request->nproceso."/", $nombrearchivo //.".".$extension
            );


        //Datos del archivo generado para retornar al la vista/front
        $datafile = array();
        $datafile[0] = $nombrearchivo; //.".".$extension;
        //$datafile[0] = "OR-".$i.".".$extension;
        $datafile[1] = $extension;
        $contacto = \App\Contacto::where('userid', '=' ,\Auth::user()->id)->firstOrFail();

        $nuevoarchivoor = new Archivoor([
          'nproceso' => $request->nproceso,
          'nombre' => $nombrearchivo,
          'fecha' => date('Y-m-d H:i:s'),
          //'companiaid' => $companiaid,
          'companiaid' => $contacto->compania,
          'userid' => \Auth::user()->id,
          'observacion' => $request->observacion,
          
        ]);
        $nuevoarchivoor->save();

        


        return $datafile;
    }

    /**
     * ELIMINA UNA IMAGEN DEL VEHICULO
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminarfoto($patente, $idfoto)
    {

        /*$images = scandir('storage/images/vehiculo/DB-1515/');*/
        \File::delete('storage/images/vehiculo/'.$patente.'/'.urldecode($idfoto));
        

        return;
        //return "Imagen eliminada: ".$idfoto;
        //return "Patente: ".$patente." Foto: ".$idfoto;
    }

    public function showFullDataAll()
    {
        $ordenesretiro = DB::table('ordenesretiro')
            ->join('vehiculos', 'vehiculos.id', '=', 'ordenesretiro.vehiculo')
            ->join('companias', 'ordenesretiro.compania', '=', 'companias.id')
            ->join('marcas', 'vehiculos.marcaid', '=', 'marcas.id')
            ->join('modelos', 'vehiculos.modeloid', '=', 'modelos.id')
            ->select('ordenesretiro.*', 'vehiculos.marcaid as modelo', 'vehiculos.modeloid as modelo', 'vehiculos.patente as patente', 'companias.nombre as nombrecompania', 'marcas.nombre as nombremarca', 'modelos.nombre as nombremodelo', 'vehiculos.anio as anio')
            ->get();
         //return User::find($id);
            foreach($ordenesretiro as $ordenretiro => $value) {
                //echo $value->name . ", " . $value->email . "<br>";
                $fecha1 = new \DateTime($value->fecha);
                $fecha2 = new \DateTime();
                $fecha = $fecha1->diff($fecha2);
                $value->diasenretiro = $fecha->format('%a días');;
                //$usuarios->setAttribute('test', 'blablabla');;
              }


         return $ordenesretiro;
    }

    public function showFullDataOR($idor)
    {
        $ordenretiro = Ordenretiro::find($idor);
        $vehiculo = Vehiculo::find($ordenretiro->vehiculo);
        $taller = Taller::find($ordenretiro->taller);
        $transportista = Transportista::find($ordenretiro->transportista);
           
        $fulldata = array();
        $fulldata["ordenretiro"]=$ordenretiro;
        $fulldata["vehiculo"]=$vehiculo;
        $fulldata["taller"]=$taller;
        $fulldata["transportista"]=$transportista;

         return $fulldata;
    }

    /**
     * ELIMINA UN ARICHO DE LA OR
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function eliminararchivo($nproceso, $nombrearchivo, $idarchivoor)
    {

        \File::delete("storage/files/or/$nproceso/$nombrearchivo");
        
        \App\Archivoor::destroy($idarchivoor);
        return ;
    }   

     /**
     * Guarda una observación asociada a la OR
     */
     public function storeObservacion(Request $request, $idordenretiro)
    {
        $contacto = \App\Contacto::where('userid', '=' ,\Auth::user()->id)->firstOrFail();

        if ($request->observprivada) {
            $privada = 1;
        }
        else
        {
            $privada = 0;
        }



        $observacionor = new Observor([
          'idordenretiro' => $request->idordenretiro,
          'fecha' => date('Y-m-d H:i:s'),
          'companiaid' => $contacto->compania,
          'userid' => \Auth::user()->id,
          'observacion' => $request->obsertxt,
          'privada' => $privada,
          
        ]);
        $observacionor->save();

        
    }
    /**
     * Muestra las observaciones de una OR (No muestra las privadas si no es el usuario logeado)
     */
     public function showObservaciones($idordenretiro)
    {

        $observaciones = DB::table('observacionesor')
            ->join('contactos', 'contactos.userid', '=', 'observacionesor.userid')
            ->join('companias', 'companias.id', '=', 'observacionesor.companiaid')
            ->select('observacionesor.*', 'contactos.nombre as nombrecontacto', 'contactos.apellido as apellidocontacto','companias.nombre as nombrecompania' )
            ->where('observacionesor.idordenretiro', '=', $idordenretiro)
            ->where('observacionesor.privada', '=', 0)
            ->orWhere(function ($query) use ($idordenretiro) {
                $query->where('observacionesor.privada', '=', 1)
                ->where('observacionesor.userid', '=', \Auth::user()->id)
                ->where('observacionesor.idordenretiro', '=', $idordenretiro);
                })
            ->get();

        //return $observaciones;



        $html='';
        $i=0;
        foreach ($observaciones as $observacion) {
            $html .= '<tr><td>'.$observacion->nombrecompania.'</td><td>'.$observacion->fecha.'</td><td>'.$observacion->nombrecontacto.' '.$observacion->apellidocontacto.'</td><td>'.nl2br(e($observacion->observacion)).'</td></tr>';
            $i++;

        }

        return $html;
 
    }
    /**
     * Muestra las observaciones de una OR (No muestra las privadas si no es el usuario logeado)
     */
     public function updateTransportista(Request $request)
    {
        //dd($request->all());
        $ordenTraslado = Ordenretiro::find($request->idordenretiro);

        $ordenTraslado->transportista = $request->idtransportista;
        $ordenTraslado->save();
        return;

    }
    /**
     * Notifica al transportista por email
     */
     public function notificarTransportista(Request $request)
    {
        //$ordenRetiro = Ordenretiro::find($request->idordenretiro);
        $ordenRetiro = $this->showFullDataOR($request->idordenretiro);

        //dd($ordenRetiro['transportista']);
        Mail::to($ordenRetiro['transportista']['email'])->send(new NotificarTransportista($ordenRetiro));

        //return json_encode($ordenRetiro);
        return;

    }

    /**
     * Valida que la OR fue retirada
     */
    public function validarRetiroOR($nproceso)
    {
        $ordenretiro = Ordenretiro::where('nproceso', '=' ,$nproceso)->first();

        $ordenretiro->estado = 'Retirado';

        $ordenretiro->save();

        //return json_encode($ordenRetiro);
        return view('orretirada')->with('nproceso', $nproceso);

    }
}
