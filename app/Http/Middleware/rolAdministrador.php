<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Alert;

class rolAdministrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            //alert()->warning('Estas logueado', 'Acceso restingido')->autoclose(3000);
            if (Auth::guard($guard)->user()->tipo!="Administrador") {
                //return redirect('/home');
                
         
                if ($request->ajax()) {
                return response('Unauthorized.', 401);
                } else {

                    alert()->warning('Opcion permitida a Administradores', 'Acceso restingido')->autoclose(3000);

                    //return redirect()->to('restricted');
                    //return redirect()->to('home');
                    return redirect()->to(Auth::guard($guard)->user()->tipo);
                }
            }
            
        }
        else
        {
            alert()->warning('Por favor ingrese sus credenciales', 'Acceso restingido')->autoclose(3000);
            return redirect()->to('login');
        }

        return $next($request);
    }
}