<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Menu;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            if (Auth::guard($guard)->user()->tipo=="Administrador") {


            return redirect('/Administrador');
            }

            if (Auth::guard($guard)->user()->tipo=="Socio") {
                return redirect('/Socio');
            }
        }
           

        return $next($request);
    }
}