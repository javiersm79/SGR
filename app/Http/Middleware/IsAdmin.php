<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Session;
use Closure;
use Flash;
use Alert;
class IsAdmin
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->auth->user()->type != 'admin') {
            
            //$this->auth->logout();
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {

                alert()->warning('Opcion permitida a Administradores', 'Acceso restingido')->autoclose(3000);

                //return redirect()->to('restricted');
                return redirect()->to('home');
            }
        }

        
        return $next($request);
        
    }
}
