<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivoor extends Model
{
    protected $table = 'archivosor';
    protected $fillable = [
          'nproceso', 'nombre', 'fecha', 'companiaid', 'userid', 'observacion',
    ];
}
