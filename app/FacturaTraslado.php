<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaTraslado extends Model
{
    protected $table = 'facturatraslado';
    protected $fillable = [
        'valor','fecha', 'nfactura', 'rut', 'razonsocial', 'concepto','nproceso'
    ];
}