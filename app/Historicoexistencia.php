<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historicoexistencia extends Model
{
    protected $table = 'historicoexist';
    protected $fillable = [
        'nproceso', 'operacion', 'usuario'
    ];
}