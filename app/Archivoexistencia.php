<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivoexistencia extends Model
{
    protected $table = 'archivosexistencia';
    protected $fillable = [
          'nproceso', 'nombre', 'fecha', 'companiaid', 'userid', 'observacion',
    ];
}
