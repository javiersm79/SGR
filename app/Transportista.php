<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
	protected $table = 'transportistas';
    protected $fillable = [
        'rutempresa', 'nombreempresa','patentecamion','telefono','email','rutchofer','nombrechofer','gruaexterna','habilitado',
    ];
}
