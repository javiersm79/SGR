<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taller extends Model
{
    protected $table = 'talleres';
    protected $fillable = [
        'nombre', 'rut','direccion','region','comuna','ciudad','telefono','email','contacto',
    ];
}
